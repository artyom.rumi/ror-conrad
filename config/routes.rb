require "sidekiq/web"

Rails.application.routes.draw do
  mount RailsAdmin::Engine => "/admin_dashboard", as: "rails_admin"
  devise_for :admins
  devise_scope :admin do
    authenticated :admin do
      root to: "rails_admin/main#dashboard"
    end
    unauthenticated :admin do
      root to: "devise/sessions#new"
    end
  end

  authenticate :admin do
    mount Sidekiq::Web => "/sidekiq"
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      namespace :my do
        resources :recently_used_props, only: :index
        resources :blocked_users, only: :index
        resources :blocked_collocutor_masks, only: :index
        resources :contacts, only: :create
        resources :outline_contacts, only: :index
        resources :inline_contacts, only: :index
        resources :incoming_reactions, only: :index
        resources :schoolmates, only: :index
        resources :keeps, only: :create
        resources :feeds, only: %i[index create update destroy]
        resources :discoveries, only: %i[index]
        resources :own_feeds, only: :index
        resources :active_friends, only: :index
        resources :invited_friends, only: :index
        resources :expired_friends, only: :index
        resources :notifications, only: %i[index update] do
          patch :update, on: :collection
        end
      end

      resources :feeds do
        resources :reposts, only: :create, module: :feeds
        resources :likes, only: :create, module: :feeds
        delete "/likes", to: "feeds/likes#destroy", module: :feeds
        resources :comments, only: %i[index create update destroy], module: :feeds
        resources :mark_comments, only: %i[create], module: :feeds
        resources :chats, only: :create, module: :feeds
        resources :rate, only: :create, module: :feeds
      end

      resources :comments do
        resources :chats, only: :create, module: :comments
      end

      resources :schoolmates, only: :index
      resources :schools, only: :index

      resources :users, only: %i[show create update] do
        resources :prop_deliveries_from_me, only: :index
        resource :blocking, only: %i[create destroy], module: :users
        get "/timeline" => "users/timelines#index", as: :feed
      end

      resources :verification_codes, only: :create
      resource :avatar, only: %i[create destroy]
      resources :auth_tokens, only: :create
      delete "/logout" => "auth_tokens#destroy"
      resources :followers, only: :index
      resources :random_friends, only: :index
      post "/contacts" => "contacts#index"
      resources :subscriptions, only: :create
      delete "/subscriptions" => "subscriptions#destroy"

      resources :prop_deliveries, only: :create do
        resource :views, only: :create, module: :prop_deliveries
        resource :reaction, only: %i[create update], module: :prop_deliveries
      end

      resources :suggest_props, only: :create
      resource :invitation, only: :create
      resources :sent_props, only: :index
      resources :incoming_requests, only: %i[index show]

      resources :chats, only: %i[index create show] do
        resources :deanonymizations, only: %i[index create], module: :chats
        delete "/deanonymizations", to: "chats/deanonymizations#destroy", module: :chats
        resources :activities, only: :create, module: :chats
      end

      resources :pictures, only: :create
      resources :search_props, only: :index
      resource :profile, only: :show

      resources :collocutor_masks, only: %i[index show update] do
        resource :blocking, only: %i[create destroy], module: :collocutor_masks
      end

      resources :props, only: :index
      resources :received_props, only: :index
      resources :popular_props, only: :index
      resources :reports, only: :create
      resources :special_props, only: :index
      resources :fake_requests, only: :create

      resources :requests, only: :create do
        resources :prop_deliveries, only: :create, module: :requests
        resources :skip_deliveries, only: :create, module: :requests
        resources :respondents, only: :index, module: :requests
      end
      resource :device, only: :update
      resources :feedbacks, only: :create
      resource :pubnub_key_set, only: :show
      resources :followees, only: :index
      resources :friend_intentions, only: %i[create]
      resources :friends, only: :index
      resources :communities, only: :index
      resources :custom_prop_deliveries, only: :create
      resources :interlocutors, only: :index
    end

    namespace :v2 do
      resources :received_props, only: :index
    end
  end
end
