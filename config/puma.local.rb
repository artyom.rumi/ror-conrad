workers 8
threads 1,4

rackup DefaultRackup
port ENV.fetch("PORT", 5000)
environment ENV.fetch("RACK_ENV", "development")

preload_app!

on_worker_boot do
  ActiveRecord::Base.establish_connection
end
