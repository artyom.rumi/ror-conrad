START_GRADE = 6
CLASS_AMOUNT = 7

current_grade = START_GRADE
last_grage = current_grade + CLASS_AMOUNT - 1
grades = [*current_grade..last_grage]

high_schools = FactoryBot.create_list(:school, 2, :high)
middle_schools = FactoryBot.create_list(:school, 2, :middle)

grades.first(4).each do |grade|
  high_schools.each do |school|
    FactoryBot.create(:grade, value: grade, school: school)
    FactoryBot.create_list(:user, 5, school: school)
  end
end

grades.last(3).each do |grade|
  middle_schools.each do |school|
    FactoryBot.create(:grade, value: grade, school: school)
    FactoryBot.create_list(:user, 5, school: school)
  end
end
