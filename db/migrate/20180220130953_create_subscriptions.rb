class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.references :subscriber, null: false, index: true
      t.references :target, null: false, index: true

      t.timestamps
    end
  end
end
