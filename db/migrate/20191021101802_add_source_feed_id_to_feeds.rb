class AddSourceFeedIdToFeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :feeds, :source_feed_id, :bigint
  end
end
