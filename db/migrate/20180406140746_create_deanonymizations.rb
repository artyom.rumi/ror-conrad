class CreateDeanonymizations < ActiveRecord::Migration[5.1]
  def change
    create_table :deanonymizations do |t|
      t.references :chat, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
