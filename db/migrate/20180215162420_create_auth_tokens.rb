class CreateAuthTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :auth_tokens do |t|
      t.string :value, null: false
      t.datetime :expires_at, null: false
      t.references :user

      t.timestamps
    end
  end
end
