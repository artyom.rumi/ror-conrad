class AddIncognitoToChat < ActiveRecord::Migration[5.1]
  def change
    add_column :chats, :incognito, :boolean, null: false, default: false
  end
end
