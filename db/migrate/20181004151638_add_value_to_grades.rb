class AddValueToGrades < ActiveRecord::Migration[5.2]
  def up
    add_column :grades, :value, :integer

    Grade.find_each do |grade|
      grade.update_column(:value, Grade::HIGHEST_GRADE + Time.current.year - grade.year_of_ending +
        AlreadyGraduated.graduation_year_inc)
    end

    change_column_null :grades, :value, false
  end

  def down
    remove_column :grades, :value
  end
end
