class AddInvitedToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :invited, :boolean, default: false
  end
end
