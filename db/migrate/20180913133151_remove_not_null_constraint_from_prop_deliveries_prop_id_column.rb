class RemoveNotNullConstraintFromPropDeliveriesPropIdColumn < ActiveRecord::Migration[5.2]
  def change
    change_column_null :prop_deliveries, :prop_id, true
  end
end
