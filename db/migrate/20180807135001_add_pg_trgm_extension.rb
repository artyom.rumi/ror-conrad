class AddPgTrgmExtension < ActiveRecord::Migration[5.1]
  def up
    execute "create extension pg_trgm;"
    execute "create extension btree_gin;"
  end

  def down
    execute "drop extension pg_trgm;"
    execute "drop extension btree_gin;"
  end
end
