class AddContentAndEmojiToPropDeliveries < ActiveRecord::Migration[5.1]
  def change
    add_column :prop_deliveries, :content, :string
    add_column :prop_deliveries, :emoji, :string
  end
end
