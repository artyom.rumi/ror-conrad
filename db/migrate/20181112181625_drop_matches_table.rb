class DropMatchesTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :matches, if_exists: true
  end
end
