class ChangeForeignKeysForDeanonymization < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :deanonymizations, :chats
    remove_foreign_key :deanonymizations, :users

    add_foreign_key :deanonymizations, :chats, on_delete: :cascade
    add_foreign_key :deanonymizations, :users, on_delete: :cascade
  end
end
