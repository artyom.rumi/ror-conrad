class AddSenderGenderToPropDelivery < ActiveRecord::Migration[5.1]
  def change
    add_column :prop_deliveries, :sender_gender, :string
  end
end
