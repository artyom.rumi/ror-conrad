class AddRegistrotionFieldsToUser < ActiveRecord::Migration[5.1]
  def change
   remove_column :users, :full_name, :string
   remove_column :users, :email, :string

   add_column :users, :age, :integer, null: false
   add_column :users, :grade, :integer, null: false
   add_column :users, :first_name, :string, null: false
   add_column :users, :last_name, :string, null: false
   add_column :users, :gender, :string, null: false
   add_column :users, :phone_number, :string, null: false, unique: true, index: true
  end
end
