class AddOwnerToChats < ActiveRecord::Migration[5.2]
  def change
    add_reference :chats, :owner, null: false, default: -1
    Chat.where(owner_id: -1).each do |chat|
      chat.update(owner: chat.users.first) if chat.users.any?
    end
  end
end
