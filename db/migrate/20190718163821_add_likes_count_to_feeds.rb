class AddLikesCountToFeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :feeds, :likes_count, :integer, default: 0
  end
end
