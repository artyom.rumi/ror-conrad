class AddAddressToSchools < ActiveRecord::Migration[5.1]
  def change
    add_column :schools, :address, :string, null: false
  end
end
