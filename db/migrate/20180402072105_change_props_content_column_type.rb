class ChangePropsContentColumnType < ActiveRecord::Migration[5.1]
  def change
    change_column :props, :content, :string
  end
end
