class AddFriendBoostAtToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :friend_boost_at, :datetime
  end
end
