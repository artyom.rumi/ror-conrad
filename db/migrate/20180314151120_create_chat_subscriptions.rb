class CreateChatSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :chat_subscriptions do |t|
      t.references :user, null: false
      t.references :chat, null: false

      t.timestamps
    end
  end
end
