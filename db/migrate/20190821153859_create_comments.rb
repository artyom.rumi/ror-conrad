class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :text, null: false
      t.string :status, null: false, default: "Unread"
      t.references :user, null: false, index: true
      t.references :feed, null: false, index: true

      t.timestamps
    end
  end
end
