class AddDiscardedAtToProps < ActiveRecord::Migration[5.2]
  def change
    add_column :props, :discarded_at, :datetime
    add_index :props, :discarded_at
  end
end
