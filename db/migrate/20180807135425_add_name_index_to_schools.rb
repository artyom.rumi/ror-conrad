class AddNameIndexToSchools < ActiveRecord::Migration[5.1]
  def change
    add_index :schools, :name, using: :gin
  end
end
