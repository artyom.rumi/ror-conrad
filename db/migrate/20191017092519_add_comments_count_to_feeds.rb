class AddCommentsCountToFeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :feeds, :comments_count, :integer, default: 0
  end
end
