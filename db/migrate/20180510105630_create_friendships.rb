class CreateFriendships < ActiveRecord::Migration[5.1]
  def change
    create_table :friendships do |t|
      t.references :first_user, null: false, index: true
      t.references :second_user, null: false, index: true

      t.timestamps
    end
  end
end
