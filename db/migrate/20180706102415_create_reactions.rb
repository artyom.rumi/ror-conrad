class CreateReactions < ActiveRecord::Migration[5.1]
  def change
    create_table :reactions do |t|
      t.references :prop_delivery, foreign_key: true, null: false
      t.string :content, null: false

      t.timestamps
    end
  end
end
