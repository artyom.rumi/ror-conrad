class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.references :notifiable, polymorphic: true, index: true
      t.string :status, null: false, default: "Unread"
      t.references :user, null: false, index: true, foreign_key: true

      t.timestamps
    end
  end
end
