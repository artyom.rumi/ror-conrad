class AddDeviceToAuthToken < ActiveRecord::Migration[5.1]
  def change
    add_reference :auth_tokens, :device, index: true
  end
end
