class ChangeUsersRoleDefault < ActiveRecord::Migration[5.1]
  def change
    change_column_default :users, :role, from: "user", to: "ordinary"
  end
end
