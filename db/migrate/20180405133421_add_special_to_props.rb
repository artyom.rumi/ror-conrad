class AddSpecialToProps < ActiveRecord::Migration[5.1]
  def change
    add_column :props, :special, :boolean, null: false, default: false
  end
end
