class AddKeepToFriendship < ActiveRecord::Migration[5.2]
  def change
    add_column :friendships, :first_user_keep, :boolean, null: false, default: false
    add_column :friendships, :second_user_keep, :boolean, null: false, default: false
  end
end
