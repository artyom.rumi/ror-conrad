class CreateSchools < ActiveRecord::Migration[5.1]
  def change
    create_table :schools do |t|
      t.string :name, null: false
      t.integer :members_amount, default: 0, null: false

      t.timestamps
    end
  end
end
