class RemoveOldAvatars < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :avatar, :string
    remove_column :collocutor_masks, :avatar, :string
  end
end
