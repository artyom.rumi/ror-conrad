class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :devices do |t|
      t.string :phone_number, null: false
      t.string :token
      t.references :user, index: true
    end
  end
end
