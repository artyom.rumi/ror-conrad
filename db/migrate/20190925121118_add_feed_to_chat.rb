class AddFeedToChat < ActiveRecord::Migration[5.2]
  def change
    add_reference :chats, :feed, index: true
  end
end
