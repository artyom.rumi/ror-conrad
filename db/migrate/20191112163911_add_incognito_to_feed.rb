class AddIncognitoToFeed < ActiveRecord::Migration[5.2]
  def change
    add_column :feeds, :incognito, :boolean, null: false, default: false
  end
end
