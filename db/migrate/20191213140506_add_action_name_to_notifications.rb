class AddActionNameToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :action_name, :string
  end
end
