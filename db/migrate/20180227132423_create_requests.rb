class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :requests do |t|
      t.references :requester, null: false, index: true
      t.references :receiver, null: false, index: true

      t.timestamps
    end
  end
end
