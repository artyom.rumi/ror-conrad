class RemovePubNubChannelNameFromChats < ActiveRecord::Migration[5.1]
  def change
    remove_column :chats, :pubnub_channel_name, :string
  end
end
