class AddFieldsToFeed < ActiveRecord::Migration[5.2]
  def change
    add_column :feeds, :text, :text
    add_column :feeds, :link, :string
  end
end
