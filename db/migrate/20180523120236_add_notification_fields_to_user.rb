class AddNotificationFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :prop_request_notification_enabled, :boolean, default: true
    add_column :users, :receiving_prop_notification_enabled, :boolean, default: true
    add_column :users, :chat_messages_notification_enabled, :boolean, default: true
    add_column :users, :new_friends_notification_enabled, :boolean, default: true
    add_column :users, :new_users_notification_enabled, :boolean, default: true
  end
end
