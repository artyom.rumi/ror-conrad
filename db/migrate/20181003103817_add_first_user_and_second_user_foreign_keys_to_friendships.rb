class AddFirstUserAndSecondUserForeignKeysToFriendships < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :friendships, :users, column: :first_user_id, primary_key: :id, on_delete: :cascade
    add_foreign_key :friendships, :users, column: :second_user_id, primary_key: :id, on_delete: :cascade
  end
end
