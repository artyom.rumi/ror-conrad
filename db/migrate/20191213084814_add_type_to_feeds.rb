class AddTypeToFeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :feeds, :type, :string, null: false, default: "Feed"
  end
end
