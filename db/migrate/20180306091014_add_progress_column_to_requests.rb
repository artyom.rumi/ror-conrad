class AddProgressColumnToRequests < ActiveRecord::Migration[5.1]
  def change
    add_column :requests, :progress, :integer, default: 0, null: false, index: true
  end
end
