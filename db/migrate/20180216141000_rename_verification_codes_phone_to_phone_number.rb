class RenameVerificationCodesPhoneToPhoneNumber < ActiveRecord::Migration[5.1]
  def change
    rename_column :verification_codes, :phone, :phone_number
  end
end
