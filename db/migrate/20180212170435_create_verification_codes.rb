class CreateVerificationCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :verification_codes do |t|
      t.string :code
      t.string :phone, null: false
      t.references :user
      t.datetime :expires_at, null: false

      t.timestamps
    end
  end
end
