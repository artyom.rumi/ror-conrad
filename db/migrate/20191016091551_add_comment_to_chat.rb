class AddCommentToChat < ActiveRecord::Migration[5.2]
  def change
    add_reference :chats, :comment, index: true
  end
end
