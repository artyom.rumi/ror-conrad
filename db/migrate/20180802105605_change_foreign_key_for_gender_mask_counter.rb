class ChangeForeignKeyForGenderMaskCounter < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :gender_masks_counters, :users
    add_foreign_key :gender_masks_counters, :users, on_delete: :cascade
  end
end
