class AddHideTopPokesToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :hide_top_pokes, :boolean, null: false, default: false
  end
end
