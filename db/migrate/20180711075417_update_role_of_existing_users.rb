class UpdateRoleOfExistingUsers < ActiveRecord::Migration[5.1]
  def up
    User.where(role: "user").update(role: "ordinary")
  end

  def down
    User.where(role: "ordinary").update(role: "user") # need to change model constraint first
  end
end
