class ChangeColumnAtDevice < ActiveRecord::Migration[5.1]
  def change
    rename_column :devices, :token, :push_token
  end
end
