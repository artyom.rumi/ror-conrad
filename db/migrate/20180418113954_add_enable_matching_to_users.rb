class AddEnableMatchingToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :matching_enabled, :boolean, default: true
  end
end
