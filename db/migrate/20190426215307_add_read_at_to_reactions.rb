class AddReadAtToReactions < ActiveRecord::Migration[5.2]
  def change
    add_column :reactions, :read_at, :datetime
  end
end
