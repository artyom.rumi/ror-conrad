class ChangePropDeliveryColumnNullToTrue < ActiveRecord::Migration[5.1]
  def change
    change_column_null :prop_deliveries, :sender_id, true
    change_column_null :prop_deliveries, :recipient_id, true
  end
end
