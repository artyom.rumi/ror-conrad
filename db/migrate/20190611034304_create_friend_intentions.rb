class CreateFriendIntentions < ActiveRecord::Migration[5.2]
  def change
    create_table :friend_intentions do |t|
      t.references :user
      t.references :target

      t.timestamps
    end
  end
end
