class AddNewPushNotificationsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :new_comments_notification_enabled, :boolean, default: true
    add_column :users, :new_reposts_notification_enabled, :boolean, default: true
    add_column :users, :new_likes_notification_enabled, :boolean, default: true
  end
end
