class AddNotNullConstraintForPropDeliveryColumns < ActiveRecord::Migration[5.2]
  def change
    change_column_null :prop_deliveries, :content, false
    change_column_null :prop_deliveries, :emoji, false
    change_column_null :prop_deliveries, :sender_gender, false
  end
end
