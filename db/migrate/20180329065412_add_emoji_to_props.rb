class AddEmojiToProps < ActiveRecord::Migration[5.1]
  def change
    add_column :props, :emoji, :string, null: false
  end
end
