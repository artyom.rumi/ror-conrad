class CreateChats < ActiveRecord::Migration[5.1]
  def change
    create_table :chats do |t|
      t.string :pubnub_channel_name

      t.timestamps
    end
  end
end
