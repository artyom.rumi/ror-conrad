class AddChatToCollocutorMasks < ActiveRecord::Migration[5.2]
  def change
    add_reference :collocutor_masks, :chat, null: false, default: 0

    up_only do
      Chat.incognito.each do |chat|
        next unless chat.chat_subscriptions.count == 2
        user_ids = chat.chat_subscriptions.pluck(:user_id)
        [user_ids, user_ids.reverse].each do |user1, user2|
          CollocutorMask.where(collocutor_id: user1, user_id: user2, chat_id: 0).update(chat_id: chat.id)
        end
      end
    end
  end
end
