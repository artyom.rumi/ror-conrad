class AddFriendsCountToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.integer :friends_count, default: 0
    end

    reversible do |dir|
      dir.up { set_counter }
    end
  end

  def set_counter
    execute <<~SQL
      UPDATE users
        SET friends_count = (
          SELECT count(1) FROM friendships
            WHERE friendships.first_user_id  = users.id
            OR    friendships.second_user_id = users.id
        )
    SQL
  end
end
