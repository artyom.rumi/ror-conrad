class DowngradeSuperfriends < ActiveRecord::Migration[5.2]
  def up
    Friendship.where(super_friends: true).update_all(super_friends: false)
  end

  def down
  end
end
