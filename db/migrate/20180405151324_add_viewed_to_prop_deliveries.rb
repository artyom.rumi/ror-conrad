class AddViewedToPropDeliveries < ActiveRecord::Migration[5.1]
  def change
    add_column :prop_deliveries, :viewed, :boolean, null: false, default: false
  end
end
