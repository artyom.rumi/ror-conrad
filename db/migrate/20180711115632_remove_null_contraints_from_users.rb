class RemoveNullContraintsFromUsers < ActiveRecord::Migration[5.1]
  def change
    change_column_null :users, :school_id, true
    change_column_null :users, :age, true
    change_column_null :users, :grade, true
    change_column_null :users, :first_name, true
    change_column_null :users, :last_name, true
    change_column_null :users, :gender, true
    change_column_null :users, :phone_number, true
  end
end
