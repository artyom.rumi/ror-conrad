class AddExpiresAtToFriendships < ActiveRecord::Migration[5.2]
  def change
    add_column :friendships, :expires_at, :datetime
  end
end
