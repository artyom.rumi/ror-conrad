class AddIsGroupToChats < ActiveRecord::Migration[5.2]
  def change
    add_column :chats, :is_group, :boolean, default: true, null: false
  end
end
