class CreateRequestRespondents < ActiveRecord::Migration[5.1]
  def change
    create_table :request_respondents do |t|
      t.references :request, null: false, index: true
      t.references :respondent, null: false, index: true
      t.integer :index, null: false, default: 0, index: true

      t.timestamps
    end
  end
end
