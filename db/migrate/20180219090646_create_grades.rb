class CreateGrades < ActiveRecord::Migration[5.1]
  def change
    create_table :grades do |t|
      t.integer :year_of_ending, null: false, index: true
      t.references :school, null: false, index: true

      t.timestamps
    end
  end
end
