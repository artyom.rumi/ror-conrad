class CreateFeed < ActiveRecord::Migration[5.2]
  def change
    create_table :feeds do |t|
      t.string :image
      t.references :user

      t.timestamps
    end
  end
end
