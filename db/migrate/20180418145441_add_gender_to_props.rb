class AddGenderToProps < ActiveRecord::Migration[5.1]
  def change
    add_column :props, :gender, :string
  end
end
