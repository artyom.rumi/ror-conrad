class AddAttemptsCountToVerificationCodes < ActiveRecord::Migration[5.1]
  def change
    add_column :verification_codes, :attempts_count, :integer, null: false, default: 0
  end
end
