class AddDefaultAvatarToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :default_avatar, :string, null: false, default: "avatars/avatar-1.png"
  end
end
