class AddBoostTypeToPropDeliveries < ActiveRecord::Migration[5.2]
  def change
    add_column :prop_deliveries, :boost_type, :integer, null: false, default: 0
  end
end
