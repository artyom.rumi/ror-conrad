class AddSuperToFriendships < ActiveRecord::Migration[5.1]
  def change
    add_column :friendships, :super_friends, :boolean, null: false, default: false
    add_index :friendships, :super_friends
  end
end
