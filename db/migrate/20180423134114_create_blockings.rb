class CreateBlockings < ActiveRecord::Migration[5.1]
  def change
    create_table :blockings do |t|
      t.references :user, null: false, index: true
      t.references :blockable, polymorphic: true, null: false, index: true

      t.timestamps
    end
  end
end
