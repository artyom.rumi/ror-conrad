class AddLockVersionToGenderMasksCounter < ActiveRecord::Migration[5.1]
  def change
    add_column :gender_masks_counters, :lock_version, :integer, default: 0
  end
end
