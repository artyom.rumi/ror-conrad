class CreatePropDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table :prop_deliveries do |t|
      t.references :sender, null: false, index: true
      t.references :recipient, null: false, index: true
      t.references :prop, null: false, index: true

      t.timestamps
    end
  end
end
