class ReplaceViewedWithViewedAtInPropDeliveries < ActiveRecord::Migration[5.1]
  def change
    remove_column :prop_deliveries, :viewed
    add_column :prop_deliveries, :viewed_at, :datetime
  end
end
