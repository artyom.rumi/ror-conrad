class AddNotNullConstraintsToUserColumns < ActiveRecord::Migration[5.2]
  def change
    change_column_null :users, :school_id, false
    change_column_null :users, :age, false
    change_column_null :users, :grade, false
    change_column_null :users, :first_name, false
    change_column_null :users, :last_name, false
    change_column_null :users, :gender, false
    change_column_null :users, :phone_number, false
  end
end
