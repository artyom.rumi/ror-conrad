class AddPokesCountToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.integer :pokes_count, default: 0
    end

    reversible do |dir|
      dir.up { set_counter }
    end
  end

  def set_counter
    execute <<~SQL
      UPDATE users
        SET pokes_count = (
          SELECT count(1) FROM prop_deliveries
            WHERE prop_deliveries.recipient_id = users.id
        )
    SQL
  end
end
