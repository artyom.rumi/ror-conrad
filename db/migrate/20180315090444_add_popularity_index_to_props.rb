class AddPopularityIndexToProps < ActiveRecord::Migration[5.1]
  def change
    add_column :props, :popularity_index, :integer, default: 0, null: false
  end
end
