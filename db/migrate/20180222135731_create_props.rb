class CreateProps < ActiveRecord::Migration[5.1]
  def change
    create_table :props do |t|
      t.text :content, null: false

      t.timestamps
    end
  end
end
