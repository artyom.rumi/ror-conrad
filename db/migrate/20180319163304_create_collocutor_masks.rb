class CreateCollocutorMasks < ActiveRecord::Migration[5.1]
  def change
    create_table :collocutor_masks do |t|
      t.string :name, null: false
      t.string :avatar
      t.references :collocutor, null: false
      t.references :user, null: false

      t.timestamps
    end
  end
end
