class CreateGenderMasksCounters < ActiveRecord::Migration[5.1]
  def change
    create_table :gender_masks_counters do |t|
      t.integer :male, null: false, default: 0
      t.integer :female, null: false, default: 0
      t.integer :non_binary, null: false, default: 0
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
