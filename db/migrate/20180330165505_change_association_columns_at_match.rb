class ChangeAssociationColumnsAtMatch < ActiveRecord::Migration[5.1]
  def change
    rename_column :matches, :boy_id, :first_user_id
    rename_column :matches, :girl_id, :second_user_id
  end
end
