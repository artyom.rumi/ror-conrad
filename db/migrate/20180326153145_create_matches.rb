class CreateMatches < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.references :boy, null: false, index: true
      t.references :girl, null: false, index: true

      t.timestamps
    end
  end
end
