class AddRequestToPropDeliveries < ActiveRecord::Migration[5.1]
  def change
    add_reference :prop_deliveries, :request, index: true
  end
end
