# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_04_080523) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "btree_gin"
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "full_name"
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "auth_tokens", force: :cascade do |t|
    t.string "value", null: false
    t.datetime "expires_at", null: false
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "device_id"
    t.index ["device_id"], name: "index_auth_tokens_on_device_id"
    t.index ["user_id"], name: "index_auth_tokens_on_user_id"
  end

  create_table "blockings", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "blockable_type", null: false
    t.bigint "blockable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blockable_type", "blockable_id"], name: "index_blockings_on_blockable_type_and_blockable_id"
    t.index ["user_id"], name: "index_blockings_on_user_id"
  end

  create_table "chat_subscriptions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "chat_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chat_id"], name: "index_chat_subscriptions_on_chat_id"
    t.index ["user_id"], name: "index_chat_subscriptions_on_user_id"
  end

  create_table "chats", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "incognito", default: false, null: false
    t.bigint "owner_id", default: -1, null: false
    t.bigint "feed_id"
    t.bigint "comment_id"
    t.string "name"
    t.boolean "is_group", default: true, null: false
    t.index ["comment_id"], name: "index_chats_on_comment_id"
    t.index ["feed_id"], name: "index_chats_on_feed_id"
    t.index ["owner_id"], name: "index_chats_on_owner_id"
  end

  create_table "collocutor_masks", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "collocutor_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "chat_id", default: 0, null: false
    t.index ["chat_id"], name: "index_collocutor_masks_on_chat_id"
    t.index ["collocutor_id"], name: "index_collocutor_masks_on_collocutor_id"
    t.index ["user_id"], name: "index_collocutor_masks_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "text", null: false
    t.string "status", default: "Unread", null: false
    t.bigint "user_id", null: false
    t.bigint "feed_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feed_id"], name: "index_comments_on_feed_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "phone_number", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.boolean "invited", default: false
    t.index ["phone_number"], name: "index_contacts_on_phone_number"
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "deanonymizations", force: :cascade do |t|
    t.bigint "chat_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chat_id"], name: "index_deanonymizations_on_chat_id"
    t.index ["user_id"], name: "index_deanonymizations_on_user_id"
  end

  create_table "devices", force: :cascade do |t|
    t.string "phone_number", null: false
    t.string "push_token"
    t.bigint "user_id"
    t.index ["user_id"], name: "index_devices_on_user_id"
  end

  create_table "feeds", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "likes_count", default: 0
    t.text "text"
    t.string "link"
    t.integer "comments_count", default: 0
    t.bigint "source_feed_id"
    t.boolean "incognito", default: false, null: false
    # added by lava on 1012
    t.boolean "ispublic", default: false, null: false
    t.string "type", default: "Feed", null: false
    # added by lava on 0120
    t.float "rate", default: 0.0, null: false
    t.integer "rate_count", default: 0
    t.index ["user_id"], name: "index_feeds_on_user_id"
  end

  create_table "friend_intentions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "target_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["target_id"], name: "index_friend_intentions_on_target_id"
    t.index ["user_id"], name: "index_friend_intentions_on_user_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "first_user_id", null: false
    t.bigint "second_user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "super_friends", default: false, null: false
    t.datetime "expires_at"
    t.boolean "first_user_keep", default: false, null: false
    t.boolean "second_user_keep", default: false, null: false
    t.index ["first_user_id"], name: "index_friendships_on_first_user_id"
    t.index ["second_user_id"], name: "index_friendships_on_second_user_id"
    t.index ["super_friends"], name: "index_friendships_on_super_friends"
  end

  create_table "gender_masks_counters", force: :cascade do |t|
    t.integer "male", default: 0, null: false
    t.integer "female", default: 0, null: false
    t.integer "non_binary", default: 0, null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "lock_version", default: 0
    t.index ["user_id"], name: "index_gender_masks_counters_on_user_id"
  end

  create_table "grades", force: :cascade do |t|
    t.integer "year_of_ending", null: false
    t.bigint "school_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "value", null: false
    t.index ["school_id"], name: "index_grades_on_school_id"
    t.index ["year_of_ending"], name: "index_grades_on_year_of_ending"
  end

  create_table "likes", force: :cascade do |t|
    t.bigint "feed_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feed_id", "user_id"], name: "index_likes_on_feed_id_and_user_id", unique: true
    t.index ["feed_id"], name: "index_likes_on_feed_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  # created rates table by lava on 0120
  create_table "rates", force: :cascade do |t|
    t.bigint "feed_id"
    t.bigint "user_id"
    t.bigint "rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feed_id", "user_id"], name: "index_rates_on_feed_id_and_user_id", unique: true
    t.index ["feed_id"], name: "index_rates_on_feed_id"
    t.index ["user_id"], name: "index_rates_on_user_id"
  end
  # end rates table by lava on 0120

  create_table "notifications", force: :cascade do |t|
    t.string "notifiable_type"
    t.bigint "notifiable_id"
    t.string "status", default: "Unread", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "action_name"
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "prop_deliveries", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "recipient_id"
    t.bigint "prop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "request_id"
    t.datetime "viewed_at"
    t.string "sender_gender", null: false
    t.string "content", null: false
    t.string "emoji", null: false
    t.integer "boost_type", default: 0, null: false
    t.index ["prop_id"], name: "index_prop_deliveries_on_prop_id"
    t.index ["recipient_id"], name: "index_prop_deliveries_on_recipient_id"
    t.index ["request_id"], name: "index_prop_deliveries_on_request_id"
    t.index ["sender_id"], name: "index_prop_deliveries_on_sender_id"
  end

  create_table "props", force: :cascade do |t|
    t.string "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "popularity_index", default: 0, null: false
    t.string "emoji", null: false
    t.boolean "special", default: false, null: false
    t.string "gender"
    t.datetime "discarded_at"
    t.index ["discarded_at"], name: "index_props_on_discarded_at"
  end

  create_table "reactions", force: :cascade do |t|
    t.bigint "prop_delivery_id", null: false
    t.string "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "read_at"
    t.index ["prop_delivery_id"], name: "index_reactions_on_prop_delivery_id"
  end

  create_table "request_respondents", force: :cascade do |t|
    t.bigint "request_id", null: false
    t.bigint "respondent_id", null: false
    t.integer "index", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["index"], name: "index_request_respondents_on_index"
    t.index ["request_id"], name: "index_request_respondents_on_request_id"
    t.index ["respondent_id"], name: "index_request_respondents_on_respondent_id"
  end

  create_table "requests", force: :cascade do |t|
    t.bigint "requester_id", null: false
    t.bigint "receiver_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "progress", default: 0, null: false
    t.index ["receiver_id"], name: "index_requests_on_receiver_id"
    t.index ["requester_id"], name: "index_requests_on_requester_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name", null: false
    t.integer "members_amount", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "address", null: false
    t.float "latitude"
    t.float "longitude"
    t.index ["name"], name: "index_schools_on_name", using: :gin
  end

  create_table "subscriptions", force: :cascade do |t|
    t.bigint "subscriber_id", null: false
    t.bigint "target_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscriber_id"], name: "index_subscriptions_on_subscriber_id"
    t.index ["target_id"], name: "index_subscriptions_on_target_id"
  end

  create_table "users", force: :cascade do |t|
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "school_id", null: false
    t.integer "age", null: false
    t.integer "grade", null: false
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "gender", null: false
    t.string "phone_number", null: false
    t.string "bio"
    t.integer "requests_count", default: 0, null: false
    t.string "default_avatar", default: "avatars/avatar-1.png", null: false
    t.boolean "matching_enabled", default: true
    t.boolean "prop_request_notification_enabled", default: true
    t.boolean "receiving_prop_notification_enabled", default: true
    t.boolean "chat_messages_notification_enabled", default: true
    t.boolean "new_friends_notification_enabled", default: true
    t.boolean "new_users_notification_enabled", default: true
    t.string "role", default: "ordinary", null: false
    t.datetime "friend_boost_at"
    t.integer "friends_count", default: 0
    t.integer "pokes_count", default: 0
    t.boolean "hide_top_pokes", default: false, null: false
    t.boolean "new_comments_notification_enabled", default: true
    t.boolean "new_reposts_notification_enabled", default: true
    t.boolean "new_likes_notification_enabled", default: true
    t.index ["school_id"], name: "index_users_on_school_id"
  end

  create_table "verification_codes", force: :cascade do |t|
    t.string "code"
    t.string "phone_number", null: false
    t.bigint "user_id"
    t.datetime "expires_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "attempts_count", default: 0, null: false
    t.index ["user_id"], name: "index_verification_codes_on_user_id"
  end

  add_foreign_key "contacts", "users", on_delete: :cascade
  add_foreign_key "deanonymizations", "chats", on_delete: :cascade
  add_foreign_key "deanonymizations", "users", on_delete: :cascade
  add_foreign_key "friendships", "users", column: "first_user_id", on_delete: :cascade
  add_foreign_key "friendships", "users", column: "second_user_id", on_delete: :cascade
  add_foreign_key "gender_masks_counters", "users", on_delete: :cascade
  add_foreign_key "likes", "feeds"
  add_foreign_key "likes", "users"
  add_foreign_key "notifications", "users"
  add_foreign_key "reactions", "prop_deliveries"
end
