class ApplicationMailer < ActionMailer::Base
  default from: ENV.fetch("SEND_FROM_EMAIL")

  layout "mailer"
end
