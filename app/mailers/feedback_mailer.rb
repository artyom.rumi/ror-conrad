class FeedbackMailer < ApplicationMailer
  def send_feedback(text, sender)
    @text = text
    @sender = sender

    mail subject: "Feedback", to: ENV.fetch("SEND_FEEDBACK_TO_EMAIL")
  end
end
