class ReportUserMailer < ApplicationMailer
  def report_user(reporter, reported_user)
    @reporter_name = reporter.full_name
    @reported_user_name = reported_user.full_name

    mail subject: "Report user", to: ENV.fetch("SEND_REPORT_TO_EMAIL")
  end
end
