class SuggestPropsMailer < ApplicationMailer
  def send_suggested_prop(content, emoji)
    @content = content
    @emoji = emoji || "no emoji"

    mail subject: "Suggested Prop", to: ENV.fetch("SEND_TO_EMAIL")
  end
end
