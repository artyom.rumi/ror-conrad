class PubNubAdapter
  attr_reader :subscribe_key, :publish_key, :pubnub_client_uuid

  PUSH_NOTIFICATION_SERVICE_TYPE = "apns".freeze
  PUB_NUB_URL = "http://pubsub.pubnub.com/v1".freeze

  def initialize
    @subscribe_key = ENV.fetch("PUBNUB_SUBSCRIBE_KEY")
    @publish_key = ENV.fetch("PUBNUB_PUBLISH_KEY")
    @pubnub_client_uuid = ENV.fetch("PUBNUB_CLIENT_UUID")
  end

  def send_message_to_channel(channel_name, message)
    pubnub.publish(
      channel: channel_name,
      message: message
    )
  end

  def add_push_notification_registrations(device_push_token, channel_names)
    request_url = "#{PUB_NUB_URL}/push/sub-key/#{subscribe_key}/devices/#{device_push_token}"

    RestClient.get request_url, params: { add: channel_names, type: PUSH_NOTIFICATION_SERVICE_TYPE }
  rescue RestClient::Exception => e
    log_error(request_error(e))
  end

  def remove_push_notification_registrations(device_push_token, channel_names)
    request_url = "#{PUB_NUB_URL}/push/sub-key/#{subscribe_key}/devices/#{device_push_token}"

    RestClient.get request_url, params: { remove: channel_names, type: PUSH_NOTIFICATION_SERVICE_TYPE }
  rescue RestClient::Exception => e
    log_error(request_error(e))
  end

  def remove_all_push_notification_registrations(device_push_token)
    request_url = "#{PUB_NUB_URL}/push/sub-key/#{subscribe_key}/devices/#{device_push_token}/remove"

    RestClient.get request_url, params: { type: PUSH_NOTIFICATION_SERVICE_TYPE }
  rescue RestClient::Exception => e
    log_error(request_error(e))
  end

  private

  def pubnub
    @pubnub ||= Pubnub.new(
      publish_key: publish_key,
      subscribe_key: subscribe_key,
      uuid: pubnub_client_uuid
    )
  end

  def log_error(error)
    Rails.logger.warn "Could not send request to PubNub. Error: #{error}"
  end

  def request_error(error)
    JSON.parse(error.http_body)["error"]
  end
end
