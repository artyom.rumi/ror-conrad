require "twilio-ruby"

class TwilioAdapter
  class SendSmsError < StandardError
  end

  def initialize
    @account_sid = ENV["TWILIO_ACCOUNT_SID"]
    @auth_token = ENV["TWILIO_AUTH_TOKEN"]
    @from_phone_number = ENV["TWILIO_PHONE_NUMBER"]
    @client = Twilio::REST::Client.new(@account_sid, @auth_token)
  end

  def send_sms(to_phone_number, body)
    @client.api.account.messages.create(
      from: @from_phone_number,
      to: to_phone_number,
      body: body
    )
  rescue Twilio::REST::TwilioError => error
    raise SendSmsError, error.message
  end
end
