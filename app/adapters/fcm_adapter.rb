class FcmAdapter
  def initialize
    @fcm_client = FCM.new(ENV["FCM_SERVER_KEY"])
  end

  def send_push(user, message)
    @fcm_client.send(device_token_ids(user), options(message))
  end

  private

  def device_token_ids(user)
    [user.device.token]
  end

  def options(message)
    { notification: { body: message } }
  end
end
