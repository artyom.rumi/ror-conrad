class Comment < ApplicationRecord
  AVAILABLE_STATUSES = %w[Unread Read].freeze

  belongs_to :user
  belongs_to :feed, counter_cache: :comments_count
  has_many :chats, dependent: :nullify
  has_one :notification, as: :notifiable, dependent: :destroy

  validates :user_id, :feed_id, :text, presence: true

  validates :status, inclusion: AVAILABLE_STATUSES
end
