class Request < ApplicationRecord
  POLLS_LIMIT = 5

  validates :requester, :receiver, :progress, presence: true
  validate :same_open_request_not_exists

  belongs_to :requester, class_name: "User"
  belongs_to :receiver, class_name: "User"

  has_many :prop_deliveries, dependent: :nullify

  has_many :request_respondents, -> { order(index: :asc) }, inverse_of: :request, dependent: :destroy
  has_many :respondents, through: :request_respondents, source: :respondent

  scope :opened, -> { where("progress < :number_of_polls", number_of_polls: POLLS_LIMIT) }

  def increment_progress
    update(progress: progress + 1)
  end

  private

  def same_open_request_not_exists
    return unless same_open_request_exists

    errors.add(:receiver, "already has open request from same user")
  end

  def same_open_request_exists
    Request.opened.where(requester: requester, receiver: receiver).where.not(id: id).exists?
  end
end
