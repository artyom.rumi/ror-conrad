class Blocking < ApplicationRecord
  belongs_to :user
  belongs_to :blockable, polymorphic: true

  validates :user, :blockable, presence: true

  validates :user,
    uniqueness: { scope: %i[blockable_id blockable_type], message: "can't block somebody twice" }

  scope :for_user, -> { where(blockable_type: "User") }
  scope :for_collocutor_mask, -> { where(blockable_type: "CollocutorMask") }

  def for_user?
    blockable_type.eql?("User")
  end

  def blocked_user
    return blockable if for_user?

    blockable.collocutor
  end
end
