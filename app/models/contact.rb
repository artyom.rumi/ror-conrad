class Contact < ApplicationRecord
  belongs_to :user

  validates :phone_number,
    presence: true,
    uniqueness: { scope: :user, message: "has already been taken for this user" },
    format: { with: /\A\+\d{11}\z/, message: "invalid phone number format" }
end
