class FriendIntention < ApplicationRecord
  belongs_to :user
  belongs_to :target, class_name: "User"

  validate :invite_is_unique

  private

  def invite_is_unique
    errors.add(:user, "already sent friend intention to this user") if same_invite_exists?
  end

  def same_invite_exists?
    FriendIntention.exists?(user: user, target: target)
  end
end
