class SuggestedProp
  include ActiveModel::Model

  attr_accessor :content, :emoji

  validates :content, presence: true
end
