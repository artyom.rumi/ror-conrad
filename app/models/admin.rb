class Admin < ApplicationRecord
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  validates :email, presence: true, uniqueness: true
end
