class Chat < ApplicationRecord
  has_many :chat_subscriptions, dependent: :destroy
  has_many :users, through: :chat_subscriptions
  has_many :deanonymizations, dependent: :destroy
  belongs_to :owner, class_name: "User"
  belongs_to :feed, optional: true
  belongs_to :comment, optional: true

  validate :cannot_chat_to_himself

  scope :incognito, -> { where(incognito: true) }
  scope :uncovered, -> { where(incognito: false) }

  def pubnub_channel_name
    "#{channel_name_prefix}chat_#{id}"
  end

  def revealed?
    users.count == deanonymizations.count
  end

  def unrevealed?
    !revealed?
  end

  private

  def channel_name_prefix
    incognito ? "incognito_" : ""
  end

  def parental_object
    comment || feed
  end

  def cannot_chat_to_himself
    return if parental_object.nil?

    error_message = "'s author and chat owner can't be one and the same user"

    errors.add(parental_object.class.name, error_message) if parental_object.user_id == owner.id
  end
end
