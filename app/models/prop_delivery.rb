class PropDelivery < ApplicationRecord
  belongs_to :sender, class_name: "User"
  belongs_to :recipient, class_name: "User", counter_cache: :pokes_count
  belongs_to :prop, class_name: "Prop", optional: true
  belongs_to :request, optional: true
  has_one :reaction, dependent: :destroy

  validates :content, :emoji, :sender_gender, presence: true
  validates :sender, presence: true
  validates :recipient, presence: true
  validates :sender_gender, inclusion: { in: User.genders.values }
  validate :sender_is_not_a_recipient

  scope :unviewed, -> { where(viewed_at: nil) }
  scope :viewed_after, ->(datetime) { where("viewed_at >= ?", datetime) }
  scope :ordinary, -> { where.not(prop_id: nil) }

  enum boost_type: %i[secret friend], _suffix: :boost
  after_save :refresh_friend_boost_at

  def type
    ordinary? ? "ordinary" : "custom"
  end

  def ordinary?
    prop.present?
  end

  def custom?
    !ordinary?
  end

  private

  def sender_is_not_a_recipient
    errors.add(:sender, "cannot send props to himself") if sender_id == recipient_id
  end

  def refresh_friend_boost_at
    recipient.update(friend_boost_at: created_at) if friend_boost?
  end
end
