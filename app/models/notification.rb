class Notification < ApplicationRecord
  AVAILABLE_ACTIONS = %w[new_like new_comment new_repost
                         12h_expired_friend 72h_expired_friend
                         new_friend renew_friendship
                         expired_friendship new_rate].freeze
  GROUPED_ACTIONS = %w[new_like new_repost].freeze
  AVAILABLE_STATUSES = %w[Read Unread].freeze

  belongs_to :user
  belongs_to :notifiable, polymorphic: true

  validates :action_name, inclusion: AVAILABLE_ACTIONS
  validates :status, inclusion: { in: AVAILABLE_STATUSES, message: "%{value} is not a valid status" }

  scope :unread, -> { where(status: "Unread") }
  scope :actual, -> { where("created_at >= ?", 7.days.ago) }
  scope :outdated, -> { where("created_at < ?", 7.days.ago) }
  scope :ungrouped, -> { where.not("status = 'Read' AND action_name IN (?)", GROUPED_ACTIONS) }
end
