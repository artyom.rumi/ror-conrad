class Repost < Feed
  has_one :notification, as: :notifiable, dependent: :destroy
  belongs_to :source_feed, class_name: "Feed"

  validate :repost_own_feed

  private

  def repost_own_feed
    errors.add(:base, "User can't reposted own feed") if user_id == source_feed.user_id
  end
end
