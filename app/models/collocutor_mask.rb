class CollocutorMask < ApplicationRecord
  has_one_attached :avatar

  belongs_to :collocutor, class_name: "User"
  belongs_to :user # user that sees and able to change this mask
  belongs_to :chat, optional: true
end
