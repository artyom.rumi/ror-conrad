class Rate < ApplicationRecord
  belongs_to :feed, counter_cache: :rate_count
  belongs_to :user
  has_one :notification, as: :notifiable, dependent: :destroy
end
