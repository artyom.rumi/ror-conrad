class Grade < ApplicationRecord
  HIGHEST_GRADE = 12

  validates :value, :school, presence: true
  validates :value, uniqueness: { scope: :school }

  belongs_to :school

  # TODO: remove this along with year_of_ending column and constraint
  before_validation :set_defaults

  private

  def set_defaults
    self.year_of_ending ||= Grade::HIGHEST_GRADE + Time.current.year - value -
                            AlreadyGraduated.graduation_year_inc
  end
end
