class Like < ApplicationRecord
  belongs_to :feed, counter_cache: :likes_count
  belongs_to :user
  has_one :notification, as: :notifiable, dependent: :destroy
end
