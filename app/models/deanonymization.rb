class Deanonymization < ApplicationRecord
  belongs_to :chat
  belongs_to :user

  validates :chat, :user, presence: true
  validate :chat_is_incognito
  validates :user, uniqueness: { scope: :chat, message: "can't be deanonymized twice in the same chat" }

  private

  def chat_is_incognito
    errors.add(:chat, "must be incognito") unless chat.incognito
  end
end
