class Subscription < ApplicationRecord
  belongs_to :subscriber, class_name: "User"
  belongs_to :target, class_name: "User"

  validates :subscriber, :target, presence: true
  validates :target, uniqueness: { scope: :subscriber }
  validate :subscriber_is_not_target

  def subscriber_is_not_target
    errors.add(:target, "target should not be equal to subscriber") if target_id == subscriber_id
  end
end
