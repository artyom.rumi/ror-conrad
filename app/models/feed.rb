class Feed < ApplicationRecord
  has_one_attached :image

  AVAILABLE_TYPES = %w[Feed Repost].freeze

  belongs_to :user
  has_many :likes, dependent: :destroy
  # added by lava on 0121
  has_many :rates, dependent: :destroy
  has_many :users, through: :likes
  # has_many :users, through: :likes
  has_many :comments, dependent: :destroy
  has_many :chats, dependent: :nullify
  has_many :reposts,
    class_name: "Repost", foreign_key: "source_feed_id", dependent: :destroy, inverse_of: :source_feed

  validates :type, inclusion: AVAILABLE_TYPES
  validate :image_or_link

  def liked?(user_id)
    users.ids.include?(user_id)
  end

  def reposted?
    type == "Repost"
  end

  def unread_comments_count
    comments.where(status: "Unread").count
  end

  def user_chat(user)
    chats.find_by(owner_id: user.id, comment_id: nil)
  end

  private

  def image_or_link
    errors.add(:base, "Specify a image or a text, not both") if image.attachment.present? && link.present?
  end
end
