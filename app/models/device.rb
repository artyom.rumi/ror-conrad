class Device < ApplicationRecord
  include ActiveModel::Dirty

  validates :phone_number, presence: true

  belongs_to :user, optional: true
  has_one :auth_token, dependent: :destroy
end
