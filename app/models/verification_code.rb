class VerificationCode < ApplicationRecord
  belongs_to :user, optional: true

  validates :phone_number, :expires_at, presence: true
  validates :phone_number, uniqueness: true

  def valid_code?(provided_code)
    expires_at.future? && provided_code == code && attempts_count >= 0
  end
end
