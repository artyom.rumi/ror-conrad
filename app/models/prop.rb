class Prop < ApplicationRecord
  include Discard::Model
  default_scope -> { kept }

  has_many :prop_deliveries, dependent: :destroy

  validates :content, :emoji, presence: true
  validates :gender, inclusion: { in: ["male", "female", nil] }

  scope :special, -> { where(special: true) }
end
