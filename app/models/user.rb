class User < ApplicationRecord # rubocop:disable Metrics/ClassLength
  include PgSearch
  using AlreadyGraduated

  has_one_attached :avatar

  has_many_attached :pictures

  MINIMAL_AGE = 12

  enum gender: { female: "female", male: "male", non_binary: "non_binary" }
  enum role: { ordinary: "ordinary", test: "test", bot: "bot" }

  validates :age, :grade, :first_name, :last_name, :gender,
    :phone_number, :school, :default_avatar, presence: true, unless: :bot?
  validates :age, numericality: { only_integer: true, greater_than_or_equal_to: MINIMAL_AGE }, unless: :bot?
  validates :phone_number, uniqueness: true, unless: :bot?

  belongs_to :school, counter_cache: :members_amount, optional: true
  has_many :schoolmates, ->(user) { where.not(id: user.id) }, through: :school, source: :users

  has_one :verification_code, dependent: :destroy
  has_one :auth_token, dependent: :destroy
  has_one :device, dependent: :destroy
  has_one :gender_masks_counter, dependent: :destroy

  has_many :outcoming_subscriptions,
    class_name: "Subscription", foreign_key: :subscriber_id, inverse_of: :subscriber, dependent: :destroy
  has_many :incoming_subscriptions,
    class_name: "Subscription", foreign_key: :target_id, inverse_of: :target, dependent: :destroy

  has_many :subscribed_by_users, through: :incoming_subscriptions, source: :subscriber
  has_many :subscribed_to_users, through: :outcoming_subscriptions, source: :target

  has_many :outcoming_requests,
    class_name: "Request", foreign_key: :requester_id, inverse_of: :requester, dependent: :destroy
  has_many :incoming_requests,
    class_name: "Request", foreign_key: :receiver_id, inverse_of: :receiver, dependent: :destroy

  has_many :outcoming_prop_deliveries,
    class_name: "PropDelivery", foreign_key: :sender_id, inverse_of: :sender, dependent: :destroy
  has_many :incoming_prop_deliveries,
    class_name: "PropDelivery", foreign_key: :recipient_id, inverse_of: :recipient, dependent: :destroy

  has_many :senders, through: :incoming_prop_deliveries, source: :sender
  has_many :recipients, through: :outcoming_prop_deliveries, source: :recipient

  has_many :chat_subscriptions, dependent: :destroy
  has_many :chats, through: :chat_subscriptions
  has_many :uncovered_chats, -> { uncovered }, through: :chat_subscriptions, source: :chat
  has_many :incognito_chats, -> { incognito }, through: :chat_subscriptions, source: :chat

  has_many :collocutor_masks, dependent: :destroy
  has_many :masks,
    foreign_key: :collocutor_id, inverse_of: :collocutor, class_name: "CollocutorMask", dependent: :destroy

  has_many :request_respondents, foreign_key: :respondent_id, inverse_of: :respondent, dependent: :destroy
  has_many :responding_request, through: :request_respondents, source: :request
  has_many :blockings, dependent: :destroy
  has_many :deanonymizations, dependent: :destroy
  has_many :contacts, dependent: :destroy, inverse_of: :user
  has_many :friend_intentions, dependent: :destroy
  has_many :feeds, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy

  scope :with_matching_enabled, -> { where(matching_enabled: true) }
  scope :peers_for, lambda { |current_user|
    if current_user.grade.already_graduated?
      where("grade < ?", AlreadyGraduated.next_graduation_year)
    else
      where("grade >= ?", AlreadyGraduated.next_graduation_year)
    end
  }

  delegate :push_token, to: :device, prefix: true, allow_nil: true

  pg_search_scope :search_by_full_name,
    against: %i[first_name last_name],
    using: {
      tsearch: { prefix: true },
      trigram: { threshold: 0.5 }
    }

  def full_name
    @full_name ||= "#{first_name} #{last_name}"
  end

  def friend_ids(scope = :all)
    Users::FriendsQuery.new(self, Friendship.public_send(scope)).all.ids
  end

  def friends_with?(user, scope = :all)
    Friendship.public_send(scope).between_users(user, self).present?
  end

  def friend_of_friends?(user)
    Friendship.where("first_user_id = :user_id AND second_user_id IN (:friends_ids) OR
      first_user_id IN (:friends_ids) AND second_user_id = :user_id",
      friends_ids: friend_ids, user_id: user.id).exists?
  end

  def friendship_status(user)
    return if user == self

    if friends_with?(user)
      friend_status(user)
    elsif friend_intentions.exists?(target_id: user.id)
      "invited"
    else
      "uninvited"
    end
  end

  def affirmed?(friend)
    friendship = Friendship.between_users(self, friend)

    return true if friendship.first_user == self && friendship.first_user_keep
    return true if friendship.second_user == self && friendship.second_user_keep

    false
  end

  def blocked_user_ids
    @blocked_user_ids ||= blockings.for_user.pluck(:blockable_id)
  end

  def blocked_user?(user)
    blocked_user_ids.include? user.id
  end

  def top_pokes(count = 3)
    @top_pokes ||= Prop.with_discarded.find(top_poke_ids(count))
  end

  def top_poke_ids(count = 3)
    @top_poke_ids ||=
      incoming_prop_deliveries
      .ordinary
      .group(:prop_id)
      .order(Arel.sql("COUNT(*) DESC"))
      .limit(count)
      .pluck(:prop_id)
  end

  def inline_contact_users
    User.where("phone_number IN (?)", contacts.pluck(:phone_number))
  end

  def outline_contact_phone
    phone_numbers = contacts.pluck(:phone_number) - inline_contact_users.pluck(:phone_number)

    Contact.where("phone_number IN (?) AND user_id = ?", phone_numbers, id)
  end

  def days_to_expired_friendship(friend)
    (friend_expires_at(friend) - Time.current.to_date).to_i
  end

  def friend_expires_at(friend)
    Friendship.between_users(self, friend).expires_at.to_date
  end

  def friends_count
    Users::FriendsQuery.new(self, Friendship.active).all.count
  end

  private

  def friend_status(user)
    if affirmed?(user)
      "affirmed"
    elsif friend_expires_at(user) > Time.current
      "friend"
    elsif friend_expires_at(user) < Time.current
      "expired"
    end
  end
end
