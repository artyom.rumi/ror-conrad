class AuthToken < ApplicationRecord
  validates :expires_at, presence: true

  has_secure_token :value

  belongs_to :user, optional: true
  belongs_to :device

  delegate :phone_number, to: :device, prefix: true

  scope :live, -> { where("expires_at > ?", Time.current) }
  scope :expired, -> { where("expires_at < ?", Time.current) }
end
