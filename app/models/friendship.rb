class Friendship < ApplicationRecord
  belongs_to :first_user, class_name: "User"
  belongs_to :second_user, class_name: "User"

  validate :users_couple_is_unique
  validates :first_user, :second_user, presence: true

  scope :super, -> { where(super_friends: true) }
  scope :simple, -> { where(super_friends: false) }
  scope :active, -> { where("expires_at > ?", Time.current) }
  scope :expired, -> { where("expires_at < ?", Time.current) }
  scope :expiring, -> { where("expires_at < ?", Time.zone.now + 72.hours) }

  SF_EXPIRATION = 7.days
  EXPIRES_AT_DAYS = 30.days

  def self.between_users(first_user, second_user)
    find_by(":first_user_id = first_user_id AND :second_user_id = second_user_id
      OR :second_user_id = first_user_id AND :first_user_id = second_user_id",
      first_user_id: first_user.id, second_user_id: second_user.id)
  end

  private

  def users_couple_is_unique
    errors.add(:first_user, "already has friendship with this user") if same_friendship_exists?
  end

  def same_friendship_exists?
    Friendship.where.not(id: id).between_users(first_user, second_user)
  end
end
