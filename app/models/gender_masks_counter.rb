class GenderMasksCounter < ApplicationRecord
  belongs_to :user

  validates :male, :female, :non_binary, presence: true
end
