class ChatSubscription < ApplicationRecord
  belongs_to :user
  belongs_to :chat

  validates :user, :chat, presence: true
  validates :user, uniqueness: { scope: :chat }
end
