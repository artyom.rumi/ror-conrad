class School < ApplicationRecord
  include PgSearch

  has_many :users, dependent: :destroy
  has_many :grades, dependent: :destroy

  validates :name, :address, presence: true

  accepts_nested_attributes_for :grades

  reverse_geocoded_by :latitude, :longitude

  pg_search_scope :search_by_name,
    against: :name,
    using: {
      tsearch: { prefix: true },
      trigram: { threshold: 0.5 }
    }
end
