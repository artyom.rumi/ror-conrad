class RequestRespondent < ApplicationRecord
  validates :request, :respondent, presence: true
  validates :respondent, uniqueness: { scope: :request }

  belongs_to :request
  belongs_to :respondent, class_name: "User"
end
