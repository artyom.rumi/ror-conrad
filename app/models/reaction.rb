class Reaction < ApplicationRecord
  has_one_attached :photo

  belongs_to :prop_delivery

  validates :prop_delivery, presence: true, uniqueness: true

  before_save :content_not_null

  def content_not_null
    self.content ||= ""
  end
end
