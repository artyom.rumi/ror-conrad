module AlreadyGraduated
  MONTH_OF_GRADUATION = 6

  refine Integer do
    def already_graduated?
      self < Time.current.year ||
        (self == Time.current.year && AlreadyGraduated.post_graduation_month?)
    end

    def grade_value
      Grade::HIGHEST_GRADE + Time.current.year - self + AlreadyGraduated.graduation_year_inc
    end
  end

  def self.next_graduation_year
    Time.current.year + graduation_year_inc
  end

  def self.graduation_year_inc
    post_graduation_month? ? 1 : 0
  end

  def self.post_graduation_month?
    MONTH_OF_GRADUATION < Time.current.month
  end
end
