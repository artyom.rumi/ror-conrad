module Api
  class ApplicationController < ActionController::API
    before_action :authenticate_user
    before_action :setup_currents

    rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

    serialization_scope :view_context_with_current_user

    private

    def view_context_with_current_user
      { view_context: view_context, current_user: current_user }
    end

    def authenticate_guest
      head :unauthorized if auth_token.blank?
    end

    def authenticate_user
      head :unauthorized if current_user.blank?
    end

    def current_user
      auth_token.present? && auth_token.user.presence
    end

    def auth_token
      @auth_token ||= AuthToken.live.find_by(value: auth_token_value)
    end

    def auth_token_value
      request.headers["X-Auth-Token"].presence
    end

    def record_not_found(error)
      render json: { error: error.message }, status: :not_found
    end

    def page
      params[:page]
    end

    def per_page
      params[:per_page]
    end

    def setup_currents
      Current.user = current_user
    end
  end
end
