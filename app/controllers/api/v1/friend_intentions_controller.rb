module Api
  module V1
    class FriendIntentionsController < ApplicationController
      def create
        result = ::FriendIntentions::Create.call(user: current_user, target: target)

        if result.success?
          render json: result.friend_intention, status: :created
        else
          render json: { error: result.error }, status: :unprocessable_entity
        end
      end

      private

      def friend_intention_params
        params.require(:friend_intention).permit(:target_id)
      end

      def target
        User.find(params[:target_id])
      end
    end
  end
end
