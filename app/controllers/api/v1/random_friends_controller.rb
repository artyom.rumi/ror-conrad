module Api
  module V1
    class RandomFriendsController < ApplicationController
      FRIENDS_QUEUE_LIMIT = 5

      expose :friends, :fetch_friends

      def index
        render json: friends.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_friends
        current_user.subscribed_to_users
                    .includes(:school, :incoming_subscriptions)
                    .order(Arel.sql("random()"))
                    .limit(FRIENDS_QUEUE_LIMIT)
      end
    end
  end
end
