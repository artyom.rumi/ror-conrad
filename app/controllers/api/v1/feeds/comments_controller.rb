module Api
  module V1
    module Feeds
      class CommentsController < ApplicationController
        expose(:comment) { current_user.comments.find(params[:id]) }

        def index
          render json: fetch_comments, each_serializer: CommentSerializer, include: %w[chat.** user]
        end

        def create
          result = ::Comments::Create.call(comment_params: comment_params)

          if result.success?
            render json: fetch_comments, status: :created, each_serializer: CommentSerializer
          else
            head :bad_request
          end
        end

        def update
          if comment.update(text: comment_params[:text])
            render json: comment, serializer: CommentSerializer
          else
            render json: comment.errors.messages, status: :unprocessable_entity
          end
        end

        def destroy
          comment.destroy

          render json: fetch_comments, each_serializer: CommentSerializer
        end

        private

        def comment_params
          params.permit(:feed_id, :text).merge(user_id: current_user.id)
        end

        def feed
          @feed ||= Feed.find(params[:feed_id])
        end

        def fetch_comments
          feed.comments.includes(:user, :chats, user: :school).order(created_at: :desc)
        end
      end
    end
  end
end
