module Api
  module V1
    module Feeds
      class MarkCommentsController < ApplicationController
        expose :feed, parent: :current_user
        expose :comments, :fetch_comments

        def create
          comments.update(status: "Read")

          render json: feed, serializer: FeedSerializer
        end

        private

        def fetch_comments
          feed.comments.where(id: params[:commentIDs])
        end
      end
    end
  end
end
