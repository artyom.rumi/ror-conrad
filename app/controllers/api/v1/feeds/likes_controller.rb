module Api
  module V1
    module Feeds
      class LikesController < ApplicationController
        def create
          result = ::Likes::Create.call(like_params: like_params)

          if result.success?
            render json: feed, status: :created, serializer: FeedSerializer
          else
            head :bad_request
          end
        end

        def destroy
          if feed.liked?(current_user.id)
            feed.likes.find_by(user_id: current_user.id).destroy

            render json: feed, serializer: FeedSerializer
          else
            head :bad_request
          end
        end

        private

        def like_params
          params.permit(:feed_id).merge(user_id: current_user.id)
        end

        def feed
          @feed ||= Feed.find(params[:feed_id])
        end
      end
    end
  end
end
