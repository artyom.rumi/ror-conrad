module Api
  module V1
    module Feeds
      class RepostsController < ApplicationController
        expose :feed

        def create
          result = ::RepostedFeeds::Create.call(user: current_user, source_feed: feed)

          if result.success?
            render json: result.activity, status: :created, serializer: FeedSerializer
          else
            render json: result.error, status: :unprocessable_entity
          end
        end
      end
    end
  end
end
