module Api
  module V1
    module Feeds
      class ChatsController < ApplicationController
        expose :chat
        expose :feed

        def create
          result = ::Chats::Create.call(chat_params)

          if result.success?
            render json: result.chat, status: :created, serializer: ChatSerializer
          else
            render json: { error: result.error }, status: :unprocessable_entity
          end
        end

        private

        def chat_params
          {
            members_ids: [current_user.id, feed.user_id],
            owner: current_user,
            incognito: true,
            feed: feed,
            is_group: false
          }
        end
      end
    end
  end
end
