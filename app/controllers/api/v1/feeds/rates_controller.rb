module Api
    module V1
      module Feeds
        class RatesController < ApplicationController
          def create
            puts("====================== this is the rate create ======================")
            puts(feed.rate)
            puts(params[:rate])
            feed.update(rate: feed.rate + params[:rate].to_f)
            result = ::Rates::Create.call(rate_params: rate_params)
            if result.success?
              render json: feed, status: :created, serializer: FeedSerializer
            else
              head :bad_request
            end
          end
  
          private
  
          def rate_params
            params.permit(:feed_id, :rate).merge(user_id: current_user.id)
          end
  
          def feed
            @feed ||= Feed.find(params[:feed_id])
          end
        end
      end
    end
  end