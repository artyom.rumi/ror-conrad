module Api
  module V1
    module PropDeliveries
      class ViewsController < ApplicationController
        expose :prop_delivery, scope: -> { current_user.incoming_prop_deliveries }

        def create
          if prop_delivery.update(viewed_at: Time.current)
            head :no_content
          else
            render json: prop_delivery.errors.full_messages, status: :unprocessable_entity
          end
        end
      end
    end
  end
end
