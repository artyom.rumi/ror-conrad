module Api
  module V1
    module PropDeliveries
      class ReactionsController < ApplicationController
        expose :reaction

        def create
          if reaction_valid? && reaction.save
            notify_reply(reaction)
            render json: reaction, serializer: ReactionSerializer, status: :created
          else
            render json: reaction.errors.messages, status: :unprocessable_entity
          end
        end

        def update
          if current_user_is_prop_sender?
            existing_reaction.update(read_at: Time.current)
            render json: existing_reaction, serializer: ReactionSerializer, status: :ok
          else
            render json: existing_reaction.errors.messages, status: :unprocessable_entity
          end
        end

        private

        def current_user_is_prop_sender?
          existing_reaction&.prop_delivery&.sender == current_user && existing_reaction.read_at.nil?
        end

        def reaction_params
          params.permit(:prop_delivery_id, :content, :photo)
        end

        def existing_reaction
          @existing_reaction ||= PropDelivery.find(params[:prop_delivery_id]).reaction
        end

        def reaction_valid?
          reaction&.prop_delivery&.recipient == current_user
        end

        def notify_reply(reaction)
          notification = PushNotification.new(
            receiver_device_push_token: reaction&.prop_delivery&.sender&.device_push_token,
            category: :poke_reply
          ).apnotic_notification.tap { |n| n.alert = notification_alert(reaction.prop_delivery.recipient) }
          SendPushNotifications.call(notifications: [notification])
        end

        def notification_alert(reply_sender)
          { body: "#{reply_sender.first_name} #{reply_sender.last_name} replied!" }
        end
      end
    end
  end
end
