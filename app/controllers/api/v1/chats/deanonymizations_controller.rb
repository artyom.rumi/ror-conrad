module Api
  module V1
    module Chats
      class DeanonymizationsController < ApplicationController
        expose :chat
        expose :deanonymizations, from: :chat

        def index
          render json: deanonymizations
        end

        def create
          if reveal.success?
            render json: reveal.deanonymization, status: :created
          else
            render json: { error: reveal.error }, status: :unprocessable_entity
          end
        end

        def destroy
          ::Users::CancelReveal.call(deanonymization: deanonymization)
          head :no_content
        end

        private

        def reveal
          @reveal ||= ::Users::Reveal.call(deanonymization_params: deanonymization_params)
        end

        def deanonymization
          @deanonymization ||= Deanonymization.find_by(chat_id: chat.id, user_id: current_user.id)
        end

        def deanonymization_params
          params.permit(:chat_id).merge(user_id: current_user.id)
        end
      end
    end
  end
end
