module Api
  module V1
    module Chats
      class ActivitiesController < ApplicationController
        expose :chat, scope: -> { current_user.chats }

        def create
          if chat.update(updated_at: Time.current)
            render json: chat
          else
            render json: chat.errors.full_messages, status: :unprocessable_entity
          end
        end
      end
    end
  end
end
