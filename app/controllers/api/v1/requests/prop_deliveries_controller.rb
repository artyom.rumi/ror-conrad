module Api
  module V1
    module Requests
      class PropDeliveriesController < ApplicationController
        def create
          if deliver_prop.success?
            render json: deliver_prop.prop_delivery, status: :created
          else
            render json: deliver_prop.errors, status: :unprocessable_entity
          end
        end

        private

        def prop_delivery_params
          params
            .permit(:recipient_id, :prop_id, :request_id)
            .merge(sender_id: current_user.id)
        end

        def deliver_prop
          @deliver_prop ||= PropsRequests::DeliverProp.call(prop_delivery_params: prop_delivery_params)
        end
      end
    end
  end
end
