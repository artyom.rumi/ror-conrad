module Api
  module V1
    module Requests
      class RespondentsController < ApplicationController
        expose :prop_request, model: Request
        expose :respondents, :fetch_respondents

        def index
          render json: respondents.with_attached_avatar, each_serializer: SchoolmateSerializer
        end

        private

        def fetch_respondents
          prop_request.respondents.includes(:school, :incoming_subscriptions)
        end
      end
    end
  end
end
