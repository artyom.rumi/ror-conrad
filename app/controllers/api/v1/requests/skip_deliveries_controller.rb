module Api
  module V1
    module Requests
      class SkipDeliveriesController < ApplicationController
        expose :prop_request, -> { Request.find(params[:request_id]) }

        def create
          PropsRequests::IncrementRequestProgress.call(request: prop_request)
          render json: prop_request, serializer: RequestSerializer
        end
      end
    end
  end
end
