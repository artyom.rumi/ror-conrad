module Api
  module V1
    class CommunitiesController < ApplicationController
      expose :community, :fetch_community

      def index
        render json: community.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_community
        ::Users::SearchQuery
          .new(keywords, User.where("id IN (?)", community_ids))
          .all
          .includes(:school, :incoming_subscriptions, :friend_intentions)
      end

      def keywords
        params[:keywords]
      end

      def community_ids
        (current_user.schoolmates + current_user.friend_intentions.pluck(:target_id)).uniq
      end
    end
  end
end
