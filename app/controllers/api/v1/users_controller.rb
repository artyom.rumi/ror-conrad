module Api
  module V1
    class UsersController < ApplicationController
      skip_before_action :authenticate_user, only: :create

      before_action :authenticate_guest, only: :create

      expose(:user)

      def show
        render json: user, serializer: SchoolmateFullSerializer
      end

      def create
        if current_user
          render json: { error: I18n.t("api.users.already_registered_error") }, status: :unprocessable_entity
        elsif register_user.success?
          render json: register_user.user, status: :created
        else
          render json: register_user.error, status: :unprocessable_entity
        end
      end

      def update
        old_school_id = current_user.school_id

        if current_user.update(user_params)
          update_subscriptions(old_school_id)
          current_user.reload

          render json: current_user, status: :ok
        else
          render json: current_user.errors.messages, status: :unprocessable_entity
        end
      end

      private

      def update_subscriptions(old_school_id)
        ::Users::UpdateSubscriptions.call(user: current_user) if current_user.school_id != old_school_id
      end

      def register_user
        @register_user ||= RegisterUser.call(
          user_params: user_params,
          auth_token: auth_token,
          phone_numbers: phone_numbers
        )
      end

      def user_params
        params
          .permit(:age, :grade, :first_name, :last_name, :gender, :school_id, :bio, :matching_enabled,
            :prop_request_notification_enabled, :receiving_prop_notification_enabled, :new_friends_notification_enabled,
            :chat_messages_notification_enabled, :new_users_notification_enabled, :new_comments_notification_enabled,
            :new_reposts_notification_enabled, :new_likes_notification_enabled, :new_rates_notification_enabled, :hide_top_pokes)
          .merge(phone_number: auth_token.device_phone_number)
      end

      def phone_numbers
        params[:phone_numbers]
      end
    end
  end
end
