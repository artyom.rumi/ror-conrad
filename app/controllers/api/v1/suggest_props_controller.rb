module Api
  module V1
    class SuggestPropsController < ApplicationController
      expose :suggested_prop

      def create
        if suggested_prop.valid?
          SuggestPropsMailer.send_suggested_prop(suggested_prop.content, suggested_prop.emoji).deliver_later
          head :no_content
        else
          render json: suggested_prop.errors.messages, status: :unprocessable_entity
        end
      end

      private

      def suggested_prop_params
        params.permit(:content, :emoji)
      end
    end
  end
end
