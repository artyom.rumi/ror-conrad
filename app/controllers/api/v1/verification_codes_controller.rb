module Api
  module V1
    class VerificationCodesController < ApplicationController
      skip_before_action :authenticate_user

      def create
        if deliver_code.success?
          render json: deliver_code.verification_code, status: :created
        else
          render json: { error: deliver_code.error }, status: :unprocessable_entity
        end
      end

      private

      def verification_code_params
        params.permit(:phone_number)
      end

      def deliver_code
        @deliver_code ||= Verifications::DeliverCode.call(verification_code_params)
      end
    end
  end
end
