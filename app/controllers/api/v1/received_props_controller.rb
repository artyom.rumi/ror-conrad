module Api
  module V1
    class ReceivedPropsController < ApplicationController
      expose :incoming_prop_deliveries, from: :current_user

      def index
        render json: fetch_received_prop_deliveries, each_serializer: ReceivedPropSerializer
      end

      private

      def fetch_received_prop_deliveries
        incoming_prop_deliveries
          .unviewed
          .or(
            incoming_prop_deliveries
            .viewed_after(3.days.ago)
          ).ordinary
          .includes(:prop, :sender, :reaction)
      end
    end
  end
end
