module Api
  module V1
    class InterlocutorsController < ApplicationController
      expose :interlocutors, :fetch_interlocutors

      def index
        render json: interlocutors.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_interlocutors
        ::Users::SearchQuery
          .new(keywords, ::Users::FriendsQuery.new(current_user).all)
          .all
          .order(first_name: :asc, last_name: :asc)
          .includes(:school)
          .page(page)
          .per(per_page)
      end

      def keywords
        params[:keywords]
      end
    end
  end
end
