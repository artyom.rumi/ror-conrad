module Api
  module V1
    class SchoolsController < ApplicationController
      skip_before_action :authenticate_user
      before_action :authenticate_guest

      def index
        render json: schools
      end

      private

      def schools
        ::Schools::FilterSchoolsQuery.new(filter_params).all.page(page).per(per_page)
      end

      def filter_params
        params.permit(:year_of_ending, :latitude, :longitude, :keywords).to_h
      end
    end
  end
end
