module Api
  module V1
    class ContactsController < ApplicationController
      expose :contacts, :fetch_contacts

      def index
        render json: contacts.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_contacts
        User
          .where(id: contact_ids)
          .includes(:school, :incoming_subscriptions)
      end

      def contact_ids
        filtered_contacts.pluck(:id)
      end

      def filtered_contacts
        users = User
                .ordinary
                .where.not(id: current_user.id)
                .where(phone_number: params[:phone_numbers])
        return users unless filter_schoolmates

        users - current_user.schoolmates
      end

      def filter_schoolmates
        params[:filter_schoolmates]
      end
    end
  end
end
