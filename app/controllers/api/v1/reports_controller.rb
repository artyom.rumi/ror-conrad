module Api
  module V1
    class ReportsController < ApplicationController
      expose :report
      expose :reported_user, model: User, id: :reported_user_id

      def create
        if report.valid?
          ReportUserMailer.report_user(current_user, reported_user).deliver_later
          head :no_content
        else
          render json: report.errors.messages, status: :unprocessable_entity
        end
      end

      private

      def report_params
        params.permit(:reported_user_id).merge(reporter: current_user)
      end
    end
  end
end
