module Api
  module V1
    class FollowersController < ApplicationController
      expose :followers, :fetch_followers

      def index
        render json: followers.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_followers
        current_user.subscribed_by_users.includes(:school, :incoming_subscriptions)
      end
    end
  end
end
