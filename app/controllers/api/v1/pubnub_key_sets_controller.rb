module Api
  module V1
    class PubnubKeySetsController < ApplicationController
      def show
        render json: {
          pubnub_publish_key: ENV.fetch("PUBNUB_PUBLISH_KEY"),
          pubnub_subscribe_key: ENV.fetch("PUBNUB_SUBSCRIBE_KEY")
        }
      end
    end
  end
end
