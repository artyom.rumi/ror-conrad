module Api
  module V1
    class PicturesController < ApplicationController
      def create
        if attachment_valid? && current_user.update(pictures: pictures_params)
          render json: current_user.pictures.last, status: :created, serializer: PictureSerializer
        else
          render json: current_user.errors.messages, status: :unprocessable_entity
        end
      end

      def pictures_params
        params.require(:image)
      end

      def attachment_valid?
        AttachmentParamValidator.new(current_user, :pictures, pictures_params).valid?
      end
    end
  end
end
