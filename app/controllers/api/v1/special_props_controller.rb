module Api
  module V1
    class SpecialPropsController < ApplicationController
      expose :props, -> { Prop.special.order(Arel.sql("random()")) }

      def index
        render json: props
      end
    end
  end
end
