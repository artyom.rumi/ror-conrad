module Api
  module V1
    class SubscriptionsController < ApplicationController
      expose :subscriptions, -> { current_user.outcoming_subscriptions }

      def create
        subscription = find_subscription || subscriptions.build(subscription_params)
        render(json: subscription, status: :conflict) and return if subscription.persisted?

        if subscription.save
          render json: subscription, status: :created
        else
          render json: subscription.errors.messages, status: :unprocessable_entity
        end
      end

      def destroy
        if destroy_subscription.success?
          head :no_content
        else
          render json: destroy_subscription.error, status: :unprocessable_entity
        end
      end

      private

      def subscription_params
        params.permit(:target_id).merge(subscriber_id: current_user.id)
      end

      def destroy_subscription
        @destroy_subscription ||= Subscription::Destroy.call(subscription: find_subscription)
      end

      def find_subscription
        return subscriptions.find(params[:id]) if params[:id]

        subscriptions.find_by(target_id: params[:target_id])
      end
    end
  end
end
