module Api
  module V1
    module CollocutorMasks
      class BlockingsController < ApplicationController
        expose :collocutor_mask
        expose :blockings, -> { current_user.blockings.for_collocutor_mask }
        expose :blocking, scope: -> { blockings }, id: :collocutor_mask_id, find_by: :blockable_id

        def create
          create_blocking = Blockings::Block.call(user: current_user, blockable: collocutor_mask)

          if create_blocking.success?
            render json: create_blocking.blocking
          else
            render json: { error: create_blocking.error }, status: :unprocessable_entity
          end
        end

        def destroy
          Blockings::Unblock.call(blocking: blocking)
          head :no_content
        end

        private

        def blocking_params
          { blockable: collocutor_mask }
        end
      end
    end
  end
end
