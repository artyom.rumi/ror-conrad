module Api
  module V1
    class DevicesController < ApplicationController
      def update
        update_device = Devices::Update.call(device: current_user.device, device_params: device_params)

        if update_device.success?
          render json: update_device.device
        else
          render json: { error: update_device.error }, status: :unprocessable_entity
        end
      end

      private

      def device_params
        params.permit(:push_token, :phone_number)
      end
    end
  end
end
