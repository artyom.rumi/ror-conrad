module Api
  module V1
    class SentPropsController < ApplicationController
      expose :sent_props, -> { current_user.outcoming_prop_deliveries.includes(:prop, recipient: [:school]) }

      def index
        render json: sent_props, each_serializer: SentPropsSerializer
      end
    end
  end
end
