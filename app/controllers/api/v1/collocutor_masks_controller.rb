module Api
  module V1
    class CollocutorMasksController < ApplicationController
      expose :collocutor_masks, from: :current_user
      expose :collocutor_mask

      def index
        render json: collocutor_masks.with_attached_avatar, status: :ok
      end

      def show
        render json: collocutor_mask
      end

      def update
        if attachment_valid? && collocutor_mask.update(collocutor_mask_params)
          render json: collocutor_mask, status: :ok
        else
          render json: collocutor_mask.errors.messages, status: :unprocessable_entity
        end
      end

      private

      def collocutor_mask_params
        params.permit(:name, :avatar)
      end

      def avatar_param
        collocutor_mask_params[:avatar]
      end

      def attachment_valid?
        avatar_param.nil? || AttachmentParamValidator.new(collocutor_mask, :avatar, avatar_param).valid?
      end
    end
  end
end
