module Api
  module V1
    class FriendsController < ApplicationController
      expose :friends, :fetch_friends

      def index
        render json: friends.with_attached_avatar, each_serializer: FriendSerializer
      end

      private

      def fetch_friends
        ::Users::SearchQuery
          .new(keywords, User.where("id IN (?)", friend_ids))
          .all
          .includes(:school, :incoming_subscriptions, :friend_intentions)
      end

      def keywords
        params[:keywords]
      end

      def friend_ids
        (current_user.friend_ids + current_user.friend_intentions.pluck(:target_id)).uniq
      end
    end
  end
end
