module Api
  module V1
    class SchoolmatesController < ApplicationController
      expose :schoolmates, :fetch_schoolmates

      def index
        render json: schoolmates.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_schoolmates
        current_user
          .schoolmates
          .peers_for(current_user)
          .includes(:school, :incoming_subscriptions, :friend_intentions)
      end
    end
  end
end
