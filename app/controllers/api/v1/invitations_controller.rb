module Api
  module V1
    class InvitationsController < ApplicationController
      def create
        invite = CreateInvitation.call(contacts: invitation_params[:contacts])

        if invite.success?
          render json: {}, status: :no_content
        else
          render json: { error: invite.error }, status: :unprocessable_entity
        end
      end

      def invitation_params
        params.permit(contacts: %i[name phone_number])
      end
    end
  end
end
