module Api
  module V1
    class CustomPropDeliveriesController < ApplicationController
      expose :prop_delivery

      def create
        if deliver_prop.success?
          render json: deliver_prop.prop_delivery, status: :created
        else
          render json: { error: deliver_prop.error }, status: :unprocessable_entity
        end
      end

      private

      def deliver_prop
        @deliver_prop ||= ::PropDeliveries::DeliverCustom.call(prop_delivery_params: custom_prop_delivery_params)
      end

      def custom_prop_delivery_params
        params.permit(:recipient_id, :content)
      end
    end
  end
end
