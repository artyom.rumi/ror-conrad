module Api
  module V1
    class FakeRequestsController < ApplicationController
      def create
        create_request = PropsRequests::Create.call(receiver: current_user, requester: requester)

        if create_request.success?
          render json: create_request.request, status: :created
        else
          render json: { error: create_request.error }, status: :unprocessable_entity
        end
      end

      private

      def requester
        current_user.subscribed_to_users.order(Arel.sql("random()")).first
      end
    end
  end
end
