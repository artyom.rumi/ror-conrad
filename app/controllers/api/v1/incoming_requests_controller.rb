module Api
  module V1
    class IncomingRequestsController < ApplicationController
      expose :user_requests, -> { current_user.incoming_requests.opened }
      expose :user_request, model: :request, scope: -> { user_requests }

      def index
        render json: user_requests, each_serializer: RequestSerializer
      end

      def show
        render json: user_request
      end
    end
  end
end
