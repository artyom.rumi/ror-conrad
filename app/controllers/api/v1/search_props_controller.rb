module Api
  module V1
    class SearchPropsController < ApplicationController
      expose :props, :fetch_props

      def index
        render json: props
      end

      private

      def fetch_props
        Props::SearchQuery.new(params[:content]).by_content
      end
    end
  end
end
