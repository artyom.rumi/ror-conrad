module Api
  module V1
    class FeedbacksController < ApplicationController
      def create
        if feedback_params[:text].present?
          FeedbackMailer.send_feedback(feedback_params[:text], current_user).deliver_later
          head :no_content
        else
          render json: { error: "Text is required" }, status: :unprocessable_entity
        end
      end

      private

      def feedback_params
        params.permit(:text)
      end
    end
  end
end
