module Api
  module V1
    class AvatarsController < ApplicationController
      def create
        if attachment_valid? && current_user.update(avatar: file_params)
          render json: current_user, status: :created
        else
          render json: current_user.errors.messages, status: :unprocessable_entity
        end
      end

      def destroy
        current_user.avatar.purge_later
        head :no_content
      end

      private

      def file_params
        params.require(:file)
      end

      def attachment_valid?
        AttachmentParamValidator.new(current_user, :avatar, file_params).valid?
      end
    end
  end
end
