module Api
  module V1
    class AuthTokensController < ApplicationController
      skip_before_action :authenticate_user, only: :create

      def create
        if create_auth_token.success?
          render json: create_auth_token.auth_token, status: :created
        else
          render json: { error: create_auth_token.error }, status: :unprocessable_entity
        end
      end

      def destroy
        current_user.auth_token.destroy

        Devices::ClearPushTokenFromChats.call(device: current_user.device, log_out_process: true)

        head :no_content
      end

      private

      def create_auth_token
        @create_auth_token ||= Verifications::Perform
                               .call(
                                 verification_code: verification_code,
                                 provided_code: params[:verification_code]
                               )
      end

      def verification_code
        @verification_code ||= VerificationCode.find_by(phone_number: params[:phone_number])
      end
    end
  end
end
