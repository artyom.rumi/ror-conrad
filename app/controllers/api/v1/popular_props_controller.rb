module Api
  module V1
    class PopularPropsController < ApplicationController
      expose :props, :gender_relevant_props

      def index
        render json: props.order(popularity_index: :desc).limit(limit)
      end

      private

      def user
        @user ||= User.find_by(id: params[:user_id])
      end

      def gender_relevant_props
        if user.present?
          Prop.where(gender: [user.gender, nil])
        else
          Prop.all
        end
      end

      def limit
        return unless params[:limit]

        params[:limit].to_i
      end
    end
  end
end
