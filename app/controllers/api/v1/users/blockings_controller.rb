module Api
  module V1
    module Users
      class BlockingsController < ApplicationController
        expose :user
        expose :blockings, -> { current_user.blockings.for_user }
        expose :blocking, scope: -> { blockings }, id: :user_id, find_by: :blockable_id

        def create
          create_blocking = Blockings::Block.call(user: current_user, blockable: user)

          if create_blocking.success?
            render json: create_blocking.blocking, serializer: BlockingSerializer
          else
            render json: { error: create_blocking.error }, status: :unprocessable_entity
          end
        end

        def destroy
          Blockings::Unblock.call(blocking: blocking)
          head :no_content
        end
      end
    end
  end
end
