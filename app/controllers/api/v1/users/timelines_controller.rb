module Api
  module V1
    module Users
      class TimelinesController < ApplicationController
        expose :feeds, :fetch_feeds
        expose :user

        def index
          render json: feeds.with_attached_image, each_serializer: FeedSerializer
        end

        private

        def fetch_feeds
          Feed.includes(:user)
              .where(incognito: false)
              .where(user_id: user.id)
              .where("created_at > ?", 7.days.ago)
              .order(created_at: :desc)
              .page(page).per(per_page)
        end
      end
    end
  end
end
