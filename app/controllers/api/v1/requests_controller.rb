module Api
  module V1
    class RequestsController < ApplicationController
      def create
        PropsRequests::SendToSubscribers.call(requester: current_user)
        head :no_content
      end
    end
  end
end
