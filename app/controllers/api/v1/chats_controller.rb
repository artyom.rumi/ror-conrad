module Api
  module V1
    class ChatsController < ApplicationController
      expose :chat
      expose :chats, :fetch_chats

      def index
        render json: chats
      end

      def show
        render json: chat
      end

      def create
        result = ::Chats::Create.call(chat_params)

        if result.success?
          render json: result.chat, status: :created
        else
          render json: { error: result.error }, status: :unprocessable_entity
        end
      end

      private

      def chat_params
        {
          members_ids: user_ids,
          owner: current_user,
          incognito: false,
          is_group: true,
          name: params["name"]
        }
      end

      def user_ids
        params[:user_ids] | [current_user.id]
      end

      def fetch_chats
        current_user.chats.includes(:deanonymizations, owner: :school, users: :school).order(updated_at: :desc)
      end
    end
  end
end
