module Api
  module V1
    class PropDeliveriesController < ApplicationController
      expose :prop_delivery

      def create
        if deliver_prop.success?
          render json: deliver_prop.prop_delivery, status: :created
        else
          render json: { error: deliver_prop.error }, status: :unprocessable_entity
        end
      end

      private

      def deliver_prop
        @deliver_prop ||= ::PropDeliveries::Deliver.call(prop_delivery_params: prop_delivery_params)
      end

      def prop_delivery_params
        params.permit(:recipient_id, :prop_id, :boost_type)
      end
    end
  end
end
