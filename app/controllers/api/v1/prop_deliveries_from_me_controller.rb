module Api
  module V1
    class PropDeliveriesFromMeController < ApplicationController
      def index
        render json: fetch_props, each_serializer: SentPropsSerializer
      end

      def fetch_props
        current_user.outcoming_prop_deliveries.where(recipient_id: params[:user_id]).includes(:prop)
      end
    end
  end
end
