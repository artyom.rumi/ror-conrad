module Api
  module V1
    class FolloweesController < ApplicationController
      expose :followees, :fetch_followees

      def index
        render json: followees.with_attached_avatar, each_serializer: SchoolmateSerializer
      end

      private

      def fetch_followees
        ::Users::SearchQuery
          .new(keywords, current_user.subscribed_to_users)
          .all
          .order(first_name: :asc, last_name: :asc)
          .includes(:school, :incoming_subscriptions)
          .page(page)
          .per(per_page)
      end

      def keywords
        params[:keywords]
      end
    end
  end
end
