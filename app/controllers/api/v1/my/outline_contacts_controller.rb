module Api
  module V1
    module My
      class OutlineContactsController < ApplicationController
        expose :contacts, :fetch_contacts

        def index
          render  json: paginate_contacts,
                  adapter: :json,
                  each_serializer: OutlineContactsSerializer,
                  meta: { users_count: contacts.count },
                  root: :users
        end

        private

        def fetch_contacts
          current_user
            .outline_contact_phone
            .order(:name)
        end

        def paginate_contacts
          contacts.page(page).per(per_page)
        end
      end
    end
  end
end
