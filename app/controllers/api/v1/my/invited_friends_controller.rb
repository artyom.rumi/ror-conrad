module Api
  module V1
    module My
      class InvitedFriendsController < ApplicationController
        expose :friends, :fetch_invited_friends

        def index
          render  json: paginate_friends.with_attached_avatar,
                  adapter: :json,
                  each_serializer: SchoolmateSerializer,
                  meta: { users_count: friends.count },
                  root: :users
        end

        private

        def fetch_invited_friends
          User
            .where("id IN (?)", current_user.friend_intentions.pluck(:target_id) - current_user.friend_ids)
            .order(first_name: :asc, last_name: :asc)
            .includes(:school, :incoming_subscriptions, :friend_intentions)
        end

        def paginate_friends
          friends.page(page).per(per_page)
        end
      end
    end
  end
end
