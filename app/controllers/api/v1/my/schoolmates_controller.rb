module Api
  module V1
    module My
      class SchoolmatesController < ApplicationController
        expose :schoolmates, :fetch_schoolmates

        def index
          render  json: paginate_schoolmates.with_attached_avatar,
                  adapter: :json,
                  each_serializer: SchoolmateSerializer,
                  meta: { users_count: schoolmates.count },
                  root: :users
        end

        private

        def fetch_schoolmates
          current_user
            .schoolmates
            .includes(:school)
            .order(first_name: :asc, last_name: :asc)
        end

        def paginate_schoolmates
          schoolmates.page(page).per(per_page)
        end
      end
    end
  end
end
