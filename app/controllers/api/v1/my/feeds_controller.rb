module Api
  module V1
    module My
      class FeedsController < ApplicationController
        expose :feed, parent: :current_user
        expose :feeds, :fetch_feeds

        def index
          render json: feeds.with_attached_image, each_serializer: FeedSerializer, include: "chat.**"
        end

        def create
          if (feed.link.present? || attachment_valid?) && feed.save
            render json: feed, status: :created, serializer: FeedSerializer
          else
            render json: feed.errors.messages, status: :unprocessable_entity
          end
        end

        def update
          if feed.update(feed_update_params)
            render json: feed, serializer: FeedSerializer
          else
            render json: feed.errors.messages, status: :unprocessable_entity
          end
        end

        def destroy
          feed.destroy
          render json: feeds.where(incognito: feed.incognito, user: current_user), each_serializer: FeedSerializer
        end

        private

        def feed_params
          params.permit(:image, :text, :link, :incognito, :ispublic).merge(user_id: current_user.id)
        end

        def feed_update_params
          params.permit(:incognito)
        end

        def image_params
          params.require(:image)
        end

        def attachment_valid?
          AttachmentParamValidator.new(feed, :image, image_params).valid?
        end

        def fetch_feeds
          users_feed_ids = current_user.friend_ids << current_user.id

          puts("----- this is the friends user ids lists ---- : #{users_feed_ids}")

          puts("----- this is the current user id ----- : #{current_user.id}")

          users_feed_ids.delete(current_user.id)

          puts("----- this is the friends user ids lists ---- : #{users_feed_ids}")

          friend_feeds = Feed
            .includes(:users, :comments, :user, :chats, chats: :users)
            .where("user_id IN (?) AND created_at > ?", users_feed_ids, 7.days.ago)
            .order(created_at: :desc)
            .page(page)
            .per(per_page)
          
          my_feeds = Feed
            .includes(:users, :comments, :user, :chats, chats: :users)
            .where("user_id IN (?) AND ispublic = ? AND created_at > ?", current_user.id, false, 7.days.ago)
            .order(created_at: :desc)
            .page(page)
            .per(per_page)

          total_feeds = my_feeds

          if my_feeds.length > 0
            total_feeds = my_feeds
            if friend_feeds.length > 0
              total_feeds = total_feeds + friend_feeds
            end
          else
            total_feeds = friend_feeds 
          end
          total_feeds
        end
      end
    end
  end
end
