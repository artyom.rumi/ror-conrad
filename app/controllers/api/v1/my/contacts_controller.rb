module Api
  module V1
    module My
      class ContactsController < ApplicationController
        def create
          if synchronize_user_contacts.success?
            render json: synchronize_user_contacts.contacts, status: :ok
          else
            render json: { error: synchronize_user_contacts.error }, status: :unprocessable_entity
          end
        end

        private

        def synchronize_user_contacts
          @synchronize_user_contacts ||=
            ::Users::SynchronizeContacts.call(phone_numbers: contacts_params[:phone_numbers])
        end

        def contacts_params
          params.permit(phone_numbers: %i[name phone_number])
        end
      end
    end
  end
end
