module Api
  module V1
    module My
      class RecentlyUsedPropsController < ApplicationController
        def index
          render json: props.limit(limit)
        end

        private

        def props
          @props ||= Props::RecentlyUsedQuery.new(current_user).all
        end

        def limit
          return unless params[:limit]

          params[:limit].to_i
        end
      end
    end
  end
end
