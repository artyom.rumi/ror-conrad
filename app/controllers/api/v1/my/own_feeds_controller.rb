module Api
  module V1
    module My
      class OwnFeedsController < ApplicationController
        expose :feeds, :fetch_feeds

        def index
          render json: feeds.with_attached_image, each_serializer: FeedSerializer
        end

        private

        def fetch_feeds
          Feed
            .includes(:users, :comments, :user, :chats, chats: :users)
            .where(feed_params)
            .where("created_at > ?", 7.days.ago)
            .order(created_at: :desc)
            .page(page).per(per_page)
        end

        def feed_params
          params.permit.merge(user_id: current_user.id, incognito: incognito?)
        end

        def incognito?
          params[:incognito] || false
        end
      end
    end
  end
end
