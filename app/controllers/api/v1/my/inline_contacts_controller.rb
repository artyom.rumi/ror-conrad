module Api
  module V1
    module My
      class InlineContactsController < ApplicationController
        expose :contacts, :fetch_contacts

        def index
          render  json: paginate_contacts.with_attached_avatar,
                  adapter: :json,
                  each_serializer: SchoolmateSerializer,
                  meta: { users_count: contacts.count },
                  root: :users
        end

        private

        def fetch_contacts
          current_user
            .inline_contact_users
            .includes(:school)
            .order(first_name: :asc, last_name: :asc)
        end

        def paginate_contacts
          contacts.page(page).per(per_page)
        end
      end
    end
  end
end
