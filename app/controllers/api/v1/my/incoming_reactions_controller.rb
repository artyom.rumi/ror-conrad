module Api
  module V1
    module My
      class IncomingReactionsController < ApplicationController
        def index
          render json: prop_deliveries_with_reactions, each_serializer: PropDeliverySenderSerializer
        end

        private

        def prop_deliveries_with_reactions
          PropDelivery.joins(:reaction).where(sender: current_user)
        end
      end
    end
  end
end
