module Api
  module V1
    module My
      class BlockedUsersController < ApplicationController
        expose :blocked_users, :fetch_blocked_users

        def index
          render json: blocked_users.with_attached_avatar, each_serializer: SchoolmateSerializer
        end

        private

        def fetch_blocked_users
          ids = current_user.blockings.for_user.pluck(:blockable_id)
          User.where(id: ids).includes(:school)
        end
      end
    end
  end
end
