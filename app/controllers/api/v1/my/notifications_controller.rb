module Api
  module V1
    module My
      class NotificationsController < ApplicationController
        def index
          result = ::Notifications::All.call(user: current_user)
          render json: result.notifications
        end

        def update
          result = Notifications::MarkAsRead.call(user: current_user, notification_ids: notification_ids)
          if result.success?
            render json: result.notifications, status: :ok
          else
            render json: result.error, status: :unprocessable_entity
          end
        end

        private

        def notification_ids
          params[:id] || params[:ids]
        end
      end
    end
  end
end
