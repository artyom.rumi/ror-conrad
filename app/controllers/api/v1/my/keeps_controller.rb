module Api
  module V1
    module My
      class KeepsController < ApplicationController
        def create
          friendship = Friendship.between_users(current_user, keep_recipient)

          result = KeepFriendship.call(friendship: friendship, keep_sender: current_user)

          if result.success?
            render json: keep_recipient, serializer: SchoolmateFullSerializer
          else
            render json: result.errors, status: :unprocessable_entity
          end
        end

        private

        def keep_recipient
          User.find(params[:user_id])
        end
      end
    end
  end
end
