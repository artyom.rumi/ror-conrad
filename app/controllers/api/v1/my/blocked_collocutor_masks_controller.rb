module Api
  module V1
    module My
      class BlockedCollocutorMasksController < ApplicationController
        expose :blocked_masks, :fetch_blocked_masks

        def index
          render json: blocked_masks.with_attached_avatar, each_serializer: CollocutorMaskSerializer
        end

        private

        def fetch_blocked_masks
          ids = current_user.blockings.for_collocutor_mask.pluck(:blockable_id)
          CollocutorMask.where(id: ids)
        end
      end
    end
  end
end
