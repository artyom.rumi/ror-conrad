module Api
  module V1
    module My
      class ExpiredFriendsController < ApplicationController
        expose :friends, :fetch_expired_friends

        def index
          render  json: paginate_friends.with_attached_avatar,
                  adapter: :json,
                  each_serializer: FriendSerializer,
                  meta: { users_count: friends.count },
                  root: :users
        end

        private

        def fetch_expired_friends
          ::Users::FriendsQuery
            .new(current_user, Friendship.expired).all
            .order(first_name: :asc, last_name: :asc)
            .includes(:school, :incoming_subscriptions, :friend_intentions)
        end

        def paginate_friends
          friends.page(page).per(per_page)
        end
      end
    end
  end
end
