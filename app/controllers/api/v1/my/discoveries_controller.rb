module Api
  module V1
    module My
      class DiscoveriesController < ApplicationController
        expose :feeds, :fetch_feeds

        def index
          render json: feeds.with_attached_image, each_serializer: FeedSerializer, include: "chat.**"
        end

        private

        def fetch_feeds
          users_feed_ids = ::Users::DiscoveriesQuery.new(current_user).all.ids

          users_friend_ids = current_user.friend_ids << current_user.id
          users_friend_ids.delete(current_user.id)

          # .where("user_id IN (?) AND created_at > ?", users_feed_ids, 7.days.ago)

          puts("----- this is the users discoveries ids ----- : #{users_feed_ids}")
          puts("----- this is the users friend ids ----- : #{users_friend_ids}")

          public_feeds = []

          if users_friend_ids.length > 0
            public_feeds = Feed
              .includes(:users, :comments, :user, :chats, :rates, chats: :users)
              .where("user_id NOT IN (?) And ispublic = ? AND created_at > ?", users_friend_ids, true, 7.days.ago)
              .order(created_at: :desc)
              .page(page)
              .per(per_page)
          else
            public_feeds = Feed
              .includes(:users, :comments, :user, :chats, :rates, chats: :users)
              .where("ispublic = ? AND created_at > ?", true, 7.days.ago)
              .order(created_at: :desc)
              .page(page)
              .per(per_page)
          end
          puts("0000000000000000000000000   this is the discoveres feeds fetch 00000000000000000000000000000000000000000 : #{public_feeds}")
          puts(public_feeds)

          public_feeds

        end
      end
    end
  end
end
