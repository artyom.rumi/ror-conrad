class NotifyExpiredFriendshipJob < ApplicationJob
  def perform
    friendships.each do |expired_friendship|
      Notifications::ExpiredFriendship.call(expired_friendship: expired_friendship)
    end
  end

  private

  def friendships
    expired_friendships = Friendship.where("expires_at < ?", Time.current)
    notifying_ids = Notification.where(notifiable_type: "Friendship", action_name: "expired_friendship")
                                .pluck(:notifiable_id)

    expired_friendships.where.not(id: notifying_ids)
  end
end
