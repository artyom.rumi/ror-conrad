class RemoveOutdatedNotificationsJob < ApplicationJob
  def perform
    Notification.outdated.destroy_all
  end
end
