class NotifyExpiringFriendshipJob < ApplicationJob
  def perform
    Friendship.expiring.each do |expiring_friendship|
      Notifications::ExpiringFriendship.call(expiring_friendship: expiring_friendship)
    end
  end
end
