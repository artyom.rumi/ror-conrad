class CreateRequestJob < ApplicationJob
  def perform(requester, receiver)
    PropsRequests::Create.call(requester: requester, receiver: receiver)
  end
end
