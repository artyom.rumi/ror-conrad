module PropDeliveries
  class DeliverCustom
    include Interactor::Organizer

    organize PrepareCustomParams,
      Create,
      CreateOrUpdateFriendship,
      BuildNotifications,
      SendPushNotifications
  end
end
