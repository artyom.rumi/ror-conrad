module PropDeliveries
  class CreateOrUpdateFriendship
    include Interactor

    delegate :prop_delivery, to: :context
    delegate :sender, :recipient, to: :prop_delivery

    def call
      return unless props_exists?

      context.friendship = Friendship.between_users(sender, recipient)
      update_friendship(context.friendship) if context.friendship
      return if context.friendship

      context.friendship = create_friendship
    end

    private

    def update_friendship(friendship)
      ::KeepFriendship.call(friendship: friendship, keep_sender: sender)
    end

    def create_friendship
      Friendship.create(first_user: sender, second_user: recipient, expires_at: expired_time)

      context.friendship_created = true
    end

    def expired_time
      @expired_time ||= Time.current + Friendship::EXPIRES_AT_DAYS
    end

    def props_exists?
      PropDelivery
        .where("created_at > ?", Time.current - Friendship::EXPIRES_AT_DAYS)
        .exists?(sender: recipient, recipient: sender)
    end
  end
end
