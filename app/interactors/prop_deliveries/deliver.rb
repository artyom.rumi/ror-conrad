module PropDeliveries
  class Deliver
    include Interactor::Organizer

    organize PrepareParams,
      Create,
      BuildNotifications,
      SendPushNotifications,
      CreateOrUpdateFriendship
  end
end
