module PropDeliveries
  class PrepareParams
    include Interactor

    delegate :prop_delivery_params, to: :context

    def call
      context.fail!(errors: "Related prop does not exist!") if prop.blank?

      prop_delivery_params.merge!(content: prop.content, emoji: prop.emoji)
    end

    private

    def prop
      @prop ||= Prop.find_by(id: prop_delivery_params[:prop_id])
    end
  end
end
