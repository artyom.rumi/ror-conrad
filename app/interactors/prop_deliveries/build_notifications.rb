module PropDeliveries
  class BuildNotifications
    include Interactor

    delegate :prop_delivery, to: :context
    delegate :device_push_token, to: :receiver, prefix: true
    delegate :sender_gender, to: :prop_delivery

    NOTIFICATION_SETTINGS_MAP = {
      "new_friend": "new_friends",
      "received_prop": "receiving_prop"
    }.freeze

    NOTIFICATION_OBJECT_MAP = {
      "new_friend": "friendship_created",
      "received_prop": "prop_delivery"
    }.freeze

    NOTIFICATION_GENDER_MAP = {
      "male": "boy",
      "female": "girl",
      "non_binary": "non_binary"
    }.freeze

    def call
      context.notifications = if receiver_device_push_token.blank?
        []
      else
        build_notifications
      end
    end

    private

    def receiver
      prop_delivery.recipient
    end

    def build_notifications
      NOTIFICATION_SETTINGS_MAP
        .select { |event_type, setting| need_to_push_event?(event_type, setting) }
        .map { |event_type, _| notification(event_type) }
    end

    def need_to_push_event?(event_type, setting)
      object_to_push?(event_object(event_type)) && receiver.public_send("#{setting}_notification_enabled?")
    end

    def event_object(event_type)
      NOTIFICATION_OBJECT_MAP[event_type]
    end

    def object_to_push?(event_type)
      context.public_send(event_type).present?
    end

    def notification(event_type)
      PushNotification.new(
        receiver_device_push_token: receiver_device_push_token,
        category: event_type,
        prop_sender_gender: NOTIFICATION_GENDER_MAP[sender_gender.to_sym]
      ).apnotic_notification.tap { |n| n.alert = notification_alert(event_type) }
    end

    def notification_alert(event_type)
      case event_type
      when :new_friend
        { body: "You are now friends with #{prop_delivery.sender.full_name}. See posts from them and their friends!" }
      when :received_prop
        { body: "#{gender_message} just passed you a note!" }
      end
    end

    def gender_message
      case sender_gender
      when "male"
        "A boy"
      when "female"
        "A girl"
      when "non_binary"
        "Someone"
      end
    end
  end
end
