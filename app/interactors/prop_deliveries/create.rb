module PropDeliveries
  class Create
    include Interactor

    delegate :prop_delivery_params, to: :context
    delegate :user, to: Current

    def call
      context.fail!(errors: prop_delivery.errors.messages) unless prop_delivery.valid?

      context.prop_delivery = prop_delivery
      context.request = prop_delivery.request
    end

    private

    def prop_delivery
      @prop_delivery ||= PropDelivery.create(prepared_params)
    end

    def prepared_params
      prop_delivery_params.merge(sender_id: user.id, sender_gender: user.gender)
    end
  end
end
