module PropDeliveries
  class PrepareCustomParams
    include Interactor

    delegate :prop_delivery_params, to: :context

    def call
      prop_delivery_params.merge!(emoji: generated_emoji)
    end

    private

    def generated_emoji
      props_seeds["emojis"].sample
    end

    def props_seeds
      YAML.load_file(Rails.root.join("db", "seeds", "props.yml"))
    end
  end
end
