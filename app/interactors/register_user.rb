class RegisterUser
  include Interactor::Organizer

  organize Registration::CreateUser,
    Registration::CreateSubscriptions,
    Registration::BuildNotifications,
    SendPushNotifications
end
