class CreateInvitation
  include Interactor

  SMS_BODY =
    "Hi, you were asked to join Friendsta by %<sender_name>s from %<school_name>s. %<link>s " \
    "Reply STOP to block.".freeze

  LINK = ENV["INVITATION_LINK"]

  ERROR_MESSAGE = "We could not deliver sms messages to: %<phone_numbers>s".freeze

  delegate :contacts, :error_phone_numbers, to: :context
  delegate :user, to: Current
  delegate :school, to: :user

  def call
    context.fail!(error: "contacts are empty") if contacts.blank?
    context.error_phone_numbers = []

    contacts.each do |contact|
      send_message(contact)
    end

    context.fail!(error: error_message) if error_phone_numbers.any?
  end

  private

  def send_message(contact)
    twilio.send_sms(contact[:phone_number], sms_text(contact[:name]))
    add_or_update_contact(contact)
  rescue TwilioAdapter::SendSmsError
    error_phone_numbers << contact[:phone_number]
  end

  def sms_text(receiver_name)
    format(
      SMS_BODY,
      receiver_name: capitalized_name(receiver_name),
      sender_name: user.full_name,
      school_name: school.name,
      link: LINK
    )
  end

  def capitalized_name(name)
    name.split.first.capitalize
  end

  def twilio
    @twilio ||= TwilioAdapter.new
  end

  def error_message
    format(ERROR_MESSAGE, phone_numbers: error_phone_numbers)
  end

  def add_or_update_contact(contact)
    old_contact = Contact.find_by(phone_number: contact[:phone_number])
    if old_contact
      update_contact(old_contact)
    else
      create_contact(contact)
    end
  end

  def update_contact(contact)
    contact.update(invited: true)
  end

  def create_contact(contact)
    user.contacts.create(name: contact[:name], phone_number: contact[:phone_number].gsub(/[^\d]/, ""))
  end
end
