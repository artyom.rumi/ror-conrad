module RepostedFeeds
  class Create
    include Interactor::Organizer

    organize Save, BuildActivityPushNotification, SendPushNotifications
  end
end
