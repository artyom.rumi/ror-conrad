module RepostedFeeds
  class Save
    include Interactor

    delegate :user, :source_feed, to: :context

    before do
      @reposted_feed = Repost.new(reposted_feed_params)
    end

    def call
      context.fail!(error: @reposted_feed.errors.messages) unless @reposted_feed.valid?

      save_reposted_feed!
      context.activity = @reposted_feed
      context.activity_type = "repost"
    end

    after do
      unless source_feed.user == user
        Notification.create(user: source_feed.user, notifiable: @reposted_feed, action_name: "new_repost")
      end
    end

    private

    def reposted_feed_params
      {
        text: source_feed.text,
        link: source_feed.link,
        source_feed_id: source_feed.id,
        user_id: user.id
      }
    end

    def save_reposted_feed!
      attach_image if source_feed.image.attached?
      @reposted_feed.save!
    end

    def attach_image
      @reposted_feed.image.attach(source_image)
    end

    def source_image
      source_feed.image.attachment.blob
    end
  end
end
