module FriendIntentions
  class Save
    include Interactor

    delegate :user, :target, to: :context

    def call
      context.fail!(error: friend_intention.errors.full_messages) unless friend_intention.valid?

      context.friend_intention = friend_intention if friend_intention.save
    end

    private

    def friend_intention
      @friend_intention ||= FriendIntention.new(user: user, target: target)
    end
  end
end
