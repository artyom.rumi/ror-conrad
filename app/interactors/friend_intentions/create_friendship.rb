module FriendIntentions
  class CreateFriendship
    include Interactor

    delegate :user, :target, to: :context

    def call
      context.fail!(error: "Friendship already exists") if friends?
      context.action_type = "friend"
      create_friendship if invites_between_users?
    end

    after do
      Notification.create(user: user, notifiable: context.friendship, action_name: "new_friend")
      Notification.create(user: target, notifiable: context.friendship, action_name: "new_friend")
    end

    private

    def friends?
      Friendship.between_users(user, target)
    end

    def invites_between_users?
      FriendIntention.exists?(user_id: user.id, target_id: target.id) &&
        FriendIntention.exists?(user_id: target.id, target_id: user.id)
    end

    def create_friendship
      context.friendship =
        Friendship.create(first_user: user, second_user: target, expires_at: Time.current + Friendship::EXPIRES_AT_DAYS)
    end
  end
end
