module FriendIntentions
  class Create
    include Interactor::Organizer

    organize Save, CreateFriendship, BuildFriendshipPushNotification, SendFriendshipPushNotifications
  end
end
