module Comments
  class Save
    include Interactor

    delegate :comment_params, to: :context

    def call
      context.fail!(error: comment.errors.messages) unless comment.valid?
      context.activity = comment
      context.activity_type = "comment"
    end

    after do
      Notification.create(user: feed.user, notifiable: comment, action_name: "new_comment") unless feed.user == user
    end

    private

    def comment
      @comment ||= Comment.create(comment_params)
    end

    def feed
      @feed ||= Feed.find(comment_params[:feed_id])
    end

    def user
      @user ||= User.find(comment_params[:user_id])
    end
  end
end
