module Chats
  class Save
    include Interactor

    delegate :members_ids, :owner, :incognito, :feed, :comment, :name, :is_group, to: :context

    def call
      context.fail!(error: chat.errors.full_messages) unless chat.valid?

      context.chat = chat
    end

    def rollback
      chat.destroy
    end

    private

    def chat
      @chat ||= Chat.create(users: members, owner: owner, incognito: incognito, feed: feed, comment: comment,
                            name: name, is_group: is_group)
    end

    def members
      @members ||= User.where(id: members_ids).includes(:school, :device, :gender_masks_counter)
      # we need this weird preloading to avoid N+1
      # on propagating associations validations chat -> users -> schools
    end
  end
end
