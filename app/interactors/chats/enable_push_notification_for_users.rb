module Chats
  class EnablePushNotificationForUsers
    include Interactor

    delegate :chat, to: :context
    delegate :users, to: :chat, prefix: true

    def call
      chat_users.each { |user| add_device_push_token_to_chat(user.device) }
    end

    def rollback
      chat_users.each { |user| remove_device_push_token_from_chat(user.device) }
    end

    private

    def add_device_push_token_to_chat(device)
      return unless device

      pubnub_adapter.add_push_notification_registrations(device.push_token, chat.pubnub_channel_name)
    end

    def remove_device_push_token_from_chat(device)
      return unless device

      pubnub_adapter.remove_push_notification_registration(device.push_token, chat.pubnub_channel_name)
    end

    def pubnub_adapter
      @pubnub_adapter ||= PubNubAdapter.new
    end
  end
end
