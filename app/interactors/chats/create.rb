module Chats
  class Create
    include Interactor::Organizer

    organize Save,
      CreateMasks,
      SendIntroMessageToIncognitoChat,
      EnablePushNotificationForUsers
  end
end
