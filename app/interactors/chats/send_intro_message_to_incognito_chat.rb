module Chats
  class SendIntroMessageToIncognitoChat
    include Rails.application.routes.url_helpers
    include Interactor

    delegate :feed, :comment, :chat, to: :context
    delegate :pubnub_channel_name, :id, to: :chat, prefix: true

    def call
      return if feed.blank? && comment.blank?

      PubNubAdapter.new.send_message_to_channel(chat_pubnub_channel_name, message)
    end

    private

    def message
      message_params = comment.present? ? comment_message_params : feed_message_params

      ChannelTextMessage.new(message_params).to_hash
    end

    def feed_message_params
      {
        chat_id: chat_id,
        text: feed.text.presence,
        sender_id: feed.user.id,
        image_urls: image_urls,
        link: feed.link.presence,
        receiver_id: nil
      }
    end

    def comment_message_params
      feed_message_params.merge(comment: comment.text)
    end

    def image_urls
      image_urls_data(feed.image) if feed.image.attached?
    end

    def image_urls_data(image)
      {
        photo_url: rails_blob_url(image),
        small_photo_url: rails_representation_url(image.variant(resize: "150x150")),
        medium_photo_url: rails_representation_url(image.variant(resize: "240x240")),
        large_photo_url: rails_representation_url(image.variant(resize: "480x480"))
      }
    end
  end
end
