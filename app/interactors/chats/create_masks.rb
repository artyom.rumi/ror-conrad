module Chats
  class CreateMasks
    include Interactor

    PSEUDONYM = "Incognito %<gender>s %<number>d".freeze
    GENDER_MAPPINGS = {
      male: "Boy",
      female: "Girl",
      non_binary: "User"
    }.freeze

    delegate :chat, to: :context

    def call
      return unless chat.incognito

      chat.users.each do |user|
        @user = user
        collocutors = chat.users - [user]

        collocutors.each do |collocutor|
          @collocutor = collocutor

          user_gender_masks_counter.increment(collocutor.gender)
          create_collocutor_mask
        end
      end
    end

    private

    attr_reader :user, :collocutor

    def create_collocutor_mask
      CollocutorMask.create!(chat: chat, user: user, collocutor: collocutor, name: pseudonym)
    end

    def pseudonym
      format(PSEUDONYM, gender: GENDER_MAPPINGS[collocutor.gender.to_sym],
                        number: user_gender_mask_counter_by_collucutor_gender)
    end

    def user_gender_mask_counter_by_collucutor_gender
      user_gender_masks_counter.public_send(collocutor.gender)
    end

    def user_gender_masks_counter
      user.gender_masks_counter || user.create_gender_masks_counter
    end
  end
end
