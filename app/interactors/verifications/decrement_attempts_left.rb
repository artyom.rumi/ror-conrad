module Verifications
  class DecrementAttemptsLeft
    include Interactor

    delegate :verification_code, to: :context

    def call
      context.fail!(error: "Verification code is required") if verification_code.blank?

      return if decrement_attempts_count

      destroy_verification_code
      context.fail!(error: "Maxium number of verification code attemts is exceeded")
    end

    def rollback
      # this action should not be rollbacked
    end

    private

    def decrement_attempts_count
      return if verification_code.attempts_count <= 0

      verification_code.update(attempts_count: verification_code.attempts_count - 1)
    end

    def destroy_verification_code
      verification_code.destroy
    end
  end
end
