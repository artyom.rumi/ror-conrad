module Verifications
  class Perform
    include Interactor::Organizer

    organize DecrementAttemptsLeft,
      VerifyCode,
      FindOrCreateDevice,
      CreateAuthToken
  end
end
