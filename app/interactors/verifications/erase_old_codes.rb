module Verifications
  class EraseOldCodes
    include Interactor

    delegate :phone_number, to: :context

    def call
      VerificationCode.where(phone_number: phone_number).destroy_all
    end
  end
end
