module Verifications
  class CreateCode
    include Interactor

    TEST_CODE = "1234".freeze
    ATTEMTS_COUNT = 3

    delegate :phone_number, to: :context

    def call
      context.verification_code = create_verification_code

      return if create_verification_code.valid?

      context.fail!(error: create_verification_code.errors.messages)
    end

    private

    def create_verification_code
      @create_verification_code ||= VerificationCode.create(
        user: user,
        phone_number: phone_number,
        code: code,
        expires_at: 2.hours.since,
        attempts_count: ATTEMTS_COUNT
      )
    end

    def code
      return TEST_CODE if user&.test?

      @code ||=
        ENV["FORCE_PHONE_NUMBER_VERIFICATION_CODE"] || rand(0..9999).to_s.rjust(4, "0")
    end

    def user
      @user ||= User.find_by(phone_number: phone_number)
    end
  end
end
