module Verifications
  class CreateAuthToken
    include Interactor

    delegate :verification_code, :device, to: :context
    delegate :user, to: :verification_code, prefix: true

    def call
      context.fail!(error: "Verification code is required") if verification_code.blank?
      context.fail!(error: auth_token.errors.full_messages) if auth_token.invalid?

      context.auth_token = auth_token
      destroy_verification_code
    end

    private

    def auth_token
      @auth_token ||=
        if verification_code_user.present? && verification_code_user.auth_token.present?
          regenerate_auth_token
        else
          create_auth_token
        end
    end

    def regenerate_auth_token
      @regenerate_auth_token ||= begin
                                   verification_code_user.auth_token.expires_at = 180.days.since
                                   verification_code_user.auth_token.regenerate_value
                                   verification_code_user.auth_token
                                 end
    end

    def create_auth_token
      @create_auth_token ||= AuthToken.create(
        user: verification_code_user,
        expires_at: 180.days.since,
        device: device
      )
    end

    def destroy_verification_code
      verification_code.destroy
    end
  end
end
