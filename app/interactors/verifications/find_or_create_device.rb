module Verifications
  class FindOrCreateDevice
    include Interactor

    delegate :phone_number, to: :context

    def call
      context.fail!(error: device.errors.full_messages) if device.invalid?

      context.device = device
    end

    def rollback
      created_device.destroy
    end

    private

    def device
      @device ||= found_device || created_device
    end

    def found_device
      @found_device ||= Device.find_by(phone_number: phone_number)
    end

    def created_device
      @created_device ||= Device.create(phone_number: phone_number)
    end
  end
end
