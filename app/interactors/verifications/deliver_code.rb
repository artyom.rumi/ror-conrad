module Verifications
  class DeliverCode
    include Interactor::Organizer

    organize EraseOldCodes,
      CreateCode,
      SendCode
  end
end
