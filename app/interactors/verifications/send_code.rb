module Verifications
  class SendCode
    include Interactor

    SMS_BODY = "%<code>s is your Friendsta verification code.".freeze

    delegate :verification_code, to: :context
    delegate :user, to: :verification_code, prefix: true

    def call
      twilio.send_sms(verification_code.phone_number, sms_text) unless verification_code_user&.test?
    rescue TwilioAdapter::SendSmsError => e
      context.fail!(error: e.message)
    end

    private

    def sms_text
      @sms_text ||= format(SMS_BODY, code: verification_code.code)
    end

    def twilio
      @twilio ||= TwilioAdapter.new
    end
  end
end
