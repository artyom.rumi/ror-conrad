module Verifications
  class VerifyCode
    include Interactor

    delegate :verification_code, :provided_code, to: :context

    def call
      context.fail!(error: "Verification code is invalid") unless valid_verification_code?
      context.phone_number = verification_code.phone_number
    end

    private

    def valid_verification_code?
      verification_code.present? &&
        verification_code.valid_code?(provided_code)
    end
  end
end
