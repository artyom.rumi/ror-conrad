module Rates
  class Save
    include Interactor
    delegate :rate_params, to: :context

    def call
      puts("=================== this is the Rates::save ----- call -----part =====================")
      context.fail!(error: rate.errors.messages) unless rate.valid?
      context.activity = rate
      context.activity_type = "rate"
    end

    after do
      puts("=================== this is the Rates::save ----- notification -----part =====================")
      Notification.create(user: feed.user, notifiable: rate, action_name: "new_rate") unless feed.user == user
    end

    private

    def rate
      @rate ||= Rate.create(rate_params)
    end

    def feed
      @feed ||= Feed.find(rate_params[:feed_id])
    end

    def user
      @user ||= User.find(rate_params[:user_id])
    end
  end
end
