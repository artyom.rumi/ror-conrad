module Rates
  class Create
    include Interactor::Organizer

    puts("=================== this is the Rates::create part =====================")

    organize Save, BuildActivityPushNotification, SendPushNotifications
  end
end
