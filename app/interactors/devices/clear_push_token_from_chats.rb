module Devices
  class ClearPushTokenFromChats
    include Interactor

    delegate :device, :log_out_process, :device_params, to: :context
    delegate :push_token, :user, to: :device, prefix: true
    delegate :chats, to: :device_user, prefix: true, allow_nil: true

    def call
      return unless device && device_user_chats.present?

      clear_push_token_from_all_channels
      delete_push_token_from_device
    end

    def rollback
      SetPushTokenToChats.call(device: device)
    end

    private

    def clear_push_token_from_all_channels
      return unless log_out_process || push_token_changed

      PubNubAdapter.new.remove_all_push_notification_registrations(device_push_token)
    end

    def delete_push_token_from_device
      return unless log_out_process

      device.update(push_token: nil)
    end

    def push_token_changed
      device_params[:push_token] && (device.push_token != device_params[:push_token])
    end
  end
end
