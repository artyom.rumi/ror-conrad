module Devices
  class Update
    include Interactor::Organizer

    organize ClearPushTokenFromChats,
      UpdateAttributes,
      SetPushTokenForChats
  end
end
