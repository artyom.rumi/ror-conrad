module Devices
  class UpdateAttributes
    include Interactor

    delegate :device, :device_params, to: :context

    def call
      return unless device

      device.update(device_params) || context.fail!(error: device.errors.full_messages)
    end

    def rollback
      previous_params = {}
      device.previous_changes.each { |k, v| previous_params[k] = v.first }

      device.update(previous_params)
    end
  end
end
