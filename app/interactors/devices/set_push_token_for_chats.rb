module Devices
  class SetPushTokenForChats
    include Interactor

    delegate :device, to: :context
    delegate :user, :push_token, to: :device, prefix: true
    delegate :chats, to: :device_user, prefix: true, allow_nil: true

    def call
      return unless device && device_user_chats.present? && device.push_token_previously_changed?

      PubNubAdapter.new.add_push_notification_registrations(device_push_token, pubnub_channels_names)
    end

    private

    def pubnub_channels_names
      device_user_chats.map(&:pubnub_channel_name).join(",")
    end
  end
end
