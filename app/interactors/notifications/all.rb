module Notifications
  class All
    include Interactor

    delegate :user, to: :context

    def call
      all_notifications = ungrouped_notifications + grouped_notifications

      context.notifications = with_data(all_notifications)
    end

    private

    def ungrouped_notifications
      @ungrouped_notifications ||= Notification.includes(:notifiable, :user).ungrouped.actual.where(user: user)
    end

    def grouped_notifications
      @grouped_notifications ||= Notifications::GroupedFeedActivitiesQuery.new(user).all
    end

    def with_data(notifications)
      notifications.map do |notification|
        NotificationData.new(notification).to_hash
      end
    end
  end
end
