module Notifications
  class ExpiredFriendship
    include Interactor

    delegate :expired_friendship, to: :context

    def call
      ActiveRecord::Base.transaction do
        delete_outdated_notification
        create_expired_notification
      end
    end

    private

    def delete_outdated_notification
      actions = %i[72h_expired_friend 12h_expired_friend]
      Notification.where(notifiable: expired_friendship, action_name: actions).destroy_all
    end

    def create_expired_notification
      Notification.create(user: expired_friendship.first_user, notifiable: expired_friendship,
                          action_name: "expired_friendship")
      Notification.create(user: expired_friendship.second_user, notifiable: expired_friendship,
                          action_name: "expired_friendship")
    end
  end
end
