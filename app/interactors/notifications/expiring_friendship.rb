module Notifications
  class ExpiringFriendship
    include Interactor

    delegate :expiring_friendship, to: :context

    def call
      if expiring?(expiring_friendship, 72)
        create_notifications(expiring_friendship, 72)
      elsif expiring?(expiring_friendship, 12)
        create_notifications(expiring_friendship, 12)
      end
    end

    private

    def expiring?(friendship, remaining_hours)
      friendship.expires_at <= Time.zone.now + remaining_hours.hours && new_expiring?(friendship, remaining_hours)
    end

    def new_expiring?(friendship, remaining_hours)
      !Notification.where(notifiable: friendship, action_name: action(remaining_hours)).exists?
    end

    def action(remaining_hours)
      "#{remaining_hours}h_expired_friend"
    end

    def create_notifications(friendship, remaining_hours)
      ActiveRecord::Base.transaction do
        Notification.create(user: friendship.first_user, notifiable: friendship, action_name: action(remaining_hours))
        Notification.create(user: friendship.second_user, notifiable: friendship, action_name: action(remaining_hours))
      end
    end
  end
end
