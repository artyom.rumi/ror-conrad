module Notifications
  class MarkAsRead
    include Interactor

    delegate :notification_ids, :user, to: :context

    def call
      context.notifications = notifications.each do |notification|
        context.fail!(message: notification.errors.messages) unless notification.update(status: "Read")
      end
    end

    private

    def notifications
      @notifications ||= Notification.includes(:user, :notifiable).where(user_id: user.id, id: notification_ids)
    end
  end
end
