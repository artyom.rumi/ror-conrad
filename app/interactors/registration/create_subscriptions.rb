class Registration::CreateSubscriptions
  include Interactor

  delegate :user, to: :context
  # Limit the scope of this interactor
  delegate :subscription_scope, to: :context
  delegate :phone_numbers, to: :context, allow_nil: true

  def call
    context.subscription_scope ||= %i[schoolmate incoming_contact outgoing_contact]
    context.subscription_limit ||= []
    context.subscriptions = Subscription.create(subscriptions_params)
  end

  private

  def subscriptions_params
    schoolmate_subscriptions_params | (incoming_contacts_subscriptions_params + outcoming_contacts_subscriptions_params)
  end

  # For school mates, subscribe bidirectionally
  def schoolmate_subscriptions_params
    return [] unless subscription_scope.include? :schoolmate

    schoolmates_ids.reduce([]) do |subscriptions_params, schoolmate_id|
      subscriptions_params << { subscriber_id: schoolmate_id, target_id: user.id }
      subscriptions_params << { subscriber_id: user.id, target_id: schoolmate_id }
    end
  end

  # For contacts in current user's phone book, subscribe those contacts
  # outcoming means outgoing here
  def outcoming_contacts_subscriptions_params
    return [] unless subscription_scope.include? :outgoing_contact

    phone_contacts_ids.map do |contact_id|
      { subscriber_id: user.id, target_id: contact_id }
    end
  end

  # If current user is in someone else's phone book, subscribe others to current user
  def incoming_contacts_subscriptions_params
    return [] unless subscription_scope.include? :incoming_contact

    with_user_in_contacts_ids.map do |user_id|
      { subscriber_id: user_id, target_id: user.id }
    end
  end

  def schoolmates_ids
    user.schoolmates.ids
  end

  def phone_contacts_ids
    User.ordinary.where(phone_number: phone_numbers).ids
  end

  def with_user_in_contacts_ids
    User.includes(:contacts).where("contacts.phone_number = ?", user.phone_number).ids
  end
end
