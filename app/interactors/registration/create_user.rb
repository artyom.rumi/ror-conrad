class Registration::CreateUser
  include Interactor

  RANGE_OF_PHOTO_NUMBERS = (1..30).freeze

  delegate :user_params, :auth_token, to: :context
  delegate :device, to: :auth_token, prefix: true

  def call
    context.fail!(error: "You already have a user associated with this phone number") if user_already_registered?

    context.fail!(error: create_user.errors.full_messages) unless register_user.persisted?

    context.user = register_user
  end

  private

  def user_already_registered?
    auth_token.user_id.present? || auth_token.device.user_id.present?
  end

  def register_user
    @register_user ||= begin
      ActiveRecord::Base.transaction do
        create_user
        bind_auth_token_to_user
        bind_device_to_user
      end
      create_user
    end
  end

  def bind_auth_token_to_user
    auth_token.update!(user: create_user)
  end

  def bind_device_to_user
    auth_token_device.update!(user: create_user)
  end

  def create_user
    @create_user ||= User.create(user_params.merge(default_avatar: default_avatar))
  end

  def default_avatar
    @default_avatar ||= "avatars/#{file_name}"
  end

  def file_name
    @file_name ||= "avatar-#{rand(RANGE_OF_PHOTO_NUMBERS)}.png"
  end
end
