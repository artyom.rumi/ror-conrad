module Registration
  class BuildNotifications
    include Interactor
    using AlreadyGraduated

    delegate :user, :subscriptions, to: :context

    def call
      context.notifications = build_notifications.compact
    end

    private

    def build_notifications
      subscriptions
        .select { |subscription| need_to_push_event?(subscription) }
        .map { |subscription| notification(subscription.subscriber) }
    end

    def need_to_push_event?(subscription)
      !subscriber_equals_user?(subscription) && can_send_notification?(subscription.subscriber)
    end

    def subscriber_equals_user?(subscription)
      subscription.subscriber_id == user.id
    end

    def can_send_notification?(receiver)
      receiver.device_push_token.present? && receiver.new_users_notification_enabled?
    end

    def notification(receiver)
      PushNotification.new(
        receiver_device_push_token: receiver.device_push_token,
        category: "new_user"
      ).apnotic_notification.tap { |n| n.alert = { body: "#{user.full_name}#{grade} is now on Friendsta" } }
    end

    def grade
      return "" if user.grade.grade_value > 12

      " from #{user.grade.grade_value.ordinalize} grade"
    end
  end
end
