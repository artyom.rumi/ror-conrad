module Users
  class SendMatchingMessageToIncognitoChat
    include Interactor

    delegate :chat, to: :context
    delegate :pubnub_channel_name, :id, to: :chat, prefix: true

    def call
      PubNubAdapter.new.send_message_to_channel(chat_pubnub_channel_name, matching_message)
    end

    private

    def matching_message
      ChannelServiceMessage.new(
        info_type: "match",
        chat_id: chat_id,
        push_notification_data: push_notification_data
      ).to_hash
    end

    def push_notification_data
      PushNotificationMessage.new(chat_id: chat_id, info_type: "match_message").to_hash
    end
  end
end
