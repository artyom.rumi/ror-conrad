module Users
  class CreateMasks
    include Interactor

    ANONYMOUS_USER_NAME = "Anonymous User".freeze

    delegate :first_user, :second_user, to: :context

    def call
      ActiveRecord::Base.transaction do
        context.fail!(error: create_second_user_mask.errors) unless create_second_user_mask
        context.fail!(error: create_first_user_mask.errors) unless create_first_user_mask
        increment_masks_counters
      end
    end

    def rollback
      create_first_user_mask.destroy
      create_second_user_mask.destroy
      first_user_counter.decrement(second_user.gender).save
      second_user_counter.decrement(first_user.gender).save
    end

    private

    def increment_masks_counters
      first_user_counter.increment(second_user.gender).save
      second_user_counter.increment(first_user.gender).save
    end

    def create_first_user_mask
      @create_first_user_mask ||= CollocutorMask.create(
        user: second_user,
        collocutor: first_user,
        name: ANONYMOUS_USER_NAME
      )
    end

    def create_second_user_mask
      @create_second_user_mask ||= CollocutorMask.create(
        user: first_user,
        collocutor: second_user,
        name: ANONYMOUS_USER_NAME
      )
    end

    def first_user_counter
      @first_user_counter ||= first_user.gender_masks_counter || first_user.create_gender_masks_counter
    end

    def second_user_counter
      @second_user_counter ||= second_user.gender_masks_counter || second_user.create_gender_masks_counter
    end

    def first_user_number
      @first_user_number ||= second_user_counter.public_send(first_user.gender) + 1
    end

    def second_user_number
      @second_user_number ||= first_user_counter.public_send(second_user.gender) + 1
    end
  end
end
