module Users
  class CreateDeanonymization
    include Interactor

    delegate :deanonymization_params, to: :context

    def call
      context.fail!(error: deanonymization.errors.messages) unless deanonymization.valid?
      context.deanonymization = deanonymization
      context.incognito_chat = deanonymization.chat
    end

    def rollback
      deanonymization.destroy
    end

    private

    def deanonymization
      @deanonymization ||= Deanonymization.create(deanonymization_params)
    end
  end
end
