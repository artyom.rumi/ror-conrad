module Users
  class CancelReveal
    include Interactor::Organizer

    organize DestroyDeanonymization,
      SendCancelRevealMessageToIncognitoChat
  end
end
