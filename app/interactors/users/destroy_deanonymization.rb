module Users
  class DestroyDeanonymization
    include Interactor

    delegate :deanonymization, to: :context

    def call
      deanonymization.destroy || context.fail!(error: deanonymization.errors.full_messages)
      context.incognito_chat = deanonymization.chat
    end
  end
end
