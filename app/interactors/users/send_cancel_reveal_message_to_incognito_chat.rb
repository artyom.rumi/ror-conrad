module Users
  class SendCancelRevealMessageToIncognitoChat
    include Interactor

    delegate :incognito_chat, :deanonymization, to: :context
    delegate :pubnub_channel_name, :id, :deanonymizations, :users, to: :incognito_chat, prefix: true
    delegate :user, to: :deanonymization, prefix: true

    after { incognito_chat.update(updated_at: Time.current) }

    def call
      PubNubAdapter.new.send_message_to_channel(incognito_chat_pubnub_channel_name, cancel_reveal_message)
    end

    private

    def cancel_reveal_message
      ChannelServiceMessage.new(cancel_reveal_message_params).to_hash
    end

    def push_notification_data
      PushNotificationMessage.new(push_notification_data_params).to_hash
    end

    def cancel_reveal_message_params
      {
        chat_id: incognito_chat_id,
        push_notification_data: push_notification_data,
        info_type: "cancel_reveal",
        receiver: receiver
      }
    end

    def push_notification_data_params
      {
        chat_id: incognito_chat_id,
        sender: deanonymization_user,
        receiver: receiver,
        info_type: "cancel_reveal"
      }
    end

    def receiver
      @receiver ||= (incognito_chat_users - [deanonymization.user]).first
    end
  end
end
