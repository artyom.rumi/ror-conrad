module Users
  class SynchronizeContacts
    include Interactor

    delegate :user, to: Current
    delegate :phone_numbers, :contacts, to: :context

    before do
      context.fail!(error: "Phone numbers list is empty") if phone_numbers.blank?
    end

    def call
      remove_old_contacts
      context.phone_numbers -= user.contacts.pluck(:phone_number)
      create_or_update_contacts
      subscribe_to_new_contacts
      context.contacts = user.contacts
    end

    private

    def remove_old_contacts
      user.contacts.where.not(phone_number: phone_numbers_without_names).destroy_all
    end

    def create_or_update_contacts
      phone_numbers.map do |contact|
        user_contact = user.contacts.find_or_create_by(phone_number: prepared_phone_number(contact[:phone_number]))
        user_contact.update(name: contact[:name])
      end
    end

    def subscribe_to_new_contacts
      ::Registration::CreateSubscriptions.call(
        user: user,
        subscription_scope: [:outgoing_contact],
        phone_numbers: phone_numbers_without_names
      )
    end

    def phone_numbers_without_names
      phone_numbers.map { |phone_number| phone_number[:phone_number] }
    end

    def prepared_phone_number(phone_number)
      return phone_number if phone_number.match(/\+\d{11}/).present?

      return "+1" << phone_number if ("+1" << phone_number).match(/\+\d{11}/).present?
    end
  end
end
