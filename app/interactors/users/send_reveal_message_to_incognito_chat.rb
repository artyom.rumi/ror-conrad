module Users
  class SendRevealMessageToIncognitoChat
    include Interactor

    delegate :incognito_chat, :deanonymization, to: :context
    delegate :pubnub_channel_name, :id, :deanonymizations, :users, to: :incognito_chat, prefix: true
    delegate :user, to: :deanonymization, prefix: true

    after { incognito_chat.update(updated_at: Time.current) }

    def call
      PubNubAdapter.new.send_message_to_channel(incognito_chat_pubnub_channel_name, reveal_message)
    end

    private

    def reveal_message
      ChannelServiceMessage.new(reveal_message_params).to_hash
    end

    def push_notification_data
      PushNotificationMessage.new(push_notification_data_params).to_hash
    end

    def reveal_message_params
      if both_users_are_deanonymized?
        base_reveal_message_params.merge(mutual_reveal_message_params)
      else
        base_reveal_message_params.merge(one_reveal_message_params)
      end
    end

    def base_reveal_message_params
      {
        chat_id: incognito_chat_id,
        push_notification_data: push_notification_data
      }
    end

    def one_reveal_message_params
      {
        info_type: "one_reveal",
        receiver: receiver
      }
    end

    def mutual_reveal_message_params
      { info_type: "mutual_reveal" }
    end

    def push_notification_data_params
      if both_users_are_deanonymized?
        base_push_notification_params.merge(mutual_reveal_push_notification_params)
      else
        base_push_notification_params.merge(one_reveal_push_notification_params)
      end
    end

    def base_push_notification_params
      { chat_id: incognito_chat_id }
    end

    def one_reveal_push_notification_params
      {
        sender: deanonymization_user,
        receiver: receiver,
        info_type: "one_reveal"
      }
    end

    def mutual_reveal_push_notification_params
      { info_type: "mutual_reveal" }
    end

    def receiver
      @receiver ||= (incognito_chat_users - [deanonymization.user]).first
    end

    def both_users_are_deanonymized?
      incognito_chat_deanonymizations.size == incognito_chat_users.size
    end
  end
end
