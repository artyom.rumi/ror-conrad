module Users
  class Reveal
    include Interactor::Organizer

    organize CreateDeanonymization,
      SendRevealMessageToIncognitoChat
  end
end
