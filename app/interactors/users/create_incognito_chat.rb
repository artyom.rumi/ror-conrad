module Users
  class CreateIncognitoChat
    include Interactor

    delegate :first_user, :second_user, :owner, :is_group, to: :context
    delegate :id, to: :first_user, prefix: true
    delegate :id, to: :second_user, prefix: true

    def call
      context.fail!(error: "Previous incognito chat with same users unrevealed yet") if same_unrevealed_chat_exists?
      context.fail!(error: create_chat.errors) if create_chat.failure?

      context.chat = create_chat.chat
    end

    def rollback
      create_chat.destroy
    end

    private

    def same_unrevealed_chat_exists?
      same_chat&.unrevealed?
    end

    def same_chat
      @same_chat ||= Chats::WithUserIdsQuery.new([first_user_id, second_user_id])
                                            .all.incognito.includes(:users, :deanonymizations).first
    end

    def create_chat
      @create_chat ||= Chats::Create.call(members: [first_user, second_user], owner: owner, incognito: true,
                                          is_group: is_group)
    end
  end
end
