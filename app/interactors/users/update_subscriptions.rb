module Users
  class UpdateSubscriptions
    include Interactor::Organizer

    organize Registration::CreateSubscriptions,
      Registration::BuildNotifications,
      SendPushNotifications
  end
end
