class BuildFriendshipPushNotification
  include Interactor

  delegate :friendship, :action_type, to: :context

  def call
    return unless context.respond_to? :friendship

    context.notifications = notifications.compact
  end

  private

  def notifications
    receivers.map do |receiver|
      next unless can_send_notification?(receiver)

      PushNotification.new(
        receiver_device_push_token: receiver.device_push_token,
        category: category
      ).apnotic_notification.tap { |n| n.alert = notification_alert((receivers - [receiver]).first) }
    end
  end

  def receivers
    [friendship.first_user, friendship.second_user]
  end

  def can_send_notification?(receiver)
    receiver.device_push_token.present? && receiver.public_send("#{category}s_notification_enabled?")
  end

  def category
    "new_#{action_type}"
  end

  def notification_alert(friend)
    case action_type
    when "friend"
      { body: "You are now friends with #{friend.full_name}. See posts from them and their friends!" }
    end
  end
end
