module PropsRequests
  class Save
    include Interactor

    delegate :requester, :receiver, to: :context

    def call
      context.fail!(error: request.errors.messages) unless request.valid?
      context.request = request
    end

    def rollback
      request.destroy
    end

    private

    def request
      @request ||= Request.create(requester: requester, receiver: receiver)
    end
  end
end
