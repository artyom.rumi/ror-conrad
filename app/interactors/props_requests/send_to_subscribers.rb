module PropsRequests
  class SendToSubscribers
    include Interactor

    delegate :requester, to: :context
    delegate :subscribed_by_users, :outcoming_requests, :requests_count, to: :requester, prefix: true

    def call
      return if subscribers_to_send.length.zero?

      ActiveRecord::Base.transaction do
        update_user_request_counter
        send_requests_to_subscribers
      end
    end

    private

    def send_requests_to_subscribers
      subscribers_to_send.each do |receiver|
        CreateRequestJob.perform_later(requester, receiver)
      end
    end

    def update_user_request_counter
      requester.update!(requests_count: requester_requests_count + 1)
    end

    def subscribers_to_send
      @subscribers_to_send ||= Requests::ReceiversQuery.new(requester).all
    end
  end
end
