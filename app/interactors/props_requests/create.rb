module PropsRequests
  class Create
    include Interactor::Organizer

    organize Save,
      GenerateBatchOfUsers
  end
end
