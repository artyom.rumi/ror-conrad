module PropsRequests
  class DeliverProp
    include Interactor::Organizer

    organize PropDeliveries::Deliver,
      IncrementRequestProgress,
      PropDeliveries::CreateOrUpdateFriendship
  end
end
