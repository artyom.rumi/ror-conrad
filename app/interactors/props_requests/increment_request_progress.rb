module PropsRequests
  class IncrementRequestProgress
    include Interactor

    delegate :request, to: :context

    def call
      request.increment_progress || context.fail!(errors: request.errors.messages)
    end
  end
end
