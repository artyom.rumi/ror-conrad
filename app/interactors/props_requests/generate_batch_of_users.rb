module PropsRequests
  class GenerateBatchOfUsers
    include Interactor

    delegate :request, to: :context
    delegate :requester, :receiver, to: :request, prefix: true
    delegate :id, to: :request_requester, prefix: true
    delegate :subscribed_to_users, to: :request_receiver, prefix: true

    def call
      return if request_receiver_subscribed_to_users.count < Request::POLLS_LIMIT

      create_request_respondents || context.fail!
    end

    private

    def create_request_respondents
      random_followees_ids.shuffle.each_with_index do |id, index|
        RequestRespondent.create(respondent_id: id, request: request, index: index)
      end
    end

    def random_followees_ids
      followees_without_requester_ids.shuffle.sample(Request::POLLS_LIMIT - 1) << request_requester_id
    end

    def followees_without_requester_ids
      request_receiver_subscribed_to_users.ids - [request_requester_id]
    end
  end
end
