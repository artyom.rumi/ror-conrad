class KeepFriendship
  include Interactor

  delegate :friendship, :keep_sender, to: :context

  def call
    context.fail!(error: "friendship not found") if friendship.blank?

    keep_friendship
  end

  private

  def keep_friendship
    if keep_sender == friendship.first_user
      update_keep_for_first_user
    elsif keep_sender == friendship.second_user
      update_keep_for_second_user
    end

    friendship
  end

  def update_keep_for_first_user
    if users_without_keeps?
      friendship.update(first_user_keep: true)

    elsif first_user_has_keep?
      ActiveRecord::Base.transaction do
        friendship.update(second_user_keep: false, expires_at: expired_time)
        update_notifications
      end
    end
  end

  def update_keep_for_second_user
    if users_without_keeps?
      friendship.update(second_user_keep: true)

    elsif second_user_has_keep?
      ActiveRecord::Base.transaction do
        friendship.update(first_user_keep: false, expires_at: expired_time)
        update_notifications
      end
    end
  end

  def users_without_keeps?
    !friendship.first_user_keep && !friendship.second_user_keep
  end

  def first_user_has_keep?
    !friendship.first_user_keep && friendship.second_user_keep
  end

  def second_user_has_keep?
    friendship.first_user_keep && !friendship.second_user_keep
  end

  def expired_time
    @expired_time ||= Time.current + Friendship::EXPIRES_AT_DAYS
  end

  def update_notifications
    delete_outdated_notifications
    Notification.create(user: friendship.first_user, notifiable: friendship, action_name: "renew_friendship")
    Notification.create(user: friendship.second_user, notifiable: friendship,
                        action_name: "renew_friendship")
  end

  def delete_outdated_notifications
    actions = %i[72h_expired_friend 12h_expired_friend expired_friendship]
    Notification.where(notifiable: friendship, action_name: actions).destroy_all
  end
end
