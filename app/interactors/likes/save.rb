module Likes
  class Save
    include Interactor

    delegate :like_params, to: :context

    def call
      context.fail!(error: like.errors.messages) unless like.valid?
      context.activity = like
      context.activity_type = "like"
    end

    after do
      Notification.create(user: feed.user, notifiable: like, action_name: "new_like") unless feed.user == user
    end

    private

    def like
      @like ||= Like.create(feed: feed, user: user)
    end

    def feed
      @feed ||= Feed.find(like_params[:feed_id])
    end

    def user
      @user ||= User.find(like_params[:user_id])
    end
  end
end
