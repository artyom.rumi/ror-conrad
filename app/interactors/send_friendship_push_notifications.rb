class SendFriendshipPushNotifications
  include Interactor

  delegate :notifications, to: :context

  def call
    return unless context.respond_to? :notifications

    notifications.each do |notification|
      next if notification.blank?

      Rails.logger.warn(notification.inspect)
      connection.push(notification)
    end

    connection.close
    @connection = nil
    context.friendship
  end

  private

  def connection
    # @connection ||= Apnotic::Connection.development(cert_path: StringIO.new(certificate_data))
    @connection ||= Apnotic::Connection.new(cert_path: StringIO.new(certificate_data))
  end

  def certificate_data
    ENV.fetch("APN_CERTIFICATE_DATA")
  end
end
