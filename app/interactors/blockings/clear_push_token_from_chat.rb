module Blockings
  class ClearPushTokenFromChat
    include Interactor

    delegate :blocking, to: :context
    delegate :user, :blocked_user, to: :blocking, prefix: true

    def call
      return unless blocking_affected_chat

      PubNubAdapter.new.remove_push_notification_registrations(
        blocking_user.device_push_token,
        blocking_affected_chat.pubnub_channel_name
      )
    end

    private

    def blocking_affected_chat
      @blocking_affected_chat ||=
        if blocking.for_user?
          chats_with_user_and_blocked_user.uncovered.first
        else
          chats_with_user_and_blocked_user.incognito.first
        end
    end

    def chats_with_user_and_blocked_user
      @chats_with_user_and_blocked_user ||=
        ::Chats::WithUserIdsQuery.new([blocking_user.id, blocking_blocked_user.id]).all
    end
  end
end
