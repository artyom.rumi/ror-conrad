module Blockings
  class Unblock
    include Interactor::Organizer

    organize Destroy,
      SetPushTokenToChat
  end
end
