module Blockings
  class Destroy
    include Interactor

    delegate :blocking, to: :context

    def call
      blocking.destroy || context.fail!(error: blocking.errors.full_messages)
    end
  end
end
