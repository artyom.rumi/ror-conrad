module Blockings
  class Create
    include Interactor

    delegate :user, :blockable, to: :context

    def call
      context.fail!(error: blocking.errors.full_messages) unless blocking.valid?
      context.blocking = blocking
    end

    def blocking
      @blocking ||= Blocking.create(user: user, blockable: blockable)
    end
  end
end
