module Blockings
  class Block
    include Interactor::Organizer

    organize Create,
      ClearPushTokenFromChat
  end
end
