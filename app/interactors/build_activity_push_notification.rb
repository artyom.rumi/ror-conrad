class BuildActivityPushNotification
  include Interactor

  delegate :activity, :activity_type, to: :context

  def call
    context.notifications = [notification]
  end

  private

  def notification
    return unless can_send_notification?

    PushNotification.new(
      receiver_device_push_token: receiver.device_push_token,
      category: category,
      sender: person_message
    ).apnotic_notification.tap { |n| n.alert = notification_alert }
  end

  def receiver
    activity_type == "repost" ? activity.source_feed.user : activity.feed.user
  end

  def sender
    activity.user
  end

  def can_send_notification?
    receiver.device_push_token.present? && receiver.public_send("#{category}s_notification_enabled?")
  end

  def category
    "new_#{activity_type}"
  end

  def notification_alert
    case activity_type
    when "like"
      { body: "#{person_message} liked your post!" }
    when "comment"
      { body: "#{person_message} commented your post!" }
    when "repost"
      { body: "#{person_message} reposted your post!"  }
    when "rate"
      { body: "#{person_message} rated your post!" }
    end
  end

  def person_message
    if friend?
      "Friend"
    elsif friend_of_your_friend?
      "A friend of your friend"
    else
      "Someone"
    end
  end

  def friend?
    receiver.friends_with?(sender)
  end

  def friend_of_your_friend?
    return false if receiver.friend_ids.blank?

    friends_ids = ::Users::FriendsQuery.new(receiver).all.pluck(:id)
    Friendship.where("first_user_id = :sender_id AND second_user_id IN (:friends_ids) OR
      first_user_id IN (:friends_ids) AND second_user_id = :sender_id",
      friends_ids: friends_ids, sender_id: sender.id).exists?
  end
end
