class Subscription::Destroy
  include Interactor

  delegate :subscription, :friendship, to: :context
  delegate :subscriber, :target, to: :subscription, prefix: true

  def call
    context.friendship = fetch_friendship

    ActiveRecord::Base.transaction do
      context.fail!(error: subscription.errors) unless subscription.destroy
      context.fail!(error: friendship.errors) unless destroy_friendship
    end
  end

  private

  def destroy_friendship
    friendship.nil? || friendship.destroy
  end

  def fetch_friendship
    Friendship.between_users(
      subscription_subscriber,
      subscription_target
    )
  end
end
