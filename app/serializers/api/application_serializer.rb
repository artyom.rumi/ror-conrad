module Api
  class ApplicationSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers

    def view_context
      scope[:view_context]
    end

    def current_user
      scope[:current_user]
    end
  end
end
