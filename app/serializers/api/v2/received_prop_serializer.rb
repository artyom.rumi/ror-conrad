module Api
  module V2
    class ReceivedPropSerializer < ApplicationSerializer
      attributes :id, :created_at, :sender_gender, :viewed_at, :type, :prop

      def prop
        return V1::PropSerializer.new(object.prop) if object.ordinary?

        { content: object.content, emoji: object.emoji }
      end

      has_one :reaction, serializer: V1::ReactionSerializer
    end
  end
end
