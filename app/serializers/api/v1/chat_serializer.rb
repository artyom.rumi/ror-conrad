module Api
  module V1
    class ChatSerializer < ApplicationSerializer
      attributes :id, :pubnub_channel_name, :updated_at, :incognito, :feed_id, :comment_id, :name, :is_group

      belongs_to :owner, serializer: SchoolmateSerializer

      has_many :users, serializer: SchoolmateSerializer do
        object.users.with_attached_avatar
      end

      has_many :deanonymizations
    end
  end
end
