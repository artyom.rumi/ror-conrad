module Api
  module V1
    class PictureSerializer < ApplicationSerializer
      attributes :id, :image, :user_id

      def image
        AttachmentSerializer.new(object)
      end

      def user_id
        object.record_id
      end
    end
  end
end
