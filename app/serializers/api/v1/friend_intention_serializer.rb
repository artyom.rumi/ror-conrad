module Api
  module V1
    class FriendIntentionSerializer < ActiveModel::Serializer
      attributes :id, :target_id
    end
  end
end
