module Api
  module V1
    class PropSerializer < ApplicationSerializer
      attributes :id, :content, :emoji, :gender
    end
  end
end
