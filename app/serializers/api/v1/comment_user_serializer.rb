module Api
  module V1
    class CommentUserSerializer < ApplicationSerializer
      attributes :id, :first_name, :last_name, :grade, :bio, :gender, :blocked, :role, :chat_available,
        :custom_prop_available, :friends_count, :pokes_count, :friendship_status, :school_name

      def top_pokes
        return nil if scope[:current_user] != object && object.hide_top_pokes

        object.top_pokes
      end

      def school_name
        object.school.name
      end

      def friendship_status
        current_user.friendship_status(object)
      end

      def blocked
        current_user.blocked_user? object
      end

      def chat_available
        current_user.friends_with?(object)
      end

      def custom_prop_available
        current_user.friends_with?(object)
      end

      def days_to_expired_friendship
        current_user.days_to_expired_friendship(object) if current_user.friends_with?(object)
      end
    end
  end
end
