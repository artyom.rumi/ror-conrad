module Api
  module V1
    class PropDeliverySerializer < ApplicationSerializer
      attributes :id, :sender_id, :recipient_id, :prop_id, :content, :emoji, :viewed_at, :sender_gender, :type

      belongs_to :request
      has_one :reaction, serializer: ReactionSerializer
    end
  end
end
