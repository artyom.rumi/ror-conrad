module Api
  module V1
    class ReactionSerializer < ApplicationSerializer
      attributes :id, :content, :photo, :read_at, :created_at

      def photo
        AttachmentSerializer.new(object.photo.attachment) if object.photo.attached? && object.read_at.nil?
      end
    end
  end
end
