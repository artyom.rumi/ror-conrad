module Api
  module V1
    class FeedSerializer < ApplicationSerializer
      attributes :id, :image, :user_id, :incognito, :likes_count, :liked, :created_at,
        :unread_comments_count, :text, :link, :count_of_engaged_friends, :comments_count,
        :reposted, :source_feed_id, :rate, :rate_count, :rates

      has_many :chats, key: :chat, serializer: ChatSerializer do |serializer|
        object.user_chat(serializer.current_user)
      end

      def image
        AttachmentSerializer.new(object.image.attachment) if object.image.attached?
      end

      def liked
        object.liked?(current_user.id)
      end

      def reposted
        object.reposted?
      end

      def count_of_engaged_friends
        ::Users::FeedEngagedFriendsQuery.new(current_user, object).count
      end
    end
  end
end
