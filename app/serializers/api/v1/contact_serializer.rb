module Api
  module V1
    class ContactSerializer < ApplicationSerializer
      attributes :id, :user_id, :phone_number, :errors

      def errors
        object.errors.any? ? object.errors.messages : []
      end
    end
  end
end
