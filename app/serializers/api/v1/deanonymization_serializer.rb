module Api
  module V1
    class DeanonymizationSerializer < ApplicationSerializer
      attributes :id, :user_id
    end
  end
end
