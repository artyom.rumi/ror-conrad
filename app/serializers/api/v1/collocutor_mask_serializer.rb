module Api
  module V1
    class CollocutorMaskSerializer < ApplicationSerializer
      attributes :id, :name, :avatar, :default_avatar, :collocutor_id, :blocked, :chat_id

      has_one :avatar, serializer: AttachmentSerializer do
        object.avatar.attachment
      end

      def default_avatar
        {
          original: view_context.image_url("avatars/avatar-incognito.png"),
          small: view_context.image_url("small-avatars/avatar-incognito.png"),
          medium: view_context.image_url("medium-avatars/avatar-incognito.png"),
          large: view_context.image_url("large-avatars/avatar-incognito.png")
        }
      end

      def blocked
        current_user.blockings.where(blockable: object).any?
      end
    end
  end
end
