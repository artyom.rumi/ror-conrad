module Api
  module V1
    class BlockingSerializer < ApplicationSerializer
      attributes :id, :user_id, :blockable_type, :blockable_id, :created_at
    end
  end
end
