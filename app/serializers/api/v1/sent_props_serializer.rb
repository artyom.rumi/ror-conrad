module Api
  module V1
    class SentPropsSerializer < ApplicationSerializer
      attributes :created_at

      belongs_to :prop
      belongs_to :recipient, serializer: SchoolmateSerializer
    end
  end
end
