module Api
  module V1
    class SchoolSerializer < ApplicationSerializer
      attributes :id, :name, :members_amount, :address
    end
  end
end
