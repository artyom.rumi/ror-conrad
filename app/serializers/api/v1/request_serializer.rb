module Api
  module V1
    class RequestSerializer < ApplicationSerializer
      attributes :id, :progress, :respondents_amount

      def respondents_amount
        Request::POLLS_LIMIT
      end
    end
  end
end
