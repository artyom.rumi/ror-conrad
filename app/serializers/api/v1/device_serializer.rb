module Api
  module V1
    class DeviceSerializer < ApplicationSerializer
      attributes :id, :user_id, :phone_number, :push_token
    end
  end
end
