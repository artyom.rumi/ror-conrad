module Api
  module V1
    class AuthTokenSerializer < ApplicationSerializer
      attributes :value, :user_id, :expires_at
    end
  end
end
