module Api
  module V1
    class AttachmentSerializer < ApplicationSerializer
      attributes :original, :small, :medium, :large

      def original
        rails_blob_url(object)
      end

      def small
        rails_representation_url(object.variant(resize: "150x150"))
      end

      def medium
        rails_representation_url(object.variant(resize: "240x240"))
      end

      def large
        rails_representation_url(object.variant(resize: "480x480"))
      end
    end
  end
end
