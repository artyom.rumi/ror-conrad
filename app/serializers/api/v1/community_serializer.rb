module Api
  module V1
    class CommunitySerializer < ApplicationSerializer
      %i[schoolmates contacts others].each do |key|
        has_many key, serializer: SchoolmateSerializer do
          object[key]
        end
      end
    end
  end
end
