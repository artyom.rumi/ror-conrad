module Api
  module V1
    class VerificationCodeSerializer < ApplicationSerializer
      attributes :phone_number, :user_id, :expires_at, :attempts_count
    end
  end
end
