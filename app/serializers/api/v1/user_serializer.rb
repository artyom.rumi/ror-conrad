module Api
  module V1
    class UserSerializer < ApplicationSerializer
      attributes :id, :first_name, :last_name, :age, :grade, :gender, :phone_number, :created_at,
        :avatar, :default_avatar, :bio, :matching_enabled, :prop_request_notification_enabled,
        :receiving_prop_notification_enabled, :chat_messages_notification_enabled,
        :new_friends_notification_enabled, :new_users_notification_enabled, :friend_boost_at,
        :friends_count, :pokes_count, :top_pokes, :hide_top_pokes

      belongs_to :school

      def avatar
        AttachmentSerializer.new(object.avatar.attachment) if object.avatar.attached?
      end

      def top_pokes
        return nil if scope[:current_user] != object && object.hide_top_pokes

        object.top_pokes
      end

      # rubocop:disable Metrics/AbcSize
      def default_avatar
        {
          original: view_context.image_url(object.default_avatar),
          small: view_context.image_url("small-#{object.default_avatar}"),
          medium: view_context.image_url("medium-#{object.default_avatar}"),
          large: view_context.image_url("large-#{object.default_avatar}")
        }
      end
      # rubocop:enable Metrics/AbcSize
    end
  end
end
