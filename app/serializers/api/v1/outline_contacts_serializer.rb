module Api
  module V1
    class OutlineContactsSerializer < ApplicationSerializer
      attributes :id, :phone_number, :name, :invited
    end
  end
end
