module Api
  module V1
    class PropDeliverySenderSerializer < PropDeliverySerializer
      belongs_to :recipient, serializer: SchoolmateSerializer
      attributes :prop

      def prop
        return PropSerializer.new(object.prop) if object.ordinary?

        { content: object.content, emoji: object.emoji }
      end
    end
  end
end
