module Api
  module V1
    class ReceivedPropSerializer < ApplicationSerializer
      attributes :id, :created_at, :sender_gender, :viewed_at, :type

      belongs_to :prop

      has_one :reaction
    end
  end
end
