module Api
  module V1
    class CommentSerializer < ApplicationSerializer
      attributes :id, :text, :status, :created_at

      belongs_to :user, serializer: CommentUserSerializer do
        object.user
      end

      has_many :chats, key: :chat, serializer: ChatSerializer do |serializer|
        object.chats.find_by(owner_id: serializer.current_user.id)
      end

      def status
        return "Read" if object.feed.user != current_user

        object.status
      end
    end
  end
end
