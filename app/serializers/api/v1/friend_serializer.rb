module Api
  module V1
    class FriendSerializer < ApplicationSerializer
      attributes :id, :first_name, :last_name, :grade, :bio, :school_name, :avatar, :default_avatar,
        :gender, :followee, :blocked, :role, :chat_available, :custom_prop_available,
        :friends_count, :pokes_count, :hide_top_pokes, :friendship_status, :days_to_expired_friendship

      def avatar
        AttachmentSerializer.new(object.avatar.attachment) if object.avatar.attached?
      end

      def top_pokes
        return nil if scope[:current_user] != object && object.hide_top_pokes

        object.top_pokes
      end

      def default_avatar
        {
          original: default_avatar_original,
          small: default_avatar_small,
          medium: default_avatar_medium,
          large: default_avatar_large
        }
      end

      def school_name
        object.school.name
      end

      def followee
        object.incoming_subscriptions.pluck(:subscriber_id).include?(current_user.id)
      end

      def blocked
        current_user.blocked_user? object
      end

      def chat_available
        current_user.friends_with?(object)
      end

      def custom_prop_available
        current_user.friends_with?(object)
      end

      def friendship_status
        current_user.friendship_status(object)
      end

      def days_to_expired_friendship
        current_user.days_to_expired_friendship(object) if current_user.friends_with?(object)
      end

      private

      def default_avatar_original
        view_context.image_url(object.default_avatar)
      end

      def default_avatar_small
        view_context.image_url("small-#{object.default_avatar}")
      end

      def default_avatar_medium
        view_context.image_url("medium-#{object.default_avatar}")
      end

      def default_avatar_large
        view_context.image_url("large-#{object.default_avatar}")
      end
    end
  end
end
