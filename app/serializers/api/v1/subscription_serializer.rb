module Api
  module V1
    class SubscriptionSerializer < ApplicationSerializer
      attributes :id, :subscriber_id, :target_id
    end
  end
end
