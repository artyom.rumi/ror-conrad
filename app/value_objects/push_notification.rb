class PushNotification
  attr_reader :category, :badge, :sound, :mutable_content, :sender, :prop_sender_gender,
    :receiver_device_push_token

  ACTIVITIES = %i[new_like new_comment new_repost new_rate].freeze
  ATTRIBUTES = %w[category badge sound mutable_content custom_payload topic].freeze

  DEFAULT_BADGE = 1
  DEFAULT_SOUND = "default".freeze
  DEFAULT_MUTABLE_CONTENT = true

  def initialize(params = {})
    @receiver_device_push_token = params.fetch(:receiver_device_push_token)
    @category = params.fetch(:category).to_sym
    @badge = params.fetch(:badge, DEFAULT_BADGE)
    @sound = params.fetch(:sound, DEFAULT_SOUND)
    @mutable_content = params.fetch(:mutable_content, DEFAULT_MUTABLE_CONTENT)
    @prop_sender_gender = params.fetch(:prop_sender_gender) if category == :received_prop
    @sender = params.fetch(:sender) if ACTIVITIES.include?(category)
  end

  def apnotic_notification
    @apnotic_notification ||=
      ATTRIBUTES.each_with_object(notification) do |attribute, notification|
        notification.public_send("#{attribute}=", send(attribute))
      end
  end

  private

  def custom_payload
    if category == :received_prop
      base_custom_payload.merge(prop_sender_gender: prop_sender_gender)
    elsif ACTIVITIES.include?(category)
      base_custom_payload.merge(sender: @sender)
    else
      base_custom_payload
    end
  end

  def base_custom_payload
    { category_type: category }
  end

  def topic
    @topic ||= ENV.fetch("APN_TOPIC")
  end

  def notification
    @notification ||= Apnotic::Notification.new(receiver_device_push_token)
  end
end
