class NotificationData
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::AssetUrlHelper

  attr_reader :notification
  delegate :image, :default_image, :message, :payload, to: :additional_notification_data

  FEED_ACTION_NAME = %w[new_like new_comment new_repost new_rate].freeze
  FRIEND_ACTION_NAME = %w[new_friend 72h_expired_friend 12h_expired_friend renew_friendship expired_friendship].freeze

  def initialize(notification)
    @notification = notification
  end

  def to_hash
    {
      id: notification.id,
      type: notification.action_name == "new_rate" ? rate.rate < 10 ? "new_rate" : "new_rate_friend" : notification.action_name,
      is_unread: notification.status == "Unread",
      created_at: notification.created_at,
      image: image_data_urls,
      message: message,
      payload: payload
    }
  end

  private

  def image_data_urls
    if image.try(:attached?)
      image_data
    elsif default_image.present?
      default_image_data
    end
  end

  def default_image_data
    {
      original: image_url(default_image, host: root_url),
      small: image_url("small-#{default_image}", host: root_url),
      medium: image_url("medium-#{default_image}", host: root_url),
      large: image_url("large-#{default_image}", host: root_url)
    }
  end

  def image_data
    {
      original: rails_blob_url(image),
      small: rails_representation_url(image.variant(resize: "150x150")),
      medium: rails_representation_url(image.variant(resize: "240x240")),
      large: rails_representation_url(image.variant(resize: "480x480"))
    }
  end

  def additional_notification_data
    if notification.try(:grouped)
      Notifications::GroupedActivitiesAdditionalData.new(notification)
    elsif FEED_ACTION_NAME.include?(notification.action_name)
      Notifications::FeedActivityAdditionalData.new(notification)
    elsif FRIEND_ACTION_NAME.include?(notification.action_name)
      Notifications::FriendshipAdditionalData.new(notification)
    end
  end

  def feed_id
    notification.action_name == "new_repost" ? activity.source_feed.id : activity.feed.id
  end

  def activity
    notification.notifiable
  end

  def sender
    notification.notifiable.user
  end

  def rate
    Rate.find_by(feed_id: feed_id, user_id: sender.id)
  end
end
