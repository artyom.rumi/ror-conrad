# has duplications with channel_service_message.rb,
# keep in mind when changing one of the classes

class ChannelTextMessage
  attr_reader :chat_id, :sender_id, :receiver_id, :text, :image_urls, :link, :comment

  PROTOCOL_VERSION = 10_000

  def initialize(params = {})
    @chat_id = params.fetch(:chat_id)
    @text = params.fetch(:text)
    @sender_id = params.fetch(:sender_id)
    @receiver_id = params.fetch(:receiver_id)
    @image_urls = params[:image_urls]
    @link = params[:link]
    @comment = params[:comment]
  end

  def to_hash
    {
      version: PROTOCOL_VERSION,
      chat_id: chat_id,
      sender_id: sender_id,
      receiver_id: receiver_id,
      method: {
        type: "publish",
        message: message
      }
    }
  end

  private

  def message
    {
      id: message_uuid,
      created_at: message_created_at,
      content: content
    }
  end

  def content
    content_attributes = { type: type, text: text }

    content_attributes[:comment] = comment if comment.present?

    return content_attributes.merge(image_urls) if type == "photo"
    return content_attributes.merge(link: link) if type == "link"

    content_attributes
  end

  def type
    case [image_urls.present?, link.present?]
    when [false, false]
      "text"
    when [true, false]
      "photo"
    when [false, true]
      "link"
    end
  end

  def message_uuid
    SecureRandom.uuid
  end

  def message_created_at
    # number of seconds from 00:00:00 UTC on 1 January 2001 (this type of date is defined on the client)
    # https://developer.apple.com/documentation/foundation/date/1779647-init
    Time.zone.now.to_i - Time.new(2001, 1, 1).to_i
  end
end
