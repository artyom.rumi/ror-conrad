class Report
  include ActiveModel::Validations

  attr_reader :reporter, :reported_user

  validates :reporter, :reported_user, presence: true
  validate :reporter_is_not_a_reported_user

  def initialize(report_params)
    @reporter = report_params[:reporter]
    @reported_user = User.find_by(id: report_params[:reported_user_id])
  end

  private

  def reporter_is_not_a_reported_user
    errors.add(:reporter, "cannot report yourself") if reporter.eql?(reported_user)
  end
end
