class ReactionReport
  include ActionView::Helpers::TextHelper

  attr_reader :user, :start_date, :end_date

  BASE_TEXT = "You made an impact on %<friend_count>s.".freeze
  REACTION_TEXT = "\n%<user_count>s reacted with \"%<reaction_content>s\".".freeze

  def initialize(params = {})
    @user = params.fetch(:user)
    @start_date = params.fetch(:start_date, 1.day.ago)
    @end_date = params.fetch(:end_date, Time.current)
  end

  def to_hash
    {
      reacted_recipients_count: reacted_recipients_count,
      reactions_counts: reactions_counts
    }
  end

  def to_s
    reactions_counts.each_with_object(base_content) do |(reaction, count), content|
      content << specific_reaction_content(reaction, count)
    end
  end

  private

  def base_content
    @base_content ||= format(BASE_TEXT, friend_count: pluralize(reacted_recipients_count, "friend"))
  end

  def specific_reaction_content(reaction_content, count)
    format(REACTION_TEXT, user_count: pluralize(count, "user"), reaction_content: reaction_content)
  end

  def reacted_recipients_count
    @reacted_recipients_count ||= reacted_recipients.count
  end

  def reactions_counts
    @reactions_counts ||= incoming_reactions.group(:content).count
  end

  def incoming_reactions
    @incoming_reactions ||= Reaction.joins(:prop_delivery)
                                    .where(prop_deliveries: { sender: user }, created_at: start_date..end_date)
  end

  def reacted_recipients
    @reacted_recipients ||= User.joins(:incoming_prop_deliveries)
                                .where(prop_deliveries: { id: incoming_reactions.pluck(:prop_delivery_id) })
                                .distinct
  end
end
