module Notifications
  class FriendshipAdditionalData
    attr_reader :notification

    def initialize(notification)
      @notification = notification
    end

    def image
      friend.avatar
    end

    def default_image
      friend.default_avatar
    end

    def message # rubocop:disable Metrics/MethodLength
      case notification.action_name
      when "new_friend"
        "You're now friends with #{friend.full_name}"
      when "renew_friendship"
        "You're friendship with #{friend.full_name} has been renewed."
      when "expired_friendship"
        "Your friendship with #{friend.full_name} has been expired."
      when "72h_expired_friend"
        "Watch out! you have 72 hours to keep friend with #{friend.full_name}."
      when "12h_expired_friend"
        "Act now! You have 12 hours before expiring the friend status with #{friend.full_name}"
      end
    end

    def payload
      {
        friend_id: friend.id
      }
    end

    private

    def friend
      friend_id = [friendship.first_user_id, friendship.second_user_id] - [notification.user_id]
      User.find(friend_id.first)
    end

    def friendship
      notification.notifiable
    end

    def receiver
      notification.user
    end
  end
end
