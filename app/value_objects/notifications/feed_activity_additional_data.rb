module Notifications
  class FeedActivityAdditionalData
    attr_reader :notification

    def initialize(notification)
      @notification = notification
    end

    def message
      case notification.action_name
      when "new_like"
        "#{person_message} liked your post!"
      when "new_comment"
        activity.text
      when "new_repost"
        "#{person_message} reposted your post!"
      when "new_rate"
        "#{rate_message}"
      end
    end

    def payload
      if notification.action_name == "new_comment"
        base_payload.merge(comment_payload)
      elsif notification.action_name == "new_repost"
        base_payload.merge(repost_payload)
      else
        base_payload
      end
    end

    def image
      notification.action_name == "new_repost" ? activity.image : activity.feed.image
    end

    def default_image
      nil
    end

    private

    def base_payload
      {
        feed_id: feed_id,
        friend_id: sender.id
      }
    end

    def comment_payload
      {
        sender: person_message,
        comment_id: activity.id
      }
    end

    def repost_payload
      {
        repost_feed_id: activity.id
      }
    end

    def person_message
      if receiver.friends_with?(sender)
        "Friend"
      elsif sender.friend_of_friends?(receiver)
        "A friend of your friend"
      else
        "Someone"
      end
    end

    def feed_id
      notification.action_name == "new_repost" ? activity.source_feed.id : activity.feed.id
    end

    def activity
      notification.notifiable
    end

    def receiver
      notification.user
    end

    def sender
      notification.notifiable.user
    end

    def rate
      Rate.find_by(feed_id: feed_id, user_id: sender.id)
    end

    def rate_message
      rate.rate < 10 ? "You've got a #{rate.rate}!" : "#{sender.first_name} gave you a 10!"
    end

  end
end
