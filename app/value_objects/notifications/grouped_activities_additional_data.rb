module Notifications
  class GroupedActivitiesAdditionalData
    include ActionView::Helpers::TextHelper
    attr_reader :notification

    def initialize(notification)
      @notification = notification
    end

    def message
      "A #{pluralize_or_empty(notification.friend_activity_count, 'friend')}" \
      "#{add_and}" \
      "#{activity_sender}" \
      " #{activity_past_form}"
    end

    def payload
      {
        feed_id: notification.feed_id,
        activities_id: notification.activities_ids,
        notifications_id: notification.notifications_ids,
        grouped: true
      }
    end

    def image
      @image ||= Feed.find(notification.feed_id).image
    end

    private

    def add_and
      " and " if notification.friend_activity_count.nonzero? && notification.not_friend_activity_count.nonzero?
    end

    def activity_past_form
      case notification.action_name
      when "new_like"
        "liked your post"
      when "new_repost"
        "reposted your post"
      end
    end

    def activity_sender
      return "someone" if notification.not_friend_activity_count == 1

      if senders_not_friends?
        return "someone and #{pluralize_or_empty(notification.not_friend_activity_count - 1, 'other')}"
      end

      pluralize_or_empty(notification.not_friend_activity_count, "other")
    end

    def senders_not_friends?
      notification.friend_activity_count.zero? && notification.not_friend_activity_count > 1
    end

    def pluralize_or_empty(count, word)
      return "" unless count.nonzero?

      pluralize(count, word)
    end
  end
end
