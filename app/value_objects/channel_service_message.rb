# has duplications with channel_text_message.rb,
# keep in mind when changing one of the classes

class ChannelServiceMessage
  attr_reader :info_type, :chat_id, :push_notification_data, :receiver

  delegate :id, to: :receiver, prefix: true
  PROTOCOL_VERSION = 10_000

  def initialize(params = {})
    @info_type = params.fetch(:info_type)
    @chat_id = params.fetch(:chat_id)
    @push_notification_data = params.fetch(:push_notification_data)
    @receiver = params.fetch(:receiver) if add_receiver?
  end

  def to_hash
    if add_receiver?
      base_service_data.merge(receiver_data)
    else
      base_service_data
    end
  end

  private

  def base_service_data
    {
      version: PROTOCOL_VERSION,
      chat_id: chat_id,
      method: {
        type: "publish",
        message: message
      },
      pn_apns: push_notification_data
    }
  end

  def receiver_data
    { receiver_id: receiver_id }
  end

  def message
    {
      id: message_uuid,
      created_at: message_created_at,
      content: {
        info_type: info_type,
        type: "info"
      }
    }
  end

  def message_uuid
    SecureRandom.uuid
  end

  def message_created_at
    # number of seconds from 00:00:00 UTC on 1 January 2001 (this type of date is defined on the client)
    # https://developer.apple.com/documentation/foundation/date/1779647-init
    Time.zone.now.to_i - Time.new(2001, 1, 1).to_i
  end

  def add_receiver?
    permissible_info_type_values = %w[one_reveal cancel_reveal]
    permissible_info_type_values.include?(info_type)
  end
end
