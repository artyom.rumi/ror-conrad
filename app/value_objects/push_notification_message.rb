class PushNotificationMessage
  attr_reader :chat_id, :sender, :receiver, :info_type

  delegate :device_push_token, to: :sender, prefix: true, allow_nil: true
  delegate :id, to: :receiver, prefix: true

  def initialize(params = {})
    @chat_id = params.fetch(:chat_id)
    @sender = params.fetch(:sender, nil)
    @receiver = params.fetch(:receiver, nil)
    @info_type = params.fetch(:info_type)
  end

  def to_hash
    data
  end

  private

  def data
    if targeted_notification?
      targeted_notification_data.merge(chat_receiver_id: receiver_id)
    else
      base_notification_data
    end
  end

  def targeted_notification_data
    if sender_device_push_token
      base_notification_data.merge(pn_exceptions: [sender_device_push_token])
    else
      base_notification_data
    end
  end

  def base_notification_data
    {
      aps: aps_data,
      category_type: "chat_message",
      chat_id: chat_id
    }
  end

  def aps_data
    {
      category: "chat_message",
      alert: {
        'title-loc-key': "apns_chat_message_title",
        'loc-key': "apns_chat_#{info_type}_description"
      },
      badge: 1,
      sound: "default",
      'mutable-content': 1
    }
  end

  def targeted_notification?
    receiver
  end
end
