class Users::DiscoveriesQuery
  def initialize(user)
    @user = user
  end

  def all
    User.where(id: discoveries_users_ids)
  end

  private

  attr_reader :user

  def discoveries_users_ids
    @discoveries_users_ids ||= fetch_user_ids - user.friend_ids - [user.id]
  end

  def fetch_user_ids
    @fetch_user_ids ||= begin
      friends_of_friends = fetch_friends_ids(user.friend_ids)
      friends_of_friends + fetch_friends_ids(friends_of_friends)
    end
  end

  def fetch_friends_ids(user_ids)
    puts "user_ids in the fetch_friends_ids: #{user_ids}"
    if user_ids!=nil && user_ids.length() > 0
      User.where(id: user_ids).sum(&:friend_ids).uniq
    else
      []
    end
  end
end
