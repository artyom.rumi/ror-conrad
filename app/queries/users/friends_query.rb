class Users::FriendsQuery
  def initialize(user, relation = Friendship.all)
    @user = user
    @relation = relation
  end

  def all
    User.where(id: friends_ids)
  end

  private

  attr_reader :user, :relation

  def friends_ids
    relation
      .where(first_user: user)
      .or(relation.where(second_user: user))
      .pluck(:first_user_id, :second_user_id).flatten - [user.id]
  end
end
