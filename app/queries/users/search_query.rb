module Users
  class SearchQuery
    attr_reader :relation, :keywords
    private :relation, :keywords

    def initialize(keywords, relation = User.ordinary.all)
      @keywords = keywords
      @relation = relation
    end

    def all
      return relation if keywords.blank?

      relation.search_by_full_name(keywords)
    end
  end
end
