module Users
  class WithIncomingPropReactionsQuery
    attr_reader :relation, :start_date, :end_date

    def initialize(params = {})
      @relation = params.fetch(:relation, User.all)
      @start_date = params.fetch(:start_date, 1.day.ago)
      @end_date = params.fetch(:end_date, Time.current)
    end

    def all
      relation.where(id: users_with_incoming_reactions_ids)
    end

    private

    def users_with_incoming_reactions_ids
      PropDelivery
        .includes(:reaction)
        .where(reactions: { created_at: start_date..end_date })
        .pluck(:sender_id)
    end
  end
end
