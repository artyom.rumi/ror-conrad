class Users::FeedEngagedFriendsQuery
  ACTIVITIES = %i[likes comments reposts].freeze
  attr_reader :user, :feed, :activities

  delegate :count, to: :all

  def initialize(user, feed, activities = ACTIVITIES)
    @user = user
    @feed = feed
    @activities = activities
  end

  def all
    @all ||=
      activities.reduce([]) do |result, activity|
        result | fetch_friends_activity(activity) if activities.include? activity
      end
  end

  private

  def fetch_friends_activity(activity)
    return friends.includes(:feeds).where(feeds: { source_feed_id: feed.id }) if activity == :reposts

    friends.includes(activity).where(activity => { feed_id: feed.id })
  end

  def friends
    @friends = Users::FriendsQuery.new(user).all
  end
end
