module Schools
  class FilterSchoolsQuery
    ALLOWED_PARAMS = %i[year_of_ending keywords].freeze

    MAX_MILES_LOOKUP = 25

    attr_reader :filter_params, :relation
    private :filter_params, :relation

    def initialize(filter_params, relation = School.all)
      @filter_params = filter_params
      @relation = relation
    end

    def all
      filtered_relation = filter_params.slice(*ALLOWED_PARAMS).reduce(relation) do |relation, (key, value)|
        value.blank? ? relation : send("by_#{key}", relation, value)
      end

      order(filtered_relation)
    end

    private

    def by_year_of_ending(relation, year_of_ending)
      Schools::ByYearOfEndingQuery.new(year_of_ending, relation).all
    end

    def by_keywords(relation, keywords)
      relation.search_by_name(keywords).with_pg_search_rank
    end

    def order(relation)
      return relation.order(name: :asc) unless coordinates_present?

      relation.near([filter_params[:latitude], filter_params[:longitude]], MAX_MILES_LOOKUP)
    end

    def coordinates_present?
      filter_params[:latitude].present? && filter_params[:longitude].present?
    end
  end
end
