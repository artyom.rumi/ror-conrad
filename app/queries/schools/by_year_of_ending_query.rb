module Schools
  class ByYearOfEndingQuery
    using AlreadyGraduated

    attr_reader :year_of_ending, :relation

    def initialize(year_of_ending, relation = School.all)
      @year_of_ending = year_of_ending.to_i
      @relation = relation
    end

    def all
      if year_of_ending.already_graduated?
        all_real_schools
      else
        schools_by_year_of_ending
      end
    end

    private

    def all_real_schools
      relation.joins(:grades).where("grades.value > ?", 0).distinct
    end

    def schools_by_year_of_ending
      relation.joins(:grades).where(grades: { value: year_of_ending.grade_value }).distinct
    end
  end
end
