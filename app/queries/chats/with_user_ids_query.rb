module Chats
  class WithUserIdsQuery
    attr_reader :user_ids, :relation

    def initialize(user_ids, relation = Chat.includes(users: :school))
      @relation = relation
      @user_ids = user_ids
    end

    def all
      relation
        .joins(:chat_subscriptions)
        .where(chat_subscriptions: { user_id: user_ids })
        .group("chats.id")
        .having("count(chat_subscriptions.user_id) = ?", user_ids.size)
    end
  end
end
