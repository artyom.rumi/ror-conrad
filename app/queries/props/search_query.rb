module Props
  class SearchQuery
    attr_reader :content

    def initialize(content)
      @content = content.squish
    end

    def by_content
      Prop.where("content ilike :content", content: pattern)
    end

    private

    def pattern
      "%" + content + "%"
    end
  end
end
