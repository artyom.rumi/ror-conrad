module Props
  class RecentlyUsedQuery
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def all
      Prop
        .all
        .includes(:prop_deliveries)
        .where(sent_by_current_user)
        .order("prop_deliveries.created_at DESC")
    end

    private

    def sent_by_current_user
      return if user.blank?

      { prop_deliveries: { sender_id: user.id } }
    end
  end
end
