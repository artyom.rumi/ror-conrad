module Requests
  class ReceiversQuery
    attr_reader :requester

    delegate :subscribed_by_users, :outcoming_requests, to: :requester, prefix: true

    MAX_REQUESTS_RATIO = 0.5

    def initialize(requester)
      @requester = requester
    end

    def all
      unrequested_followers
        .joins(join_subscriptions_and_requests_sql)
        .group("users.id")
        .having(having_less_than_max_request_sql)
    end

    private

    def join_subscriptions_and_requests_sql
      <<-SQL
        join subscriptions on subscriptions.subscriber_id = users.id
        left join requests on users.id = requests.receiver_id
      SQL
    end

    def having_less_than_max_request_sql
      <<-SQL
        CASE WHEN (select count(*) from subscriptions where (subscriptions.subscriber_id = users.id)) < #{Request::POLLS_LIMIT} THEN FALSE
        ELSE
          (select count(*) from requests where (requests.progress < #{Request::POLLS_LIMIT} and requests.receiver_id = users.id))
          <
          (select count(*) * #{MAX_REQUESTS_RATIO} from subscriptions where (subscriptions.subscriber_id = users.id))
        END
      SQL
    end

    def unrequested_followers
      ids = requester_subscribed_by_users.ids - requester_outcoming_requests.opened.pluck(:receiver_id)
      User.where(id: ids)
    end
  end
end
