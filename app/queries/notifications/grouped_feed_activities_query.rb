module Notifications
  class GroupedFeedActivitiesQuery
    attr_reader :user, :feeds

    def initialize(user)
      @user = user
      @feeds = Feed.where(user_id: user.id)
    end

    def all
      @all ||=
        Notification::GROUPED_ACTIONS.reduce([]) do |result, action_name|
          result + fetch_grouped_notifications(activity_name[action_name])
        end
    end

    private

    def fetch_grouped_notifications(activity_name)
      action_table_name = activity_name == "reposts" ? "reposts_feeds" : activity_name

      feeds.joins(activity_name => %i[notification user])
           .where("notifications.status = 'Read'")
           .joins(joins_friendships(action_table_name))
           .group("feeds.id, notifications.status, notifications.action_name")
           .select("feeds.id AS feed_id, COUNT(friendships) AS friend_activity_count,
              COUNT(#{action_table_name}) - COUNT(friendships) AS not_friend_activity_count,
              MAX(notifications.created_at) AS created_at,
              notifications.status, notifications.action_name, TRUE AS grouped,
              array_agg(#{action_table_name}.id) AS activities_ids, array_agg(notifications.id) AS notifications_ids")
    end

    def joins_friendships(action_table_name)
      <<-SQL
        LEFT JOIN friendships
          ON friendships.first_user_id = feeds.user_id AND friendships.second_user_id = #{action_table_name}.user_id OR
          friendships.second_user_id = feeds.user_id AND friendships.first_user_id = #{action_table_name}.user_id
      SQL
    end

    def activity_name
      {
        "new_like" => "likes",
        "new_repost" => "reposts"
      }
    end
  end
end
