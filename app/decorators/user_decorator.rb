class UserDecorator < ApplicationDecorator
  delegate :id, :full_name, :email

  def full_name_with_email
    "#{object.full_name} (#{object.email})"
  end

  def relationship_status_with(user)
    return "A friend" if object.friends_with?(user)
    return "A friend of your friend" if object.friend_of_friends?(user)

    "Someone"
  end
end
