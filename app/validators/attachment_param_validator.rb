# Active storage does not have a validation.
# The `#attach` method can fall with an error if you pass the invalid value in the parameter.
# Therefore, it became necessary to validate the parameter before assigning and writing.

class AttachmentParamValidator
  CONTENT_TYPES = %w[image/png image/jpg image/jpeg].freeze
  ERROR_MESSAGE = "could not be uploaded".freeze

  def initialize(record, attribute, value)
    @record = record
    @attribute = attribute
    @value = value
  end

  def valid?
    record.errors.add(attribute, ERROR_MESSAGE) unless type_valid? && content_type_valid?

    record.errors[attribute].empty?
  end

  private

  attr_reader :record, :attribute, :value

  def type_valid?
    value.class == ActionDispatch::Http::UploadedFile
  end

  def content_type_valid?
    value.content_type.in?(CONTENT_TYPES)
  end
end
