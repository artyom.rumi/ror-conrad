require "csv"
require "activerecord-import/base"
require "activerecord-import/active_record/adapters/postgresql_adapter"

MIDDLE_SCHOOLS_FILE_PATH = "db/seeds/middle_schools.csv".freeze
HIGH_SCHOOLS_FILE_PATH = "db/seeds/high_schools.csv".freeze
MIDDLE_SCHOOLS_GRADES = [6, 7, 8].freeze
HIGH_SCHOOLS_GRADES = [9, 10, 11, 12].freeze
SCHOOL_GROUP_SIZE = 100

namespace :schools do
  desc "Import schools DB from csv files"
  task import_from_csv: :environment do
    schools_attributes = []
    schools = []

    CSV.foreach(MIDDLE_SCHOOLS_FILE_PATH, headers: true, header_converters: :symbol) do |row|
      schools_attributes << school_and_grades_attributes(row, "middle")
    end

    CSV.foreach(HIGH_SCHOOLS_FILE_PATH, headers: true, header_converters: :symbol) do |row|
      schools_attributes << school_and_grades_attributes(row, "high")
    end

    # Group schools that are duplicated in two lists
    schools_groups = schools_attributes.group_by do |school|
      "#{school[:name]}-#{school[:latitude]}-#{school[:longitude]}-#{school[:address]}"
    end

    schools_attributes = []

    # Merge duplicated schools grades attributes
    schools_groups.map do |school_group|
      schools_attributes <<
        if school_group[1].size == 1
          school_group[1][0]
        else
          school_group[1][1..-1].each_with_object(school_group[1][0]) do |elem, school|
            school[:grades_attributes] |= elem[:grades_attributes]
          end
        end
    end

    # Import unique schools into DB with chunks
    schools_attributes.in_groups_of(SCHOOL_GROUP_SIZE, false) do |group|
      group.map do |school_attributes|
        schools << School.new(school_attributes)
      end
      School.import(schools, validate: false, recursive: true)
      schools = []
    end
  end
end

def middle_schools_grades_attributes
  @middle_schools_grades_attributes ||= grades_attributes(MIDDLE_SCHOOLS_GRADES)
end

def high_schools_grades_attributes
  @high_schools_grades_attributes ||= grades_attributes(HIGH_SCHOOLS_GRADES)
end

def grades_attributes(grades)
  grades.each_with_object([]) { |grade, arr| arr << { value: grade } }
end

def school_attributes(row)
  {
    name: capitalize_name(row[:name]),
    address: "#{capitalize_name(row[:city])}, #{row[:state]}",
    latitude: convert_coordinate(row[:latitude]),
    longitude: convert_coordinate(row[:longitude])
  }
end

def school_and_grades_attributes(row, school_type)
  school_attributes(row).merge(grades_attributes: send("#{school_type}_schools_grades_attributes"))
end

def convert_coordinate(value)
  value == "#N/A" ? nil : value.to_f
end

def capitalize_name(name)
  name.split.map(&:capitalize).join(" ")
end
