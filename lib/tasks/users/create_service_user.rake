namespace :users do
  desc "Creates service school with service user"
  task create_service_user: :environment do
    service_school = School.find_or_create_by!(
      name: "Bot school",
      address: "Botsville, NV"
    )

    User.find_or_create_by!(
      first_name: "Nicely",
      last_name: "",
      age: 0,
      grade: 1900,
      gender: :non_binary,
      school: service_school,
      role: :bot,
      phone_number: ""
    )
  end
end
