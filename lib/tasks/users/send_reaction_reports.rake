namespace :users do
  desc "Send reaction reports to service chats between users and bot"
  task send_reaction_reports: :environment do
  end
end
