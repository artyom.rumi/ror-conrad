namespace :incognito_chats do
  desc "Removing of incognito chats that inactive for 30 days ago"
  task remove_outdated: :environment do
    Chat.incognito.where("updated_at <= ?", 30.days.ago).delete_all
  end
end
