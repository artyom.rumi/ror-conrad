require "open-uri"

# TODO: delete after migration. PT Story: https://www.pivotaltracker.com/story/show/160655536
namespace :attaches do
  desc "Migrate attaches from CarrierWave to ActiveStorage"
  task migrate: :environment do
    migrate_user_attaches
    migrate_collocutor_masks_attaches
  end

  def migrate_user_attaches
    User.find_each do |user|
      migrate_user_avatar(user)
      migrate_user_pictures(user)
    end
  end

  def migrate_user_avatar(user)
    return unless user.old_avatar.url

    avatar_uri = URI.parse(user.old_avatar.url)
    user.avatar.attach(io: avatar_uri.open, filename: "avatar")
  rescue OpenURI::HTTPError
    puts "User #{user.id} does not have avatar in s3"
  end

  def migrate_user_pictures(user)
    user.old_pictures.each do |old_picture|
      next unless old_picture.image.url

      begin
        image_uri = URI.parse(old_picture.image.url)
        user.pictures.attach(io: image_uri.open, filename: "image")
      rescue OpenURI::HTTPError
        puts "Picture #{old_picture.id} does not have image in s3"
      end
    end
  end

  def migrate_collocutor_masks_attaches
    CollocutorMask.find_each do |collocutor_mask|
      if collocutor_mask.old_avatar.url
        begin
          avatar_uri = URI.parse(collocutor_mask.old_avatar.url)
          collocutor_mask.avatar.attach(io: avatar_uri.open, filename: "avatar")
        rescue OpenURI::HTTPError
          puts "CollocutorMask #{collocutor_mask.id} does not have avatar in s3"
        end
      end
    end
  end
end
