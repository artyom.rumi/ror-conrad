namespace :auth_tokens do
  desc "Removing of expired auth tokens"
  task remove_outdated: :environment do
    AuthToken.expired.find_each do |auth_token|
      ActiveRecord::Base.transaction do
        Devices::ClearPushTokenFromChats.call!(user: auth_token.user)
        auth_token.destroy!
      end
    end
  end
end
