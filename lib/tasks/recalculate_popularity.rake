namespace :props do
  desc "Recalculation usage amount of props and updating props index"
  task recalculate_popularity: :environment do
    unused_props.update_all(popularity_index: 0) # rubocop:disable Rails/SkipsModelValidations

    asc_sorted_by_usage.each_with_index do |prop, index|
      prop.update(popularity_index: index + 1)
    end
  end

  def grouped_with_count_of_use
    @grouped_with_count_of_use ||= PropDelivery.group(:prop_id).count
  end

  def unused_props
    Prop.where.not(id: grouped_with_count_of_use.keys)
  end

  def asc_sorted_by_usage
    Prop.where(id: grouped_with_count_of_use.sort_by(&:last).to_h.keys)
  end
end
