namespace :props do
  EMOJI = ["\xF0\x9F\x98\x89", "\xF0\x9F\x98\x8A", "\xF0\x9F\x98\x8C", "\xF0\x9F\x98\x8D", "\xF0\x9F\x98\x8F",
           "\xF0\x9F\x98\x9C", "\xF0\x9F\x98\xB8", "\xF0\x9F\x98\xBB", "\xF0\x9F\x8C\x9F", "\xF0\x9F\x92\xAB",
           "\xF0\x9F\x98\x87", "\xF0\x9F\x98\x9B"].freeze
  GENDERS = { male: "Boy", female: "Girl" }.freeze
  MAX_LENGTH = 60

  desc "parse list of neutral compliments"
  task parse_neutral_compliments: :environment do
    CSV.foreach("db/seeds/list_of_compliments_121317.csv", headers: true, header_converters: :symbol) do |row|
      Prop.create(content: row[:compliment], emoji: emoji)
    end
  end

  desc "parse list of gender specific compliments"
  task parse_gender_compliments: :environment do
    CSV.foreach("db/seeds/list_of_сompliments_ES_EN_021218.csv", headers: true, header_converters: :symbol) do |row|
      next if row[:count].to_i > MAX_LENGTH

      Prop.create(content: row[:compliment], emoji: emoji, special: special?(row[:category]),
                  gender: GENDERS.key(row[:gender]))
    end
  end

  def special?(category)
    category.eql?("Emily")
  end

  def emoji
    EMOJI.sample
  end
end
