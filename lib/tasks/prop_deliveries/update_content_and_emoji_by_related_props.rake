namespace :prop_deliveries do
  desc "Add related props content and emoji values to content and emoji fields"
  task update_content_and_emoji_by_related_props: :environment do
    prop_deliveries.find_each do |prop_delivery|
      related_prop = prop_delivery.prop

      prop_delivery.update(content: related_prop.content, emoji: related_prop.emoji) if related_prop
    end
  end

  def prop_deliveries
    PropDelivery.where("content IS NULL OR emoji IS NULL").includes(:prop)
  end
end
