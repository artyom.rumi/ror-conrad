# v1/Pictures API

## Upload an image

### POST /api/v1/pictures

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| image | Image that user sent to chat | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: multipart/form-data; boundary=----------XnJLe9ZIbbGUYtzPQJ16u1
X-Auth-Token: Fs9NpBRecut5GxSpkyncgHND
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/pictures</pre>

#### Body

<pre>------------XnJLe9ZIbbGUYtzPQJ16u1
Content-Disposition: form-data; name="image"; filename="avatar.png"
Content-Type: image/png
Content-Length: 1451087

[uploaded data]
------------XnJLe9ZIbbGUYtzPQJ16u1--</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;dbc7d6c0103ecc69ad79fb425b545ba4&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 1ff8de90-5dfa-415d-aac9-913a7d3001ea
X-Runtime: 0.403122
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1318</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 156,
  "image": {
    "original": "http://lvh.me:5000/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWlk9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d68e17d5e813e4daf072a9ab0367e94a7a4f2887/avatar.png",
    "small": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWlk9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d68e17d5e813e4daf072a9ab0367e94a7a4f2887/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTVRVd2VERTFNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--6961321111510a9086c6dbc85bb89f5b4d42e35b/avatar.png",
    "medium": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWlk9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d68e17d5e813e4daf072a9ab0367e94a7a4f2887/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTWpRd2VESTBNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--ae12e1e8ac781d9820431268ef8fc8f409f55b8d/avatar.png",
    "large": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWlk9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d68e17d5e813e4daf072a9ab0367e94a7a4f2887/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTkRnd2VEUTRNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--22e536a873c911c90d972fe877286eed82b6351b/avatar.png"
  },
  "user_id": 8042
}</pre>
