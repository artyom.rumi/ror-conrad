# v1/Avatar API

## Destroy avatar

### DELETE /api/v1/avatar
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: YQZux4yP7qMDeWuVE8m9YM3V
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/avatar</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 0936e084-264f-4e6b-8b21-0b584df79d00
X-Runtime: 0.050347
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

