# v1/Avatar API

## Upload an image

### POST /api/v1/avatar

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| file | User photo | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: multipart/form-data; boundary=----------XnJLe9ZIbbGUYtzPQJ16u1
X-Auth-Token: snu96XMM9STgqnY51HvMEEFr
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/avatar</pre>

#### Body

<pre>------------XnJLe9ZIbbGUYtzPQJ16u1
Content-Disposition: form-data; name="file"; filename="avatar.png"
Content-Type: image/png
Content-Length: 1451087

[uploaded data]
------------XnJLe9ZIbbGUYtzPQJ16u1--</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;a68390fae11950fb20c4c8f2999fd163&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 74b9b90b-04a7-416b-90a0-f6e5dee1c972
X-Runtime: 2.180840
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2221</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 7385,
  "first_name": "Thomas",
  "last_name": "Mitchell",
  "age": 15,
  "grade": 2026,
  "gender": "female",
  "phone_number": "+13909399186",
  "created_at": "2020-02-10T08:59:58.962+00:00",
  "avatar": {
    "original": "http://lvh.me:5000/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWDQ9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--f80dba2f1bf3853c5ae8964d5da57b5e50671e35/avatar.png",
    "small": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWDQ9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--f80dba2f1bf3853c5ae8964d5da57b5e50671e35/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTVRVd2VERTFNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--6961321111510a9086c6dbc85bb89f5b4d42e35b/avatar.png",
    "medium": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWDQ9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--f80dba2f1bf3853c5ae8964d5da57b5e50671e35/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTWpRd2VESTBNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--ae12e1e8ac781d9820431268ef8fc8f409f55b8d/avatar.png",
    "large": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWDQ9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--f80dba2f1bf3853c5ae8964d5da57b5e50671e35/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTkRnd2VEUTRNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--22e536a873c911c90d972fe877286eed82b6351b/avatar.png"
  },
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-22.png",
    "small": "http://example.org/assets/small-avatars/avatar-22.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-22.png",
    "large": "http://example.org/assets/large-avatars/avatar-22.png"
  },
  "bio": "Consequatur animi dolore et.",
  "matching_enabled": true,
  "prop_request_notification_enabled": true,
  "receiving_prop_notification_enabled": true,
  "chat_messages_notification_enabled": true,
  "new_friends_notification_enabled": true,
  "new_users_notification_enabled": true,
  "friend_boost_at": null,
  "friends_count": 0,
  "pokes_count": 0,
  "top_pokes": [

  ],
  "hide_top_pokes": false,
  "school": {
    "id": 7410,
    "name": "Qui repellendus qui occaecati.",
    "members_amount": 54,
    "address": "South Wileymouth, Washington"
  }
}</pre>
