# v1/my/Blocked collocutor masks API

## List of collocutor masks

### GET /api/v1/my/blocked_collocutor_masks
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: BHkW4R7euFq9feLNVz2bH3Xd
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/blocked_collocutor_masks</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7bbf6e369d6ae9eacc4eca9897cfc5c9&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 19e68ae9-f351-477b-b59c-c2d382a4df6d
X-Runtime: 0.054375
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 798</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 591,
    "name": "Isaac Fisher",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-incognito.png",
      "small": "http://example.org/assets/small-avatars/avatar-incognito.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-incognito.png",
      "large": "http://example.org/assets/large-avatars/avatar-incognito.png"
    },
    "collocutor_id": 7817,
    "blocked": true,
    "chat_id": 0
  },
  {
    "id": 592,
    "name": "Ms. Abram Wolff",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-incognito.png",
      "small": "http://example.org/assets/small-avatars/avatar-incognito.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-incognito.png",
      "large": "http://example.org/assets/large-avatars/avatar-incognito.png"
    },
    "collocutor_id": 7819,
    "blocked": true,
    "chat_id": 0
  }
]</pre>
