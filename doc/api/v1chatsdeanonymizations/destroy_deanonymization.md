# v1/Chats/Deanonymizations API

## destroy deanonymization

### DELETE /api/v1/chats/:chat_id/deanonymizations

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| chat_id | ID of chat where user wants to cancel deanonymization | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: oph2Ws7k6t1JSzpbsg5MoNLe
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/chats/535/deanonymizations</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 6274f060-0772-49f8-a748-7e35fa07ac85
X-Runtime: 0.051352
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

