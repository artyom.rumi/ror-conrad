# v1/Chats/Deanonymizations API

## List deanonymizations for specified chat

### GET /api/v1/chats/:chat_id/deanonymizations

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| chat_id | ID of chat where user wants to check deanonymizations | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: NBVjzyUSpCtK51uHJ3LWKR6T
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/chats/531/deanonymizations</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;d7f91323d4ffd471b7097f6ccff844be&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 7443b0c0-d57f-4c2f-b064-1aaf048fa4b0
X-Runtime: 0.013277
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 53</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 158,
    "user_id": 7412
  },
  {
    "id": 159,
    "user_id": 7413
  }
]</pre>
