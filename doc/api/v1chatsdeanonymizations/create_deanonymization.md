# v1/Chats/Deanonymizations API

## Create deanonymization

### POST /api/v1/chats/:chat_id/deanonymizations

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| chat_id | ID of chat where user wants to be deanonymized | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 6QEAsyJsPnpQnzB8FhHF8ni9
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/chats/527/deanonymizations</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;a74aaab9d33e2259240a0106726b5ada&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: bcaa296e-7783-454d-9948-97b1329b0066
X-Runtime: 0.099611
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 25</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 157,
  "user_id": 7399
}</pre>
