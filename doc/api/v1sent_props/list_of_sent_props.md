# v1/Sent props API

## list of sent props

### GET /api/v1/sent_props
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: hNNRbxmXSut9SDcfQPsfo4ND
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/sent_props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;676282e126979b935ed8515872598f1f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 8ef879f2-cf8a-41d3-9707-3439ba73e797
X-Runtime: 0.258703
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 801</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "created_at": "2020-02-10T09:00:51.039+00:00",
    "prop": {
      "id": 1025,
      "content": "Sapiente est vitae laborum.",
      "emoji": "😍",
      "gender": "female"
    },
    "recipient": {
      "id": 8228,
      "first_name": "Prudence",
      "last_name": "Trantow",
      "grade": 2025,
      "bio": "Libero quod rerum reiciendis.",
      "school_name": "Quas itaque harum velit.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-17.png",
        "small": "http://example.org/assets/small-avatars/avatar-17.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-17.png",
        "large": "http://example.org/assets/large-avatars/avatar-17.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 1,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  }
]</pre>
