# v1/Chats API

## always create a new one regardless

### POST /api/v1/chats

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_ids | Users ids to chat with (current user id will be added implicitly if not present in array) | true |  |
| name | Chat name | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: rL6BuiR4Ecqk9MBWoubVHRHJ
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/chats</pre>

#### Body

<pre>{"user_ids":[7452],"name":"Friends"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;946da7fbf624308bde6f8d5f3e0b57f3&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 15889f32-5448-41a2-a8e9-0fab26e1d9c7
X-Runtime: 0.175695
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2146</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 559,
  "pubnub_channel_name": "chat_559",
  "updated_at": "2020-02-10T09:00:06.538+00:00",
  "incognito": false,
  "feed_id": null,
  "comment_id": null,
  "name": "Friends",
  "is_group": true,
  "owner": {
    "id": 7451,
    "first_name": "Norene",
    "last_name": "Fisher",
    "grade": 2024,
    "bio": "Occaecati eos exercitationem ipsum.",
    "school_name": "Ad rerum quas repudiandae.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-24.png",
      "small": "http://example.org/assets/small-avatars/avatar-24.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
      "large": "http://example.org/assets/large-avatars/avatar-24.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": null
  },
  "users": [
    {
      "id": 7451,
      "first_name": "Norene",
      "last_name": "Fisher",
      "grade": 2024,
      "bio": "Occaecati eos exercitationem ipsum.",
      "school_name": "Ad rerum quas repudiandae.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-24.png",
        "small": "http://example.org/assets/small-avatars/avatar-24.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
        "large": "http://example.org/assets/large-avatars/avatar-24.png"
      },
      "gender": "non_binary",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    {
      "id": 7452,
      "first_name": "Jan",
      "last_name": "Bosco",
      "grade": 2011,
      "bio": "Voluptatem vero ut ut.",
      "school_name": "Et vel omnis est.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-19.png",
        "small": "http://example.org/assets/small-avatars/avatar-19.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-19.png",
        "large": "http://example.org/assets/large-avatars/avatar-19.png"
      },
      "gender": "non_binary",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  ],
  "deanonymizations": [

  ]
}</pre>
