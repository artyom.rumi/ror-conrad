# v1/Chats API

## List of chats

### GET /api/v1/chats
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Cpr67B2kk2kx9b8bVX2Hy2Nk
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/chats</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;cba3803756dcde62a49cdf709d8fc277&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 00bb4247-7f80-4fd6-b6a4-0c04f5385c41
X-Runtime: 0.231359
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 6524</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 540,
    "pubnub_channel_name": "incognito_chat_540",
    "updated_at": "2020-02-10T09:00:04.259+00:00",
    "incognito": true,
    "feed_id": null,
    "comment_id": null,
    "name": null,
    "is_group": true,
    "owner": {
      "id": 7431,
      "first_name": "Wilmer",
      "last_name": "Hilpert",
      "grade": 2024,
      "bio": "Molestias ratione iure tempore.",
      "school_name": "Voluptatem beatae sunt autem.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-25.png",
        "small": "http://example.org/assets/small-avatars/avatar-25.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
        "large": "http://example.org/assets/large-avatars/avatar-25.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    "users": [
      {
        "id": 7431,
        "first_name": "Wilmer",
        "last_name": "Hilpert",
        "grade": 2024,
        "bio": "Molestias ratione iure tempore.",
        "school_name": "Voluptatem beatae sunt autem.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-25.png",
          "small": "http://example.org/assets/small-avatars/avatar-25.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
          "large": "http://example.org/assets/large-avatars/avatar-25.png"
        },
        "gender": "female",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": null
      },
      {
        "id": 7432,
        "first_name": "Garrett",
        "last_name": "Dicki",
        "grade": 2011,
        "bio": "Quis eveniet omnis commodi.",
        "school_name": "Voluptatem voluptates dolor corrupti.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-20.png",
          "small": "http://example.org/assets/small-avatars/avatar-20.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-20.png",
          "large": "http://example.org/assets/large-avatars/avatar-20.png"
        },
        "gender": "non_binary",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": "uninvited"
      }
    ],
    "deanonymizations": [
      {
        "id": 168,
        "user_id": 7431
      }
    ]
  },
  {
    "id": 538,
    "pubnub_channel_name": "chat_538",
    "updated_at": "2020-02-10T09:00:04.207+00:00",
    "incognito": false,
    "feed_id": null,
    "comment_id": null,
    "name": null,
    "is_group": true,
    "owner": {
      "id": 7431,
      "first_name": "Wilmer",
      "last_name": "Hilpert",
      "grade": 2024,
      "bio": "Molestias ratione iure tempore.",
      "school_name": "Voluptatem beatae sunt autem.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-25.png",
        "small": "http://example.org/assets/small-avatars/avatar-25.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
        "large": "http://example.org/assets/large-avatars/avatar-25.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    "users": [
      {
        "id": 7431,
        "first_name": "Wilmer",
        "last_name": "Hilpert",
        "grade": 2024,
        "bio": "Molestias ratione iure tempore.",
        "school_name": "Voluptatem beatae sunt autem.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-25.png",
          "small": "http://example.org/assets/small-avatars/avatar-25.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
          "large": "http://example.org/assets/large-avatars/avatar-25.png"
        },
        "gender": "female",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": null
      },
      {
        "id": 7432,
        "first_name": "Garrett",
        "last_name": "Dicki",
        "grade": 2011,
        "bio": "Quis eveniet omnis commodi.",
        "school_name": "Voluptatem voluptates dolor corrupti.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-20.png",
          "small": "http://example.org/assets/small-avatars/avatar-20.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-20.png",
          "large": "http://example.org/assets/large-avatars/avatar-20.png"
        },
        "gender": "non_binary",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": "uninvited"
      }
    ],
    "deanonymizations": [

    ]
  },
  {
    "id": 537,
    "pubnub_channel_name": "chat_537",
    "updated_at": "2020-02-10T09:00:04.176+00:00",
    "incognito": false,
    "feed_id": null,
    "comment_id": null,
    "name": null,
    "is_group": true,
    "owner": {
      "id": 7431,
      "first_name": "Wilmer",
      "last_name": "Hilpert",
      "grade": 2024,
      "bio": "Molestias ratione iure tempore.",
      "school_name": "Voluptatem beatae sunt autem.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-25.png",
        "small": "http://example.org/assets/small-avatars/avatar-25.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
        "large": "http://example.org/assets/large-avatars/avatar-25.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    "users": [
      {
        "id": 7431,
        "first_name": "Wilmer",
        "last_name": "Hilpert",
        "grade": 2024,
        "bio": "Molestias ratione iure tempore.",
        "school_name": "Voluptatem beatae sunt autem.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-25.png",
          "small": "http://example.org/assets/small-avatars/avatar-25.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
          "large": "http://example.org/assets/large-avatars/avatar-25.png"
        },
        "gender": "female",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": null
      },
      {
        "id": 7432,
        "first_name": "Garrett",
        "last_name": "Dicki",
        "grade": 2011,
        "bio": "Quis eveniet omnis commodi.",
        "school_name": "Voluptatem voluptates dolor corrupti.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-20.png",
          "small": "http://example.org/assets/small-avatars/avatar-20.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-20.png",
          "large": "http://example.org/assets/large-avatars/avatar-20.png"
        },
        "gender": "non_binary",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": "uninvited"
      }
    ],
    "deanonymizations": [

    ]
  }
]</pre>
