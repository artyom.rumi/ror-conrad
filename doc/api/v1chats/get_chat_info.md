# v1/Chats API

## Get chat info

### GET /api/v1/chats/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the chat | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: smVHngCP9XN5Kfu1hC4wsL2U
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/chats/553</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;587d3f570223fd61b2d8dba74c2c165a&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 72ffa63d-94b6-4755-b484-f0614fe9186b
X-Runtime: 0.090429
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 840</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 553,
  "pubnub_channel_name": "chat_553",
  "updated_at": "2020-02-10T09:00:05.521+00:00",
  "incognito": false,
  "feed_id": null,
  "comment_id": null,
  "name": null,
  "is_group": true,
  "owner": {
    "id": 7440,
    "first_name": "Racheal",
    "last_name": "Kihn",
    "grade": 2022,
    "bio": "Nemo optio et possimus.",
    "school_name": "Non dolores recusandae in.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-10.png",
      "small": "http://example.org/assets/small-avatars/avatar-10.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-10.png",
      "large": "http://example.org/assets/large-avatars/avatar-10.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": null
  },
  "users": [

  ],
  "deanonymizations": [

  ]
}</pre>
