# v1/Chats API

## create chat

### POST /api/v1/chats

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_ids | Users ids to chat with (current user id will be added implicitly if not present in array) | true |  |
| name | Chat name | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: H1ZwFniReq5b732NVWA5ttdr
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/chats</pre>

#### Body

<pre>{"user_ids":[7446],"name":"Friends"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;8c92383bf27e50f6d07af6b91ce59178&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 2cb9a555-a57c-4c73-bb05-1a5ebb29e4ee
X-Runtime: 0.172782
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2134</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 557,
  "pubnub_channel_name": "chat_557",
  "updated_at": "2020-02-10T09:00:06.067+00:00",
  "incognito": false,
  "feed_id": null,
  "comment_id": null,
  "name": "Friends",
  "is_group": true,
  "owner": {
    "id": 7445,
    "first_name": "Alphonse",
    "last_name": "Dickens",
    "grade": 2025,
    "bio": "Sint harum quo rerum.",
    "school_name": "Optio deleniti vel veritatis.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-12.png",
      "small": "http://example.org/assets/small-avatars/avatar-12.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-12.png",
      "large": "http://example.org/assets/large-avatars/avatar-12.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": null
  },
  "users": [
    {
      "id": 7445,
      "first_name": "Alphonse",
      "last_name": "Dickens",
      "grade": 2025,
      "bio": "Sint harum quo rerum.",
      "school_name": "Optio deleniti vel veritatis.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-12.png",
        "small": "http://example.org/assets/small-avatars/avatar-12.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-12.png",
        "large": "http://example.org/assets/large-avatars/avatar-12.png"
      },
      "gender": "non_binary",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    {
      "id": 7446,
      "first_name": "Leia",
      "last_name": "Breitenberg",
      "grade": 2012,
      "bio": "Est ullam culpa quis.",
      "school_name": "Ipsum cum cumque rerum.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-2.png",
        "small": "http://example.org/assets/small-avatars/avatar-2.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-2.png",
        "large": "http://example.org/assets/large-avatars/avatar-2.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  ],
  "deanonymizations": [

  ]
}</pre>
