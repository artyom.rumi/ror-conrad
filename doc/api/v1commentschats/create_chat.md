# v1/Comments/Chats API

## create chat

### POST /api/v1/comments/:comment_id/chats

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| comment_id | Feed Id for chat | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 6d6u9fo8bdAfKTzLn3kNceCn
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/comments/380/chats</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;dcc57a9572b146c4b144debe5bbcd48d&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: a56d6ec9-6ebe-4cdc-a02d-25b13f4bed5d
X-Runtime: 0.209446
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2141</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 560,
  "pubnub_channel_name": "incognito_chat_560",
  "updated_at": "2020-02-10T09:00:09.345+00:00",
  "incognito": true,
  "feed_id": 1169,
  "comment_id": 380,
  "name": null,
  "is_group": false,
  "owner": {
    "id": 7513,
    "first_name": "Claire",
    "last_name": "Abshire",
    "grade": 2028,
    "bio": "Nemo est rerum eaque.",
    "school_name": "Sed dignissimos porro nesciunt.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-10.png",
      "small": "http://example.org/assets/small-avatars/avatar-10.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-10.png",
      "large": "http://example.org/assets/large-avatars/avatar-10.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": null
  },
  "users": [
    {
      "id": 7513,
      "first_name": "Claire",
      "last_name": "Abshire",
      "grade": 2028,
      "bio": "Nemo est rerum eaque.",
      "school_name": "Sed dignissimos porro nesciunt.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-10.png",
        "small": "http://example.org/assets/small-avatars/avatar-10.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-10.png",
        "large": "http://example.org/assets/large-avatars/avatar-10.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    {
      "id": 7515,
      "first_name": "Perla",
      "last_name": "Jacobs",
      "grade": 2028,
      "bio": "Perspiciatis non accusantium eius.",
      "school_name": "Voluptatem et fugit ea.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-25.png",
        "small": "http://example.org/assets/small-avatars/avatar-25.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
        "large": "http://example.org/assets/large-avatars/avatar-25.png"
      },
      "gender": "male",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  ],
  "deanonymizations": [

  ]
}</pre>
