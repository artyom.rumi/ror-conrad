# v1/My/OutContacts API

## List of outline contacts

### GET /api/v1/my/outline_contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: QA8W5rKxd5kQN8STKmmkxBWm
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/outline_contacts?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;8d4d972da1134e62b892b05e99fc174d&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 4aae003f-d1e0-4b33-8882-a45bf331311d
X-Runtime: 0.050127
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 174</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "users": [
    {
      "id": 177,
      "phone_number": "+11234567891",
      "name": null,
      "invited": false
    },
    {
      "id": 178,
      "phone_number": "+11234567892",
      "name": null,
      "invited": false
    }
  ],
  "meta": {
    "users_count": 2
  }
}</pre>
