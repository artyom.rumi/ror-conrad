# v1/Device API

## update user device

### PATCH /api/v1/device

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| push_token | FCM registration token | false |  |
| phone_number | Phone number | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Hm9GgnCyZrGiwoYiS3gc9BjN
Host: example.org
Cookie: </pre>

#### Route

<pre>PATCH /api/v1/device</pre>

#### Body

<pre>{"push_token":"bLa1Bla2bLA3:rEg1sTrAt10nT0k3nfr0mfcmS3rvic3","phone_number":"+88005553535"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;e7f5bc451005cf31e5a29a6dae99fbf4&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 07131182-454e-4244-babc-be1823608ac1
X-Runtime: 0.059653
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 116</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 7656,
  "user_id": 7545,
  "phone_number": "+88005553535",
  "push_token": "bLa1Bla2bLA3:rEg1sTrAt10nT0k3nfr0mfcmS3rvic3"
}</pre>
