# v1/My/OwnFeeds API

## user&#39;s anonymous feeds

### GET /api/v1/my/own_feeds

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| incognito | Incognito status | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: iPGAkXfh3nuboUUiVjMx9Vo2
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/own_feeds?page=1&amp;per_page=5&amp;incognito=true</pre>

#### Query Parameters

<pre>page: 1
per_page: 5
incognito: true</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;3cb9dadd3fc429cbb2e5885f795b9f1f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 84ec3001-0f2f-4943-851c-ff3f783baa0b
X-Runtime: 0.041096
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 282</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1326,
    "image": null,
    "user_id": 8011,
    "incognito": true,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-10T09:00:33.904+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
