# v1/My/OwnFeeds API

## List of feeds

### GET /api/v1/my/own_feeds

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: ThDNYSgtyg9q2733n6NmZNMv
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/own_feeds?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;0fdce4629b27efbc300047d901375ec8&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e3551d80-be4b-4ed3-b985-67d060bd256b
X-Runtime: 0.086221
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 565</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1304,
    "image": null,
    "user_id": 8002,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-08T09:00:33.447+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  },
  {
    "id": 1303,
    "image": null,
    "user_id": 8002,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-07T09:00:33.443+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
