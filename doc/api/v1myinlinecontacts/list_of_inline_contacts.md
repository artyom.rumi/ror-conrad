# v1/My/InlineContacts API

## List of inline contacts

### GET /api/v1/my/inline_contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: aMmGJRLZYFwhVFtzXMoJusvi
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/inline_contacts?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;42f47b3d2757bdbc03ac48357298dbd7&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 20f4608f-6ac8-4a77-ac0e-ee2a8c1827da
X-Runtime: 0.072707
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1335</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "users": [
    {
      "id": 7934,
      "first_name": "1",
      "last_name": "Rodriguez",
      "grade": 2021,
      "bio": "Aut reiciendis aperiam sunt.",
      "school_name": "Exercitationem ut enim est.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-18.png",
        "small": "http://example.org/assets/small-avatars/avatar-18.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-18.png",
        "large": "http://example.org/assets/large-avatars/avatar-18.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    },
    {
      "id": 7935,
      "first_name": "2",
      "last_name": "Baumbach",
      "grade": 2014,
      "bio": "Sit similique quia eum.",
      "school_name": "Voluptatem molestias distinctio explicabo.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-17.png",
        "small": "http://example.org/assets/small-avatars/avatar-17.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-17.png",
        "large": "http://example.org/assets/large-avatars/avatar-17.png"
      },
      "gender": "male",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  ],
  "meta": {
    "users_count": 2
  }
}</pre>
