# v2/Received props API

## list of unviewed props and props that viewed in the last 3 days

### GET /api/v2/received_props
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: b2r7HkWCCNoQvw61LExmdZJX
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v2/received_props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;d22960dfe16f2c8c19a13a77250c7f86&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 53d945d1-b225-496a-bdd8-0eb256c087dd
X-Runtime: 0.089105
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 788</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 607,
    "created_at": "2020-02-10T09:00:56.168+00:00",
    "sender_gender": "male",
    "viewed_at": null,
    "type": "ordinary",
    "prop": {
      "id": 1046,
      "content": "Ut omnis consequatur voluptates.",
      "emoji": "😍",
      "gender": null
    },
    "reaction": {
      "id": 61,
      "content": "Iusto maiores voluptas sed.",
      "photo": null,
      "read_at": null,
      "created_at": "2020-02-10T09:00:56.175+00:00"
    }
  },
  {
    "id": 608,
    "created_at": "2020-02-10T09:00:56.200+00:00",
    "sender_gender": "non_binary",
    "viewed_at": "2020-02-08T09:00:56.177+00:00",
    "type": "ordinary",
    "prop": {
      "id": 1047,
      "content": "Aut dolorum natus repellendus.",
      "emoji": "😍",
      "gender": "female"
    },
    "reaction": null
  },
  {
    "id": 609,
    "created_at": "2020-02-10T09:00:56.381+00:00",
    "sender_gender": "male",
    "viewed_at": null,
    "type": "custom",
    "prop": {
      "content": "Non eligendi est rerum.",
      "emoji": "😍"
    },
    "reaction": null
  }
]</pre>
