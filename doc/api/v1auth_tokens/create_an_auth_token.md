# v1/Auth tokens API

## Create an auth token

### POST /api/v1/auth_tokens

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_number | Phone number | true |  |
| verification_code | Verification code received in SMS message | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/auth_tokens</pre>

#### Body

<pre>{"phone_number":"+12345","verification_code":"1234"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7df98c8713a1b946b4303cd6d708d944&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: fbf90ecc-8fa8-49a3-ae1e-c80e1c01d43d
X-Runtime: 0.067086
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 96</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "value": "meUyBccgshJ71aqC2tUoMKYp",
  "user_id": 7383,
  "expires_at": "2020-08-08T08:59:58.761+01:00"
}</pre>
