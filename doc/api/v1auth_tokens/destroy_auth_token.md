# v1/Auth tokens API

## Destroy auth token

### DELETE /api/v1/logout
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 9nqUsHHuyNdG7Bdjs7rWtHm4
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/logout</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 22a91a7c-a506-47ac-b5b2-101b4700a160
X-Runtime: 0.081126
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

