# v1/Auth tokens API

## Invalid or expired verification code

### POST /api/v1/auth_tokens

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_number | Phone number | true |  |
| verification_code | Verification code received in SMS message | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/auth_tokens</pre>

#### Body

<pre>{"phone_number":"+12345"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: f800d8da-036e-4055-b556-b9af4ea894fc
X-Runtime: 0.145684
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 40</pre>

#### Status

<pre>422 Unprocessable Entity</pre>

#### Body

<pre>{
  "error": "Verification code is invalid"
}</pre>
