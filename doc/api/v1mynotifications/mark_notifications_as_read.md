# v1/My/Notifications API

## Mark notifications as read

### PATCH /api/v1/my/notifications

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| ids | Notification Ids | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: rJjnc6MgzctutZMhRbaT7Sox
Host: example.org
Cookie: </pre>

#### Route

<pre>PATCH /api/v1/my/notifications</pre>

#### Body

<pre>{"ids":[553,554]}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;4d2917f6b7ff1c341d52ac4829e3cfe5&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: fe9b21b7-947a-421b-b7c2-2a9a815aeb8b
X-Runtime: 0.025961
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 414</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 553,
    "user_id": 7984,
    "notifiable_type": "Comment",
    "notifiable_id": 426,
    "status": "Read",
    "action_name": "new_comment",
    "created_at": "2020-02-10T09:00:32.625+00:00",
    "updated_at": "2020-02-10T09:00:32.674+00:00"
  },
  {
    "id": 554,
    "user_id": 7984,
    "notifiable_type": "Feed",
    "notifiable_id": 1295,
    "status": "Read",
    "action_name": "new_repost",
    "created_at": "2020-02-10T09:00:32.632+00:00",
    "updated_at": "2020-02-10T09:00:32.678+00:00"
  }
]</pre>
