# v1/My/Notifications API

## Mark notification as read

### PATCH /api/v1/my/notifications/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Notification Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: qB9oSqMCTojeyMGDNEhcAEkd
Host: example.org
Cookie: </pre>

#### Route

<pre>PATCH /api/v1/my/notifications/537</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;0748738f2705023c6c31c92842540eca&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f48e78eb-6b0d-4749-9477-729071300783
X-Runtime: 0.033310
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 209</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 537,
    "user_id": 7975,
    "notifiable_type": "Comment",
    "notifiable_id": 422,
    "status": "Read",
    "action_name": "new_comment",
    "created_at": "2020-02-10T09:00:32.087+00:00",
    "updated_at": "2020-02-10T09:00:32.111+00:00"
  }
]</pre>
