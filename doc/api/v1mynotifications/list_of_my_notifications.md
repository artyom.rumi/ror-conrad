# v1/My/Notifications API

## List of my notifications

### GET /api/v1/my/notifications
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: mBraoBQhqAod4dGdrn46WHtt
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/notifications</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;753da9ee300909f89b410b01d61b7205&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f593bfbb-4593-45b5-b027-dfe895b17ade
X-Runtime: 0.080435
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2921</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 532,
    "type": "new_friend",
    "is_unread": true,
    "created_at": "2020-02-10T09:00:31.863+00:00",
    "image": {
      "original": "http://lvh.me:5000/images/avatars/avatar-11.png",
      "small": "http://lvh.me:5000/images/small-avatars/avatar-11.png",
      "medium": "http://lvh.me:5000/images/medium-avatars/avatar-11.png",
      "large": "http://lvh.me:5000/images/large-avatars/avatar-11.png"
    },
    "message": "You're now friends with Tomas Satterfield",
    "payload": {
      "friend_id": 7974
    }
  },
  {
    "id": 533,
    "type": "72h_expired_friend",
    "is_unread": true,
    "created_at": "2020-02-10T09:00:31.866+00:00",
    "image": {
      "original": "http://lvh.me:5000/images/avatars/avatar-11.png",
      "small": "http://lvh.me:5000/images/small-avatars/avatar-11.png",
      "medium": "http://lvh.me:5000/images/medium-avatars/avatar-11.png",
      "large": "http://lvh.me:5000/images/large-avatars/avatar-11.png"
    },
    "message": "Watch out! you have 72 hours to keep friend with Tomas Satterfield.",
    "payload": {
      "friend_id": 7974
    }
  },
  {
    "id": 534,
    "type": "12h_expired_friend",
    "is_unread": true,
    "created_at": "2020-02-10T09:00:31.869+00:00",
    "image": {
      "original": "http://lvh.me:5000/images/avatars/avatar-11.png",
      "small": "http://lvh.me:5000/images/small-avatars/avatar-11.png",
      "medium": "http://lvh.me:5000/images/medium-avatars/avatar-11.png",
      "large": "http://lvh.me:5000/images/large-avatars/avatar-11.png"
    },
    "message": "Act now! You have 12 hours before expiring the friend status with Tomas Satterfield",
    "payload": {
      "friend_id": 7974
    }
  },
  {
    "id": null,
    "type": "new_like",
    "is_unread": false,
    "created_at": "2020-02-10T09:00:31.836+00:00",
    "image": {
      "original": "http://lvh.me:5000/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWTA9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--101a83293a2065496286d48b9cf6acec029ac2f4/avatar.png",
      "small": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWTA9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--101a83293a2065496286d48b9cf6acec029ac2f4/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTVRVd2VERTFNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--6961321111510a9086c6dbc85bb89f5b4d42e35b/avatar.png",
      "medium": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWTA9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--101a83293a2065496286d48b9cf6acec029ac2f4/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTWpRd2VESTBNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--ae12e1e8ac781d9820431268ef8fc8f409f55b8d/avatar.png",
      "large": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWTA9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--101a83293a2065496286d48b9cf6acec029ac2f4/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTkRnd2VEUTRNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--22e536a873c911c90d972fe877286eed82b6351b/avatar.png"
    },
    "message": "A someone liked your post",
    "payload": {
      "feed_id": 1284,
      "activities_id": [
        284
      ],
      "notifications_id": [
        529
      ],
      "grouped": true
    }
  }
]</pre>
