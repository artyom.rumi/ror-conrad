# v1/FriendIntentions API

## Friend intention

### POST /api/v1/friend_intentions

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| target_id | User id for friend intentions | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: tEkt19P1R93AeciGs3fi6BRU
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/friend_intentions</pre>

#### Body

<pre>{"target_id":7705}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;36c526a1d3bfa4b82a3133bcfd6ead63&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 228c6d75-490c-48fb-a646-c1ea707efee8
X-Runtime: 0.059760
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 26</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 66,
  "target_id": 7705
}</pre>
