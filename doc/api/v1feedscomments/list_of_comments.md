# v1/Feeds/Comments API

## List of comments

### GET /api/v1/feeds/:feed_id/comments

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for comments | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: ardc3X3Bp6Q8YmZccdhjpwDR
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/feeds/1182/comments</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7c3bb7249658eac474fe39a2ab6d8639&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 19b05bfe-e102-4777-8d9a-d9dd7776f1fd
X-Runtime: 0.089309
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1733</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 385,
    "text": "Fuga id consequatur provident.",
    "status": "Read",
    "created_at": "2020-02-10T09:00:14.588+00:00",
    "user": {
      "id": 7593,
      "first_name": "Bert",
      "last_name": "Smitham",
      "grade": 2016,
      "bio": "Voluptas quis saepe incidunt.",
      "gender": "male",
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "friendship_status": "uninvited",
      "school_name": "Labore fugit iure et."
    },
    "chat": {
      "id": 562,
      "pubnub_channel_name": "chat_562",
      "updated_at": "2020-02-10T09:00:14.655+00:00",
      "incognito": false,
      "feed_id": null,
      "comment_id": 385,
      "name": null,
      "is_group": true,
      "owner": {
        "id": 7591,
        "first_name": "Theo",
        "last_name": "Breitenberg",
        "grade": 2021,
        "bio": "Quidem sapiente aspernatur atque.",
        "school_name": "Voluptas natus quibusdam vel.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-1.png",
          "small": "http://example.org/assets/small-avatars/avatar-1.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-1.png",
          "large": "http://example.org/assets/large-avatars/avatar-1.png"
        },
        "gender": "male",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 0,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": null
      },
      "users": [

      ],
      "deanonymizations": [

      ]
    }
  },
  {
    "id": 386,
    "text": "Qui et dolores delectus.",
    "status": "Read",
    "created_at": "2020-02-09T09:00:14.621+00:00",
    "user": {
      "id": 7594,
      "first_name": "Martine",
      "last_name": "Schumm",
      "grade": 2021,
      "bio": "Amet inventore non eius.",
      "gender": "male",
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "friendship_status": "uninvited",
      "school_name": "Nihil iste distinctio vitae."
    },
    "chat": null
  }
]</pre>
