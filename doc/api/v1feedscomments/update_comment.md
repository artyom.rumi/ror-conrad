# v1/Feeds/Comments API

## Update comment

### PUT /api/v1/feeds/:feed_id/comments/:comment_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for comments | true |  |
| text | Text comment | true |  |
| comment_id | Id for comment | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: pYQmCu5ud8GVf6NUKbarLKUd
Host: example.org
Cookie: </pre>

#### Route

<pre>PUT /api/v1/feeds/1186/comments/393</pre>

#### Body

<pre>{"text":"Updated text"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;dccf6557ac7c892bb4f3f700a94db90b&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: af131ba1-47e1-4c97-b2d1-dd00508736d4
X-Runtime: 0.029620
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 414</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 393,
  "text": "Updated text",
  "status": "Read",
  "created_at": "2020-02-10T09:00:15.358+00:00",
  "user": {
    "id": 7608,
    "first_name": "Kera",
    "last_name": "Effertz",
    "grade": 2018,
    "bio": "Unde dolores harum animi.",
    "gender": "female",
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "friendship_status": null,
    "school_name": "Dolor optio vero et."
  },
  "chat": null
}</pre>
