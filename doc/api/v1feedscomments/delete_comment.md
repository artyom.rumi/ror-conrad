# v1/Feeds/Comments API

## Delete comment

### DELETE /api/v1/feeds/:feed_id/comments/:comment_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for comments | true |  |
| comment_id | Id for comment | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: eZ77zoZWorTjVVLd74z1SXyx
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/feeds/1190/comments/398</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;83b273b3bb8ebcd22a8027655a5d3aa1&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: a5a82cc9-9dc4-4b7d-91e4-c3fba7c01b59
X-Runtime: 0.097720
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 426</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 397,
    "text": "Error dicta ipsa ab.",
    "status": "Read",
    "created_at": "2020-02-10T09:00:16.066+00:00",
    "user": {
      "id": 7617,
      "first_name": "Roxann",
      "last_name": "Stark",
      "grade": 2021,
      "bio": "Neque iusto dignissimos ipsa.",
      "gender": "female",
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "friendship_status": null,
      "school_name": "Vel quae ut saepe."
    },
    "chat": null
  }
]</pre>
