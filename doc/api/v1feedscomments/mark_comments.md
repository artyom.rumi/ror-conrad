# v1/Feeds/Comments API

## Mark comments

### POST /api/v1/feeds/:feed_id/mark_comments

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for comments | true |  |
| commentIDs | Comment Ids for mark | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: WemvTJ2akeyNvCPUXAPcJTAJ
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feeds/1206/mark_comments</pre>

#### Body

<pre>{"commentIDs":[413]}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: cec7225e-9f1a-439e-84a6-35e3913c2917
X-Runtime: 0.010658
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 80</pre>

#### Status

<pre>404 Not Found</pre>

#### Body

<pre>{
  "error": "Couldn't find Feed with 'id'=1206 [WHERE \"feeds\".\"user_id\" = $1]"
}</pre>
