# v1/Feeds/Comments API

## Create comment

### POST /api/v1/feeds/:feed_id/comments

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for comments | true |  |
| text | Comment text | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: po13BCnUt9m4neo3Xiefdubk
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feeds/1178/comments</pre>

#### Body

<pre>{"text":"Some text"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5b2206e7c526e17edc90bb022723f11f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e47957dd-efc7-4998-ac3e-d0a9cc4c09d6
X-Runtime: 0.914058
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 419</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>[
  {
    "id": 384,
    "text": "Some text",
    "status": "Unread",
    "created_at": "2020-02-10T09:00:13.438+00:00",
    "user": {
      "id": 7586,
      "first_name": "Bryce",
      "last_name": "Greenholt",
      "grade": 2016,
      "bio": "Nulla magnam optio quidem.",
      "gender": "male",
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "friendship_status": null,
      "school_name": "Vel in eius assumenda."
    },
    "chat": null
  }
]</pre>
