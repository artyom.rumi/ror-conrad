# v1/Feedback API

## Send feedback

### POST /api/v1/feedbacks

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| text | Feedback text | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: QVt6ftmQ4rpdUrwVaZ1Au4sD
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feedbacks</pre>

#### Body

<pre>{"text":"Facere vero recusandae illo."}</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 34141d67-25d2-4cc9-8388-ca714fafa651
X-Runtime: 0.056958
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

