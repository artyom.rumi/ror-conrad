# v1/Special props from Emily API

## list of special props

### GET /api/v1/special_props
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: pybep1ELnEquXEUt4aBqy6pd
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/special_props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7b578cd67e78218d7b3934d747ff86c4&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: ab1707fd-8645-4f59-ab7d-93897a18bd8c
X-Runtime: 0.147860
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 261</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1031,
    "content": "Est numquam animi voluptatem.",
    "emoji": "😍",
    "gender": "female"
  },
  {
    "id": 1029,
    "content": "Sapiente minus adipisci necessitatibus.",
    "emoji": "😍",
    "gender": "female"
  },
  {
    "id": 1030,
    "content": "Nulla aut nemo sunt.",
    "emoji": "😍",
    "gender": "male"
  }
]</pre>
