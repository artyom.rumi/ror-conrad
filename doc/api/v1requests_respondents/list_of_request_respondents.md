# v1/requests Respondents API

## List of request respondents

### GET /api/v1/requests/:request_id/respondents

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| request_id | ID of request which was received by the respondents | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: TQPM1Ynd9KQUMoGVQgYJQsJt
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/requests/311/respondents</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;ee70a88fb7988020b7201e70043ace57&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: c19cf3f6-d68d-4ea0-91af-6036feed9172
X-Runtime: 0.169544
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 3276</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8167,
    "first_name": "Kareem",
    "last_name": "Schroeder",
    "grade": 2028,
    "bio": "Recusandae est hic nostrum.",
    "school_name": "Mollitia ullam molestias fugit.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-4.png",
      "small": "http://example.org/assets/small-avatars/avatar-4.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-4.png",
      "large": "http://example.org/assets/large-avatars/avatar-4.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8168,
    "first_name": "Abe",
    "last_name": "Schultz",
    "grade": 2016,
    "bio": "Similique doloribus provident odit.",
    "school_name": "Iusto praesentium quia dolores.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-8.png",
      "small": "http://example.org/assets/small-avatars/avatar-8.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-8.png",
      "large": "http://example.org/assets/large-avatars/avatar-8.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8169,
    "first_name": "Deandre",
    "last_name": "Wisozk",
    "grade": 2014,
    "bio": "Sint nihil architecto maxime.",
    "school_name": "Libero voluptatibus possimus qui.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-24.png",
      "small": "http://example.org/assets/small-avatars/avatar-24.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
      "large": "http://example.org/assets/large-avatars/avatar-24.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8170,
    "first_name": "Orville",
    "last_name": "Dibbert",
    "grade": 2030,
    "bio": "Rerum perferendis consequatur accusantium.",
    "school_name": "Eligendi autem illum voluptatem.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-26.png",
      "small": "http://example.org/assets/small-avatars/avatar-26.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-26.png",
      "large": "http://example.org/assets/large-avatars/avatar-26.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8171,
    "first_name": "Nella",
    "last_name": "McCullough",
    "grade": 2013,
    "bio": "Aperiam quam magnam est.",
    "school_name": "Itaque quo autem est.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-16.png",
      "small": "http://example.org/assets/small-avatars/avatar-16.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-16.png",
      "large": "http://example.org/assets/large-avatars/avatar-16.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
