# v1/CollocutorMasks/Blocking API

## create new blocking to specific collocutor mask

### POST /api/v1/collocutor_masks/:collocutor_mask_id/blocking

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| collocutor_mask_id | ID of the collocutor mask to block | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: GYtME5UHQcJc5ZQBFzZngWPj
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/collocutor_masks/561/blocking</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7f9eeb781c38f81d92db97ad806f271b&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 60597301-1aab-439c-968a-696dba0b3134
X-Runtime: 0.073496
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 123</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 171,
  "user_id": 7453,
  "blockable_type": "CollocutorMask",
  "blockable_id": 561,
  "created_at": "2020-02-10T09:00:06.803+00:00"
}</pre>
