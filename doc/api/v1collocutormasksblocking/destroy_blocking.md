# v1/CollocutorMasks/Blocking API

## destroy blocking

### DELETE /api/v1/collocutor_masks/:collocutor_mask_id/blocking

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| collocutor_mask_id | ID of the collocutor mask to unblock | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 7VoKYiJcSXX6NfztmrV9YmCL
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/collocutor_masks/565/blocking</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 5aad305b-f090-4249-b42f-eef2103fe631
X-Runtime: 0.030940
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

