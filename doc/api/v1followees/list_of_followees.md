# v1/Followees API

## List of followees

### GET /api/v1/followees

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: ZowsH2EgXFEu2rBnVMywwkuE
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/followees</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;819c9681d4e18a9661fe3372c503e648&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 2776143c-5ec0-4bc0-91ad-919040be15a6
X-Runtime: 0.073305
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1922</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7664,
    "first_name": "Brad",
    "last_name": "Pitt",
    "grade": 2028,
    "bio": "Commodi eum quia voluptatem.",
    "school_name": "Fugit in et accusantium.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-20.png",
      "small": "http://example.org/assets/small-avatars/avatar-20.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-20.png",
      "large": "http://example.org/assets/large-avatars/avatar-20.png"
    },
    "gender": "female",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 7661,
    "first_name": "George",
    "last_name": "Clooney",
    "grade": 2018,
    "bio": "Blanditiis quia et qui.",
    "school_name": "Est earum sunt fugiat.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-19.png",
      "small": "http://example.org/assets/small-avatars/avatar-19.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-19.png",
      "large": "http://example.org/assets/large-avatars/avatar-19.png"
    },
    "gender": "female",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 7662,
    "first_name": "Matt",
    "last_name": "Damon",
    "grade": 2024,
    "bio": "Minima molestiae eum nesciunt.",
    "school_name": "Fugit in et accusantium.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-18.png",
      "small": "http://example.org/assets/small-avatars/avatar-18.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-18.png",
      "large": "http://example.org/assets/large-avatars/avatar-18.png"
    },
    "gender": "female",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
