# v1/Followees API

## List of folowees matched by keywords

### GET /api/v1/followees

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Emw3oQdq5qzxPgUAqxEKBRk9
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/followees?keywords=Brad</pre>

#### Query Parameters

<pre>keywords: Brad</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;086e376c581c0fec228c0ca9a867e8b4&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: cb0d11b0-486e-4b80-9371-3fc9b8568fcb
X-Runtime: 0.054157
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 635</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7690,
    "first_name": "Brad",
    "last_name": "Pitt",
    "grade": 2013,
    "bio": "Aut voluptatibus vel est.",
    "school_name": "Odit et doloribus unde.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-20.png",
      "small": "http://example.org/assets/small-avatars/avatar-20.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-20.png",
      "large": "http://example.org/assets/large-avatars/avatar-20.png"
    },
    "gender": "male",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
