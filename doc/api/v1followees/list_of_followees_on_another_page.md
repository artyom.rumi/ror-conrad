# v1/Followees API

## List of followees on another page

### GET /api/v1/followees

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: fYQ6WZx5BWiERjAoMKeVP3Xy
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/followees?page=2&amp;per_page=2</pre>

#### Query Parameters

<pre>page: 2
per_page: 2</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5e45fb810c877b5994c24f61a6d8e9ce&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: ef8db57e-b5f8-42e2-850d-148435db7b66
X-Runtime: 0.026662
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 647</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7683,
    "first_name": "Matt",
    "last_name": "Damon",
    "grade": 2011,
    "bio": "Et necessitatibus aut id.",
    "school_name": "Sed tempore deserunt perspiciatis.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-28.png",
      "small": "http://example.org/assets/small-avatars/avatar-28.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-28.png",
      "large": "http://example.org/assets/large-avatars/avatar-28.png"
    },
    "gender": "male",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
