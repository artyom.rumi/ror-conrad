# v1/Received props API

## list of ordinary unviewed props and props that viewed in the last 3 days

### GET /api/v1/received_props
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: cC3x9E5wK66VYRCGSpri6KA4
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/received_props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;1b016971060bb126490f1a8c1110864b&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: ab58f0fb-3bb8-4233-9154-c29a9577400b
X-Runtime: 0.149621
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 589</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 588,
    "created_at": "2020-02-10T09:00:41.486+00:00",
    "sender_gender": "non_binary",
    "viewed_at": "2020-02-08T09:00:41.438+00:00",
    "type": "ordinary",
    "prop": {
      "id": 1006,
      "content": "Amet illum nihil quod.",
      "emoji": "😍",
      "gender": null
    },
    "reaction": null
  },
  {
    "id": 587,
    "created_at": "2020-02-10T09:00:41.412+00:00",
    "sender_gender": "female",
    "viewed_at": null,
    "type": "ordinary",
    "prop": {
      "id": 1005,
      "content": "Veniam doloribus quaerat ad.",
      "emoji": "😍",
      "gender": "male"
    },
    "reaction": {
      "id": 57,
      "content": "Dolore molestiae harum quia.",
      "photo": null,
      "read_at": null,
      "created_at": "2020-02-10T09:00:41.428+00:00"
    }
  }
]</pre>
