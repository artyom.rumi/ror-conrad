# v1/My/Contacts API

## Synchronize community for new contacts

### POST /api/v1/my/contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_numbers | Phone numbers from user contacts list | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: z5kXhp3kuMjwkTng4N6iSygT
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/my/contacts</pre>

#### Body

<pre>{"phone_numbers":[{"name":"Hae","phone_number":"+15009466141"}]}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;e4f479ca0601f0cc9f6b33c083aa0944&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 301bbb8a-978f-4def-9afd-455e9cd4cd4f
X-Runtime: 0.031767
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 69</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 163,
    "user_id": 7851,
    "phone_number": "+15009466141",
    "errors": [

    ]
  }
]</pre>
