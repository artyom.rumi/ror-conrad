# v1/My/Contacts API

## Synchronize contacts list

### POST /api/v1/my/contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_numbers | Phone numbers from user contacts list | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Fh1NZnc4FYbbpyRbAoQ3Sw7u
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/my/contacts</pre>

#### Body

<pre>{"phone_numbers":[{"name":"John","phone_number":"1234567890"},{"name":"Piter","phone_number":"1234567891"}]}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;1c717d5275dd0819a6393355ec327aee&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 0905e397-2de6-40ce-a3f4-fb2cc073fe18
X-Runtime: 0.057989
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 137</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 161,
    "user_id": 7850,
    "phone_number": "+11234567890",
    "errors": [

    ]
  },
  {
    "id": 162,
    "user_id": 7850,
    "phone_number": "+11234567891",
    "errors": [

    ]
  }
]</pre>
