# v1/CollocutorMasks API

## Info about collocutor mask

### GET /api/v1/collocutor_masks/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the collocutor mask | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: n7479cNAEHtjjD1MFpbrysDJ
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/collocutor_masks/579</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;823e4e22a2050c9ffce14874c5b5f54e&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 947320c7-282f-4b71-b61b-1967691c1881
X-Runtime: 0.015427
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 399</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 579,
  "name": "Romana Jones I",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-incognito.png",
    "small": "http://example.org/assets/small-avatars/avatar-incognito.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-incognito.png",
    "large": "http://example.org/assets/large-avatars/avatar-incognito.png"
  },
  "collocutor_id": 7488,
  "blocked": false,
  "chat_id": 0
}</pre>
