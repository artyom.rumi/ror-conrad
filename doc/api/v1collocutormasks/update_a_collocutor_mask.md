# v1/CollocutorMasks API

## Update a collocutor mask

### PATCH /api/v1/collocutor_masks/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the collocutor mask | true |  |
| name | New name for collocutor mask | false |  |
| avatar | New avatar for collocutor mask | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: multipart/form-data; boundary=----------XnJLe9ZIbbGUYtzPQJ16u1
X-Auth-Token: 9Mmxgb6fMMLvRHzeJFDo5SHk
Host: example.org
Cookie: </pre>

#### Route

<pre>PATCH /api/v1/collocutor_masks/583</pre>

#### Body

<pre>------------XnJLe9ZIbbGUYtzPQJ16u1
Content-Disposition: form-data; name="name"

John Doe
------------XnJLe9ZIbbGUYtzPQJ16u1
Content-Disposition: form-data; name="avatar"; filename="avatar.png"
Content-Type: image/png
Content-Length: 1451087

[uploaded data]
------------XnJLe9ZIbbGUYtzPQJ16u1--</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;3edf5d2a05395ea924a03e81b29b129c&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 802e8020-4ff6-467f-b6eb-b91483bcf4bb
X-Runtime: 0.163456
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1673</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 583,
  "name": "John Doe",
  "avatar": {
    "original": "http://lvh.me:5000/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWU09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--aba15f9269809102fabc36161be7d0990d08d72a/avatar.png",
    "small": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWU09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--aba15f9269809102fabc36161be7d0990d08d72a/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTVRVd2VERTFNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--6961321111510a9086c6dbc85bb89f5b4d42e35b/avatar.png",
    "medium": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWU09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--aba15f9269809102fabc36161be7d0990d08d72a/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTWpRd2VESTBNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--ae12e1e8ac781d9820431268ef8fc8f409f55b8d/avatar.png",
    "large": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWU09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--aba15f9269809102fabc36161be7d0990d08d72a/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTkRnd2VEUTRNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--22e536a873c911c90d972fe877286eed82b6351b/avatar.png"
  },
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-incognito.png",
    "small": "http://example.org/assets/small-avatars/avatar-incognito.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-incognito.png",
    "large": "http://example.org/assets/large-avatars/avatar-incognito.png"
  },
  "collocutor_id": 7501,
  "blocked": false,
  "chat_id": 0
}</pre>
