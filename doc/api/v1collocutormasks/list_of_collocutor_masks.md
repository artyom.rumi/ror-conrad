# v1/CollocutorMasks API

## List of collocutor masks

### GET /api/v1/collocutor_masks
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 9rWSvjx2JmT2zfa51HbLVXBb
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/collocutor_masks</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5d35007fdcde66d39650653c3bdf7628&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: b875e6a1-cb45-4fb4-8826-46e442f1f284
X-Runtime: 0.057277
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 799</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 567,
    "name": "Hailey Collier",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-incognito.png",
      "small": "http://example.org/assets/small-avatars/avatar-incognito.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-incognito.png",
      "large": "http://example.org/assets/large-avatars/avatar-incognito.png"
    },
    "collocutor_id": 7467,
    "blocked": false,
    "chat_id": 0
  },
  {
    "id": 568,
    "name": "Salome Kutch",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-incognito.png",
      "small": "http://example.org/assets/small-avatars/avatar-incognito.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-incognito.png",
      "large": "http://example.org/assets/large-avatars/avatar-incognito.png"
    },
    "collocutor_id": 7468,
    "blocked": false,
    "chat_id": 0
  }
]</pre>
