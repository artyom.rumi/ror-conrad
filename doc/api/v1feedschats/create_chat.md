# v1/Feeds/Chats API

## create chat

### POST /api/v1/feeds/:feed_id/chats

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for chat | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: b4jFCuNqoat7fTKH5ZADNkrh
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feeds/1173/chats</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;8901c548d2fbdc2b8acb3d86d66c1c70&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f5ce0e9f-8173-4ef0-8e37-1bad1da9db19
X-Runtime: 0.167634
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2173</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 561,
  "pubnub_channel_name": "incognito_chat_561",
  "updated_at": "2020-02-10T09:00:12.914+00:00",
  "incognito": true,
  "feed_id": 1173,
  "comment_id": null,
  "name": null,
  "is_group": false,
  "owner": {
    "id": 7576,
    "first_name": "Marna",
    "last_name": "Rippin",
    "grade": 2025,
    "bio": "Aut architecto voluptatum in.",
    "school_name": "Ducimus quis perspiciatis velit.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-18.png",
      "small": "http://example.org/assets/small-avatars/avatar-18.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-18.png",
      "large": "http://example.org/assets/large-avatars/avatar-18.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": null
  },
  "users": [
    {
      "id": 7576,
      "first_name": "Marna",
      "last_name": "Rippin",
      "grade": 2025,
      "bio": "Aut architecto voluptatum in.",
      "school_name": "Ducimus quis perspiciatis velit.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-18.png",
        "small": "http://example.org/assets/small-avatars/avatar-18.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-18.png",
        "large": "http://example.org/assets/large-avatars/avatar-18.png"
      },
      "gender": "non_binary",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    },
    {
      "id": 7577,
      "first_name": "Chantel",
      "last_name": "Wilderman",
      "grade": 2017,
      "bio": "Est sunt error architecto.",
      "school_name": "Doloremque molestias dolore soluta.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-10.png",
        "small": "http://example.org/assets/small-avatars/avatar-10.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-10.png",
        "large": "http://example.org/assets/large-avatars/avatar-10.png"
      },
      "gender": "male",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  ],
  "deanonymizations": [

  ]
}</pre>
