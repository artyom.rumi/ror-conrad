# v1/Feeds/Chats API

## it cannot be created

### POST /api/v1/feeds/:feed_id/chats

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for chat | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 8UH1aAPttodXpei5hyEm68Bn
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feeds/1177/chats</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: 14c0663b-ed01-49b4-9008-21caf9ac9fd6
X-Runtime: 0.066582
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 74</pre>

#### Status

<pre>422 Unprocessable Entity</pre>

#### Body

<pre>{
  "error": [
    "Feed 's author and chat owner can't be one and the same user"
  ]
}</pre>
