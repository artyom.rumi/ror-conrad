# v1/Props API

## get full list of props

### GET /api/v1/props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | User to filter props by | false |  |
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: MWi98FYntRWt7Mbcy62w9seb
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;ab4eeb55b88f92da205976d53afd1b84&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 8a4bcaa6-a326-4eb7-bde7-58e444e6e90e
X-Runtime: 0.028845
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 249</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 980,
    "content": "Culpa minima rerum repellendus.",
    "emoji": "😍",
    "gender": "male"
  },
  {
    "id": 981,
    "content": "Iure quibusdam totam omnis.",
    "emoji": "😍",
    "gender": "female"
  },
  {
    "id": 982,
    "content": "Est rem placeat eligendi.",
    "emoji": "😍",
    "gender": null
  }
]</pre>
