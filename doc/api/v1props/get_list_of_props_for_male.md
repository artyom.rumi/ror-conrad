# v1/Props API

## get list of props for male

### GET /api/v1/props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | User to filter props by | false |  |
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: QXV2aT3FMgeHjrr51RTrHArV
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/props?user_id=8107</pre>

#### Query Parameters

<pre>user_id: 8107</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;84085b01b776d02743853d3deb3bb7fa&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: a1d251d4-3317-4ab6-a1b0-dccb28eafa8c
X-Runtime: 0.011541
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 178</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 996,
    "content": "Et laudantium molestiae repellat.",
    "emoji": "😍",
    "gender": "male"
  },
  {
    "id": 998,
    "content": "Voluptas exercitationem delectus et.",
    "emoji": "😍",
    "gender": null
  }
]</pre>
