# v1/Props API

## get non-binary list of props

### GET /api/v1/props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | User to filter props by | false |  |
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: U3HGRddEvEkYmFTjQ5AARUhF
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/props?user_id=8109</pre>

#### Query Parameters

<pre>user_id: 8109</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;79166b0ce68d1d813ac17087f5cd6ee0&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f2616975-3c85-4cf2-8863-bc087d85513e
X-Runtime: 0.012040
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 81</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1002,
    "content": "Deserunt assumenda a quos.",
    "emoji": "😍",
    "gender": null
  }
]</pre>
