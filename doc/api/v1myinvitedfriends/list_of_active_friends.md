# v1/My/InvitedFriends API

## List of active friends

### GET /api/v1/my/invited_friends

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: xAC2myuRGQkqGPiUfs8yjmiU
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/invited_friends?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;747f756c458b3b6b3743c08b3b11f637&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 21640832-4207-4cd3-9b93-5118df71dad7
X-Runtime: 0.109754
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 685</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "users": [
    {
      "id": 7947,
      "first_name": "Stacey",
      "last_name": "Douglas",
      "grade": 2012,
      "bio": "Omnis in quisquam ut.",
      "school_name": "Magnam sunt quam exercitationem.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-12.png",
        "small": "http://example.org/assets/small-avatars/avatar-12.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-12.png",
        "large": "http://example.org/assets/large-avatars/avatar-12.png"
      },
      "gender": "non_binary",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "invited"
    }
  ],
  "meta": {
    "users_count": 1
  }
}</pre>
