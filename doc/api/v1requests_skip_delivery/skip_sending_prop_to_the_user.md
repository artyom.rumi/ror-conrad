# v1/requests Skip Delivery API

## Skip sending prop to the user

### POST /api/v1/requests/:request_id/skip_deliveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| request_id | Request for props | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: SYTgCm27kX2aVohg93cRQWaa
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/requests/315/skip_deliveries</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;2b2290cff44f89871bce0d9c7b716f62&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: a4e8f566-2aed-4b9a-846a-c959a479fc01
X-Runtime: 0.142244
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 46</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 315,
  "progress": 1,
  "respondents_amount": 5
}</pre>
