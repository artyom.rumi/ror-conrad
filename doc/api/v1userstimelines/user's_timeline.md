# v1/Users/Timelines API

## user&#39;s timeline

### GET api/v1/users/:user_id/timeline

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | Id of the user | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: a3mauaNWKNJUKaXpgfJeLg9Z
Host: example.org
Cookie: </pre>

#### Route

<pre>GET api/v1/users/8270/timeline</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;ad0d79d3658ae05e213f2b73219ba22e&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 4eb28069-17d5-404e-826a-7d42e5dbb4b8
X-Runtime: 0.134135
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 565</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1327,
    "image": null,
    "user_id": 8270,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-10T09:00:54.584+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  },
  {
    "id": 1328,
    "image": null,
    "user_id": 8270,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-10T08:53:54.587+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
