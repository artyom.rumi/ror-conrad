# v1/Incoming requests API

## List of opened incoming requests

### GET /api/v1/incoming_requests
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Qxp9GrtEngdZ4MQhtfacsTpr
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/incoming_requests</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;69f2571a1607529ef2973c16c072ccd1&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: ee8af255-f29e-4a55-88ae-ecf44f501f2b
X-Runtime: 0.031750
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 95</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 283,
    "progress": 0,
    "respondents_amount": 5
  },
  {
    "id": 284,
    "progress": 0,
    "respondents_amount": 5
  }
]</pre>
