# v1/Incoming requests API

## Specific request

### GET /api/v1/incoming_requests/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the incoming request | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: EMykbtND7qAay7NfQgpkaMdu
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/incoming_requests/306</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;59e952393fb946b1e91a1f8169509339&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 6c7b1d4a-5613-444f-8766-771d0c548740
X-Runtime: 0.009987
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 46</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 306,
  "progress": 0,
  "respondents_amount": 5
}</pre>
