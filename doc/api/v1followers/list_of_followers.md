# v1/Followers API

## List of followers

### GET /api/v1/followers
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: dFjedUojRTZYVW2ebC5shcq7
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/followers</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;216df896bb31cdbd7bcb23c039872c24&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: ee041680-cb09-4f1c-9184-d7133e79e80d
X-Runtime: 0.058539
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1298</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7692,
    "first_name": "Santos",
    "last_name": "Ortiz",
    "grade": 2019,
    "bio": "Facere officia eligendi nihil.",
    "school_name": "Magni repellat rerum officiis.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-22.png",
      "small": "http://example.org/assets/small-avatars/avatar-22.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-22.png",
      "large": "http://example.org/assets/large-avatars/avatar-22.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 7693,
    "first_name": "Kurtis",
    "last_name": "Conn",
    "grade": 2021,
    "bio": "Sed sequi autem nulla.",
    "school_name": "Magni repellat rerum officiis.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-15.png",
      "small": "http://example.org/assets/small-avatars/avatar-15.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-15.png",
      "large": "http://example.org/assets/large-avatars/avatar-15.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
