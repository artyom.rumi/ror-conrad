# v1/Interlocutors API

## List of interlocutors matched by keywords

### GET /api/v1/interlocutors

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: ghBWdbrtssEESmL9nB45Zw4H
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/interlocutors?keywords=Brad</pre>

#### Query Parameters

<pre>keywords: Brad</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;059d19e334c95bee1bbef73b834e741e&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 5b811a94-b37f-46b4-9b77-0a18283eb02b
X-Runtime: 0.033802
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 640</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7795,
    "first_name": "Brad",
    "last_name": "Pitt",
    "grade": 2030,
    "bio": "Qui voluptatem rerum beatae.",
    "school_name": "Rerum vitae maiores facere.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-22.png",
      "small": "http://example.org/assets/small-avatars/avatar-22.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-22.png",
      "large": "http://example.org/assets/large-avatars/avatar-22.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 1,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "friend"
  }
]</pre>
