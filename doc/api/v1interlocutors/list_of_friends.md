# v1/Interlocutors API

## List of friends

### GET /api/v1/interlocutors

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: efQ9hX8r3yaVBRdyBG2YHA76
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/interlocutors</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5725f1863007c219975fa7e273b419a5&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f3f94e4b-9372-483a-bd1a-87e5c28ba98b
X-Runtime: 0.088023
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1893</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7774,
    "first_name": "Brad",
    "last_name": "Pitt",
    "grade": 2030,
    "bio": "Et quibusdam maxime iste.",
    "school_name": "Rerum odit sunt exercitationem.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-4.png",
      "small": "http://example.org/assets/small-avatars/avatar-4.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-4.png",
      "large": "http://example.org/assets/large-avatars/avatar-4.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 1,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "friend"
  },
  {
    "id": 7775,
    "first_name": "George",
    "last_name": "Clooney",
    "grade": 2014,
    "bio": "Velit ea sed minima.",
    "school_name": "Labore in qui dicta.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-24.png",
      "small": "http://example.org/assets/small-avatars/avatar-24.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
      "large": "http://example.org/assets/large-avatars/avatar-24.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 1,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "friend"
  },
  {
    "id": 7776,
    "first_name": "Matt",
    "last_name": "Damon",
    "grade": 2030,
    "bio": "Dolor nam omnis vel.",
    "school_name": "Debitis aliquid ab distinctio.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-4.png",
      "small": "http://example.org/assets/small-avatars/avatar-4.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-4.png",
      "large": "http://example.org/assets/large-avatars/avatar-4.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 1,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "friend"
  }
]</pre>
