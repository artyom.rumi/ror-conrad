# v1/Interlocutors API

## List of friends on another page

### GET /api/v1/interlocutors

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: tB6zW7AoUUmfwc5W5ZB3kXUF
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/interlocutors?page=2&amp;per_page=2</pre>

#### Query Parameters

<pre>page: 2
per_page: 2</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;f3684f6e8720b42ae7d9a7f41091d89c&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 1d50973b-825d-4d08-813c-bc607baa7459
X-Runtime: 0.032305
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 645</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7793,
    "first_name": "Matt",
    "last_name": "Damon",
    "grade": 2010,
    "bio": "Dolorem earum rerum labore.",
    "school_name": "Accusantium qui accusamus nulla.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-26.png",
      "small": "http://example.org/assets/small-avatars/avatar-26.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-26.png",
      "large": "http://example.org/assets/large-avatars/avatar-26.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 1,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "friend"
  }
]</pre>
