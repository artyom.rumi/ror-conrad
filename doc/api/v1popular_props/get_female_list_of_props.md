# v1/Popular Props API

## get female list of props

### GET /api/v1/popular_props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | User to filter props by | false |  |
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: HzbDXqXK9txLKpWfLbe57yS7
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/popular_props?user_id=8055</pre>

#### Query Parameters

<pre>user_id: 8055</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;33d970bef3bbbead1e162d8f79e7520f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 3d19addf-6698-4853-9a53-5583c0f38f4f
X-Runtime: 0.010916
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 170</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 959,
    "content": "Voluptatem dolores quaerat et.",
    "emoji": "😍",
    "gender": null
  },
  {
    "id": 960,
    "content": "Quasi eligendi voluptas quod.",
    "emoji": "😍",
    "gender": "female"
  }
]</pre>
