# v1/Popular Props API

## get list of props for male

### GET /api/v1/popular_props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | User to filter props by | false |  |
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: iSLa9p3Ax6gGomz7dKxdeAs2
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/popular_props?user_id=8053</pre>

#### Query Parameters

<pre>user_id: 8053</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7c5ad1bbe641efad6aeec7a6b0315624&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 6a9b0cba-f8c9-4846-a5a2-54b945cfbd5a
X-Runtime: 0.012741
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 235</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 954,
    "content": "Expedita id ut reprehenderit.",
    "emoji": "😍",
    "gender": "male"
  },
  {
    "id": 953,
    "content": "Voluptas et et eveniet.",
    "emoji": "😍",
    "gender": "male"
  },
  {
    "id": 955,
    "content": "Qui enim fugit non.",
    "emoji": "😍",
    "gender": null
  }
]</pre>
