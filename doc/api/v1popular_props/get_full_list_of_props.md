# v1/Popular Props API

## get full list of props

### GET /api/v1/popular_props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | User to filter props by | false |  |
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: ehTsNCnsLsFC2mESUe3GqK2S
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/popular_props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;1aa3d617ef8de0c3ad99209fbb2c1c6c&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 8050b7a7-c9d1-426e-8cfb-69f9a8a187ec
X-Runtime: 0.034961
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 321</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 938,
    "content": "Sunt facere expedita illum.",
    "emoji": "😍",
    "gender": "male"
  },
  {
    "id": 937,
    "content": "Maxime sit quod quia.",
    "emoji": "😍",
    "gender": "male"
  },
  {
    "id": 939,
    "content": "Officia quis recusandae non.",
    "emoji": "😍",
    "gender": null
  },
  {
    "id": 940,
    "content": "Cum rerum ut reiciendis.",
    "emoji": "😍",
    "gender": "female"
  }
]</pre>
