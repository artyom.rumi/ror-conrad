# v1/schoolmates API

## List of schoolmates

### GET /api/v1/schoolmates
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: QMHUJBrV5cDmJizmqQwXicPh
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/schoolmates</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;6ae184fed573ef93f4285547c3c10f54&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e1efb562-d99c-4875-a142-c3b0caa422f3
X-Runtime: 0.056421
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 658</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8216,
    "first_name": "Samantha",
    "last_name": "Denesik",
    "grade": 2015,
    "bio": "Et praesentium necessitatibus ipsa.",
    "school_name": "Ea dignissimos enim occaecati.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-9.png",
      "small": "http://example.org/assets/small-avatars/avatar-9.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-9.png",
      "large": "http://example.org/assets/large-avatars/avatar-9.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
