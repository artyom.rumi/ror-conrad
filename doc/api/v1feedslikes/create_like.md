# v1/Feeds/Likes API

## Create like

### POST /api/v1/feeds/:feed_id/likes

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for like | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: F7jsXNwjTKg5hNzvC2yZkSa8
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feeds/1194/likes</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;36ee72c0d8168aadcbb04fdd0f6d04da&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: ac9749c1-f5f7-4e3e-8f4a-4ff79b2945c9
X-Runtime: 0.958373
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 280</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 1194,
  "image": null,
  "user_id": 7627,
  "incognito": false,
  "likes_count": 1,
  "liked": true,
  "created_at": "2020-02-10T09:00:16.461+00:00",
  "unread_comments_count": 0,
  "text": null,
  "link": null,
  "count_of_engaged_friends": 0,
  "comments_count": 0,
  "reposted": false,
  "source_feed_id": null,
  "chat": null
}</pre>
