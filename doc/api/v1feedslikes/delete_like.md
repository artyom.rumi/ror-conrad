# v1/Feeds/Likes API

## Delete like

### DELETE /api/v1/feeds/:feed_id/likes

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id for delete like | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: aMwgz9RhGAQK9xNZFUq5cSRo
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/feeds/1198/likes</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;13cbb814bbf2a7fd048492f08ba13b9b&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 1a14e0b9-0b77-4a8f-9ac3-793e914b11ed
X-Runtime: 0.035622
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 281</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 1198,
  "image": null,
  "user_id": 7636,
  "incognito": false,
  "likes_count": 0,
  "liked": false,
  "created_at": "2020-02-10T09:00:17.678+00:00",
  "unread_comments_count": 0,
  "text": null,
  "link": null,
  "count_of_engaged_friends": 0,
  "comments_count": 0,
  "reposted": false,
  "source_feed_id": null,
  "chat": null
}</pre>
