# v1/User API

## Get user info

### GET /api/v1/users/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the user | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 1Auj24myPZGxcv5jPFob9eJ9
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/users/8276</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;c1735106ebb2da83e852c74b718e9280&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 9df51bdf-b23b-4b29-a275-4a96fc65bd47
X-Runtime: 0.119279
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 681</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 8276,
  "first_name": "Chasity",
  "last_name": "Lind",
  "grade": 2018,
  "bio": "Rerum consequatur distinctio aliquid.",
  "school_name": "Temporibus facilis eveniet voluptatibus.",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-24.png",
    "small": "http://example.org/assets/small-avatars/avatar-24.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
    "large": "http://example.org/assets/large-avatars/avatar-24.png"
  },
  "gender": "male",
  "followee": false,
  "blocked": false,
  "role": "ordinary",
  "chat_available": false,
  "custom_prop_available": false,
  "friends_count": 0,
  "pokes_count": 0,
  "hide_top_pokes": false,
  "friendship_status": "uninvited",
  "top_pokes": [

  ]
}</pre>
