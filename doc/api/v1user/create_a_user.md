# v1/User API

## Create a user

### POST /api/v1/users

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| first_name | User first name | true |  |
| last_name | User last name | true |  |
| age | User age | true |  |
| grade | Year of graduation | true |  |
| gender | User gender | true |  |
| school_id | User school id | true |  |
| bio | User bio | false |  |
| matching_enabled | Should system perform smart matching or not | false |  |
| phone_numbers | Phone numbers | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: V2CyPTy769py2CtMbMjFk7Ab
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/users</pre>

#### Body

<pre>{"first_name":"Eloise","last_name":"Cartwright","age":18,"grade":2026,"gender":"non_binary","school_id":8298,"bio":"Nemo et voluptas sed.","matching_enabled":true,"phone_numbers":["+18269600479"]}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7884b313420da01b7594d4e33969dd99&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 434c95c1-a87b-4dd9-b214-98999d3d3694
X-Runtime: 0.071515
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 923</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 8273,
  "first_name": "Eloise",
  "last_name": "Cartwright",
  "age": 18,
  "grade": 2026,
  "gender": "non_binary",
  "phone_number": "+13479958885",
  "created_at": "2020-02-10T09:00:54.950+00:00",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-4.png",
    "small": "http://example.org/assets/small-avatars/avatar-4.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-4.png",
    "large": "http://example.org/assets/large-avatars/avatar-4.png"
  },
  "bio": "Nemo et voluptas sed.",
  "matching_enabled": true,
  "prop_request_notification_enabled": true,
  "receiving_prop_notification_enabled": true,
  "chat_messages_notification_enabled": true,
  "new_friends_notification_enabled": true,
  "new_users_notification_enabled": true,
  "friend_boost_at": null,
  "friends_count": 0,
  "pokes_count": 0,
  "top_pokes": [

  ],
  "hide_top_pokes": false,
  "school": {
    "id": 8298,
    "name": "Eum fuga cum autem.",
    "members_amount": 17,
    "address": "Botsfordfort, South Dakota"
  }
}</pre>
