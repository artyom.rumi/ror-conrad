# v1/User API

## subscribe to new school mates

### PATCH /api/v1/users/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the user to be updated | true |  |
| first_name | User first name | false |  |
| last_name | User last name | false |  |
| age | User age | false |  |
| grade | Year of graduation | false |  |
| gender | User gender | false |  |
| school_id | User school id | false |  |
| bio | User bio | false |  |
| matching_enabled | Should system perform smart matching or not | false |  |
| prop_request_notification_enabled | Should system send notification of prop requests | false |  |
| receiving_prop_notification_enabled | Should system send notification of received props | false |  |
| chat_messages_notification_enabled | Should system send notification of chat messages | false |  |
| new_friends_notification_enabled | Should system send notification of new friends | false |  |
| new_users_notification_enabled | Should system send notification of new users | false |  |
| hide_top_pokes | Whether top pokes are shown | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: xshSC3ZsUM4CHFU8iW8XG2Nd
Host: example.org
Cookie: </pre>

#### Route

<pre>PATCH /api/v1/users/8287</pre>

#### Body

<pre>{"first_name":"Ricky","last_name":"Cartwright","age":16,"grade":2022,"gender":"non_binary","school_id":8317,"bio":"Placeat voluptates tenetur libero.","matching_enabled":false,"prop_request_notification_enabled":true,"receiving_prop_notification_enabled":true,"chat_messages_notification_enabled":true,"new_friends_notification_enabled":true,"new_users_notification_enabled":true}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;e4c628a4ff98e7910b4813ea4483b5fa&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: cd8dca0e-5148-4c29-90c9-0bc816fcd692
X-Runtime: 0.063284
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 944</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 8287,
  "first_name": "Ricky",
  "last_name": "Cartwright",
  "age": 16,
  "grade": 2022,
  "gender": "non_binary",
  "phone_number": "+12173065880",
  "created_at": "2020-02-10T09:00:55.910+00:00",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-21.png",
    "small": "http://example.org/assets/small-avatars/avatar-21.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-21.png",
    "large": "http://example.org/assets/large-avatars/avatar-21.png"
  },
  "bio": "Placeat voluptates tenetur libero.",
  "matching_enabled": false,
  "prop_request_notification_enabled": true,
  "receiving_prop_notification_enabled": true,
  "chat_messages_notification_enabled": true,
  "new_friends_notification_enabled": true,
  "new_users_notification_enabled": true,
  "friend_boost_at": null,
  "friends_count": 0,
  "pokes_count": 0,
  "top_pokes": [

  ],
  "hide_top_pokes": false,
  "school": {
    "id": 8317,
    "name": "Sed quam atque labore.",
    "members_amount": 39,
    "address": "Harrietchester, Mississippi"
  }
}</pre>
