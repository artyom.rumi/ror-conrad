# v1/User API

## Attempt to Create a user without confirmed device

### POST /api/v1/users

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| first_name | User first name | true |  |
| last_name | User last name | true |  |
| age | User age | true |  |
| grade | Year of graduation | true |  |
| gender | User gender | true |  |
| school_id | User school id | true |  |
| bio | User bio | false |  |
| matching_enabled | Should system perform smart matching or not | false |  |
| phone_numbers | Phone numbers | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/users</pre>

#### Body

<pre>{"first_name":"Connie","last_name":"Goyette","age":14,"grade":2010,"gender":"male","school_id":8296,"bio":"Magnam fugiat necessitatibus qui.","matching_enabled":true}</pre>

### Response

#### Headers

<pre>Content-Type: text/html
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: 8846082a-e19b-448e-b8a3-b3c76e1a6c36
X-Runtime: 0.024983
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 0</pre>

#### Status

<pre>401 Unauthorized</pre>

