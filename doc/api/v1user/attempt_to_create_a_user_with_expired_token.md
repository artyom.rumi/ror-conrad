# v1/User API

## Attempt to create a user with expired token

### POST /api/v1/users

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| first_name | User first name | true |  |
| last_name | User last name | true |  |
| age | User age | true |  |
| grade | Year of graduation | true |  |
| gender | User gender | true |  |
| school_id | User school id | true |  |
| bio | User bio | false |  |
| matching_enabled | Should system perform smart matching or not | false |  |
| phone_numbers | Phone numbers | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 7R53hmYZA1ysvxdNDnhNapZv
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/users</pre>

#### Body

<pre>{"first_name":"Kylee","last_name":"Corkery","age":18,"grade":2023,"gender":"non_binary","school_id":8302,"bio":"In facere eveniet officia.","matching_enabled":true}</pre>

### Response

#### Headers

<pre>Content-Type: text/html
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: c3ab832c-d0b3-41ec-acfb-c564c0a16d5e
X-Runtime: 0.008885
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 0</pre>

#### Status

<pre>401 Unauthorized</pre>

