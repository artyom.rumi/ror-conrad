# v1/User API

## Update a user

### PATCH /api/v1/users/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the user to be updated | true |  |
| first_name | User first name | false |  |
| last_name | User last name | false |  |
| age | User age | false |  |
| grade | Year of graduation | false |  |
| gender | User gender | false |  |
| school_id | User school id | false |  |
| bio | User bio | false |  |
| matching_enabled | Should system perform smart matching or not | false |  |
| prop_request_notification_enabled | Should system send notification of prop requests | false |  |
| receiving_prop_notification_enabled | Should system send notification of received props | false |  |
| chat_messages_notification_enabled | Should system send notification of chat messages | false |  |
| new_friends_notification_enabled | Should system send notification of new friends | false |  |
| new_users_notification_enabled | Should system send notification of new users | false |  |
| hide_top_pokes | Whether top pokes are shown | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: SriuwSQJGmrwEGcAbjs5M6dA
Host: example.org
Cookie: </pre>

#### Route

<pre>PATCH /api/v1/users/8286</pre>

#### Body

<pre>{"first_name":"Gary","last_name":"Daugherty","age":13,"grade":2010,"gender":"male","school_id":8315,"bio":"Laborum nihil sed quisquam.","matching_enabled":false,"prop_request_notification_enabled":true,"receiving_prop_notification_enabled":true,"chat_messages_notification_enabled":true,"new_friends_notification_enabled":true,"new_users_notification_enabled":true}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;68a08fb0c578facfa9ede0f297914897&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e52c126a-d74d-494d-a5dc-9d0f1b06e5df
X-Runtime: 0.080737
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 926</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 8286,
  "first_name": "Gary",
  "last_name": "Daugherty",
  "age": 13,
  "grade": 2010,
  "gender": "male",
  "phone_number": "+18452683917",
  "created_at": "2020-02-10T09:00:55.760+00:00",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-24.png",
    "small": "http://example.org/assets/small-avatars/avatar-24.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
    "large": "http://example.org/assets/large-avatars/avatar-24.png"
  },
  "bio": "Laborum nihil sed quisquam.",
  "matching_enabled": false,
  "prop_request_notification_enabled": true,
  "receiving_prop_notification_enabled": true,
  "chat_messages_notification_enabled": true,
  "new_friends_notification_enabled": true,
  "new_users_notification_enabled": true,
  "friend_boost_at": null,
  "friends_count": 0,
  "pokes_count": 0,
  "top_pokes": [

  ],
  "hide_top_pokes": false,
  "school": {
    "id": 8315,
    "name": "Ipsa temporibus at ab.",
    "members_amount": 62,
    "address": "East Alfredshire, Hawaii"
  }
}</pre>
