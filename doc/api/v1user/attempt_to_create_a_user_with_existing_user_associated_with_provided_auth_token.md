# v1/User API

## Attempt to create a user with existing user associated with provided auth token

### POST /api/v1/users

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| first_name | User first name | true |  |
| last_name | User last name | true |  |
| age | User age | true |  |
| grade | Year of graduation | true |  |
| gender | User gender | true |  |
| school_id | User school id | true |  |
| bio | User bio | false |  |
| matching_enabled | Should system perform smart matching or not | false |  |
| phone_numbers | Phone numbers | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: SzhwXtyzN3fy8jLMjWyU2WiE
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/users</pre>

#### Body

<pre>{"first_name":"Virginia","last_name":"Feest","age":18,"grade":2020,"gender":"female","school_id":8301,"bio":"Et ratione quaerat deserunt.","matching_enabled":true}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: fbf65ce4-8b13-4876-ae6e-1225be4048ab
X-Runtime: 0.011090
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 69</pre>

#### Status

<pre>422 Unprocessable Entity</pre>

#### Body

<pre>{
  "error": "You already have a user associated with this phone number"
}</pre>
