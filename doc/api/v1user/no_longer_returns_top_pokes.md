# v1/User API

## no longer returns top pokes

### GET /api/v1/users/:id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the user | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: D8hACrJ1o2bDhbipXHPRrw1r
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/users/8285</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;4cb23f047047d89923789a30fb86031f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e6b3b226-7ae5-4e4e-92fa-d01f55e4d001
X-Runtime: 0.036190
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 661</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 8285,
  "first_name": "Stacy",
  "last_name": "Herman",
  "grade": 2017,
  "bio": "Dolorem distinctio saepe et.",
  "school_name": "Et facere rerum earum.",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-19.png",
    "small": "http://example.org/assets/small-avatars/avatar-19.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-19.png",
    "large": "http://example.org/assets/large-avatars/avatar-19.png"
  },
  "gender": "non_binary",
  "followee": false,
  "blocked": false,
  "role": "ordinary",
  "chat_available": false,
  "custom_prop_available": false,
  "friends_count": 0,
  "pokes_count": 0,
  "hide_top_pokes": true,
  "friendship_status": "uninvited",
  "top_pokes": null
}</pre>
