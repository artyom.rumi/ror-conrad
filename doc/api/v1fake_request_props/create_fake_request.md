# v1/Fake request props API

## Create fake request

### POST /api/v1/fake_requests
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 15LHLG6NvJ4bSPdKQD67Tupe
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/fake_requests</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;b0b91dea6fe8899d84c90b39dca2282c&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f8a4d648-26a2-43cd-aca9-5ad900aab344
X-Runtime: 0.119712
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 46</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 281,
  "progress": 0,
  "respondents_amount": 5
}</pre>
