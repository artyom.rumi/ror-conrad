# v1/Invitations API

## Send invite message to friends

### POST /api/v1/invitation

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| contacts | Phone numbers | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: itQHfjDXXDryQpwBFscyN96b
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/invitation</pre>

#### Body

<pre>{"contacts":[{"name":"Johny B. Goode","phone_number":"+1424242424242"},{"name":"King Creole","phone_number":"+1424242424243"}]}</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 30eaeef3-9526-4ac1-9f90-7d0d4e9e6346
X-Runtime: 0.114191
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

