# v1/Subscription API

## Destroy subscription by id

### DELETE /api/v1/subscriptions

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Id of the subscription to be deleted | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: i2UTZDrixEkKNhVoouRghAFD
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/subscriptions</pre>

#### Body

<pre>{"id":1297}</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 635cccf5-747b-489d-86b1-c1b3d9df4008
X-Runtime: 0.021571
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

