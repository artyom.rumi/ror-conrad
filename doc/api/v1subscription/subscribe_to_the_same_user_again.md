# v1/Subscription API

## Subscribe to the same user again

### POST /api/v1/subscriptions

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| target_id | User to subscribe | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: stmFZR7Dv2V9PcWMq9kUjXgh
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/subscriptions</pre>

#### Body

<pre>{"target_id":8244}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: f13ee1cf-4f1b-4a34-9154-89e52878219d
X-Runtime: 0.015620
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 49</pre>

#### Status

<pre>409 Conflict</pre>

#### Body

<pre>{
  "id": 1296,
  "subscriber_id": 8243,
  "target_id": 8244
}</pre>
