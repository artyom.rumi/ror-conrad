# v1/Subscription API

## Destroy subscription by id of user to unsubscribe from

### DELETE /api/v1/subscriptions

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| target_id | Id of user to unsubscribe from | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Qemjy7FcipPHvpFXPSLePZYW
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/subscriptions</pre>

#### Body

<pre>{"target_id":8248}</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 65f431d5-7927-4b58-a86d-c0ac982b3d07
X-Runtime: 0.032649
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

