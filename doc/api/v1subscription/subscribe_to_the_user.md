# v1/Subscription API

## Subscribe to the user

### POST /api/v1/subscriptions

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| target_id | User to subscribe | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: QjM95LrNStNdgaEgGokDBx1v
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/subscriptions</pre>

#### Body

<pre>{"target_id":8242}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;e2597616ebc3b76c5538c4094166cc5e&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 4b347119-3c2b-4570-905f-77378915dfe0
X-Runtime: 0.096575
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 49</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 1295,
  "subscriber_id": 8241,
  "target_id": 8242
}</pre>
