# v1/Feeds/Reposts API

## Create repost feed

### POST /api/v1/feeds/:feed_id/reposts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: obFtUBAJxJ1J6sCFNDNTeZcT
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/feeds/1207/reposts</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;858c8e1714de6b0be2cd971220894bf5&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 95ae901b-1de5-4a37-ac7a-ff24622742e0
X-Runtime: 0.902751
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1567</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 1208,
  "image": {
    "original": "http://lvh.me:5000/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWVE9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3ed8200e04771b645ac876760cd727c41a0b2fad/avatar.png",
    "small": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWVE9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3ed8200e04771b645ac876760cd727c41a0b2fad/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTVRVd2VERTFNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--6961321111510a9086c6dbc85bb89f5b4d42e35b/avatar.png",
    "medium": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWVE9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3ed8200e04771b645ac876760cd727c41a0b2fad/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTWpRd2VESTBNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--ae12e1e8ac781d9820431268ef8fc8f409f55b8d/avatar.png",
    "large": "http://lvh.me:5000/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWVE9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3ed8200e04771b645ac876760cd727c41a0b2fad/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lNTkRnd2VEUTRNQVk2QmtWVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--22e536a873c911c90d972fe877286eed82b6351b/avatar.png"
  },
  "user_id": 7651,
  "incognito": false,
  "likes_count": 0,
  "liked": false,
  "created_at": "2020-02-10T09:00:18.350+00:00",
  "unread_comments_count": 0,
  "text": "some text",
  "link": null,
  "count_of_engaged_friends": 0,
  "comments_count": 0,
  "reposted": true,
  "source_feed_id": 1207,
  "chat": null
}</pre>
