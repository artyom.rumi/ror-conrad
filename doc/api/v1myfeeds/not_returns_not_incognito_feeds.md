# v1/My/Feeds API

## not returns not incognito feeds

### DELETE /api/v1/my/feeds/:feed_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: zBZE1Vqwk6vLbhRYYdiRU62P
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/my/feeds/1272</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;bcccc5a500c5ce47b66bac52c38e96a3&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: fd24acb2-3226-4e3a-a73c-67a8cbffdda6
X-Runtime: 0.042751
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 282</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1273,
    "image": null,
    "user_id": 7931,
    "incognito": true,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-10T09:00:29.482+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
