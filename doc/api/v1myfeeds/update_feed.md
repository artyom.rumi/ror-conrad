# v1/My/Feeds API

## Update feed

### PUT /api/v1/my/feeds/:feed_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id | true |  |
| incognito | Feed status | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 8yD7QZH2zZiNjVukaGPTgRCY
Host: example.org
Cookie: </pre>

#### Route

<pre>PUT /api/v1/my/feeds/1235</pre>

#### Body

<pre>{"incognito":"true"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;a3c57db3e244af808ec7941508ff789a&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 4865a3f2-0999-4f94-b4ba-13c5876f355e
X-Runtime: 0.033030
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 280</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 1235,
  "image": null,
  "user_id": 7902,
  "incognito": true,
  "likes_count": 0,
  "liked": false,
  "created_at": "2020-02-10T09:00:27.483+00:00",
  "unread_comments_count": 0,
  "text": null,
  "link": null,
  "count_of_engaged_friends": 0,
  "comments_count": 0,
  "reposted": false,
  "source_feed_id": null,
  "chat": null
}</pre>
