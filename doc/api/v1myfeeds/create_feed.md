# v1/My/Feeds API

## Create feed

### POST /api/v1/my/feeds

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| text | Feed text | false |  |
| link | Feed with link | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: DAtwY6rG4uBF2ausXPkgHLyS
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/my/feeds</pre>

#### Body

<pre>{"text":"Some text","link":"https://www.google.com"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;ed1f7c98970d96e6bc4f9e70f6689775&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f5ee090f-f215-48c7-a1f6-cff454259455
X-Runtime: 0.029418
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 308</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 1234,
  "image": null,
  "user_id": 7901,
  "incognito": false,
  "likes_count": 0,
  "liked": false,
  "created_at": "2020-02-10T09:00:27.425+00:00",
  "unread_comments_count": 0,
  "text": "Some text",
  "link": "https://www.google.com",
  "count_of_engaged_friends": 0,
  "comments_count": 0,
  "reposted": false,
  "source_feed_id": null,
  "chat": null
}</pre>
