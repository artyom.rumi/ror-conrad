# v1/My/Feeds API

## Delete feed

### DELETE /api/v1/my/feeds/:feed_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: DzCADxkb9ZuMEiBhcPgzELYm
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/my/feeds/1264</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;4f53cda18c2baa0c0354bb5f9a3ecbe5&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: bc463910-1128-412f-9af3-7001bc7e1e02
X-Runtime: 0.030791
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[

]</pre>
