# v1/My/Feeds API

## Delete feed and its reposts

### DELETE /api/v1/my/feeds/:feed_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: MzwZhYQRisZriFU6AcPnwfhr
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/my/feeds/1265</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;4f53cda18c2baa0c0354bb5f9a3ecbe5&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 3bed60d8-4ceb-4e60-b68b-4f9d29f79c88
X-Runtime: 0.091575
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 2</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[

]</pre>
