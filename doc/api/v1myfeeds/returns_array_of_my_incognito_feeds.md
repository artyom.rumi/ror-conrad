# v1/My/Feeds API

## returns array of my incognito feeds

### DELETE /api/v1/my/feeds/:feed_id

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| feed_id | Feed Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: PE97nLLLC4519LYdiXcdukyA
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/my/feeds/1268</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;17f76d6449e62b9e2176c2cf19984c82&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: f59b5a35-0e26-4ea0-9400-8dc3a1922caa
X-Runtime: 0.046061
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 282</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1269,
    "image": null,
    "user_id": 7929,
    "incognito": true,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-10T09:00:29.350+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
