# v1/My/Feeds API

## List of feeds

### GET /api/v1/my/feeds

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: TjC9ysmF28WKd7pjX9gHFvNF
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/feeds?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;a9a804e453efda7a65de9339e565875b&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e0e4436a-eba1-407a-819e-4e1f32e60e8c
X-Runtime: 0.092470
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1689</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1239,
    "image": null,
    "user_id": 7908,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-09T09:00:27.694+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": {
      "id": 566,
      "pubnub_channel_name": "chat_566",
      "updated_at": "2020-02-10T09:00:27.750+00:00",
      "incognito": false,
      "feed_id": 1239,
      "comment_id": null,
      "name": null,
      "is_group": true,
      "owner": {
        "id": 7907,
        "first_name": "Jen",
        "last_name": "Donnelly",
        "grade": 2011,
        "bio": "Sed excepturi nam error.",
        "school_name": "Repellat vitae unde corrupti.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-30.png",
          "small": "http://example.org/assets/small-avatars/avatar-30.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-30.png",
          "large": "http://example.org/assets/large-avatars/avatar-30.png"
        },
        "gender": "female",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 2,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": null
      },
      "users": [

      ],
      "deanonymizations": [

      ]
    }
  },
  {
    "id": 1241,
    "image": null,
    "user_id": 7908,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-08T09:00:27.703+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  },
  {
    "id": 1240,
    "image": null,
    "user_id": 7907,
    "incognito": false,
    "likes_count": 1,
    "liked": false,
    "created_at": "2020-02-07T09:00:27.698+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 1,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
