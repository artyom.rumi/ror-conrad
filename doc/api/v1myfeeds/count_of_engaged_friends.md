# v1/My/Feeds API

## Count of engaged friends

### GET /api/v1/my/feeds

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: UT5kVam5QQiASgpbENtdQ96D
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/feeds?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;f6068548b3eb9e432529a0f51f3ab01a&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 433e24e5-7572-406c-9ccb-9bc3e0e29416
X-Runtime: 0.127239
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1695</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1260,
    "image": null,
    "user_id": 7924,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-09T09:00:28.584+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": {
      "id": 571,
      "pubnub_channel_name": "chat_571",
      "updated_at": "2020-02-10T09:00:28.649+00:00",
      "incognito": false,
      "feed_id": 1260,
      "comment_id": null,
      "name": null,
      "is_group": true,
      "owner": {
        "id": 7923,
        "first_name": "Frederic",
        "last_name": "Bashirian",
        "grade": 2025,
        "bio": "Sint fugit quam illo.",
        "school_name": "Qui laboriosam labore iusto.",
        "avatar": null,
        "default_avatar": {
          "original": "http://example.org/assets/avatars/avatar-23.png",
          "small": "http://example.org/assets/small-avatars/avatar-23.png",
          "medium": "http://example.org/assets/medium-avatars/avatar-23.png",
          "large": "http://example.org/assets/large-avatars/avatar-23.png"
        },
        "gender": "non_binary",
        "followee": false,
        "blocked": false,
        "role": "ordinary",
        "chat_available": false,
        "custom_prop_available": false,
        "friends_count": 2,
        "pokes_count": 0,
        "hide_top_pokes": false,
        "friendship_status": null
      },
      "users": [

      ],
      "deanonymizations": [

      ]
    }
  },
  {
    "id": 1262,
    "image": null,
    "user_id": 7924,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-08T09:00:28.592+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  },
  {
    "id": 1261,
    "image": null,
    "user_id": 7923,
    "incognito": false,
    "likes_count": 1,
    "liked": false,
    "created_at": "2020-02-07T09:00:28.589+00:00",
    "unread_comments_count": 1,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 2,
    "comments_count": 1,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
