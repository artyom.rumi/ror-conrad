# v1/Prop deliveries from me API

## list of sent props

### GET /api/v1/users/:user_id/prop_deliveries_from_me

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | Id of the another user | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: dGhpUVqicwNag7LXnfiaeWZz
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/users/8079/prop_deliveries_from_me</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;797284751685d5789612a9abae147a18&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: d5b18d1f-4c07-4336-a7c7-5c62cd6af9cd
X-Runtime: 0.076380
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 784</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "created_at": "2020-02-10T09:00:37.809+00:00",
    "prop": {
      "id": 969,
      "content": "Qui dolore sequi rerum.",
      "emoji": "😍",
      "gender": "female"
    },
    "recipient": {
      "id": 8079,
      "first_name": "Romelia",
      "last_name": "Wiza",
      "grade": 2029,
      "bio": "In excepturi atque hic.",
      "school_name": "Qui quis repudiandae et.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-19.png",
        "small": "http://example.org/assets/small-avatars/avatar-19.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-19.png",
        "large": "http://example.org/assets/large-avatars/avatar-19.png"
      },
      "gender": "male",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 1,
      "hide_top_pokes": false,
      "friendship_status": "uninvited"
    }
  }
]</pre>
