# v1/Search props API

## List of props

### GET /api/v1/search_props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| content | Content of the searched prop | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: jrGiW787EktDSnVajyjpze9Q
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/search_props?content=my+heart</pre>

#### Query Parameters

<pre>content: my heart</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7cf86a43bc5c1aa33c92e205a48479f8&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 95d6c13d-ee79-4c6b-a57e-1c52322b69f3
X-Runtime: 0.294526
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 76</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1017,
    "content": "You warm my heart",
    "emoji": "😍",
    "gender": "female"
  }
]</pre>
