# v1/Contacts API

## List of users from contacts

### POST /api/v1/contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_numbers | Phone numbers | true |  |
| filter_schoolmates | Filter out schoolmates | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: qPAyJ7vkSK5TLs9WrdoEpXzy
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/contacts</pre>

#### Body

<pre>{"phone_numbers":["+12461073028","+19334154609"],"filter_schoolmates":null}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7b584d07a9fd1eac32d1619839221644&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: b87df5f8-45cb-49ee-928b-aa0ffd2b29b1
X-Runtime: 0.068568
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1292</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7527,
    "first_name": "Jerry",
    "last_name": "Deckow",
    "grade": 2011,
    "bio": "Aut et ut sit.",
    "school_name": "Excepturi debitis id assumenda.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-24.png",
      "small": "http://example.org/assets/small-avatars/avatar-24.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-24.png",
      "large": "http://example.org/assets/large-avatars/avatar-24.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 7528,
    "first_name": "Humberto",
    "last_name": "Dietrich",
    "grade": 2030,
    "bio": "Quibusdam sit aut laboriosam.",
    "school_name": "Mollitia quia qui molestiae.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-8.png",
      "small": "http://example.org/assets/small-avatars/avatar-8.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-8.png",
      "large": "http://example.org/assets/large-avatars/avatar-8.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
