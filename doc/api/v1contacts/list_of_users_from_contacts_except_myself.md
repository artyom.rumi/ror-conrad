# v1/Contacts API

## List of users from contacts except myself

### POST /api/v1/contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_numbers | Phone numbers | true |  |
| filter_schoolmates | Filter out schoolmates | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: BaZSBxvA1AjZzC9rozhaYnkY
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/contacts</pre>

#### Body

<pre>{"phone_numbers":["+12403909970","+19790575681","+15700418248"],"filter_schoolmates":null}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7a6a17d187294fc0c74d69be237d48d9&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 1038b8b1-328c-4d94-b41d-0cea2f660a20
X-Runtime: 0.034039
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1285</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7537,
    "first_name": "Bo",
    "last_name": "Treutel",
    "grade": 2011,
    "bio": "Ut id ratione ut.",
    "school_name": "Veniam minus quisquam et.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-30.png",
      "small": "http://example.org/assets/small-avatars/avatar-30.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-30.png",
      "large": "http://example.org/assets/large-avatars/avatar-30.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 7538,
    "first_name": "Regine",
    "last_name": "Okuneva",
    "grade": 2028,
    "bio": "Inventore neque possimus deleniti.",
    "school_name": "Placeat labore sint numquam.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-29.png",
      "small": "http://example.org/assets/small-avatars/avatar-29.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-29.png",
      "large": "http://example.org/assets/large-avatars/avatar-29.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
