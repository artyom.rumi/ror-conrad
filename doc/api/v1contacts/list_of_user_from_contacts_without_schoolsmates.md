# v1/Contacts API

## List of user from contacts without schoolsmates

### POST /api/v1/contacts

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_numbers | Phone numbers | true |  |
| filter_schoolmates | Filter out schoolmates | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: XTbc6PjjKjshGou7mdLnZWYU
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/contacts</pre>

#### Body

<pre>{"phone_numbers":["+14886768512","+10431191042"],"filter_schoolmates":true}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;4f657f5f167fce438b923d96e72171dc&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 20a41a68-76f5-471a-8603-5f0798a1178e
X-Runtime: 0.031594
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 651</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7535,
    "first_name": "Dung",
    "last_name": "Hane",
    "grade": 2029,
    "bio": "Blanditiis saepe qui voluptas.",
    "school_name": "Tempora consectetur non ea.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-17.png",
      "small": "http://example.org/assets/small-avatars/avatar-17.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-17.png",
      "large": "http://example.org/assets/large-avatars/avatar-17.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
