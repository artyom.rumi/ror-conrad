# v1/My/ActiveFriends API

## List of active friends

### GET /api/v1/my/active_friends

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: xwYeFSZjNv5V5s1fWSbmGSE1
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/active_friends?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5cc7e6d450cf266b70bb8372f070fa36&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: d7a4a55c-d0af-4b27-873a-2c5f8b5a1960
X-Runtime: 0.070713
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 711</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "users": [
    {
      "id": 7804,
      "first_name": "Elease",
      "last_name": "Zboncak",
      "grade": 2026,
      "bio": "Quaerat similique exercitationem iusto.",
      "school_name": "Et in sunt expedita.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-6.png",
        "small": "http://example.org/assets/small-avatars/avatar-6.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-6.png",
        "large": "http://example.org/assets/large-avatars/avatar-6.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": true,
      "custom_prop_available": true,
      "friends_count": 1,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "friend",
      "days_to_expired_friendship": 5
    }
  ],
  "meta": {
    "users_count": 1
  }
}</pre>
