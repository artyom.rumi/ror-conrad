# v1/My/Keeps API

## Create keep

### POST /api/v1/my/keeps

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | Keep to user | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: AP8ng3yTvvD3e6JMQ8XHrhKF
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/my/keeps</pre>

#### Body

<pre>{"user_id":7956}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;1d7e8109e4dfe4e91acc1a4b843bdcaf&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 43a7cfa4-ca6b-4cca-8145-fc9ee3e8176c
X-Runtime: 0.085313
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 668</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 7956,
  "first_name": "Jason",
  "last_name": "Braun",
  "grade": 2028,
  "bio": "Cumque saepe sapiente similique.",
  "school_name": "Labore architecto cumque recusandae.",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-15.png",
    "small": "http://example.org/assets/small-avatars/avatar-15.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-15.png",
    "large": "http://example.org/assets/large-avatars/avatar-15.png"
  },
  "gender": "female",
  "followee": false,
  "blocked": false,
  "role": "ordinary",
  "chat_available": true,
  "custom_prop_available": true,
  "friends_count": 1,
  "pokes_count": 0,
  "hide_top_pokes": false,
  "friendship_status": "friend",
  "top_pokes": [

  ]
}</pre>
