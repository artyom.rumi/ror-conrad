# v1/My/Discoveries API

## List of discoveries feeds

### GET /api/v1/my/discoveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Kj9aw9wSok4otQfgHQh9G8m8
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/discoveries?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;d839bb331e4a7132c979ed1f9dbc57a9&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 594c1754-737c-4c12-b587-6196168a6138
X-Runtime: 0.108186
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 565</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 1214,
    "image": null,
    "user_id": 7859,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-08T09:00:25.837+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  },
  {
    "id": 1215,
    "image": null,
    "user_id": 7860,
    "incognito": false,
    "likes_count": 0,
    "liked": false,
    "created_at": "2020-02-04T09:00:25.858+00:00",
    "unread_comments_count": 0,
    "text": null,
    "link": null,
    "count_of_engaged_friends": 0,
    "comments_count": 0,
    "reposted": false,
    "source_feed_id": null,
    "chat": null
  }
]</pre>
