# v1/Verification Codes API

## Send verification code to a phone

### POST /api/v1/verification_codes

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| phone_number | Phone number to send SMS with the verification code to | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/verification_codes</pre>

#### Body

<pre>{"phone_number":"+11052578967"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;faee13476516b347be83a159c47a96c2&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: a569cfaa-4ad1-4da8-a1af-34020f29e296
X-Runtime: 0.048197
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 110</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "phone_number": "+11052578967",
  "user_id": null,
  "expires_at": "2020-02-10T11:00:56.055+00:00",
  "attempts_count": 3
}</pre>
