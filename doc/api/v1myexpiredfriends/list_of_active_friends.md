# v1/My/ExpiredFriends API

## List of active friends

### GET /api/v1/my/expired_friends

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: kCoVdkq7BvdBig3Bi6Pc8Kns
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/expired_friends?page=1&amp;per_page=5</pre>

#### Query Parameters

<pre>page: 1
per_page: 5</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;0fe4c0399643825ec8821335decc523f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 974533f3-4fba-40c7-b142-0168260265ea
X-Runtime: 0.069694
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 710</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "users": [
    {
      "id": 7884,
      "first_name": "Kieth",
      "last_name": "Dietrich",
      "grade": 2030,
      "bio": "Quidem officiis eum magni.",
      "school_name": "Hic voluptate ad qui.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-23.png",
        "small": "http://example.org/assets/small-avatars/avatar-23.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-23.png",
        "large": "http://example.org/assets/large-avatars/avatar-23.png"
      },
      "gender": "non_binary",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": true,
      "custom_prop_available": true,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": "expired",
      "days_to_expired_friendship": -50
    }
  ],
  "meta": {
    "users_count": 1
  }
}</pre>
