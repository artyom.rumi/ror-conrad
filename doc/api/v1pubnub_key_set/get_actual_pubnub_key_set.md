# v1/PubNub key set API

## get actual pubnub key set

### GET /api/v1/pubnub_key_set
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 8kSthpxNo3yf1adbqY9w4g5H
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/pubnub_key_set</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;dba28adc300360782a6db94478558344&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 753ce795-f8fa-4b3d-8e8e-4a90d0aa878e
X-Runtime: 0.033306
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 63</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "pubnub_publish_key": "hakuna",
  "pubnub_subscribe_key": "matata"
}</pre>
