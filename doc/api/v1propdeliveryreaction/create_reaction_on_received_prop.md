# v1/PropDelivery/Reaction API

## Create reaction on received prop

### POST /api/v1/prop_deliveries/:prop_delivery_id/reaction

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| prop_delivery_id | ID of prop delivery for replying | true |  |
| content | Text content of reaction | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 4KbvNaAY6vmRTGhyf6CfnpcG
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/prop_deliveries/566/reaction</pre>

#### Body

<pre>{"content":"Your boost is so-so."}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;240b9534a4b21554ab66eb2af7ed8052&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: b688077d-af4b-470d-b14e-7ecd2524382b
X-Runtime: 0.852193
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 115</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 56,
  "content": "Your boost is so-so.",
  "photo": null,
  "read_at": null,
  "created_at": "2020-02-10T09:00:36.271+00:00"
}</pre>
