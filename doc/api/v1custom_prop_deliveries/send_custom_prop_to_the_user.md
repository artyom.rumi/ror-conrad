# v1/Custom prop deliveries API

## Send custom prop to the user

### POST /api/v1/custom_prop_deliveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| recipient_id | User to recieve the prop | true |  |
| content | Content of custom prop | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: gxHcSXjR5f8HdLaPJocZngtQ
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/custom_prop_deliveries</pre>

#### Body

<pre>{"recipient_id":7540,"content":"You are the best person I know!"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;e1bd8a8d380fb71029feff516bbace6f&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e9772002-bdc2-427f-acf6-6557f4b3495f
X-Runtime: 1.019798
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 208</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 545,
  "sender_id": 7539,
  "recipient_id": 7540,
  "prop_id": null,
  "content": "You are the best person I know!",
  "emoji": "😍",
  "viewed_at": null,
  "sender_gender": "male",
  "type": "custom",
  "request": null,
  "reaction": null
}</pre>
