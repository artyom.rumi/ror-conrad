# v1/Friends API

## List of friends matched by keywords

### GET /api/v1/friends

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: KBGAu2qDnYgem1vAhXCU8ZUn
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/friends?keywords=Brad</pre>

#### Query Parameters

<pre>keywords: Brad</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;e64abe0fb86ca8165d679238c9eb74c8&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: b702facb-262d-4555-ac0e-5ddd60559160
X-Runtime: 0.040504
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 683</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7728,
    "first_name": "Brad",
    "last_name": "Pitt",
    "grade": 2027,
    "bio": "Nihil harum cum et.",
    "school_name": "Temporibus distinctio exercitationem omnis.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-18.png",
      "small": "http://example.org/assets/small-avatars/avatar-18.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-18.png",
      "large": "http://example.org/assets/large-avatars/avatar-18.png"
    },
    "gender": "non_binary",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "expired",
    "days_to_expired_friendship": 0
  }
]</pre>
