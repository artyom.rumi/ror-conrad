# v1/Friends API

## List of friends

### GET /api/v1/friends

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| page | Page number | false |  |
| per_page | Per page | false |  |
| keywords | Full name search keywords | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: QjWDJ9MUeoKgUJLV1pQq7Cje
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/friends</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5bcfddd8dc4c94b1266b198c0a7ad35c&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: def18ed6-abff-4696-8b56-408d7304ad4c
X-Runtime: 0.101580
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1986</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7711,
    "first_name": "Brad",
    "last_name": "Pitt",
    "grade": 2014,
    "bio": "Sit totam est quia.",
    "school_name": "Est non modi fuga.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-26.png",
      "small": "http://example.org/assets/small-avatars/avatar-26.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-26.png",
      "large": "http://example.org/assets/large-avatars/avatar-26.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "expired",
    "days_to_expired_friendship": 0
  },
  {
    "id": 7712,
    "first_name": "George",
    "last_name": "Clooney",
    "grade": 2014,
    "bio": "Illo occaecati aut sint.",
    "school_name": "Blanditiis et porro sed.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-21.png",
      "small": "http://example.org/assets/small-avatars/avatar-21.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-21.png",
      "large": "http://example.org/assets/large-avatars/avatar-21.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "expired",
    "days_to_expired_friendship": -10
  },
  {
    "id": 7713,
    "first_name": "Matt",
    "last_name": "Damon",
    "grade": 2012,
    "bio": "Totam molestiae ut minima.",
    "school_name": "Ipsa fugiat natus est.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-15.png",
      "small": "http://example.org/assets/small-avatars/avatar-15.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-15.png",
      "large": "http://example.org/assets/large-avatars/avatar-15.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": true,
    "custom_prop_available": true,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "expired",
    "days_to_expired_friendship": -40
  }
]</pre>
