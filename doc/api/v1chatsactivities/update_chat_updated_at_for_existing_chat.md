# v1/Chats/Activities API

## update chat updated_at for existing chat

### POST /api/v1/chats/:id/activities

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Chat Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: w6cZcSnPbHYfzyh5fh9MGyNt
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/chats/525/activities</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;de61c98b253567998a1401bc2780e628&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 913a4898-3ee3-4c49-80ab-9535b2e20856
X-Runtime: 0.132827
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1485</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 525,
  "pubnub_channel_name": "chat_525",
  "updated_at": "2020-02-10T09:00:02.213+00:00",
  "incognito": false,
  "feed_id": null,
  "comment_id": null,
  "name": null,
  "is_group": true,
  "owner": {
    "id": 7395,
    "first_name": "Ronny",
    "last_name": "Blanda",
    "grade": 2016,
    "bio": "Commodi autem occaecati inventore.",
    "school_name": "Saepe et illo molestiae.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-1.png",
      "small": "http://example.org/assets/small-avatars/avatar-1.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-1.png",
      "large": "http://example.org/assets/large-avatars/avatar-1.png"
    },
    "gender": "female",
    "followee": false,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": null
  },
  "users": [
    {
      "id": 7395,
      "first_name": "Ronny",
      "last_name": "Blanda",
      "grade": 2016,
      "bio": "Commodi autem occaecati inventore.",
      "school_name": "Saepe et illo molestiae.",
      "avatar": null,
      "default_avatar": {
        "original": "http://example.org/assets/avatars/avatar-1.png",
        "small": "http://example.org/assets/small-avatars/avatar-1.png",
        "medium": "http://example.org/assets/medium-avatars/avatar-1.png",
        "large": "http://example.org/assets/large-avatars/avatar-1.png"
      },
      "gender": "female",
      "followee": false,
      "blocked": false,
      "role": "ordinary",
      "chat_available": false,
      "custom_prop_available": false,
      "friends_count": 0,
      "pokes_count": 0,
      "hide_top_pokes": false,
      "friendship_status": null
    }
  ],
  "deanonymizations": [

  ]
}</pre>
