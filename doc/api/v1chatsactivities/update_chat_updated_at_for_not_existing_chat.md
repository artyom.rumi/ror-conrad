# v1/Chats/Activities API

## update chat updated_at for not existing chat

### POST /api/v1/chats/:id/activities

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Chat Id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 2bdQjEJ58QVG7aTsPuaBB1FX
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/chats/526/activities</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
Cache-Control: no-cache
X-Request-Id: 389c5440-b44f-49f0-8a34-7b663263264b
X-Runtime: 0.026635
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 92</pre>

#### Status

<pre>404 Not Found</pre>

#### Body

<pre>{
  "error": "Couldn't find Chat with 'id'=526 [WHERE \"chat_subscriptions\".\"user_id\" = $1]"
}</pre>
