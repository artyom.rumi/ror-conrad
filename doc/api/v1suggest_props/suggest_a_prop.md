# v1/Suggest props API

## Suggest a prop

### POST /api/v1/suggest_props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| content | Suggested prop content | true |  |
| emoji | Suggested prop emoji | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: CzJEhZvpiCoMCyeVhe1Zn6jt
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/suggest_props</pre>

#### Body

<pre>{"content":"Autem commodi enim non.","emoji":"🌺"}</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: eef64d1e-2870-485b-9543-cfd0708693af
X-Runtime: 0.062894
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

