# Report user API

## Report user

### POST /api/v1/reports

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| reported_user_id | Reported user id | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: vnq5URPSVMKQutUPko89M4az
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/reports</pre>

#### Body

<pre>{"reported_user_id":8162}</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 2881aed2-5f7a-46a4-8a98-a8c607351bac
X-Runtime: 0.027178
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

