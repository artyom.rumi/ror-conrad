# v1/Schools API

## List of schools found by name

### GET /api/v1/schools

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| year_of_ending | Year of ending | true |  |
| latitude | User latitude | false |  |
| longitude | User longitude | false |  |
| keywords | School name keywords | false |  |
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: bknhZZ5AbZTCSpbXSg97ecRc
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/schools?year_of_ending=2021&amp;keywords=info</pre>

#### Query Parameters

<pre>year_of_ending: 2021
keywords: info</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;c4fa3d680c4f11140bb4aff6e8cb3f58&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 818ac3e8-9215-4f4c-8efc-1796bd357240
X-Runtime: 0.203040
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 111</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8242,
    "name": "School of Information Technologies",
    "members_amount": 7,
    "address": "New Loydview, Oklahoma"
  }
]</pre>
