# v1/Schools API

## List of schools on another page

### GET /api/v1/schools

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| year_of_ending | Year of ending | true |  |
| latitude | User latitude | false |  |
| longitude | User longitude | false |  |
| keywords | School name keywords | false |  |
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: Fo5s2MTZHt7d8Wjkep2HbJcU
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/schools?year_of_ending=2021&amp;page=2&amp;per_page=2</pre>

#### Query Parameters

<pre>year_of_ending: 2021
page: 2
per_page: 2</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;65eaeed13b26509bc3b4788039c8998c&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: cb90a958-d08f-4351-9fa9-10212526087e
X-Runtime: 0.028078
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 105</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8235,
    "name": "School of Language",
    "members_amount": 17,
    "address": "South Venessabury, Rhode Island"
  }
]</pre>
