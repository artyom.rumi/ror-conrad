# v1/Schools API

## List of schools

### GET /api/v1/schools

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| year_of_ending | Year of ending | true |  |
| latitude | User latitude | false |  |
| longitude | User longitude | false |  |
| keywords | School name keywords | false |  |
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 6gFfXCdYKp5GXo9n4agRzxLr
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/schools?year_of_ending=2021</pre>

#### Query Parameters

<pre>year_of_ending: 2021</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;13d4077141ce358a9319737edfc25d8b&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 3d567fbd-f819-4618-8a5e-1793cf99ccea
X-Runtime: 0.021724
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 205</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8244,
    "name": "Judges Academy",
    "members_amount": 62,
    "address": "O'Connerfort, Hawaii"
  },
  {
    "id": 8246,
    "name": "School of Information Technologies",
    "members_amount": 74,
    "address": "Watsicabury, New Hampshire"
  }
]</pre>
