# v1/Schools API

## List of schools close to a specific place

### GET /api/v1/schools

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| year_of_ending | Year of ending | true |  |
| latitude | User latitude | false |  |
| longitude | User longitude | false |  |
| keywords | School name keywords | false |  |
| page | Page number | false |  |
| per_page | Per page | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: dUJwhtG3xRNj6ZP7FrBEfocu
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/schools?year_of_ending=2021&amp;latitude=1.1&amp;longitude=1.1</pre>

#### Query Parameters

<pre>year_of_ending: 2021
latitude: 1.1
longitude: 1.1</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;3644972ec47f38205213deec225f8dd8&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 84e044fe-acb6-452d-9ed3-000c957f0b07
X-Runtime: 0.179222
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 205</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8238,
    "name": "School of Information Technologies",
    "members_amount": 11,
    "address": "Corkeryfurt, New Mexico"
  },
  {
    "id": 8236,
    "name": "Judges Academy",
    "members_amount": 47,
    "address": "South Roycetown, Kansas"
  }
]</pre>
