# v1/requests PropDelivery API

## Send prop to the user

### POST /api/v1/requests/:request_id/prop_deliveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| request_id | Request for props | true |  |
| recipient_id | User to recieve the prop | true |  |
| prop_id | Prop to send to user | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: VQfQjTjRDi9dEWFKDs5u1EMq
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/requests/310/prop_deliveries</pre>

#### Body

<pre>{"recipient_id":8164,"prop_id":1016}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;26696447ffff8a17d756d6fa232d4866&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: e6dadce4-a176-40fd-a997-1325f6b45b29
X-Runtime: 0.073801
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 290</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 601,
  "sender_id": 8163,
  "recipient_id": 8164,
  "prop_id": 1016,
  "created_at": "2020-02-10T09:00:42.451+00:00",
  "updated_at": "2020-02-10T09:00:42.451+00:00",
  "request_id": 310,
  "viewed_at": null,
  "sender_gender": "female",
  "content": "Ea deleniti tempore adipisci.",
  "emoji": "😍",
  "boost_type": "secret"
}</pre>
