# v1/Profiles API

## Show current user

### GET /api/v1/profile
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: rj1QvW8odfTLA86LHRJ4j7ST
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/profile</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;374f17ae17e7a72ec95839f89b23cf50&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 6d6709fc-cab1-4704-bcd6-57a0e346ad84
X-Runtime: 0.043616
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 936</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 8056,
  "first_name": "Mel",
  "last_name": "Emard",
  "age": 18,
  "grade": 2029,
  "gender": "non_binary",
  "phone_number": "+13268826842",
  "created_at": "2020-02-10T09:00:36.012+00:00",
  "avatar": null,
  "default_avatar": {
    "original": "http://example.org/assets/avatars/avatar-17.png",
    "small": "http://example.org/assets/small-avatars/avatar-17.png",
    "medium": "http://example.org/assets/medium-avatars/avatar-17.png",
    "large": "http://example.org/assets/large-avatars/avatar-17.png"
  },
  "bio": "Nostrum veniam repellendus sit.",
  "matching_enabled": true,
  "prop_request_notification_enabled": true,
  "receiving_prop_notification_enabled": true,
  "chat_messages_notification_enabled": true,
  "new_friends_notification_enabled": true,
  "new_users_notification_enabled": true,
  "friend_boost_at": null,
  "friends_count": 0,
  "pokes_count": 0,
  "top_pokes": [

  ],
  "hide_top_pokes": false,
  "school": {
    "id": 8060,
    "name": "Quia sunt molestiae beatae.",
    "members_amount": 41,
    "address": "Dorindabury, South Dakota"
  }
}</pre>
