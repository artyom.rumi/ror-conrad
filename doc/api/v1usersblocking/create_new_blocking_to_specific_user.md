# v1/Users/Blocking API

## create new blocking to specific user

### POST /api/v1/users/:user_id/blocking

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | ID of the user to block | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: FSo63FkFyjBrdbtMC6JePobT
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/users/8251/blocking</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7b27338da068894ae380bab6584177cf&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 72adaa07-db9d-46ae-b663-f6f251e46a09
X-Runtime: 0.076132
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 114</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>{
  "id": 190,
  "user_id": 8250,
  "blockable_type": "User",
  "blockable_id": 8251,
  "created_at": "2020-02-10T09:00:53.664+00:00"
}</pre>
