# v1/Users/Blocking API

## destroy blocking

### DELETE /api/v1/users/:user_id/blocking

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| user_id | ID of the user to unblock | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: PrAAyBLPbiz6yTWCU5irVC3f
Host: example.org
Cookie: </pre>

#### Route

<pre>DELETE /api/v1/users/8264/blocking</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 7fe30a93-87f3-4402-b21c-dfb3d547e743
X-Runtime: 0.040220
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

