# v1/Request props API

## Request props

### POST /api/v1/requests
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: SH2Cy8Afe7yXtVFHDEPNFoHd
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/requests</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 455c4f5b-33ae-4390-b6b9-e4914a9c186f
X-Runtime: 0.045229
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

