# v1/PropDelivery/Views API

## mark prop delivery as viewed

### POST /api/v1/prop_deliveries/:id/views

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| id | Prop delivery ID | true |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 6Kz2wZ6nC6Y7huZCz5gsgdr6
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/prop_deliveries/570/views</pre>

### Response

#### Headers

<pre>Cache-Control: no-cache
X-Request-Id: 220d724e-4775-4980-b10d-f52ba258200e
X-Runtime: 0.084332
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly</pre>

#### Status

<pre>204 No Content</pre>

