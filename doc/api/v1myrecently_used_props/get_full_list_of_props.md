# v1/my/Recently used Props API

## get full list of props

### GET /api/v1/my/recently_used_props

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| limit | Amount of props to be fetched | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: HdjvQfxgxuF8E5knRhWH41VY
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/recently_used_props</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;41496ee48f6fff4139378afc39164d58&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 27479e47-b447-428d-805c-abfbdf24fceb
X-Runtime: 0.048929
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 243</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 922,
    "content": "Qui omnis mollitia aliquid.",
    "emoji": "😍",
    "gender": null
  },
  {
    "id": 921,
    "content": "Et facere minima veniam.",
    "emoji": "😍",
    "gender": null
  },
  {
    "id": 923,
    "content": "Ipsa corporis perspiciatis iure.",
    "emoji": "😍",
    "gender": null
  }
]</pre>
