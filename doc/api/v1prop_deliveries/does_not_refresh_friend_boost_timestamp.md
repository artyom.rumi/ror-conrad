# v1/Prop deliveries API

## does not refresh friend boost timestamp

### POST /api/v1/prop_deliveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| recipient_id | User to recieve the prop | true |  |
| prop_id | Prop to send to user | true |  |
| boost_type | Type of boost, secret (default) or friend | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: fW7ikuxugEcDgZu2X376ejxS
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/prop_deliveries</pre>

#### Body

<pre>{"recipient_id":8100,"prop_id":979,"boost_type":"secret"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;5f812f82dd1b8ef1b26c66d973c2bc90&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 4cae7a7f-0176-4109-9ae8-7c3e9f4d48f9
X-Runtime: 0.034046
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 213</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 584,
  "sender_id": 8099,
  "recipient_id": 8100,
  "prop_id": 979,
  "content": "Ratione repudiandae culpa at.",
  "emoji": "😍",
  "viewed_at": null,
  "sender_gender": "non_binary",
  "type": "ordinary",
  "request": null,
  "reaction": null
}</pre>
