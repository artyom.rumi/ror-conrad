# v1/Prop deliveries API

## Send prop to the user

### POST /api/v1/prop_deliveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| recipient_id | User to recieve the prop | true |  |
| prop_id | Prop to send to user | true |  |
| boost_type | Type of boost, secret (default) or friend | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: eSHszZsHvPJu7gCEsugjAG1V
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/prop_deliveries</pre>

#### Body

<pre>{"recipient_id":8092,"prop_id":977}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;768d08252d99fa61a13184937f8ce012&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 953e0f77-a12a-4b27-8243-e3808ad9e0d7
X-Runtime: 0.059876
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 217</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 582,
  "sender_id": 8091,
  "recipient_id": 8092,
  "prop_id": 977,
  "content": "Et assumenda dolorem praesentium.",
  "emoji": "😍",
  "viewed_at": null,
  "sender_gender": "non_binary",
  "type": "ordinary",
  "request": null,
  "reaction": null
}</pre>
