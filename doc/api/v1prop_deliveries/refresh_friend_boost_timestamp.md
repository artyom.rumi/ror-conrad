# v1/Prop deliveries API

## refresh friend boost timestamp

### POST /api/v1/prop_deliveries

### Parameters

| Name | Description | Required | Scope |
|------|-------------|----------|-------|
| recipient_id | User to recieve the prop | true |  |
| prop_id | Prop to send to user | true |  |
| boost_type | Type of boost, secret (default) or friend | false |  |

### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: gbQBW5R8wGWrE98SYf7bm69v
Host: example.org
Cookie: </pre>

#### Route

<pre>POST /api/v1/prop_deliveries</pre>

#### Body

<pre>{"recipient_id":8098,"prop_id":978,"boost_type":"friend"}</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;00d8bda7f4620882ce2e9ed5bca5bba0&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: dbbce616-bed9-4532-b093-cb0e6f5d04fd
X-Runtime: 0.042815
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 203</pre>

#### Status

<pre>201 Created</pre>

#### Body

<pre>{
  "id": 583,
  "sender_id": 8097,
  "recipient_id": 8098,
  "prop_id": 978,
  "content": "Qui saepe ratione enim.",
  "emoji": "😍",
  "viewed_at": null,
  "sender_gender": "female",
  "type": "ordinary",
  "request": null,
  "reaction": null
}</pre>
