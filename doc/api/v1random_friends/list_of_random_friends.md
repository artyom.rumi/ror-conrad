# v1/Random friends API

## List of random friends

### GET /api/v1/random_friends
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: wC3qpgoJTPRDVeEMeUiNjrQv
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/random_friends</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;7a74986e822582800f465e7337bc29d6&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: 5afc623c-863b-43c8-9ba2-4589e1e28299
X-Runtime: 0.140004
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 3255</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 8117,
    "first_name": "Gerard",
    "last_name": "Davis",
    "grade": 2011,
    "bio": "Earum in blanditiis officiis.",
    "school_name": "Est impedit dolor ad.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-7.png",
      "small": "http://example.org/assets/small-avatars/avatar-7.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-7.png",
      "large": "http://example.org/assets/large-avatars/avatar-7.png"
    },
    "gender": "male",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8119,
    "first_name": "Diego",
    "last_name": "Willms",
    "grade": 2013,
    "bio": "Harum eos explicabo qui.",
    "school_name": "Quia architecto quos ratione.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-18.png",
      "small": "http://example.org/assets/small-avatars/avatar-18.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-18.png",
      "large": "http://example.org/assets/large-avatars/avatar-18.png"
    },
    "gender": "non_binary",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8120,
    "first_name": "Chante",
    "last_name": "Dietrich",
    "grade": 2011,
    "bio": "Repellat sit et dolorum.",
    "school_name": "Temporibus molestias laudantium nihil.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-7.png",
      "small": "http://example.org/assets/small-avatars/avatar-7.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-7.png",
      "large": "http://example.org/assets/large-avatars/avatar-7.png"
    },
    "gender": "female",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8118,
    "first_name": "Van",
    "last_name": "Prosacco",
    "grade": 2014,
    "bio": "Repudiandae blanditiis repellendus assumenda.",
    "school_name": "Ullam officiis consequatur itaque.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-25.png",
      "small": "http://example.org/assets/small-avatars/avatar-25.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-25.png",
      "large": "http://example.org/assets/large-avatars/avatar-25.png"
    },
    "gender": "male",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 8116,
    "first_name": "Demetrius",
    "last_name": "Mueller",
    "grade": 2021,
    "bio": "Quos ducimus voluptatem beatae.",
    "school_name": "Ratione nostrum ex ipsa.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-16.png",
      "small": "http://example.org/assets/small-avatars/avatar-16.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-16.png",
      "large": "http://example.org/assets/large-avatars/avatar-16.png"
    },
    "gender": "female",
    "followee": true,
    "blocked": false,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
