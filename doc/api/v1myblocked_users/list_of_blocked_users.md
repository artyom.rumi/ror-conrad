# v1/My/Blocked users API

## List of blocked users

### GET /api/v1/my/blocked_users
### Request

#### Headers

<pre>Accept: application/json
Content-Type: application/json
X-Auth-Token: 5VsHjcEb5toAntj2Uay491rx
Host: example.org
Cookie: </pre>

#### Route

<pre>GET /api/v1/my/blocked_users</pre>

### Response

#### Headers

<pre>Content-Type: application/json; charset=utf-8
Vary: Accept-Encoding
ETag: W/&quot;2afc0709cd17f67a9275acaaaab60c71&quot;
Cache-Control: max-age=0, private, must-revalidate
X-Request-Id: a883678c-605e-4d36-a075-5edfbd557320
X-Runtime: 0.065950
Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly
Content-Length: 1294</pre>

#### Status

<pre>200 OK</pre>

#### Body

<pre>[
  {
    "id": 7838,
    "first_name": "Dan",
    "last_name": "Gibson",
    "grade": 2014,
    "bio": "Vero molestiae expedita vel.",
    "school_name": "Aliquam quibusdam vel asperiores.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-11.png",
      "small": "http://example.org/assets/small-avatars/avatar-11.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-11.png",
      "large": "http://example.org/assets/large-avatars/avatar-11.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": true,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  },
  {
    "id": 7839,
    "first_name": "Lemuel",
    "last_name": "Hoeger",
    "grade": 2014,
    "bio": "Provident porro reiciendis quasi.",
    "school_name": "Sequi harum amet possimus.",
    "avatar": null,
    "default_avatar": {
      "original": "http://example.org/assets/avatars/avatar-7.png",
      "small": "http://example.org/assets/small-avatars/avatar-7.png",
      "medium": "http://example.org/assets/medium-avatars/avatar-7.png",
      "large": "http://example.org/assets/large-avatars/avatar-7.png"
    },
    "gender": "male",
    "followee": false,
    "blocked": true,
    "role": "ordinary",
    "chat_available": false,
    "custom_prop_available": false,
    "friends_count": 0,
    "pokes_count": 0,
    "hide_top_pokes": false,
    "friendship_status": "uninvited"
  }
]</pre>
