# Props api


## Report user

* [Report user](report_user/report_user.md)

## v1/Auth tokens

* [Create an auth token](v1auth_tokens/create_an_auth_token.md)
* [Destroy auth token](v1auth_tokens/destroy_auth_token.md)
* [Invalid or expired verification code](v1auth_tokens/invalid_or_expired_verification_code.md)

## v1/Avatar

* [Destroy avatar](v1avatar/destroy_avatar.md)
* [Upload an image](v1avatar/upload_an_image.md)

## v1/Chats

* [Get chat info](v1chats/get_chat_info.md)
* [List of chats](v1chats/list_of_chats.md)
* [always create a new one regardless](v1chats/always_create_a_new_one_regardless.md)
* [create chat](v1chats/create_chat.md)

## v1/Chats/Activities

* [update chat updated_at for existing chat](v1chatsactivities/update_chat_updated_at_for_existing_chat.md)
* [update chat updated_at for not existing chat](v1chatsactivities/update_chat_updated_at_for_not_existing_chat.md)

## v1/Chats/Deanonymizations

* [Create deanonymization](v1chatsdeanonymizations/create_deanonymization.md)
* [List deanonymizations for specified chat](v1chatsdeanonymizations/list_deanonymizations_for_specified_chat.md)
* [destroy deanonymization](v1chatsdeanonymizations/destroy_deanonymization.md)

## v1/CollocutorMasks

* [Info about collocutor mask](v1collocutormasks/info_about_collocutor_mask.md)
* [List of collocutor masks](v1collocutormasks/list_of_collocutor_masks.md)
* [Update a collocutor mask](v1collocutormasks/update_a_collocutor_mask.md)

## v1/CollocutorMasks/Blocking

* [create new blocking to specific collocutor mask](v1collocutormasksblocking/create_new_blocking_to_specific_collocutor_mask.md)
* [destroy blocking](v1collocutormasksblocking/destroy_blocking.md)

## v1/Comments/Chats

* [create chat](v1commentschats/create_chat.md)

## v1/Contacts

* [List of user from contacts without schoolsmates](v1contacts/list_of_user_from_contacts_without_schoolsmates.md)
* [List of users from contacts](v1contacts/list_of_users_from_contacts.md)
* [List of users from contacts except myself](v1contacts/list_of_users_from_contacts_except_myself.md)

## v1/Custom prop deliveries

* [Send custom prop to the user](v1custom_prop_deliveries/send_custom_prop_to_the_user.md)

## v1/Device

* [update user device](v1device/update_user_device.md)

## v1/Fake request props

* [Create fake request](v1fake_request_props/create_fake_request.md)

## v1/Feedback

* [Send feedback](v1feedback/send_feedback.md)

## v1/Feeds/Chats

* [create chat](v1feedschats/create_chat.md)
* [it cannot be created](v1feedschats/it_cannot_be_created.md)

## v1/Feeds/Comments

* [Create comment](v1feedscomments/create_comment.md)
* [Delete comment](v1feedscomments/delete_comment.md)
* [List of comments](v1feedscomments/list_of_comments.md)
* [Mark comments](v1feedscomments/mark_comments.md)
* [Mark comments](v1feedscomments/mark_comments.md)
* [Update comment](v1feedscomments/update_comment.md)

## v1/Feeds/Likes

* [Create like](v1feedslikes/create_like.md)
* [Delete like](v1feedslikes/delete_like.md)

## v1/Feeds/Reposts

* [Create repost feed](v1feedsreposts/create_repost_feed.md)

## v1/Followees

* [List of followees](v1followees/list_of_followees.md)
* [List of followees on another page](v1followees/list_of_followees_on_another_page.md)
* [List of folowees matched by keywords](v1followees/list_of_folowees_matched_by_keywords.md)

## v1/Followers

* [List of followers](v1followers/list_of_followers.md)

## v1/FriendIntentions

* [Friend intention](v1friendintentions/friend_intention.md)

## v1/Friends

* [List of friends](v1friends/list_of_friends.md)
* [List of friends matched by keywords](v1friends/list_of_friends_matched_by_keywords.md)

## v1/Incoming requests

* [List of opened incoming requests](v1incoming_requests/list_of_opened_incoming_requests.md)
* [Specific request](v1incoming_requests/specific_request.md)

## v1/Interlocutors

* [List of friends](v1interlocutors/list_of_friends.md)
* [List of friends on another page](v1interlocutors/list_of_friends_on_another_page.md)
* [List of interlocutors matched by keywords](v1interlocutors/list_of_interlocutors_matched_by_keywords.md)

## v1/Invitations

* [Send invite message to friends](v1invitations/send_invite_message_to_friends.md)

## v1/My/ActiveFriends

* [List of active friends](v1myactivefriends/list_of_active_friends.md)

## v1/My/Blocked users

* [List of blocked users](v1myblocked_users/list_of_blocked_users.md)

## v1/My/Contacts

* [Synchronize community for new contacts](v1mycontacts/synchronize_community_for_new_contacts.md)
* [Synchronize contacts list](v1mycontacts/synchronize_contacts_list.md)

## v1/My/Discoveries

* [List of discoveries feeds](v1mydiscoveries/list_of_discoveries_feeds.md)

## v1/My/ExpiredFriends

* [List of active friends](v1myexpiredfriends/list_of_active_friends.md)

## v1/My/Feeds

* [Count of engaged friends](v1myfeeds/count_of_engaged_friends.md)
* [Count of engaged friends](v1myfeeds/count_of_engaged_friends.md)
* [Create feed](v1myfeeds/create_feed.md)
* [Create feed](v1myfeeds/create_feed.md)
* [Create feed](v1myfeeds/create_feed.md)
* [Delete feed](v1myfeeds/delete_feed.md)
* [Delete feed and its reposts](v1myfeeds/delete_feed_and_its_reposts.md)
* [List of feeds](v1myfeeds/list_of_feeds.md)
* [Update feed](v1myfeeds/update_feed.md)
* [not returns not incognito feeds](v1myfeeds/not_returns_not_incognito_feeds.md)
* [returns array of my incognito feeds](v1myfeeds/returns_array_of_my_incognito_feeds.md)

## v1/My/InlineContacts

* [List of inline contacts](v1myinlinecontacts/list_of_inline_contacts.md)

## v1/My/InvitedFriends

* [List of active friends](v1myinvitedfriends/list_of_active_friends.md)

## v1/My/Keeps

* [Create keep](v1mykeeps/create_keep.md)

## v1/My/Notifications

* [List of my notifications](v1mynotifications/list_of_my_notifications.md)
* [List of my notifications](v1mynotifications/list_of_my_notifications.md)
* [Mark notification as read](v1mynotifications/mark_notification_as_read.md)
* [Mark notifications as read](v1mynotifications/mark_notifications_as_read.md)

## v1/My/OutContacts

* [List of outline contacts](v1myoutcontacts/list_of_outline_contacts.md)

## v1/My/OwnFeeds

* [List of feeds](v1myownfeeds/list_of_feeds.md)
* [user&#39;s anonymous feeds](v1myownfeeds/user&#39;s_anonymous_feeds.md)

## v1/Pictures

* [Upload an image](v1pictures/upload_an_image.md)

## v1/Popular Props

* [get female list of props](v1popular_props/get_female_list_of_props.md)
* [get full list of props](v1popular_props/get_full_list_of_props.md)
* [get list of props for male](v1popular_props/get_list_of_props_for_male.md)

## v1/Profiles

* [Show current user](v1profiles/show_current_user.md)

## v1/Prop deliveries

* [Send prop to the user](v1prop_deliveries/send_prop_to_the_user.md)
* [does not refresh friend boost timestamp](v1prop_deliveries/does_not_refresh_friend_boost_timestamp.md)
* [refresh friend boost timestamp](v1prop_deliveries/refresh_friend_boost_timestamp.md)

## v1/Prop deliveries from me

* [list of sent props](v1prop_deliveries_from_me/list_of_sent_props.md)

## v1/PropDelivery/Reaction

* [Create reaction on received prop](v1propdeliveryreaction/create_reaction_on_received_prop.md)

## v1/PropDelivery/Views

* [mark prop delivery as viewed](v1propdeliveryviews/mark_prop_delivery_as_viewed.md)

## v1/Props

* [get full list of props](v1props/get_full_list_of_props.md)
* [get list of props for male](v1props/get_list_of_props_for_male.md)
* [get non-binary list of props](v1props/get_non-binary_list_of_props.md)

## v1/PubNub key set

* [get actual pubnub key set](v1pubnub_key_set/get_actual_pubnub_key_set.md)

## v1/Random friends

* [List of random friends](v1random_friends/list_of_random_friends.md)

## v1/Received props

* [list of ordinary unviewed props and props that viewed in the last 3 days](v1received_props/list_of_ordinary_unviewed_props_and_props_that_viewed_in_the_last_3_days.md)

## v1/Request props

* [Request props](v1request_props/request_props.md)

## v1/Schools

* [List of schools](v1schools/list_of_schools.md)
* [List of schools](v1schools/list_of_schools.md)
* [List of schools close to a specific place](v1schools/list_of_schools_close_to_a_specific_place.md)
* [List of schools found by name](v1schools/list_of_schools_found_by_name.md)
* [List of schools on another page](v1schools/list_of_schools_on_another_page.md)

## v1/Search props

* [List of props](v1search_props/list_of_props.md)

## v1/Sent props

* [list of sent props](v1sent_props/list_of_sent_props.md)

## v1/Special props from Emily

* [list of special props](v1special_props_from_emily/list_of_special_props.md)

## v1/Subscription

* [Destroy subscription by id](v1subscription/destroy_subscription_by_id.md)
* [Destroy subscription by id of user to unsubscribe from](v1subscription/destroy_subscription_by_id_of_user_to_unsubscribe_from.md)
* [Subscribe to the same user again](v1subscription/subscribe_to_the_same_user_again.md)
* [Subscribe to the user](v1subscription/subscribe_to_the_user.md)

## v1/Suggest props

* [Suggest a prop](v1suggest_props/suggest_a_prop.md)

## v1/User

* [Attempt to Create a user without confirmed device](v1user/attempt_to_create_a_user_without_confirmed_device.md)
* [Attempt to create a user with existing user associated with provided auth token](v1user/attempt_to_create_a_user_with_existing_user_associated_with_provided_auth_token.md)
* [Attempt to create a user with expired token](v1user/attempt_to_create_a_user_with_expired_token.md)
* [Create a user](v1user/create_a_user.md)
* [Create a user](v1user/create_a_user.md)
* [Get user info](v1user/get_user_info.md)
* [Update a user](v1user/update_a_user.md)
* [no longer returns top pokes](v1user/no_longer_returns_top_pokes.md)
* [subscribe to new school mates](v1user/subscribe_to_new_school_mates.md)

## v1/Users/Blocking

* [create new blocking to specific user](v1usersblocking/create_new_blocking_to_specific_user.md)
* [destroy blocking](v1usersblocking/destroy_blocking.md)

## v1/Users/Timelines

* [user&#39;s timeline](v1userstimelines/user&#39;s_timeline.md)

## v1/Verification Codes

* [Send verification code to a phone](v1verification_codes/send_verification_code_to_a_phone.md)

## v1/my/Blocked collocutor masks

* [List of collocutor masks](v1myblocked_collocutor_masks/list_of_collocutor_masks.md)

## v1/my/Recently used Props

* [get full list of props](v1myrecently_used_props/get_full_list_of_props.md)

## v1/requests PropDelivery

* [Send prop to the user](v1requests_propdelivery/send_prop_to_the_user.md)

## v1/requests Respondents

* [List of request respondents](v1requests_respondents/list_of_request_respondents.md)

## v1/requests Skip Delivery

* [Skip sending prop to the user](v1requests_skip_delivery/skip_sending_prop_to_the_user.md)

## v1/schoolmates

* [List of schoolmates](v1schoolmates/list_of_schoolmates.md)
* [List of schoolmates](v1schoolmates/list_of_schoolmates.md)
* [List of schoolmates](v1schoolmates/list_of_schoolmates.md)
* [List of schoolmates](v1schoolmates/list_of_schoolmates.md)

## v2/Received props

* [list of unviewed props and props that viewed in the last 3 days](v2received_props/list_of_unviewed_props_and_props_that_viewed_in_the_last_3_days.md)

