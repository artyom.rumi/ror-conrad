require "rails_helper"

describe Comments::Save do
  subject(:interactor_call) { described_class.call(comment_params: comment_params) }

  let(:feed) { create :feed, user: feed_author }
  let(:feed_author) { create :user }
  let(:another_user) { create :user }

  context "when Comment is valid" do
    let(:comment_params) { { feed_id: feed.id, user_id: another_user.id, text: "Example comment" } }

    it "creates comment" do
      expect { interactor_call }.to change(Comment, :count).by(1)
      is_expected.to be_a_success
    end

    it "creates notification after interactor call" do
      expect { interactor_call }.to change(Notification, :count).by(1)
    end

    context "when comment and feed author is a same user" do
      let(:comment_params) { { feed_id: feed.id, user_id: feed_author.id, text: "Example comment" } }

      it { expect { interactor_call }.not_to change(Notification, :count) }
    end
  end
end
