require "rails_helper"

describe Comments::Create do
  let(:expected_interactors) do
    [
      Comments::Save,
      BuildActivityPushNotification,
      SendPushNotifications
    ]
  end

  it "organizes expected interactors" do
    expect(described_class.organized).to eq(expected_interactors)
  end
end
