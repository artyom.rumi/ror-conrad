require "rails_helper"

describe Devices::SetPushTokenForChats do
  subject(:interactor_call) { described_class.call(device: device) }

  let(:pubnub_client) { instance_double "PubNubAdapter" }

  before do
    allow(PubNubAdapter).to receive(:new) { pubnub_client }
  end

  context "when device does not exists" do
    let(:device) { nil }

    it "doesn't set push token to all chats" do
      expect(pubnub_client)
        .not_to receive(:add_push_notification_registrations)

      interactor_call
    end
  end

  context "when device exists" do
    let(:device) { create :device, :with_user }
    let(:user) { device.user }

    context "when device user has no chats" do
      it "doesn't set push token to all chats" do
        expect(pubnub_client)
          .not_to receive(:add_push_notification_registrations)

        interactor_call
      end
    end

    context "when device user has chats" do
      let(:another_user) { create :user }

      let(:pubnub_channel_names) { "chat_1,incognito_chat_2" }

      before do
        create :chat, id: 1, users: [user, another_user]
        create :chat, id: 2, users: [user, another_user], incognito: true
      end

      context "when device push token was not updated" do
        before { device.update(phone_number: "+1234567891011") }

        it "doesn't set push token to all chats" do
          expect(pubnub_client)
            .not_to receive(:add_push_notification_registrations)

          interactor_call
        end
      end

      context "when device push token was updated" do
        before { device.update(push_token: "new_push_token") }

        it "sets push token to all chats" do
          expect(pubnub_client)
            .to receive(:add_push_notification_registrations)
            .with(device.push_token, pubnub_channel_names)

          interactor_call
        end
      end
    end
  end
end
