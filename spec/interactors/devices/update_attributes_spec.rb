require "rails_helper"

describe Devices::UpdateAttributes do
  subject(:interactor_call) { described_class.call(device: device, device_params: device_params) }

  let(:device) { create :device }

  context "when device params are valid" do
    let(:device_params) { { phone_number: "+10089789179", push_token: "new-push-token" } }

    it "updates device" do
      expect { interactor_call }
        .to change(device.reload, :phone_number).to(device_params[:phone_number])
                                                .and change(device.reload, :push_token).to(device_params[:push_token])
      is_expected.to be_a_success
    end
  end

  context "when device params are invalid" do
    let(:device_params) { { phone_number: "", push_token: "" } }

    it "doesn't update device" do
      expect { interactor_call }.not_to change { [device.reload.phone_number, device.reload.push_token] }

      is_expected.to be_a_failure
    end
  end
end
