require "rails_helper"

describe Devices::ClearPushTokenFromChats do
  subject(:clear_push_token) do
    described_class.call(
      device: device,
      device_params: device_params,
      log_out_process: log_out_process
    )
  end

  let(:log_out_process) { false }
  let(:pubnub_client) { instance_double "PubNubAdapter" }

  before do
    allow(PubNubAdapter).to receive(:new) { pubnub_client }
  end

  context "when device does not exists" do
    let(:device) { nil }
    let(:device_params) { nil }

    it "doesn't clear push token from all chats" do
      expect(pubnub_client).not_to receive(:remove_all_push_notification_registrations)

      clear_push_token
    end
  end

  context "when device exists" do
    let(:device) { create :device, :with_user }

    context "when device user has no chats" do
      let(:device_params) { { push_token: "new_push_token" } }

      it "doesn't set push token to all chats" do
        expect(pubnub_client).not_to receive(:add_push_notification_registrations)

        clear_push_token
      end
    end

    context "when device user has chats" do
      let(:user) { device.user }
      let(:another_user) { create :user }

      let(:pubnub_channel_names) { "chat_1,incognito_chat_2" }

      before do
        create :chat, id: 1, users: [user, another_user]
        create :chat, id: 2, users: [user, another_user], incognito: true
      end

      context "when there are new push token in params" do
        let(:device_params) { { push_token: "new_push_token" } }

        it "clears user push token from all chats" do
          expect(pubnub_client).to receive(:remove_all_push_notification_registrations).with(device.push_token)

          clear_push_token
        end
      end

      context "when log out processes" do
        let(:device_params) { nil }
        let(:log_out_process) { true }

        before do
          allow(pubnub_client).to receive(:remove_all_push_notification_registrations).with(device.push_token)
        end

        it "clears user push token from all chats" do
          expect(pubnub_client).to receive(:remove_all_push_notification_registrations).with(device.push_token)

          clear_push_token
        end

        it "deletes push token from device" do
          clear_push_token

          expect(device.push_token).to be_nil
        end
      end

      context "when there is no new push token in params and it's not a part of log out process" do
        let(:device_params) { { phone_number: "+18432343233" } }
        let(:log_out_process) { nil }

        it "doesn't clear push token from all chats" do
          expect(pubnub_client).not_to receive(:remove_all_push_notification_registrations).with(device.push_token)

          clear_push_token
        end
      end
    end
  end
end
