require "rails_helper"

describe PropDeliveries::PrepareParams do
  subject(:add_prop_attributes_to_params) { described_class.call(prop_delivery_params: prop_delivery_params) }

  let!(:prop) { create :prop, content: "I love you!", emoji: "😍" }

  let(:prop_delivery_params) do
    {
      prop_id: prop.id,
      recipient_id: 2
    }
  end

  let(:prepared_params) { add_prop_attributes_to_params.prop_delivery_params }

  let(:expected_params) do
    {
      prop_id: prop.id,
      recipient_id: 2,
      content: "I love you!",
      emoji: "😍"
    }
  end

  describe ".call" do
    context "when prop_id not specified in params" do
      let(:prop_delivery_params) do
        {
          recipient_id: 2
        }
      end

      it "doesn't prepare prop delivery params" do
        expect(prepared_params).to eq(prop_delivery_params)

        is_expected.to be_failure
      end
    end

    context "when correct prop_id specified in params" do
      it "prepares prop delivery params with related prop" do
        expect(prepared_params).to eq(expected_params)

        is_expected.to be_success
      end
    end
  end
end
