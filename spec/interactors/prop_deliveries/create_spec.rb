require "rails_helper"

describe PropDeliveries::Create do
  subject(:save_prop_delivery) { described_class.call(prop_delivery_params: prop_delivery_params) }

  let(:user) { create :user, gender: "male" }
  let(:another_user) { create :user }

  let(:prop) { create :prop, content: "I love you!", emoji: "😍" }
  let(:prop_delivery) { build :prop_delivery, :with_request, prop: prop }

  let(:prop_delivery_params) do
    {
      recipient_id: another_user.id,
      prop_id: prop.id,
      content: "I love you!",
      emoji: "😍"
    }
  end

  let(:expected_attributes) do
    {
      recipient_id: another_user.id,
      sender_id: user.id,
      sender_gender: "male",
      prop_id: prop.id,
      content: "I love you!",
      emoji: "😍"
    }
  end

  before do
    allow(Current).to receive(:user).and_return(user)
  end

  describe ".call" do
    context "when prop delivery is valid" do
      it "creates prop delivery with sender attributes" do
        expect { save_prop_delivery }.to change(PropDelivery, :count).by(1)

        is_expected.to be_success

        expect(save_prop_delivery.prop_delivery).to have_attributes(expected_attributes)
      end
    end

    context "when prop delivery is invalid" do
      let(:prop_delivery_params) { prop_delivery.slice(:recipient) }

      it "doesn't create prop delivery" do
        expect { save_prop_delivery }.not_to change(PropDelivery, :count)

        is_expected.to be_failure
      end
    end
  end
end
