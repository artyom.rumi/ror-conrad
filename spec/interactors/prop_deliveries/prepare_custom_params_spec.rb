require "rails_helper"

describe PropDeliveries::PrepareCustomParams do
  subject(:add_generated_prop_to_params) { described_class.call(prop_delivery_params: prop_delivery_params) }

  let(:prop_delivery_params) do
    {
      recipient_id: 2,
      content: "I love you!"
    }
  end

  let(:prepared_params) { add_generated_prop_to_params.prop_delivery_params }

  let(:expected_params) do
    {
      recipient_id: 2,
      content: "I love you!",
      emoji: "😍"
    }
  end

  let(:emojis) { ["😍", "😇"] }
  let(:props_seeds_hash) { { "emojis" => emojis } }

  before do
    allow(YAML).to receive(:load_file).and_return(props_seeds_hash)
    allow(emojis).to receive(:sample).and_return("😍")
  end

  describe ".call" do
    it "prepares prop delivery params with generated emoji" do
      expect(prepared_params).to eq(expected_params)

      is_expected.to be_success
    end
  end
end
