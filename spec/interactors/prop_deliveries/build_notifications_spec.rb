require "rails_helper"

describe PropDeliveries::BuildNotifications do
  subject(:interactor_call) { described_class.call(request: request, prop_delivery: prop_delivery) }

  let(:requester) { create :user }
  let(:prop_delivery) { create :prop_delivery, sender: requester }
  let(:receiver) { prop_delivery.recipient }
  let(:request) { create :request, requester: requester, receiver: receiver }
  let(:prop_sender_gender) do
    PropDeliveries::BuildNotifications::NOTIFICATION_GENDER_MAP[requester.gender.to_sym]
  end

  let(:notification) { instance_double "PushNotification" }
  let(:apnotic_notification) { instance_double "Apnotic::Notification" }

  before do
    allow(PushNotification).to receive(:new) { notification }
    allow(notification).to receive(:apnotic_notification) { apnotic_notification }
    allow(apnotic_notification).to receive(:alert=)
  end

  context "when receiver has blank device push token" do
    before do
      receiver.device.update(push_token: "")
    end

    it "doesn't create notifications" do
      expect(PushNotification).not_to receive(:new)
      expect(interactor_call.notifications).to be_empty

      is_expected.to be_success
    end
  end

  context "when receiver has device push token" do
    context "when there is no friendship object in context" do
      it "creates only notification for prop_delivery" do
        expect(PushNotification)
          .to receive(:new)
          .once
          .with(
            receiver_device_push_token: receiver.device_push_token,
            category: :received_prop,
            prop_sender_gender: prop_sender_gender
          )
        expect(interactor_call.notifications).to eq([apnotic_notification])

        is_expected.to be_success
      end

      context "when receiver turned off props notifications" do
        before do
          receiver.update(receiving_prop_notification_enabled: false)
        end

        it "doesn't create notifications" do
          expect(PushNotification).not_to receive(:new)
          expect(interactor_call.notifications).to be_empty

          is_expected.to be_success
        end
      end
    end

    context "when there is friendship object in context" do
      subject(:interactor_call) do
        described_class.call(
          request: request,
          prop_delivery: prop_delivery,
          friendship: friendship,
          friendship_created: true
        )
      end

      let(:friendship) { create :friendship, first_user: requester, second_user: receiver }

      it "creates both notifications" do
        expect(PushNotification).to receive(:new).twice
        expect(interactor_call.notifications).to eq([apnotic_notification, apnotic_notification])

        is_expected.to be_success
      end

      context "when receiver turned off friends notifications" do
        before do
          receiver.update(new_friends_notification_enabled: false)
        end

        it "creates only notification for prop_delivery" do
          expect(PushNotification)
            .to receive(:new)
            .once
            .with(
              receiver_device_push_token: receiver.device_push_token,
              category: :received_prop,
              prop_sender_gender: prop_sender_gender
            )
          expect(interactor_call.notifications).to eq([apnotic_notification])

          is_expected.to be_success
        end
      end
    end
  end
end
