require "rails_helper"

describe PropDeliveries::DeliverCustom do
  let(:expected_interactors) do
    [
      PropDeliveries::PrepareCustomParams,
      PropDeliveries::Create,
      PropDeliveries::CreateOrUpdateFriendship,
      PropDeliveries::BuildNotifications,
      SendPushNotifications
    ]
  end

  it "organizes expected interactors" do
    expect(described_class.organized).to eq(expected_interactors)
  end
end
