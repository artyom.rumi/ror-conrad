require "rails_helper"

describe PropDeliveries::CreateOrUpdateFriendship do
  subject(:create_or_update_friendship) { described_class.call(prop_delivery: prop_delivery) }

  let(:user) { create :user }
  let(:another_user) { create :user }

  let(:prop_delivery) { create :prop_delivery, sender: user, recipient: another_user }

  describe ".call" do
    context "when recipient sent boost (props) to sender before" do
      before do
        create :prop_delivery, sender: another_user, recipient: user
      end

      context "when friendship already exists" do
        before do
          create :friendship, first_user: another_user, second_user: user
        end

        it "does not create friendship" do
          expect { create_or_update_friendship }.not_to change(Friendship, :count)
          is_expected.to be_success
        end
      end

      it "creates friendship for two users" do
        expect { create_or_update_friendship }.to change(Friendship, :count).from(0).to(1)
        is_expected.to be_success
      end
    end

    context "when recipient didn't send boost (props) to sender before" do
      it "doesn't creates friendship for two users" do
        expect { create_or_update_friendship }.not_to change(Friendship, :count)
        is_expected.to be_success
      end
    end
  end
end
