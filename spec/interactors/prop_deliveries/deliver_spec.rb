require "rails_helper"

describe PropDeliveries::Deliver do
  let(:expected_interactors) do
    [
      PropDeliveries::PrepareParams,
      PropDeliveries::Create,
      PropDeliveries::BuildNotifications,
      SendPushNotifications,
      PropDeliveries::CreateOrUpdateFriendship
    ]
  end

  it "organizes expected interactors" do
    expect(described_class.organized).to eq(expected_interactors)
  end
end
