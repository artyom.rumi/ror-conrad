require "rails_helper"

describe SendPushNotifications do
  include_context :apnotic_connection

  subject(:interactor_call) { described_class.call(notifications: notifications) }

  let(:receiver) { create :user }
  let(:prop_request) { create :request, receiver: receiver }

  context "when notification doesn't exists" do
    let(:notifications) { [] }

    it "doesn't send notification" do
      expect(Apnotic::Connection).not_to receive(:new)

      is_expected.to be_success
    end
  end

  context "when notification exists" do
    let(:notification) { instance_double("Apnotic::Notification") }
    let(:notifications) { [notification] }

    it "sends specified notification" do
      expect(Apnotic::Connection).to receive(:new)

      expect(apns_connection).to receive(:push).with(notification)
      expect(apns_connection).to receive(:close)

      is_expected.to be_success
    end
  end
end
