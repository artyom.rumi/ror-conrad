require "rails_helper"
require "twilio-ruby"

describe CreateInvitation do
  let(:contacts) do
    [
      { name: "Johny B. Goode", phone_number: "+14242424242" },
      { name: "King Creole", phone_number: "+14242424242" }
    ]
  end

  let(:school) { create :school, name: "Millburn High School" }
  let!(:user) { create :user, first_name: "John", last_name: "Smith", school: school }

  let(:first_sms_body) do
    "Hi Johny, you were asked to join Friendsta by John Smith from Millburn High School. http://example.com " \
    "Reply STOP to block."
  end

  let(:second_sms_body) do
    "Hi King, you were asked to join Friendsta by John Smith from Millburn High School. http://example.com " \
    "Reply STOP to block."
  end

  let(:twilio_client) { instance_double(TwilioAdapter) }

  before do
    stub_const("CreateInvitation::LINK", "http://example.com")
    allow(Current).to receive(:user).and_return(user)

    allow(TwilioAdapter).to receive(:new) { twilio_client }
    allow(twilio_client).to receive(:send_sms)
      .and_return(instance_double("Twilio::REST::Api::V2010::AccountContext::MessageInstance"))
  end

  describe ".call" do
    subject(:result) { described_class.call(contacts: contacts) }

    it "sends invitation messages" do
      expect(twilio_client)
        .to receive(:send_sms)
        .with("+14242424242", first_sms_body).once
      expect(twilio_client)
        .to receive(:send_sms)
        .with("+14242424242", second_sms_body).once

      result
    end

    context "when phone number is invalid" do
      before do
        allow(twilio_client)
          .to receive(:send_sms)
          .with("+14242424242", second_sms_body)
          .and_raise(TwilioAdapter::SendSmsError)
      end

      it "sends invitation messages" do
        expect(result.error_phone_numbers).to eq(["+14242424242"])
        expect(result.error).to eq("We could not deliver sms messages to: [\"+14242424242\"]")
      end
    end

    context "when contacts list is empty" do
      let(:contacts) { [] }

      it { is_expected.to be_failure }
    end
  end
end
