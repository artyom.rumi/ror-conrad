require "rails_helper"

describe RegisterUser do
  let(:expected_interactors) do
    [
      Registration::CreateUser,
      Registration::CreateSubscriptions,
      Registration::BuildNotifications,
      SendPushNotifications
    ]
  end

  it "organize interactors" do
    expect(described_class.organized).to eq(expected_interactors)
  end
end
