require "rails_helper"

describe Notifications::ExpiringFriendship do
  subject(:interactor_call) do
    described_class.call(expiring_friendship: friendship)
  end

  let(:first_user) { create :user }
  let(:second_user) { create :user }

  describe "#call" do
    context "when friendship soon expires" do
      let!(:friendship) do
        create :friendship, first_user: first_user, second_user: second_user, expires_at: Time.zone.now + 15.hours
      end

      it "create notifications for friends about expiring friendship" do
        expect { interactor_call }.to change(Notification, :count).from(0).to(2)
      end
    end

    context "when the friendship hasn't expired" do
      let!(:friendship) do
        create :friendship, first_user: first_user, second_user: second_user, expires_at: Time.zone.now + 80.hours
      end

      it "don't create notifications" do
        expect { interactor_call }.not_to change(Notification, :count)
      end
    end
  end
end
