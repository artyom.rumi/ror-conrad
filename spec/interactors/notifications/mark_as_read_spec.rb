require "rails_helper"

describe Notifications::MarkAsRead do
  subject(:interactor_call) do
    described_class.call(notification_ids: notification_ids, user: first_user)
  end

  let(:first_user) { create :user }
  let(:second_user) { create :user }

  let(:feed) { create :feed, user: first_user }

  let(:my_like) { create :like, user: first_user, feed: feed }
  let(:comment) { create :comment, user: second_user, feed: feed }
  let(:repost) { create :repost, user: second_user, source_feed: feed }

  let!(:my_like_notification) do
    create :notification, notifiable: my_like, user: first_user, action_name: "new_like", status: "Read"
  end

  let!(:new_comment_notification) do
    create :notification, notifiable: comment, user: first_user, action_name: "new_comment"
  end

  let!(:new_repost_notification) do
    create :notification, notifiable_type: "Repost",
                          notifiable_id: repost.id, user: first_user, action_name: "new_repost"
  end

  describe "#call" do
    context "when only 1 notification" do
      let(:notification_ids) { new_comment_notification.id }

      it "update status to Read" do
        interactor_call

        expect(new_comment_notification.reload.status).to eq("Read")
      end
    end

    context "when array of notification's id" do
      let(:notification_ids) { [new_comment_notification.id, my_like_notification.id, new_repost_notification.id] }

      it "update status to Read" do
        interactor_call

        expect(new_comment_notification.reload.status).to eq("Read")
        expect(new_repost_notification.reload.status).to eq("Read")
      end
    end
  end
end
