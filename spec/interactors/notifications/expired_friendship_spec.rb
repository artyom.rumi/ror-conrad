require "rails_helper"

describe Notifications::ExpiredFriendship do
  subject(:interactor_call) do
    described_class.call(expired_friendship: friendship)
  end

  let(:first_user) { create :user }
  let(:second_user) { create :user }

  describe "#call" do
    context "when friendship has been expired" do
      let!(:friendship) do
        create :friendship, first_user: first_user, second_user: second_user, expires_at: Time.zone.yesterday
      end

      let!(:expiring_notification) do
        create :notification, user: first_user, notifiable: friendship,
                              action_name: "12h_expired_friend"
      end

      it "create notifications for friends about expired friendship" do
        expect { interactor_call }.to change(Notification, :count).from(1).to(2)

        expect(Notification.where(id: expiring_notification.id)).not_to exist
      end
    end
  end
end
