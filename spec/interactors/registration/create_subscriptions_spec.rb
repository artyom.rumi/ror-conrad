require "rails_helper"

describe Registration::CreateSubscriptions do
  let(:school) { create :school }
  let(:user) { create :user, school: school, phone_number: "+11234567890" }

  let!(:unrelated_user) { create :user }
  let!(:contact_user) { create :user }
  let!(:schoolmate) { create :user, school: school }
  let!(:user_with_registered_user_in_contacts) { create :user }

  before { create :contact, user: user_with_registered_user_in_contacts, phone_number: "+11234567890" }

  describe ".call" do
    subject(:result) { described_class.call(user: user, phone_numbers: phone_numbers) }

    let(:phone_numbers) { [contact_user.phone_number] }

    it "subscribes to friends" do
      is_expected.to be_success

      expect(user.subscribed_to_users).to match_array([contact_user, schoolmate])
      expect(schoolmate.subscribed_to_users).to match_array([user])
      expect(user_with_registered_user_in_contacts.subscribed_to_users).to match_array([user])
    end
  end
end
