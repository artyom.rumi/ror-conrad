require "rails_helper"

describe Registration::BuildNotifications do
  subject(:interactor_call) { described_class.call(user: user, subscriptions: subscriptions) }

  let(:user) { create :user }
  let(:subscriptions) { [forward_subscription, backward_subscription] }
  let(:forward_subscription) { create :subscription, target: user }
  let(:backward_subscription) { create :subscription, subscriber: user }
  let(:receiver) { forward_subscription.subscriber }

  let(:notification) { instance_double "PushNotification" }
  let(:apnotic_notification) { instance_double "Apnotic::Notification" }

  before do
    allow(PushNotification).to receive(:new) { notification }
    allow(notification).to receive(:apnotic_notification) { apnotic_notification }
    allow(apnotic_notification).to receive(:alert=)
  end

  context "when receiver has blank device push token" do
    before do
      receiver.device.update(push_token: "")
    end

    it "doesn't create notifications" do
      expect(PushNotification).not_to receive(:new)
      expect(interactor_call.notifications).to be_empty

      is_expected.to be_success
    end
  end

  context "when receiver has device push token" do
    context "when there are no subscriptions in context" do
      let(:subscriptions) { [] }

      it "doesn't create notifications" do
        expect(PushNotification).not_to receive(:new)
        expect(interactor_call.notifications).to be_empty

        is_expected.to be_success
      end
    end

    context "when receiver turned off new users notifications" do
      before do
        receiver.update(new_users_notification_enabled: false)
      end

      it "doesn't create notifications" do
        expect(PushNotification).not_to receive(:new)
        expect(interactor_call.notifications).to be_empty

        is_expected.to be_success
      end
    end

    it "creates notification for subscripber differrent from current user" do
      expect(PushNotification).to receive(:new).once
      expect(interactor_call.notifications).to eq([apnotic_notification])

      is_expected.to be_success
    end
  end
end
