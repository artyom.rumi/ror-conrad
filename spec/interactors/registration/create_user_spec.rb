require "rails_helper"

describe Registration::CreateUser do
  let(:school) { create :school }
  let(:user_params) do
    {
      first_name: "Steven",
      last_name: "Seagal",
      phone_number: "+12345678901",
      grade: 2010,
      age: 18,
      gender: :male,
      bio: "There is atlas of my emotions",
      school_id: school.id,
      default_avatar: nil
    }
  end

  describe ".call" do
    subject(:result) { described_class.call(user_params: user_params, auth_token: auth_token) }

    let(:user) { result.user }

    context "with orphan auth token" do
      let(:auth_token) { create :auth_token, user: nil }

      it "creates user" do
        is_expected.to be_a_success

        expect(user.default_avatar).to be_present
        expect(auth_token.reload.user).to eq user
        expect(auth_token.device.user).to eq user
      end

      context "when user params are invalid" do
        let(:user_params) { attributes_for :user, first_name: "", school_id: school.id }

        it { is_expected.to be_a_failure }
      end
    end

    context "with user auth token" do
      let(:auth_token) { create :auth_token }

      it "does not create a user" do
        is_expected.to be_a_failure

        expect(user).to be_nil
      end
    end
  end
end
