require "rails_helper"

describe Verifications::DecrementAttemptsLeft do
  subject(:interactor_call) { described_class.call(input_parameters) }

  context "when several attempts left" do
    let!(:verification_code) { create :verification_code, attempts_count: 2 }

    context "when no verification code specified" do
      let(:input_parameters) { {} }

      it "fails to decrement attempts left" do
        expect { interactor_call }.not_to change(verification_code, :attempts_count)
        expect(interactor_call.success?).to eq false
      end
    end

    context "when verification code specified" do
      let(:input_parameters) { { verification_code: verification_code } }

      it "decreases attempts left" do
        expect { interactor_call }.to change(verification_code, :attempts_count).by(-1)
        expect(interactor_call.success?).to eq true
      end
    end
  end

  context "when no attempts left" do
    let!(:verification_code) { create :verification_code, attempts_count: 0 }

    context "when no verification code specified" do
      let(:input_parameters) { {} }

      it "fails to decrease attempts left" do
        expect { interactor_call }.not_to change(verification_code, :attempts_count)
        expect(interactor_call.success?).to eq false
      end
    end

    context "when verification code specified" do
      let(:input_parameters) { { verification_code: verification_code } }

      it "removes verification code" do
        expect { interactor_call }.to change(VerificationCode, :count).by(-1)
        expect(interactor_call.success?).to eq false
      end
    end
  end
end
