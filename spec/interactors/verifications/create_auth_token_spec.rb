require "rails_helper"

describe Verifications::CreateAuthToken do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) do
    {}
      .merge(verification_code_params)
      .merge(device_params)
  end

  let(:verification_code_params) { { verification_code: verification_code } }
  let(:verification_code) { nil }
  let(:device_params) { { device: device } }
  let(:device) { nil }

  it "fails to generate an auth token" do
    expect { interactor_call }.not_to change(AuthToken, :count)
    expect(interactor_call.success?).to eq false
  end

  context "with device provided" do
    let(:device) { create :device }

    it "fails to generate an auth token" do
      expect { interactor_call }.not_to change(AuthToken, :count)
      expect(interactor_call.success?).to eq false
    end

    context "with verification code provided" do
      let!(:verification_code) { create :verification_code }

      it "generates auth token" do
        expect { interactor_call }.to change(AuthToken, :count).from(0).to(1)
        expect(interactor_call.success?).to eq true
        expect(interactor_call.auth_token.user_id).to be_nil
        expect(interactor_call.auth_token.value).not_to be_nil
        expect(interactor_call.auth_token.expires_at).not_to eq be_nil
      end
    end

    context "when verification code associated with user" do
      let!(:user) { create :user, :without_token }
      let!(:verification_code) { create :verification_code, user: user }

      it "generates auth token" do
        expect { interactor_call }
          .to change(AuthToken, :count)
          .from(0).to(1)
          .and change(VerificationCode, :count)
          .from(1).to(0)

        expect(interactor_call.success?).to eq true
        expect(interactor_call.auth_token.user_id).to eq user.id
        expect(interactor_call.auth_token.value).not_to be_nil
        expect(interactor_call.auth_token.expires_at).not_to eq be_nil
        expect(interactor_call.auth_token.device).to eq device
      end

      context "when user has auth token" do
        let!(:auth_token) { create :auth_token, value: "ex-token", user: user }

        it "does not creates a new auth token" do
          expect { interactor_call }.not_to change(AuthToken, :count)
        end

        it "regenerates auth token" do
          expect { interactor_call }
            .to change(auth_token, :value)
            .and change(auth_token, :expires_at)
            .and change(VerificationCode, :count).from(1).to(0)

          expect(interactor_call.success?).to eq true
          expect(interactor_call.auth_token).to eq auth_token
          expect(interactor_call.auth_token.device).not_to eq device
        end
      end
    end
  end
end
