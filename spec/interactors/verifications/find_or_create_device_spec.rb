require "rails_helper"

describe Verifications::FindOrCreateDevice do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) { {} }

  it "fails to create device without paraters" do
    expect { interactor_call }.not_to change(Device, :count)
    expect(interactor_call).to be_failure
  end

  context "when device is present" do
    let!(:device) { create :device, phone_number: "+12345678901" }

    it "fails to create device" do
      expect { interactor_call }.not_to change(Device, :count)
      expect(interactor_call).to be_failure
    end

    context "when existing device phone is specified" do
      let(:input_parameters) { { phone_number: "+12345678901" } }

      it "finds existing device" do
        expect { interactor_call }.not_to change(Device, :count)
        expect(interactor_call).to be_success
        expect(interactor_call.device).to eq device
      end
    end

    context "when new phone is specified" do
      let(:input_parameters) { { phone_number: "+12345678909" } }

      it "creates new device" do
        expect { interactor_call }.to change(Device, :count)
        expect(interactor_call).to be_success
        expect(interactor_call.device).to be_persisted
        expect(interactor_call.device).not_to eq device
      end
    end
  end
end
