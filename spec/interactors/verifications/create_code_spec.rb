require "rails_helper"

describe Verifications::CreateCode do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) { {} }

  it "fails to generate a code" do
    expect { interactor_call }.not_to change(VerificationCode, :count)
    expect(interactor_call.success?).to eq false
  end

  context "when phone number is specified" do
    let(:input_parameters) { { phone_number: "4242" } }

    it "generates a code" do
      expect { interactor_call }.to change(VerificationCode, :count).from(0).to(1)
      expect(interactor_call.success?).to eq true
      expect(VerificationCode.last.phone_number).to eq "4242"
      expect(VerificationCode.last.code).not_to be_nil
      expect(VerificationCode.last.user_id).to eq nil
      expect(VerificationCode.last.attempts_count).to eq 3
    end

    context "when there is a user with this phone number" do
      let!(:user) { create :user, phone_number: "4242" }

      it "generetes a code with user" do
        expect { interactor_call }.to change(VerificationCode, :count).from(0).to(1)
        expect(interactor_call.success?).to eq true
        expect(VerificationCode.last.phone_number).to eq "4242"
        expect(VerificationCode.last.code).not_to be_nil
        expect(VerificationCode.last.user_id).to eq user.id
        expect(VerificationCode.last.attempts_count).to eq 3
      end
    end

    context "when user is test" do
      let!(:user) { create :user, :test, phone_number: "4242" }

      it "generates test code" do
        expect { interactor_call }.to change(VerificationCode, :count).from(0).to(1)
        expect(interactor_call.success?).to eq true
        expect(VerificationCode.last.phone_number).to eq "4242"
        expect(VerificationCode.last.code).to eq "1234"
        expect(VerificationCode.last.user_id).to eq user.id
        expect(VerificationCode.last.attempts_count).to eq 3
      end
    end
  end
end
