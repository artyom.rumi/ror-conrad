require "rails_helper"

describe Verifications::EraseOldCodes do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) { { phone_number: "4242" } }

  let!(:my_verification_code) { create :verification_code, phone_number: "4242" }

  let!(:other_verification_code) { create :verification_code, phone_number: "4343" }
  let!(:third_verification_code) { create :verification_code, phone_number: "4345" }

  it "wipes associated verification codes" do
    expect { interactor_call }.to change(VerificationCode, :count).from(3).to(2)
  end
end
