require "rails_helper"
require "twilio-ruby"

describe Verifications::SendCode do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:verification_code) { build_stubbed :verification_code, phone_number: user_phone_number }

  let(:input_parameters) { { verification_code: verification_code } }

  let(:twilio_client) { instance_double "TwilioAdapter" }

  let(:user_phone_number) { "+1424242424242" }
  let(:sms_body) { "#{verification_code.code} is your Friendsta verification code." }

  before do
    allow(TwilioAdapter).to receive(:new) { twilio_client }
  end

  it "wipes associated verification codes" do
    expect(twilio_client)
      .to receive(:send_sms)
      .with(user_phone_number, sms_body)
      .and_return(instance_double("Twilio::REST::Api::V2010::AccountContext::MessageInstance"))

    interactor_call
  end

  context "when user is test" do
    let(:test_user) { create :user }
    let(:verification_code) { build_stubbed :verification_code, code: "1234", user: test_user }

    it "doesn't send verification code" do
      expect(twilio_client)
        .not_to receive(:send_sms)
        .with(user_phone_number, sms_body)

      interactor_call
    end
  end
end
