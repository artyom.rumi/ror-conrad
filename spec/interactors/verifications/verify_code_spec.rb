require "rails_helper"

describe Verifications::VerifyCode do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) do
    {}
      .merge(verification_code_params)
      .merge(provided_code_params)
  end

  let(:verification_code_params) { { verification_code: verification_code } }

  context "when verification code is expired" do
    let!(:verification_code) { create :verification_code, code: "1234", expires_at: 1.minute.ago }
    let(:provided_code_params) { { provided_code: "1234" } }

    it "fails verify code" do
      expect(interactor_call.success?).to eq false
    end
  end

  context "when verification code attempts limit is exceeded" do
    let!(:verification_code) { create :verification_code, code: "1234", attempts_count: -1 }

    let(:provided_code_params) { { provided_code: "1234" } }

    it "fails to generate an auth token" do
      expect(interactor_call.success?).to eq false
    end
  end

  context "when verification code is active" do
    let!(:verification_code) { create :verification_code, code: "1234" }

    context "when user inputed wrong verification code" do
      let(:provided_code_params) { { provided_code: "1235" } }

      it "fails to generate an auth token" do
        expect(interactor_call.success?).to eq false
      end
    end

    context "when user inputed correct verification code" do
      let(:provided_code_params) { { provided_code: "1234" } }

      it "generates auth token" do
        expect(interactor_call.success?).to eq true
        expect(interactor_call.phone_number).to eq verification_code.phone_number
      end
    end
  end
end
