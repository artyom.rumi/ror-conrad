require "rails_helper"

describe PropsRequests::Save do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) { { requester: requester, receiver: receiver } }
  let(:requester) { create :user }

  context "when request is valid" do
    let(:receiver) { create :user }

    it "creates request" do
      expect { interactor_call }.to change(Request, :count).by(1)
      is_expected.to be_a_success
    end
  end

  context "when request is invalid" do
    let(:receiver) { nil }

    it "dosen't create request" do
      expect { interactor_call }.to change(Request, :count).by(0)
      is_expected.to be_a_failure
    end
  end
end
