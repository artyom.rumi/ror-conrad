require "rails_helper"

describe PropsRequests::IncrementRequestProgress do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) { { request: request } }

  let(:request) { create :request }

  it "update request progress" do
    expect { interactor_call }.to change(request, :progress).by(1)
    is_expected.to be_a_success
  end
end
