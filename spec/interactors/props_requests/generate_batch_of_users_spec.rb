require "rails_helper"

describe PropsRequests::GenerateBatchOfUsers do
  subject(:interactor_call) { described_class.call(request: request) }

  let(:request) { create :request, requester: requester, receiver: receiver }
  let(:receiver) { create :user }
  let(:requester) { create :user }

  context "when receiver has 5 followees" do
    before do
      create_list :subscription, 4, subscriber: receiver
      create :subscription, subscriber: receiver, target: requester
    end

    it "creates five request respondents" do
      expect { interactor_call }.to change(RequestRespondent, :count).by(5)

      expect(request.respondents).to include(requester)
      expect(request.respondents).to match_array(receiver.subscribed_to_users)
      expect(request.respondents.length).to eq(5)
      expect(request.respondents.uniq.length).to eq(5)

      is_expected.to be_a_success
    end
  end

  context "when receiver has less than 5 followees" do
    it "creates five request respondents" do
      expect { interactor_call }.to change(RequestRespondent, :count).by(0)

      expect(request.respondents.length).to eq(0)
      is_expected.to be_a_success
    end
  end
end
