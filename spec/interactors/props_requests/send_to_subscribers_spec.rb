require "rails_helper"

describe PropsRequests::SendToSubscribers do
  subject(:interactor_call) { described_class.call(input_parameters) }

  let(:input_parameters) do
    { requester: requester }
  end

  let(:requester) { create :user }
  let(:first_subscriber) { create :user }
  let(:second_subscriber) { create :user }

  let(:receivers_query) { instance_double "ReceiversQuery", all: [first_subscriber] }

  before do
    allow(CreateRequestJob).to receive(:perform_later)
    allow(Requests::ReceiversQuery).to receive(:new).with(requester) { receivers_query }
  end

  it "create requests just for one subscriber" do
    expect(CreateRequestJob).to receive(:perform_later).once.with(requester, first_subscriber)
    interactor_call
  end

  it "increments requester requests count" do
    expect { interactor_call }.to change(requester, :requests_count).by(1)
  end
end
