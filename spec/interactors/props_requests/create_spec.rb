require "rails_helper"

describe PropsRequests::Create do
  let(:expected_interactors) do
    [
      PropsRequests::Save,
      PropsRequests::GenerateBatchOfUsers
    ]
  end

  it "organizes expected interactors" do
    expect(described_class.organized).to eq(expected_interactors)
  end
end
