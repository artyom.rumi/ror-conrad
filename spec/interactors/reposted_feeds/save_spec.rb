require "rails_helper"

describe RepostedFeeds::Save do
  subject(:interactor_call) { described_class.call(user: user, source_feed: feed) }

  let(:feed) { create :feed, user: feed_author }
  let(:feed_author) { create :user }
  let(:user) { create :user }

  context "when Reposted Feed is valid" do
    it "creates reposted_feed" do
      expect { interactor_call }.to change(Feed, :count).by(2)
      is_expected.to be_a_success
    end

    it "creates notification after interactor call" do
      expect { interactor_call }.to change(Notification, :count).by(1)
    end

    context "when repost and feed author is a same user" do
      let(:user) { feed_author }

      it { is_expected.not_to be_a_success }

      describe "#error" do
        it "returns active record error message" do
          error_message = interactor_call.error[:base].first

          expect(error_message).to eq("User can't reposted own feed")
        end
      end
    end
  end
end
