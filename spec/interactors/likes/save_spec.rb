require "rails_helper"

describe Likes::Save do
  subject(:interactor_call) { described_class.call(like_params: like_params) }

  let(:feed) { create :feed, user: feed_author }
  let(:feed_author) { create :user }
  let(:another_user) { create :user }

  context "when Like is valid" do
    let(:like_params) { { feed_id: feed.id, user_id: another_user.id } }

    it "creates like" do
      expect { interactor_call }.to change(Like, :count).by(1)
      is_expected.to be_a_success
    end

    it "creates notification after interactor call" do
      expect { interactor_call }.to change(Notification, :count).by(1)
    end

    context "when like and feed author is a same user" do
      let(:like_params) { { feed_id: feed.id, user_id: feed_author.id } }

      it { expect { interactor_call }.not_to change(Notification, :count) }
    end
  end
end
