require "rails_helper"

describe Blockings::Destroy do
  subject(:interactor_call) { described_class.call(blocking: blocking) }

  context "when blocking for user" do
    let!(:blocking) { create :blocking, :for_user }

    it "destroys blocking" do
      expect { interactor_call }.to change(Blocking.for_user, :count).by(-1)

      is_expected.to be_a_success
    end
  end

  context "when blocking for collocutor_mask" do
    let!(:blocking) { create :blocking, :for_collocutor_mask }

    it "destroys blocking" do
      expect { interactor_call }.to change(Blocking.for_collocutor_mask, :count).by(-1)

      is_expected.to be_a_success
    end
  end
end
