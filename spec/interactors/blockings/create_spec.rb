require "rails_helper"

describe Blockings::Create do
  subject(:interactor_call) { described_class.call(user: current_user, blockable: blockable) }

  let(:current_user) { create :user }

  context "when blockable is user" do
    let(:blockable) { create :user }

    it "creates new blocking for user" do
      expect { interactor_call }.to change(Blocking.for_user, :count).by(1)

      is_expected.to be_success
    end
  end

  context "when blockable is collocutor mask" do
    let(:blockable) { create :collocutor_mask }

    it "creates new blocking for collocutor mask" do
      expect { interactor_call }.to change(Blocking.for_collocutor_mask, :count).by(1)

      is_expected.to be_success
    end
  end
end
