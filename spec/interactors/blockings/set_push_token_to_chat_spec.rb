require "rails_helper"

describe Blockings::SetPushTokenToChat do
  subject(:set_push_token) { described_class.call(blocking: blocking) }

  let(:current_user) { create :user }

  let(:pubnub_client) { instance_double PubNubAdapter }

  before do
    allow(PubNubAdapter).to receive(:new) { pubnub_client }
  end

  context "when blockable type is user" do
    let(:blocking) { create :blocking, user: current_user, blockable: another_user }
    let(:another_user) { create :user }

    context "when chat between users exists" do
      let(:chat) { create :chat, users: [current_user, another_user] }
      let(:pubnub_channel_name) { chat.pubnub_channel_name }

      it "makes request to pubnub" do
        expect(pubnub_client)
          .to receive(:add_push_notification_registrations)
          .with(current_user.device_push_token, chat.pubnub_channel_name)

        set_push_token
      end
    end

    context "when there is no chat between users" do
      it "does not make request to pubnub" do
        expect(pubnub_client).not_to receive(:add_push_notification_registrations)

        set_push_token
      end
    end
  end

  context "when blockable type is collocutor mask" do
    let(:blocking) { create :blocking, user: current_user, blockable: collocutor_mask }
    let(:collocutor_mask) { create :collocutor_mask, user: current_user }
    let(:another_user) { collocutor_mask.collocutor }

    context "when chat between users exists" do
      let(:chat) { create :chat, users: [current_user, another_user], incognito: true }

      it "makes request to pubnub" do
        expect(pubnub_client)
          .to receive(:add_push_notification_registrations)
          .with(current_user.device_push_token, chat.pubnub_channel_name)

        set_push_token
      end
    end

    context "when there is no chat between users" do
      it "does not make request to pubnub" do
        expect(pubnub_client).not_to receive(:add_push_notification_registrations)

        set_push_token
      end
    end
  end
end
