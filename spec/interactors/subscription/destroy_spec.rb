require "rails_helper"

describe Subscription::Destroy do
  let(:john) { create :user }
  let(:dan) { create :user }

  let(:subscription) { create :subscription, subscriber: john, target: dan }

  describe ".call" do
    subject(:result) { described_class.call(subscription: subscription) }

    let(:user) { result.user }

    it "destroys subscription" do
      is_expected.to be_success

      expect(result.subscription).to be_destroyed
    end

    context "when users have been friends" do
      before { create :friendship, first_user: john, second_user: dan }

      it "destroys subscription and friendship" do
        is_expected.to be_success

        expect(result.subscription).to be_destroyed
        expect(result.friendship).to be_destroyed
      end
    end

    context "when impossible to remove friendships" do
      let(:friendship) { create :friendship, first_user: john, second_user: dan }

      before do
        allow(Friendship).to receive(:between_users).and_return(friendship)
        allow(friendship).to receive(:destroy).and_return(false)
      end

      it "does not destroy subscription and friendship" do
        is_expected.to be_failure

        expect(result.subscription).to be_persisted
        expect(result.friendship).to be_persisted
      end
    end
  end
end
