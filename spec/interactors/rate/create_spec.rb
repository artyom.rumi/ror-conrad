require "rails_helper"

describe Rate::Create do
  let(:expected_interactors) do
    [
      Rate::Save,
      BuildActivityPushNotification,
      SendPushNotifications
    ]
  end

  it "organizes expected interactors" do
    expect(described_class.organized).to eq(expected_interactors)
  end
end
