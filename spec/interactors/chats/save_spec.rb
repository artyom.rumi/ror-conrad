require "rails_helper"

describe Chats::Save do
  subject(:interactor_call) do
    described_class.call(members_ids: [current_user.id, another_user.id], owner: current_user, incognito: incognito,
                         is_group: true)
  end

  let(:current_user) { create :user }
  let(:another_user) { create :user }
  let(:chat_params) { { users: [current_user, another_user] } }

  describe "#call" do
    context "when incognito is false" do
      let(:incognito) { false }

      it "creates new uncovered chat" do
        expect { interactor_call }.to change(Chat, :count).by(1)

        expected_chat = Chat.first
        expect(expected_chat.users).to match_array [current_user, another_user]
        expect(expected_chat.incognito).to be false

        is_expected.to be_success
      end
    end

    context "when incognito is true" do
      let(:incognito) { true }

      it "creates new incognito chat" do
        expect { interactor_call }.to change(Chat, :count).by(1)

        expected_chat = Chat.first
        expect(expected_chat.users).to match_array [current_user, another_user]
        expect(expected_chat.incognito).to be true

        is_expected.to be_success
      end
    end
  end
end
