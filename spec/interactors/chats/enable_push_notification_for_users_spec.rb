require "rails_helper"

describe Chats::EnablePushNotificationForUsers do
  subject(:interactor_call) do
    described_class.call(members: [first_user, second_user], chat: incognito_chat)
  end

  let(:first_user) { create :user }
  let(:second_user) { create :user }

  let(:first_user_device) { first_user.device }
  let(:second_user_device) { second_user.device }

  let(:incognito_chat) { create :chat, users: [first_user, second_user], incognito: true }

  let(:pubnub_client) { instance_double "PubNubAdapter" }

  before do
    allow(PubNubAdapter).to receive(:new) { pubnub_client }
    allow(pubnub_client).to receive(:add_push_notification_registrations)
  end

  describe "#call" do
    it "add user device push tokens to incognito chat pubnub channel" do
      expect(pubnub_client)
        .to receive(:add_push_notification_registrations)
        .with(first_user_device.push_token, incognito_chat.pubnub_channel_name)

      expect(pubnub_client)
        .to receive(:add_push_notification_registrations)
        .with(second_user_device.push_token, incognito_chat.pubnub_channel_name)

      interactor_call
    end
  end
end
