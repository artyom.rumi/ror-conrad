require "rails_helper"

describe Chats::CreateMasks do
  subject(:interactor_call) { described_class.call(chat: chat) }

  let(:current_user) { create :user, :male }
  let(:first_user) { create :user, :female }
  let(:second_user) { create :user, :non_binary }
  let(:chat) { build :chat, users: [current_user, first_user, second_user], owner: current_user, incognito: true }
  let(:users) { [current_user, first_user, second_user] }

  shared_examples "creates collocutor masks" do
    it "creates collocutor masks for user" do
      is_expected.to be_a_success

      collocutors.each do |collocutor_id, mask_name|
        expect(user.collocutor_masks.find_by(collocutor_id: collocutor_id).name).to eq(mask_name)
      end
    end

    it "increment gender masks counter" do
      is_expected.to be_a_success

      collocutors.each do |collocutor_id, _|
        expect(user.gender_masks_counter.public_send(User.find(collocutor_id).gender)).to eq(1)
      end
    end
  end

  describe "#call" do
    context "when collocutor mask is valid" do
      before { interactor_call }

      include_examples "creates collocutor masks" do
        let(:user) { current_user }
        let(:collocutors) { { first_user.id => "Incognito Girl 1", second_user.id => "Incognito User 1" } }
      end

      include_examples "creates collocutor masks" do
        let(:user) { first_user }
        let(:collocutors) { { current_user.id => "Incognito Boy 1", second_user.id => "Incognito User 1" } }
      end

      include_examples "creates collocutor masks" do
        let(:user) { second_user }
        let(:collocutors) { { current_user.id => "Incognito Boy 1", first_user.id => "Incognito Girl 1" } }
      end
    end
  end
end
