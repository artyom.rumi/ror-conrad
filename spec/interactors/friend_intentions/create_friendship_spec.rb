require "rails_helper"

describe FriendIntentions::CreateFriendship do
  subject(:interactor) { described_class.call(user: user, target: target) }

  let(:user) { create :user }
  let(:target) { create :user }

  context "when user and target user are friends" do
    let!(:friendship) { create :friendship, first_user: user, second_user: target }

    it "doesn't create friendship" do
      expect { interactor.run }.not_to change(Friendship, :count)
    end
  end

  context "when users send invites each other" do
    let!(:first_friend_intention) { create :friend_intention, user: user, target: target }
    let!(:second_friend_intention) { create :friend_intention, user: target, target: user }

    it "creates friendship" do
      expect { interactor.run }.to change(Friendship, :count).by(1)
    end

    it "creates notification after interactor call" do
      expect { interactor }.to change(Notification, :count).by(2)
    end
  end

  context "when one user send invite" do
    let!(:first_friend_intention) { create :friend_intention, user: user, target: target }

    it "don't create friendship" do
      expect { interactor.run }.not_to change(Friendship, :count)
    end

    it "creates notification after interactor call" do
      expect { interactor }.not_to change(Notification, :count)
    end
  end
end
