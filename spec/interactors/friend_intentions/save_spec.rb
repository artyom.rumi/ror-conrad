require "rails_helper"

describe FriendIntentions::Save do
  subject(:interactor) { described_class.call(user: user, target: target) }

  let(:user) { create :user }
  let(:target) { create :user }

  context "when user send invite to another target user" do
    it "creates friend_intention" do
      expect { interactor.run }.to change(FriendIntention, :count).from(0).to(1)
    end
  end

  context "when the user has already sent the invitation" do
    let!(:friend_intention) { create :friend_intention, user: user, target: target }

    it "doesn't create friend_intention" do
      is_expected.to be_a_failure
    end
  end
end
