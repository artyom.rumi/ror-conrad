require "rails_helper"

describe BuildFriendshipPushNotification do
  subject(:interactor_call) { described_class.call(friendship: friendship, action_type: action_type) }

  let(:notification) { instance_double "PushNotification" }
  let(:apnotic_notification) { instance_double "Apnotic::Notification" }
  let(:friendship) { create :friendship }

  before do
    allow(PushNotification).to receive(:new) { notification }
    allow(notification).to receive(:apnotic_notification) { apnotic_notification }
    allow(apnotic_notification).to receive(:alert=)
  end

  context "when recievers has blank device push tokens" do
    let(:action_type) { "friend" }

    before do
      friendship.first_user.device.update(push_token: "")
      friendship.second_user.device.update(push_token: "")
    end

    it "doesn't create notifications" do
      expect(PushNotification).not_to receive(:new)
      expect(interactor_call.notifications.first).to be_nil

      is_expected.to be_success
    end
  end

  context "when receivers turned off new users notifications" do
    let(:action_type) { "friend" }

    before do
      friendship.first_user.update(new_friends_notification_enabled: false)
      friendship.second_user.update(new_friends_notification_enabled: false)
    end

    it "doesn't create notifications" do
      expect(PushNotification).not_to receive(:new)
      expect(interactor_call.notifications.first).to be_nil

      is_expected.to be_success
    end
  end
  #
  # context "when there is like object in activity context" do
  #   let(:activity) { create :like, user: requester, feed: feed }
  #   let(:activity_type) { "like" }
  #
  #   context "when receiver and requester are friends" do
  #     let!(:friendship) { create :friendship, first_user: receiver, second_user: requester }
  #
  #     it "creates notification for like" do
  #       expect(PushNotification)
  #         .to receive(:new)
  #         .once
  #         .with(
  #           receiver_device_push_token: receiver.device_push_token,
  #           category: "new_like",
  #           sender: "A friend"
  #         )
  #       expect(interactor_call.notifications.first).to eq(apnotic_notification)
  #
  #       is_expected.to be_success
  #     end
  #   end
  #
  #   context "when receiver is friend of requester's friends" do
  #     let(:requester) { create :user, age: 21 }
  #     let!(:friend) { create :user }
  #
  #     before do
  #       create :friendship, first_user: friend, second_user: requester
  #       create :friendship, first_user: friend, second_user: receiver
  #     end
  #
  #     it "creates notification for like" do
  #       expect(PushNotification)
  #         .to receive(:new)
  #         .once
  #         .with(
  #           receiver_device_push_token: receiver.device_push_token,
  #           category: "new_like",
  #           sender: "A friend of your friend"
  #         )
  #       expect(interactor_call.notifications.first).to eq(apnotic_notification)
  #
  #       is_expected.to be_success
  #     end
  #   end
  #
  #   context "when receiver and requester are not friends" do
  #     it "creates notification for like" do
  #       expect(PushNotification)
  #         .to receive(:new)
  #         .once
  #         .with(
  #           receiver_device_push_token: receiver.device_push_token,
  #           category: "new_like",
  #           sender: "Someone"
  #         )
  #       expect(interactor_call.notifications.first).to eq(apnotic_notification)
  #
  #       is_expected.to be_success
  #     end
  #   end
  # end
  #
  # context "when there is comment object in activity context" do
  #   let(:activity) { create :comment, user: requester, feed: feed }
  #   let(:activity_type) { "comment" }
  #
  #   it "creates notification for comment" do
  #     expect(PushNotification)
  #       .to receive(:new)
  #       .once
  #       .with(
  #         receiver_device_push_token: receiver.device_push_token,
  #         category: "new_comment",
  #         sender: "Someone"
  #       )
  #     expect(interactor_call.notifications.first).to eq(apnotic_notification)
  #
  #     is_expected.to be_success
  #   end
  # end
  #
  # context "when there is reposted feed object in activity context" do
  #   let(:activity) { create :repost, user: requester, source_feed_id: feed.id }
  #   let(:activity_type) { "repost" }
  #
  #   it "creates notification for reposted_feed" do
  #     expect(PushNotification)
  #       .to receive(:new)
  #       .once
  #       .with(
  #         receiver_device_push_token: receiver.device_push_token,
  #         category: "new_repost",
  #         sender: "Someone"
  #       )
  #     expect(interactor_call.notifications.first).to eq(apnotic_notification)
  #
  #     is_expected.to be_success
  #   end
  # end
end
