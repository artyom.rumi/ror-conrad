require "rails_helper"

describe Users::SynchronizeContacts do
  subject(:create_contacts) { described_class.call(phone_numbers: phone_numbers) }

  let(:current_user) { create :user }
  let(:phone_numbers) { [{ name: "John", phone_number: "+11234567890" }] }
  let(:expected_contact_params) do
    {
      user: current_user, phone_number: phone_numbers.first[:phone_number], name: phone_numbers.first[:name]
    }
  end

  before { allow(Current).to receive(:user).and_return(current_user) }

  describe ".call" do
    it "creates new user contacts" do
      expect { create_contacts }.to change(current_user.contacts, :count).from(0).to(1)
      expect(current_user.contacts.first).to have_attributes(expected_contact_params)

      is_expected.to be_success
    end

    context "when phone number is already in contact list" do
      before { create :contact, user: current_user, name: "not John", phone_number: "+11234567890" }

      it "doesn't create new contacts" do
        expect { create_contacts }.not_to change(current_user.contacts, :count)
        expect(current_user.contacts.first).to have_attributes(expected_contact_params)

        is_expected.to be_success
      end
    end

    context "when there is contact with phone number that not present in phone numbers list" do
      before { create :contact, user: current_user, phone_number: "+11234567891" }

      it "removes old contact and adds new" do
        create_contacts

        expect(current_user.contacts.count).to eq(1)
        expect(current_user.contacts.first).to have_attributes(expected_contact_params)

        is_expected.to be_success
      end
    end

    context "when phone numbers list is empty" do
      let(:phone_numbers) { [] }

      it { is_expected.to be_failure }
    end
  end
end
