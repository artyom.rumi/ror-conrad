require "rails_helper"

describe Users::CreateMasks do
  subject(:interactor_call) { described_class.call(first_user: first_user, second_user: second_user) }

  let(:boy) { create :user, :male }
  let!(:boy_gender_counter) { create :gender_masks_counter, user: boy, male: 1, female: 2, non_binary: 3 }

  let(:first_user) { boy }

  context "when girl is matched" do
    let!(:girl) { create :user, :female }
    let!(:girl_gender_counter) { create :gender_masks_counter, user: girl, male: 4, female: 3, non_binary: 1 }

    let(:second_user) { girl }

    it "creates corresponding masks" do
      expect { interactor_call }
        .to change(boy.collocutor_masks, :count).from(0).to(1)
                                                .and change(girl.collocutor_masks, :count).from(0).to(1)

      expect(boy.collocutor_masks.first).to have_attributes name: "Anonymous User", collocutor_id: girl.id
      expect(boy_gender_counter.reload).to have_attributes male: 1, female: 3, non_binary: 3

      expect(girl.collocutor_masks.first).to have_attributes name: "Anonymous User", collocutor_id: boy.id
      expect(girl_gender_counter.reload).to have_attributes male: 5, female: 3, non_binary: 1
    end
  end

  context "when non-binary is matched" do
    let!(:non_binary) { create :user, :non_binary }

    let!(:non_binary_gender_counter) do
      create :gender_masks_counter, user: non_binary, male: 6, female: 5, non_binary: 1
    end

    let(:second_user) { non_binary }

    it "creates corresponding masks" do
      expect { interactor_call }
        .to change(boy.collocutor_masks, :count).from(0).to(1)
                                                .and change(non_binary.collocutor_masks, :count).from(0).to(1)

      expect(boy.collocutor_masks.first).to have_attributes name: "Anonymous User", collocutor_id: non_binary.id
      expect(boy_gender_counter.reload).to have_attributes male: 1, female: 2, non_binary: 4

      expect(non_binary.collocutor_masks.first.reload).to have_attributes name: "Anonymous User", collocutor_id: boy.id
      expect(non_binary_gender_counter.reload).to have_attributes male: 7, female: 5, non_binary: 1
    end
  end

  context "when one of users hasn't gender masks counter" do
    let!(:girl) { create :user, :female }

    let(:second_user) { girl }

    it "creates and increments new gender masks counter" do
      expect { interactor_call }
        .to change(GenderMasksCounter, :count).from(1).to(2)

      expect(girl.collocutor_masks.first.reload).to have_attributes name: "Anonymous User", collocutor_id: boy.id
      expect(girl.gender_masks_counter).to have_attributes male: 1, female: 0, non_binary: 0
    end
  end
end
