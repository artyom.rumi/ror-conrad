require "rails_helper"

describe Users::CreateDeanonymization do
  subject(:interactor_call) { described_class.call(deanonymization_params: deanonymization_params) }

  let(:first_user) { create :user }
  let(:second_user) { create :user }
  let(:incognito_chat) { create :chat, users: [first_user, second_user], incognito: true }
  let(:deanonymization_params) { { chat: incognito_chat, user: first_user } }

  describe "#call" do
    it "creates new deanonymization" do
      expect { interactor_call }.to change(Deanonymization, :count).from(0).to(1)
      expect(Deanonymization.first).to have_attributes deanonymization_params
    end
  end
end
