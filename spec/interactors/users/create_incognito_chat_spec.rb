require "rails_helper"

describe Users::CreateIncognitoChat do
  subject(:create_incognito_chat) do
    described_class.call(first_user: current_user, second_user: second_user, owner: current_user, is_group: true)
  end

  let!(:current_user) { create :user }
  let!(:second_user) { create :user }

  let(:pubnub_client) { instance_double "PubNubAdapter" }

  before do
    allow(PubNubAdapter).to receive(:new) { pubnub_client }
    allow(pubnub_client).to receive(:add_push_notification_registrations)
  end

  context "when there is no unrevealed incognito chat with same users" do
    it "creates new incognito chat" do
      expect { create_incognito_chat }.to change(Chat, :count).from(0).to(1)

      expect(create_incognito_chat).to be_success
      expect(create_incognito_chat.chat).to be_persisted
    end
  end

  context "when there is revealed incognito chat with same users" do
    let(:incognito_chat) { create :chat, :incognito, users: [current_user, second_user] }

    before do
      create :deanonymization, chat: incognito_chat, user: current_user
      create :deanonymization, chat: incognito_chat, user: second_user
    end

    it "creates new incognito chat" do
      expect { create_incognito_chat }.to change(Chat, :count).from(1).to(2)

      expect(create_incognito_chat).to be_success
      expect(create_incognito_chat.chat).to be_persisted
    end
  end

  context "when there is unrevealed incognito chat with same users" do
    before { create :chat, :incognito, users: [current_user, second_user] }

    it "doesn't create new incognito chat" do
      expect { create_incognito_chat }.not_to change(Chat, :count)

      expect(create_incognito_chat).to be_failure
      expect(create_incognito_chat.chat).to eq nil
    end
  end
end
