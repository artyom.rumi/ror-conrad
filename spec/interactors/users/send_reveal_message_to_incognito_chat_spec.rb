require "rails_helper"

describe Users::SendRevealMessageToIncognitoChat do
  subject(:interactor_call) { described_class.call(incognito_chat: incognito_chat, deanonymization: deanonymization) }

  let(:incognito_chat) { create :chat, users: [first_user, second_user], incognito: true }
  let(:first_user) { create :user }
  let(:second_user) { create :user }

  let(:incognito_chat_id) { incognito_chat.id }
  let(:pubnub_channel_name) { incognito_chat.pubnub_channel_name }

  let(:pubnub_client) { instance_double "PubNubAdapter" }
  let(:channel_service_message) { instance_double "ChannelServiceMessage" }
  let(:push_notification_message) { instance_double "PushNotificationMessage" }

  let(:message_uuid) { "message_uuid" }
  let(:message_created_at) { 0 }

  let(:reveal_data) do
    {
      version: 10_000,
      chat_id: incognito_chat_id,
      method: {
        type: "publish",
        message: message
      },

      pn_apns: push_notification_data
    }
  end

  let(:message) do
    {
      id: message_uuid,
      created_at: message_created_at,
      content: {
        info_type: info_type,
        type: "info"
      }
    }
  end

  let(:push_notification_data) do
    {
      aps: aps_data,
      category_type: "chat_message",
      chat_id: incognito_chat_id
    }
  end

  let(:aps_data) do
    {
      category: "chat_message",

      alert: {
        'title-loc-key': "apns_chat_message_title",
        'loc-key': "apns_chat_#{info_type}_description"
      },

      badge: 1,
      sound: "default",
      'mutable-content': 1
    }
  end

  before do
    allow(ChannelServiceMessage).to receive(:new) { channel_service_message }
    allow(channel_service_message).to receive(:to_hash) { reveal_data }

    allow(PushNotificationMessage).to receive(:new) { push_notification_message }
    allow(push_notification_message).to receive(:to_hash) { push_notification_data }

    allow(PubNubAdapter).to receive(:new) { pubnub_client }
    allow(pubnub_client).to receive(:send_message_to_channel)
  end

  context "when one user is deanonymized" do
    let(:info_type) { "one_reveal" }
    let(:deanonymization) { create :deanonymization, user: first_user, chat: incognito_chat }
    let(:first_user_device) { first_user.device }

    before do
      reveal_data[:receiver_id] = second_user.id
      push_notification_data[:chat_receiver_id] = second_user.id
      push_notification_data[:pn_exceptions] = [first_user_device.push_token]
    end

    it "creates one reveal message" do
      expect(ChannelServiceMessage)
        .to receive(:new)
        .with(
          info_type: info_type,
          chat_id: incognito_chat_id,
          push_notification_data: push_notification_data,
          receiver: second_user
        )

      interactor_call
    end

    it "creates one reveal notification message" do
      expect(PushNotificationMessage)
        .to receive(:new)
        .with(
          info_type: info_type,
          chat_id: incognito_chat_id,
          sender: first_user,
          receiver: second_user
        )

      interactor_call
    end

    it "sends one reveal message to incognito chat pubnub channel" do
      expect(pubnub_client)
        .to receive(:send_message_to_channel)
        .with(pubnub_channel_name, reveal_data)

      interactor_call
    end

    it "updates :updated_at chat field" do
      expect(incognito_chat).to receive(:update)

      interactor_call
    end
  end

  context "when both of users are deanonymized" do
    let(:info_type) { "mutual_reveal" }
    let(:deanonymization) { create :deanonymization, user: first_user, chat: incognito_chat }

    before do
      create :deanonymization, user: second_user, chat: incognito_chat
    end

    it "creates mutual reveal message" do
      expect(ChannelServiceMessage)
        .to receive(:new)
        .with(
          info_type: info_type,
          chat_id: incognito_chat_id,
          push_notification_data: push_notification_data
        )

      interactor_call
    end

    it "creates mutual reveal notification message" do
      expect(PushNotificationMessage)
        .to receive(:new)
        .with(
          info_type: info_type,
          chat_id: incognito_chat_id
        )

      interactor_call
    end

    it "sends mutual reveal message to incognito chat pubnub channel" do
      expect(pubnub_client)
        .to receive(:send_message_to_channel)
        .with(pubnub_channel_name, reveal_data)

      interactor_call
    end

    it "updates :updated_at chat field" do
      expect(incognito_chat).to receive(:update)

      interactor_call
    end
  end
end
