require "rails_helper"

describe Users::SendMatchingMessageToIncognitoChat do
  subject(:interactor_call) { described_class.call(chat: incognito_chat) }

  let(:incognito_chat) { create :chat, :incognito }
  let(:chat_id) { incognito_chat.id }

  let(:pubnub_channel_name) { incognito_chat.pubnub_channel_name }

  let(:pubnub_client) { instance_double "PubNubAdapter" }
  let(:channel_service_message) { instance_double "ChannelServiceMessage" }
  let(:push_notification_message) { instance_double "PushNotificationMessage" }

  let(:info_type) { "match" }
  let(:notification_info_type) { "match_message" }
  let(:message_uuid) { "message_uuid" }
  let(:message_created_at) { 0 }

  let(:matching_data) do
    {
      version: 10_000,
      chat_id: chat_id,
      method: {
        type: "publish",
        message: message
      },

      pn_apns: push_notification_data
    }
  end

  let(:message) do
    {
      id: message_uuid,
      created_at: message_created_at,
      content: {
        info_type: info_type,
        type: "info"
      }
    }
  end

  let(:push_notification_data) do
    {
      aps: aps_data,
      category_type: "chat_message",
      chat_id: chat_id
    }
  end

  let(:aps_data) do
    {
      category: "chat_message",

      alert: {
        'title-loc-key': "apns_chat_message_title",
        'loc-key': "apns_chat_match_message_description"
      },

      badge: 1,
      sound: "default",
      'mutable-content': 1
    }
  end

  before do
    allow(ChannelServiceMessage).to receive(:new) { channel_service_message }
    allow(channel_service_message).to receive(:to_hash) { matching_data }

    allow(PushNotificationMessage).to receive(:new) { push_notification_message }
    allow(push_notification_message).to receive(:to_hash) { push_notification_data }

    allow(PubNubAdapter).to receive(:new) { pubnub_client }
    allow(pubnub_client).to receive(:send_message_to_channel)
  end

  it "creates matching message" do
    expect(ChannelServiceMessage)
      .to receive(:new)
      .with(info_type: info_type, chat_id: chat_id, push_notification_data: push_notification_data)

    interactor_call
  end

  it "creates push notification message" do
    expect(PushNotificationMessage)
      .to receive(:new)
      .with(chat_id: chat_id, info_type: notification_info_type)

    interactor_call
  end

  it "sends matching message to incognito chat pubnub channel" do
    expect(pubnub_client)
      .to receive(:send_message_to_channel)
      .with(pubnub_channel_name, matching_data)

    interactor_call
  end
end
