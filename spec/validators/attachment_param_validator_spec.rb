require "rails_helper"

describe AttachmentParamValidator do
  subject(:validator) { described_class.new(record, attribute, value) }

  let(:record) { create :user }
  let(:attribute) { :avatar }
  let(:value) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }
  let(:record_errors) { record.errors }

  before do
    allow(Rack::Test::UploadedFile).to receive(:==).with(ActionDispatch::Http::UploadedFile).and_return(true)
  end

  describe "#valid?" do
    it { is_expected.to be_valid }

    context "when value is nil" do
      let(:value) { nil }

      it "does not to be a valid" do
        is_expected.not_to be_valid

        expect(record_errors[:avatar]).to eq(["could not be uploaded"])
      end
    end

    context "when value is unexpected object" do
      let(:value) { "avatar" }

      it "does not to be a valid" do
        is_expected.not_to be_valid

        expect(record_errors[:avatar]).to eq(["could not be uploaded"])
      end
    end
  end
end
