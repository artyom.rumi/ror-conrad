require "rails_helper"

describe Schools::FilterSchoolsQuery do
  subject(:query) { described_class.new(filter_params) }

  let(:filter_params) { {} }

  let(:first_school) { create :school, name: "Judges Academy", latitude: 1, longitude: 1 }
  let(:second_school) { create :school, name: "School of Information Systems", latitude: 1.02, longitude: 1.02 }
  let(:third_school) { create :school, name: "Tredyffrin-Easttown MS", latitude: 1.1, longitude: 1.1 }

  let!(:first_grade) { create :grade, school: first_school, value: 11 }
  let!(:second_grade) { create :grade, school: second_school, value: 10 }
  let!(:third_grade) { create :grade, school: third_school, value: 11 }

  describe "#all" do
    subject(:result) { query.all }

    it { is_expected.to eq([first_school, second_school, third_school]) }

    context "when year_of_ending specified" do
      let(:filter_params) do
        {
          year_of_ending: 2021
        }
      end

      it { is_expected.to eq([first_school, third_school]) }
    end

    context "when latitude and longitude specified" do
      let(:filter_params) do
        {
          latitude: 1.1,
          longitude: 1.1
        }
      end

      it { is_expected.to eq([third_school, second_school, first_school]) }
    end

    context "when keywords specified" do
      let(:filter_params) do
        {
          keywords: "Tredy"
        }
      end

      it { is_expected.to eq([third_school]) }
    end
  end
end
