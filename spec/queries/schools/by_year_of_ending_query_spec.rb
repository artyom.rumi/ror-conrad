require "rails_helper"

describe Schools::ByYearOfEndingQuery do
  using AlreadyGraduated

  include_context :time_is_frozen

  subject(:query) { described_class.new(year_of_ending) }

  let(:previous_year) { 2019 }
  let(:current_year) { 2020 }
  let(:next_year) { 2021 }

  let(:first_school) { create :school }
  let(:second_school) { create :school }
  let(:service_school) { create :school } # school for service needs with outdated grade

  before do
    create :grade, value: current_year.grade_value, school: first_school
    create :grade, value: previous_year.grade_value, school: first_school
    create :grade, value: next_year.grade_value, school: second_school
    create :grade, value: 0, school: service_school
  end

  describe "#all" do
    let(:current_time) { Time.zone.local(current_year) }

    context "when not yet graduated" do
      let(:year_of_ending) { next_year }

      it "returns schools by specified year of ending" do
        expect(query.all).to match_array([second_school])
      end
    end

    context "when already graduated" do
      let(:year_of_ending) { previous_year }

      it "returns all real schools" do
        expect(query.all).to match_array([first_school, second_school])
      end
    end

    context "when current year specified" do
      let(:year_of_ending) { current_year }

      it "returns schools by specified year of ending" do
        expect(query.all).to match_array([first_school])
      end

      context "when already graduated" do
        let(:current_time) { Time.zone.local(current_year, 7) }

        it "returns all real schools" do
          expect(query.all).to match_array([first_school, second_school])
        end
      end
    end
  end
end
