require "rails_helper"

describe Chats::WithUserIdsQuery do
  subject(:query) { described_class.new(user_ids) }

  let(:user_ids) { [first_user.id, second_user.id] }

  let(:first_user) { create :user }
  let(:second_user) { create :user }
  let(:third_user) { create :user }

  let!(:chat) { create :chat, users: [first_user, second_user] }
  let!(:another_chat) { create :chat, users: [first_user, second_user], incognito: true }

  before do
    create :chat, users: [first_user, third_user]
  end

  describe "#all" do
    subject(:chats_with_user_ids) { query.all }

    it "returns chats with the specified user ids" do
      expect(chats_with_user_ids).to match_array([chat, another_chat])
    end
  end
end
