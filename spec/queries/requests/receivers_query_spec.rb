require "rails_helper"

describe Requests::ReceiversQuery do
  subject(:query) { described_class.new(requester) }

  let(:requester) { create :user }
  let(:crowd) { create_list :user, 5, school: requester.school }

  def add_5_followers(user)
    crowd.each do |follower|
      create :subscription, target: user, subscriber: follower
    end
  end

  def add_5_friends(user)
    crowd.each do |follower|
      create :subscription, target: follower, subscriber: user
    end
  end

  describe "#all" do
    subject(:searched_users) { query.all }

    let!(:first_friend) { create :user }
    let!(:second_friend) { create :user }
    let!(:third_friend) { create :user }

    before do
      create :subscription, target: requester, subscriber: first_friend
      create :subscription, target: requester, subscriber: second_friend
      create :subscription, target: requester, subscriber: third_friend
      # third friend should not receive any requests because he has no friends

      create :subscription, target: first_friend, subscriber: second_friend
      create :subscription, target: second_friend, subscriber: first_friend

      add_5_followers(first_friend)  # friend's followers should not affect
      add_5_followers(second_friend) # how many requests he can get

      add_5_friends(first_friend)
      add_5_friends(second_friend)
    end

    context "when requester hasn't yet sent requests and his friends haven't requests" do
      it "returns all requestrer friends with more than 5 friednds" do
        expect(searched_users.length).to eq(2)
        expect(searched_users).to match_array([first_friend, second_friend])
      end
    end

    context "when requester sent request to one of his friends" do
      before do
        create :request, requester: requester, receiver: first_friend
      end

      it "returns just one requester friend" do
        expect(searched_users.length).to eq(1)
        expect(searched_users).to match_array([second_friend])
      end
    end

    context "when requester's friend has requests from half of their friends" do
      before do
        create :subscription, target: first_friend, subscriber: requester

        create_list :request, 4, receiver: first_friend
      end

      it "returns just one requester friend" do
        expect(searched_users.length).to eq(1)
        expect(searched_users).to match_array([second_friend])
      end
    end

    context "when requester friend has closed request from requester" do
      before do
        create :request, :closed, receiver: first_friend, requester: requester
      end

      it "returns two requester's friends" do
        expect(searched_users.length).to eq(2)
        expect(searched_users).to match_array([second_friend, first_friend])
      end
    end

    context "when requester's friends have closed requests from more than half of their friends" do
      before do
        create :subscription, target: first_friend, subscriber: requester

        create_list :request, 4, :closed, receiver: first_friend
      end

      it "returns two requester friend" do
        expect(searched_users.length).to eq(2)
        expect(searched_users).to match_array([second_friend, first_friend])
      end
    end
  end
end
