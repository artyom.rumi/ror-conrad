require "rails_helper"

describe Users::SearchQuery do
  subject(:query) { described_class.new(keywords) }

  let(:eric) { create :user, first_name: "Eric", last_name: "Cartman" }
  let(:kyle) { create :user, first_name: "Kyle", last_name: "Broflovski" }

  describe "#all" do
    subject(:result) { query.all }

    context "when first name specified as keywords" do
      let(:keywords) { "Eric" }

      it "returns all users matched by keyword" do
        is_expected.to match_array([eric])
      end
    end

    context "when last name specified as keywords" do
      let(:keywords) { "Broflovski" }

      it "returns all users matched by keyword" do
        is_expected.to match_array([kyle])
      end
    end
  end
end
