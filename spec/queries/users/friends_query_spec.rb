require "rails_helper"

describe Users::FriendsQuery do
  subject(:query) { described_class.new(user, relation) }

  let(:user) { create :user, first_name: "John" }
  let(:relation) { Friendship.all }

  let(:tina) { create :user, first_name: "Tina", last_name: "Turner" }
  let(:john) { create :user, first_name: "John", last_name: "Bon Jovi" }
  let(:ritchie) { create :user, first_name: "Ritchie", last_name: "Blackmore" }
  let(:patty) { create :user, first_name: "Patti", last_name: "Smith" }
  let(:piter) { create :user, first_name: "Piter", last_name: "Parker" }

  before do
    create :friendship, first_user: ritchie, second_user: tina, expires_at: Time.current
    create :friendship, first_user: patty, second_user: tina, expires_at: Time.current
    create :friendship, first_user: piter, second_user: tina, expires_at: Time.current
  end

  describe "#all" do
    subject(:result) { query.all }

    let(:friends) { [] }

    it { is_expected.to match_array(friends) }

    context "when user has friends" do
      before do
        create :friendship, first_user: user, second_user: tina, expires_at: Time.current + 10.days
        create :friendship, first_user: ritchie, second_user: user, expires_at: Time.current + 10.days
        create :friendship, first_user: user, second_user: john, expires_at: Time.current + 10.days
      end

      let(:friends) { [tina, ritchie, john] }

      it { is_expected.to match_array(friends) }
    end

    context "with active friendships" do
      before do
        create :friendship, first_user: user, second_user: tina, expires_at: Time.current + 10.days
        create :friendship, first_user: ritchie, second_user: user, expires_at: Time.current + 10.days
        create :friendship,
          first_user: user,
          second_user: john,
          expires_at: Time.current - (Friendship::EXPIRES_AT_DAYS + 10.days)
      end

      let(:relation) { Friendship.active }

      let(:friends) { [tina, ritchie] }

      it { is_expected.to match_array(friends) }
    end
  end
end
