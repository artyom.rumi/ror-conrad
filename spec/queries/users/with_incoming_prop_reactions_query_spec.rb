require "rails_helper"

describe Users::WithIncomingPropReactionsQuery do
  subject(:query) { described_class.new(start_date: start_date, end_date: end_date) }

  describe "#all" do
    let(:result) { query.all }

    let(:user_with_relevant_reaction) { create :user }
    let(:user_with_irrelevant_reaction) { create :user }

    let(:first_prop_delivery) { create :prop_delivery, sender: user_with_relevant_reaction }

    let(:start_date) { 4.days.ago }
    let(:end_date) { 2.days.ago }

    before do
      create :prop_delivery, :with_reaction, sender: user_with_irrelevant_reaction

      create :reaction, prop_delivery: first_prop_delivery, created_at: 3.days.ago
    end

    it "returns users with incoming prop reactions created at specified date interval" do
      expect(result).to match_array([user_with_relevant_reaction])
    end
  end
end
