require "rails_helper"

describe Props::RecentlyUsedQuery do
  subject(:query) { described_class.new(user) }

  let!(:first_prop) { create :prop }
  let!(:second_prop) { create :prop }
  let!(:third_prop) { create :prop }

  let!(:first_prop_delivery) { create :prop_delivery, prop: first_prop, created_at: 3.days.ago }
  let!(:second_prop_delivery) { create :prop_delivery, prop: second_prop, created_at: 5.days.ago }
  let!(:third_prop_delivery) { create :prop_delivery, prop: third_prop, created_at: 2.days.ago }

  let(:recent_props_ids) { [third_prop.id, first_prop.id, second_prop.id] }

  describe "#all" do
    subject(:recent_props) { query.all }

    let(:user) { nil }

    it "returns all recently used props" do
      expect(recent_props).to match_array(Prop.all)
      expect(recent_props.map(&:id)).to eq(recent_props_ids)
    end

    context "when user specified" do
      let!(:user) { create :user }

      let!(:my_first_delivery) { create :prop_delivery, prop: first_prop, created_at: 1.day.ago, sender: user }
      let!(:my_third_delivery) { create :prop_delivery, prop: third_prop, sender: user }

      let!(:props_used_by_me_ids) { [third_prop.id, first_prop.id] }
      let!(:props_used_by_me) { Prop.where(id: props_used_by_me_ids) }

      it "returns all props, recently used by specific user" do
        expect(recent_props).to match_array(props_used_by_me)
        expect(recent_props.map(&:id)).to eq(props_used_by_me_ids)
      end
    end
  end
end
