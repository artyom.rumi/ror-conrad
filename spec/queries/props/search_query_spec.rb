require "rails_helper"

describe Props::SearchQuery do
  subject(:query) { described_class.new(content) }

  let!(:prop) { create :prop, content: "Awesome prop" }
  let!(:second_prop) { create :prop, content: "You are cute" }
  let!(:last_prop) { create :prop, content: "You are nice" }

  describe "#by_content" do
    subject(:searched_props) { query.by_content }

    context "when there are two props with the searched content" do
      let(:content) { "You are" }

      it "returns two props" do
        expect(searched_props.size).to eq(2)
        expect(searched_props).to match_array([second_prop, last_prop])
      end
    end

    context "when there are no one prop with searched content" do
      let(:content) { "cool" }

      it "returns zero props" do
        expect(searched_props.size).to eq(0)
      end
    end

    context "when query written in uppercase" do
      let(:content) { "AWESOME" }

      it "returns one prop" do
        expect(searched_props.size).to eq(1)
        expect(searched_props).to include(prop)
      end
    end

    context "when query written in lowercase with spaces" do
      let(:content) { " are    nice  " }

      it "returns one prop" do
        expect(searched_props.size).to eq(1)
        expect(searched_props).to include(last_prop)
      end
    end
  end
end
