require "rails_helper"

describe "props:recalculate_popularity" do
  let(:prop) { create :prop }
  let(:second_prop) { create :prop }
  let!(:last_prop) { create :prop }

  before do
    create_list :prop_delivery, 2, prop: second_prop
    create_list :prop_delivery, 3, prop: prop
  end

  it "change popularity index of used props" do
    expect { task.execute }
      .to change { prop.reload.popularity_index }.to(2)
                                                 .and change { second_prop.reload.popularity_index }.to(1)
    expect(last_prop.reload.popularity_index).to eq(0)
  end
end
