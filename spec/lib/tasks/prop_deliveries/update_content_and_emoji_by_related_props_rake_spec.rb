require "rails_helper"

describe "prop_deliveries:update_content_and_emoji_by_related_props" do
  let(:prop) { create :prop }
  let(:prop_delivery) { create :prop_delivery, prop: prop }

  let(:another_prop) { create :prop }
  let(:another_prop_delivery) { create :prop_delivery, prop: another_prop }

  let(:required_prop_deliveries) { instance_double(ActiveRecord::Relation) }

  before do
    allow(PropDelivery).to receive(:where).and_return(required_prop_deliveries)

    allow(required_prop_deliveries).to receive(:includes).and_return(required_prop_deliveries)
    allow(required_prop_deliveries).to receive(:find_each).and_yield(prop_delivery).and_yield(another_prop_delivery)
  end

  it "updates prop_delivery records with content and emoji of related props" do
    expect(prop_delivery).to receive(:update).with(content: prop.content, emoji: prop.emoji)
    expect(another_prop_delivery).to receive(:update).with(content: another_prop.content, emoji: another_prop.emoji)

    task.execute
  end
end
