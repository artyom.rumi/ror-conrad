require "rails_helper"

describe "auth_tokens:remove_outdated" do
  let!(:live_auth_token) { create :auth_token }
  let!(:expired_auth_token) { create :auth_token, expires_at: 1.day.ago }

  before do
    stub_pubnub_api
    allow(Devices::ClearPushTokenFromChats).to receive(:call!)
  end

  it "calls service that unsubscribes user's push token from all his chat channels" do
    expect(Devices::ClearPushTokenFromChats).to receive(:call!).with(user: expired_auth_token.user)
    task.execute
  end

  it "removes all expired auth tokens" do
    expect { task.execute }.to change(AuthToken, :count).from(2).to(1)
    expect(AuthToken.all).to match_array([live_auth_token])
  end
end
