require "rails_helper"

describe "incognito_chats:remove_outdated" do
  let!(:chat) { create :chat, :incognito, updated_at: Time.current }
  let!(:second_chat) { create :chat, :incognito, updated_at: 29.days.ago }

  before do
    create :chat, :incognito, updated_at: 30.days.ago
    create :chat, :incognito, updated_at: 31.days.ago
    create :chat, :incognito, updated_at: 90.days.ago
    create :chat, updated_at: 90.days.ago
  end

  it "removes all incognito chats that updated more than or equal 30 days ago" do
    expect { task.execute }.to change(Chat.incognito, :count).from(5).to(2)
    expect(Chat.incognito.all).to match_array([chat, second_chat])
  end
end
