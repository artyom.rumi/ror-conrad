require "rails_helper"

describe ChannelTextMessage do
  include_context :time_is_frozen

  subject(:channel_text_message) do
    described_class.new(
      chat_id: chat_id,
      sender_id: sender_id,
      receiver_id: receiver_id,
      text: text,
      image_urls: image_urls,
      link: link,
      comment: comment
    )
  end

  let(:chat_id) { 777 }
  let(:text) { "some text" }
  let(:sender_id) { 123 }
  let(:receiver_id) { 321 }

  let(:image_urls) { nil }
  let(:link) { nil }
  let(:comment) { nil }

  let(:protocol_version) { 10_000 }
  let(:message_uuid) { "l01-k3k" }
  let(:created_at) { 0 }

  let(:service_data) do
    {
      version: protocol_version,
      chat_id: chat_id,
      sender_id: sender_id,
      receiver_id: receiver_id,
      method: {
        type: "publish",
        message: message
      }
    }
  end

  let(:message) do
    {
      id: message_uuid,
      created_at: created_at,
      content: {
        type: "text",
        text: "some text"
      }
    }
  end

  before do
    allow(SecureRandom).to receive(:uuid) { message_uuid }
  end

  describe "#to_hash" do
    it "returns service text message hash" do
      expect(channel_text_message.to_hash).to eq(service_data)
    end
  end

  context "when feed with image" do
    let(:image_urls) do
      {
        photo_url: "image_url",
        small_photo_url: "image_url",
        medium_photo_url: "image_url",
        large_photo_url: "image_url"
      }
    end

    let(:message) do
      {
        id: message_uuid,
        created_at: created_at,
        content: {
          type: "photo",
          text: "some text",
          photo_url: "image_url",
          small_photo_url: "image_url",
          medium_photo_url: "image_url",
          large_photo_url: "image_url"
        }
      }
    end

    describe "#to_hash" do
      it "returns service text message hash" do
        expect(channel_text_message.to_hash).to eq(service_data)
      end
    end
  end

  context "when feed with link" do
    let(:link) { "google.com" }

    let(:message) do
      {
        id: message_uuid,
        created_at: created_at,
        content: {
          type: "link",
          link: "google.com",
          text: "some text"
        }
      }
    end

    describe "#to_hash" do
      it "returns service text message hash" do
        expect(channel_text_message.to_hash).to eq(service_data)
      end
    end
  end

  context "when feed with comment" do
    let(:comment) { "some comment" }

    let(:message) do
      {
        id: message_uuid,
        created_at: created_at,
        content: {
          type: "text",
          comment: "some comment",
          text: "some text"
        }
      }
    end

    describe "#to_hash" do
      it "returns service text message hash" do
        expect(channel_text_message.to_hash).to eq(service_data)
      end
    end
  end
end
