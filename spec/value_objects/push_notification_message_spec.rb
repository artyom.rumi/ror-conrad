require "rails_helper"

describe PushNotificationMessage do
  subject(:push_notification_message) do
    described_class.new(
      chat_id: chat_id,
      info_type: info_type,
      receiver: receiver,
      sender: sender
    )
  end

  let(:chat_id) { "123" }
  let(:info_type) { "" }

  let(:push_notification_data) do
    {
      aps: aps_data,
      category_type: "chat_message",
      chat_id: chat_id
    }
  end

  let(:aps_data) do
    {
      category: "chat_message",

      alert: {
        'title-loc-key': "apns_chat_message_title",
        'loc-key': "apns_chat_#{info_type}_description"
      },

      badge: 1,
      sound: "default",
      'mutable-content': 1
    }
  end

  describe "#to_hash" do
    context "when info type is 'match message'" do
      let(:info_type) { "match_message" }
      let(:receiver) { nil }
      let(:sender) { nil }

      it "returns match message push notification message hash" do
        expect(push_notification_message.to_hash).to eq(push_notification_data)
      end
    end

    context "when info type is 'one reveal'" do
      let(:info_type) { "one_reveal" }
      let(:receiver) { create :user }
      let(:sender) { create :user }
      let(:sender_device) { sender.device }

      before do
        push_notification_data[:chat_receiver_id] = receiver.id
        push_notification_data[:pn_exceptions] = [sender_device.push_token]
      end

      it "returns one reveal message push notification message hash" do
        expect(push_notification_message.to_hash).to eq(push_notification_data)
      end
    end

    context "when info type is 'mutual reveal'" do
      let(:info_type) { "mutual_reveal" }
      let(:receiver) { nil }
      let(:sender) { nil }

      it "returns mutual reveal push notification message hash" do
        expect(push_notification_message.to_hash).to eq(push_notification_data)
      end
    end
  end
end
