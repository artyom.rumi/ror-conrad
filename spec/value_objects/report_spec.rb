require "rails_helper"

describe Report do
  subject(:report) { described_class.new(report_params) }

  describe "#valid?" do
    let(:report_params) { { reporter: reporter, reported_user_id: reported_user_id } }
    let(:reporter) { create :user }

    context "when there is no reported user" do
      let(:reported_user_id) { User.last.id + 1 }

      it { is_expected.not_to be_a_valid }
    end

    context "when reporter and reported user are the same person" do
      let(:reported_user_id) { reporter.id }

      it { is_expected.not_to be_a_valid }
    end

    context "when reporter and reported user are different people" do
      let(:reported_user) { create :user }
      let(:reported_user_id) { reported_user.id }

      it { is_expected.to be_a_valid }
    end

    context "when there is no reporter" do
      let(:reported_user) { create :user }
      let(:reported_user_id) { reported_user.id }
      let(:reporter) { nil }

      it { is_expected.not_to be_a_valid }
    end
  end
end
