require "rails_helper"

describe ChannelServiceMessage do
  include_context :time_is_frozen

  subject(:channel_service_message) do
    described_class.new(
      info_type: info_type,
      chat_id: chat_id,
      push_notification_data: push_notification_data,
      receiver: receiver
    )
  end

  let(:chat_id) { 777 }

  let(:protocol_version) { 10_000 }
  let(:message_uuid) { "l01-k3k" }
  let(:created_at) { 0 }

  let(:service_data) do
    {
      version: protocol_version,
      chat_id: chat_id,
      method: {
        type: "publish",
        message: message
      },

      pn_apns: push_notification_data
    }
  end

  let(:message) do
    {
      id: message_uuid,
      created_at: created_at,
      content: {
        info_type: info_type,
        type: "info"
      }
    }
  end

  let(:push_notification_data) { { aps: "some data" } }

  before do
    allow(SecureRandom).to receive(:uuid) { message_uuid }
  end

  describe "#to_hash" do
    context "when info type is 'match'" do
      let(:info_type) { "match" }
      let(:receiver) { nil }

      it "returns match service message hash" do
        expect(channel_service_message.to_hash).to eq(service_data)
      end
    end

    context "when info type is 'one reveal'" do
      let(:info_type) { "one_reveal" }
      let!(:receiver) { create :user }

      before do
        service_data[:receiver_id] = receiver.id
      end

      it "returns match service message hash" do
        expect(channel_service_message.to_hash).to eq(service_data)
      end
    end

    context "when info type is 'cancel reveal'" do
      let(:info_type) { "cancel_reveal" }
      let!(:receiver) { create :user }

      before do
        service_data[:receiver_id] = receiver.id
      end

      it "returns match service message hash" do
        expect(channel_service_message.to_hash).to eq(service_data)
      end
    end

    context "when info type is 'mutual reveal'" do
      let(:info_type) { "mutual_reveal" }
      let(:receiver) { nil }

      it "returns match service message hash" do
        expect(channel_service_message.to_hash).to eq(service_data)
      end
    end
  end
end
