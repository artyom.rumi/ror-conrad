require "rails_helper"

describe PushNotification do
  subject(:push_notification) { described_class.new(push_notification_params) }

  let(:base_push_notification_params) do
    {
      receiver_device_push_token: "somepushtoken",
      badge: 2,
      sound: "quack.aiff",
      mutable_content: false
    }
  end

  let(:apn_topic) { "some.topic.com" }

  before do
    allow(ENV).to receive(:fetch).and_call_original
    allow(ENV).to receive(:fetch).with("APN_TOPIC") { apn_topic }
  end

  describe "#apnotic_notification" do
    context "when category is 'received_prop'" do
      let(:push_notification_params) do
        base_push_notification_params.merge(category: category, prop_sender_gender: sender_gender)
      end

      let(:category) { "received_prop" }
      let(:sender_gender) { "non_binary" }

      it "returns push notification object with specified attributes" do
        apnotic_notification = push_notification.apnotic_notification

        expect(apnotic_notification.badge).to eq(base_push_notification_params[:badge])
        expect(apnotic_notification.sound).to eq(base_push_notification_params[:sound])
        expect(apnotic_notification.mutable_content).to eq(base_push_notification_params[:mutable_content])
        expect(apnotic_notification.topic).to eq(apn_topic)
        expect(apnotic_notification.custom_payload)
          .to eq(category_type: category.to_sym, prop_sender_gender: sender_gender)
      end
    end

    context "when category is 'new_friend'" do
      let(:push_notification_params) do
        base_push_notification_params.merge(category: category)
      end

      let(:category) { "new_friend" }

      it "returns push notification object with specified attributes" do
        apnotic_notification = push_notification.apnotic_notification

        expect(apnotic_notification.badge).to eq(base_push_notification_params[:badge])
        expect(apnotic_notification.sound).to eq(base_push_notification_params[:sound])
        expect(apnotic_notification.mutable_content).to eq(base_push_notification_params[:mutable_content])
        expect(apnotic_notification.topic).to eq(apn_topic)
      end
    end

    context "when category is 'new_user'" do
      let(:push_notification_params) do
        base_push_notification_params.merge(category: category)
      end

      let(:category) { "new_user" }

      it "returns push notification object with specified attributes" do
        apnotic_notification = push_notification.apnotic_notification

        expect(apnotic_notification.badge).to eq(base_push_notification_params[:badge])
        expect(apnotic_notification.sound).to eq(base_push_notification_params[:sound])
        expect(apnotic_notification.mutable_content).to eq(base_push_notification_params[:mutable_content])
        expect(apnotic_notification.topic).to eq(apn_topic)
      end
    end

    context "when category is 'new_like'" do
      let(:push_notification_params) do
        base_push_notification_params.merge(category: category, sender: sender)
      end

      let(:category) { "new_like" }
      let(:sender) { "Someone" }

      it "returns push notification object with specified attributes" do
        apnotic_notification = push_notification.apnotic_notification

        expect(apnotic_notification.badge).to eq(base_push_notification_params[:badge])
        expect(apnotic_notification.sound).to eq(base_push_notification_params[:sound])
        expect(apnotic_notification.mutable_content).to eq(base_push_notification_params[:mutable_content])
        expect(apnotic_notification.topic).to eq(apn_topic)
        expect(apnotic_notification.custom_payload).to eq(category_type: category.to_sym, sender: sender)
      end
    end

    context "when category is 'new_comment'" do
      let(:push_notification_params) do
        base_push_notification_params.merge(category: category, sender: sender)
      end

      let(:category) { "new_comment" }
      let(:sender) { "A friend of your friend" }

      it "returns push notification object with specified attributes" do
        apnotic_notification = push_notification.apnotic_notification

        expect(apnotic_notification.badge).to eq(base_push_notification_params[:badge])
        expect(apnotic_notification.sound).to eq(base_push_notification_params[:sound])
        expect(apnotic_notification.mutable_content).to eq(base_push_notification_params[:mutable_content])
        expect(apnotic_notification.topic).to eq(apn_topic)
        expect(apnotic_notification.custom_payload).to eq(category_type: category.to_sym, sender: sender)
      end
    end

    context "when category is 'new_repost'" do
      let(:push_notification_params) do
        base_push_notification_params.merge(category: category, sender: sender)
      end

      let(:category) { "new_repost" }
      let(:sender) { "A friend" }

      it "returns push notification object with specified attributes" do
        apnotic_notification = push_notification.apnotic_notification

        expect(apnotic_notification.badge).to eq(base_push_notification_params[:badge])
        expect(apnotic_notification.sound).to eq(base_push_notification_params[:sound])
        expect(apnotic_notification.mutable_content).to eq(base_push_notification_params[:mutable_content])
        expect(apnotic_notification.topic).to eq(apn_topic)
        expect(apnotic_notification.custom_payload).to eq(category_type: category.to_sym, sender: sender)
      end
    end
  end
end
