require "rails_helper"

describe NotificationData do
  subject(:notification_data_subject) { described_class.new(notification) }

  let(:feed) { create :feed, user: current_user }
  let(:current_user) { create :user }
  let(:another_user) { create :user }

  let(:notification_data) do
    {
      id: notification_id,
      type: action_name,
      is_unread: status == "Unread",
      created_at: created_at,
      image: image,
      message: message,
      payload: payload
    }
  end

  let(:notification_id) { notification.id }
  let(:action_name) { notification.action_name }
  let(:status) { notification.status }
  let(:created_at) { notification.created_at }

  context "when notification is Like" do
    let(:like) { create :like, user: another_user, feed: feed }

    let!(:notification) do
      create :notification, notifiable: like, user: current_user, action_name: "new_like"
    end

    let(:image) { nil }
    let(:message) { "Someone liked your post!" }
    let(:payload) { { feed_id: notification.notifiable.feed_id } }

    describe "#to_hash" do
      it "returns ungrouped notification hash for Like" do
        expect(notification_data_subject.to_hash).to eq(notification_data)
      end
    end
  end

  context "when notification is Repost" do
    let(:repost) { create :repost, user: another_user, source_feed: feed }

    let!(:notification) do
      create :notification, notifiable_type: "Repost",
                            notifiable_id: repost.id, user: current_user, action_name: "new_repost"
    end

    let(:image) { nil }
    let(:message) { "Someone reposted your post!" }
    let(:payload) { { repost_feed_id: notification.notifiable.id, feed_id: notification.notifiable.source_feed_id } }

    describe "#to_hash" do
      it "returns ungrouped notification hash for Repost" do
        expect(notification_data_subject.to_hash).to eq(notification_data)
      end
    end
  end

  context "when notification is Friendship" do
    let(:friendship) { create :friendship, first_user: current_user, second_user: another_user }

    let!(:notification) do
      create :notification, notifiable: friendship, user: current_user, action_name: "new_friend"
    end

    let(:image) do
      {
        original: "http://#{ENV.fetch('HOST')}/images/#{another_user.default_avatar}",
        small: "http://#{ENV.fetch('HOST')}/images/small-#{another_user.default_avatar}",
        medium: "http://#{ENV.fetch('HOST')}/images/medium-#{another_user.default_avatar}",
        large: "http://#{ENV.fetch('HOST')}/images/large-#{another_user.default_avatar}"
      }
    end

    let(:message) { "You're now friends with #{another_user.full_name}" }
    let(:payload) { { friend_id: another_user.id } }

    describe "#to_hash" do
      it "returns ungrouped notification hash for Friendship" do
        expect(notification_data_subject.to_hash).to eq(notification_data)
      end
    end
  end

  context "when notification is Comment" do
    let(:comment) { create :comment, user: another_user, feed: feed, text: "Some text" }

    let!(:notification) do
      create :notification, notifiable: comment, user: current_user, action_name: "new_comment"
    end

    let(:image) { nil }
    let(:message) { "Some text" }
    let(:payload) { { feed_id: notification.notifiable.feed_id, sender: "Someone", comment_id: comment.id } }

    describe "#to_hash" do
      it "returns ungrouped notification hash for Comment" do
        expect(notification_data_subject.to_hash).to eq(notification_data)
      end
    end
  end
end
