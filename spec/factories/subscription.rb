FactoryBot.define do
  factory :subscription do
    association :target, factory: :user
    association :subscriber, factory: :user
  end
end
