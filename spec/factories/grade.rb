FactoryBot.define do
  factory :grade do
    value { rand(6..12) }
    school
  end
end
