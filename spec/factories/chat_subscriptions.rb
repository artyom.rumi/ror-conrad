FactoryBot.define do
  factory :chat_subscription do
    user
    chat
  end
end
