FactoryBot.define do
  factory :feed do
    user

    trait :with_image do
      after(:create) do |feed|
        feed.image.attach(
          io: File.open(Rails.root.join("spec", "support", "fixtures", "avatar.png")),
          filename: "image.jpeg", content_type: "image/jpeg"
        )
      end
    end
  end
end
