FactoryBot.define do
  sequence(:email) { Faker::Internet.email }
  sequence(:title) { |n| "#{Faker::Lorem.words} #{n}" }
  sequence(:phone_number) { "+1" + rand.to_s[2..11] }
end
