FactoryBot.define do
  factory :request do
    progress { 0 }
    association :requester, factory: :user
    association :receiver, factory: :user

    trait :closed do
      progress { 5 }
    end
  end
end
