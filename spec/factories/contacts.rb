FactoryBot.define do
  factory :contact do
    phone_number { "+11234567890" }
    user
  end
end
