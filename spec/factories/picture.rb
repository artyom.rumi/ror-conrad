FactoryBot.define do
  factory :picture do
    image { Rails.root.join("spec", "support", "fixtures", "avatar.png").to_s }
    user { nil }
  end
end
