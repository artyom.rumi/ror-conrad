FactoryBot.define do
  factory :request_respondent do
    association :request
    association :respondent, factory: :user
  end
end
