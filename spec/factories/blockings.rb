FactoryBot.define do
  factory :blocking do
    user

    trait :for_user do
      association :blockable, factory: :user
    end

    trait :for_collocutor_mask do
      association :blockable, factory: :collocutor_mask
    end
  end
end
