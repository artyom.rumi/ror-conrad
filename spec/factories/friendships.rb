FactoryBot.define do
  factory :friendship do
    association :first_user, factory: :user
    association :second_user, factory: :user

    expires_at { Time.current + Friendship::EXPIRES_AT_DAYS }
  end
end
