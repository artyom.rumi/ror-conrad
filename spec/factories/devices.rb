FactoryBot.define do
  factory :device do
    push_token { SecureRandom.urlsafe_base64 }
    phone_number

    trait :with_user do
      user
    end
  end
end
