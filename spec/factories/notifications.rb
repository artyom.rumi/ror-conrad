FactoryBot.define do
  factory :notification do
    user
    status { "Unread" }
  end
end
