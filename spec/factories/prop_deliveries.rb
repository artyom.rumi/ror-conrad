FactoryBot.define do
  factory :prop_delivery do
    association :sender, factory: :user
    association :recipient, factory: :user
    association :prop, factory: :prop

    content { prop.content }
    emoji { prop.emoji }
    sender_gender { sender.gender }
    viewed_at { nil }
    boost_type { :secret }

    trait :with_request do
      request
    end

    trait :with_reaction do
      after(:create) do |prop_delivery|
        create(:reaction, prop_delivery: prop_delivery)
      end
    end

    trait :custom do
      prop { nil }
      content { Faker::Lorem.sentence }
      emoji { "\xF0\x9F\x98\x8D" }
    end

    trait :viewed do
      viewed_at { Time.current }
    end
  end
end
