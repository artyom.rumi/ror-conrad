FactoryBot.define do
  factory :prop do
    popularity_index { 0 }
    content { Faker::Lorem.sentence }
    emoji { "\xF0\x9F\x98\x8D" }
    special { false }
    gender { ["male", "female", nil].sample }

    trait :special do
      special { true }
    end

    trait :for_male do
      gender { :male }
    end

    trait :for_female do
      gender { :female }
    end

    trait :discarded do
      discarded_at { 1.hour.ago }
    end
  end
end
