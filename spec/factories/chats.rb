FactoryBot.define do
  factory :chat do
    owner { create(:user) }
    trait :with_users do
      transient do
        users_count { 2 }
      end

      after(:create) do |chat, evaluator|
        (1..evaluator.users_count).each do
          chat.users << create(:user)
        end
        chat.owner = chat.users.first
      end
    end

    trait :incognito do
      incognito { true }
    end
  end
end
