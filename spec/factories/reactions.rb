FactoryBot.define do
  factory :reaction do
    prop_delivery
    content { Faker::Lorem.sentence }
  end
end
