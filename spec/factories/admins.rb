FactoryBot.define do
  factory :admin do
    email
    password { "123456" }
    full_name { Faker::Name.name }
  end
end
