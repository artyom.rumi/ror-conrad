FactoryBot.define do
  factory :comment do
    text { Faker::Lorem.sentence }
    feed
    user
  end
end
