FactoryBot.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    phone_number
    grade { rand(2010..2030) }
    age { rand(12..18) }
    gender { %i[male female non_binary].sample }
    bio { Faker::Lorem.sentence }
    requests_count { 0 }
    school
    default_avatar { "avatars/avatar-" + rand(1..30).to_s + ".png" }
    matching_enabled { true }
    prop_request_notification_enabled { true }
    receiving_prop_notification_enabled { true }
    chat_messages_notification_enabled { true }
    new_friends_notification_enabled { true }
    new_users_notification_enabled { true }
    role { "ordinary" }

    transient do
      with_token { true }
      with_masks_counters { false }
    end

    trait :without_token do
      with_token { false }
    end

    trait :with_masks_counters do
      with_masks_counters { true }
    end

    after(:create) do |user, evaluator|
      evaluator.with_token &&
        create(:auth_token, user: user)

      evaluator.with_masks_counters &&
        create(:gender_masks_counter, user: user)
    end

    trait :male do
      gender { :male }
    end

    trait :female do
      gender { :female }
    end

    trait :non_binary do
      gender { :non_binary }
    end

    trait :with_matching_disabled do
      matching_enabled { false }
    end

    trait :test do
      role { "test" }
    end

    trait :bot do
      role { "bot" }
    end
  end
end
