FactoryBot.define do
  factory :auth_token do
    expires_at { 180.days.since }
    user { create :user, :without_token }
    device { create :device, user: user }
  end
end
