FactoryBot.define do
  factory :school do
    name { Faker::Lorem.sentence }
    members_amount { rand(1..100) }
    address { [Faker::Address.city, Faker::Address.state].join(", ") }

    trait :high do
      name { "High school " + Faker::Lorem.word }
    end

    trait :middle do
      name { "Middle school " + Faker::Lorem.word }
    end
  end
end
