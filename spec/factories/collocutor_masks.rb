FactoryBot.define do
  factory :collocutor_mask do
    name { Faker::Name.name }
    association :collocutor, factory: :user
    association :user
  end
end
