FactoryBot.define do
  factory :verification_code do
    code { rand(0..9999).to_s.rjust(4, "0") }
    phone_number
    expires_at { 5.minutes.since }
    attempts_count { 3 }

    trait :with_user do
      association :user
    end
  end
end
