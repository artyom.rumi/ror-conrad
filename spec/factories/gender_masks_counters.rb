FactoryBot.define do
  factory :gender_masks_counter do
    user
    male { rand(0..10) }
    female { rand(0..10) }
    non_binary { rand(0..10) }
  end
end
