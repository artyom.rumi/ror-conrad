FactoryBot.define do
  factory :deanonymization do
    association :chat, incognito: true
    user
  end
end
