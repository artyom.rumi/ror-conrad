FactoryBot.define do
  factory :friend_intention do
    user
    association :target, factory: :user
  end
end
