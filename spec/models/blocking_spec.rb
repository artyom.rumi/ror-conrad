require "rails_helper"

describe Blocking do
  subject(:blocking) { build_stubbed :blocking, blockable: blockable }

  describe "#blocked_user" do
    context "when blockable is another user" do
      let(:blockable) { build_stubbed :user }

      it "returns blocked user" do
        expect(blocking.blocked_user).to eq(blockable)
      end
    end

    context "when blockable is collocutor mask" do
      let(:another_user) { build_stubbed :user }
      let(:blockable) { build :collocutor_mask, collocutor: another_user }

      it "returns the user pointed to by the collocutor mask" do
        expect(blocking.blocked_user).to eq(another_user)
      end
    end
  end
end
