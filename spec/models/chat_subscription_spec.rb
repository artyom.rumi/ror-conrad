require "rails_helper"

describe ChatSubscription do
  describe "validations" do
    it { is_expected.to validate_presence_of :user }
    it { is_expected.to validate_presence_of :chat }
  end
end
