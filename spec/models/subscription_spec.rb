require "rails_helper"

describe Subscription do
  describe "validations" do
    let!(:subscriber) { create :user }
    let!(:target) { create :user }

    context "with equal target and subscriber" do
      let(:subscription) { build :subscription, target: target, subscriber: target }

      it "validates that user is not subscribing to himself" do
        expect(subscription.valid?).to eq false
      end
    end

    context "with different target and subscriber" do
      let(:subscription) { build :subscription, target: target, subscriber: subscriber }

      it "validates that user is not subscribing to himself" do
        expect(subscription.valid?).to eq true
      end
    end
  end
end
