require "rails_helper"

describe Deanonymization do
  let(:chat) { create :chat, :with_users }
  let(:user) { chat.users.first }
  let!(:deanonymization) { build :deanonymization, chat: chat, user: user }

  context "with non-incognito chat" do
    it "is not valid" do
      expect(deanonymization.valid?).to eq false
    end
  end

  context "with incognito chat" do
    it "is valid" do
      chat.incognito = true
      expect(deanonymization.valid?).to eq true
    end
  end
end
