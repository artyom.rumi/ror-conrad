require "rails_helper"

describe Request do
  describe "validations" do
    let!(:requester) { create :user }
    let!(:receiver) { create :user }

    context "when there exists open request with same requester and receiver" do
      before do
        create :request, requester: requester, receiver: receiver
      end

      let(:request) { build :request, requester: requester, receiver: receiver }

      it "is not valid" do
        expect(request.valid?).to eq false
      end
    end

    context "when there not exists open request with same requester and receiver" do
      let(:request) { build :request, requester: requester, receiver: receiver }

      it "is valid" do
        expect(request.valid?).to eq true
      end
    end
  end
end
