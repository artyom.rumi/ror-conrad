require "rails_helper"

describe VerificationCode do
  describe "#valid_code?" do
    context "with expired code" do
      let(:verification_code) { build_stubbed :verification_code, code: "1234", expires_at: 1.minute.ago }

      it "validates the code" do
        expect(verification_code.valid_code?("1234")).to eq false
        expect(verification_code.valid_code?("1235")).to eq false
      end
    end

    context "with attempts exceeded" do
      let(:verification_code) { build_stubbed :verification_code, code: "1234", attempts_count: -1 }

      it "validates the code" do
        expect(verification_code.valid_code?("1234")).to eq false
        expect(verification_code.valid_code?("1235")).to eq false
      end
    end

    context "with active code" do
      let(:verification_code) { build_stubbed :verification_code, code: "1234", expires_at: 1.minute.since }

      it "validates the code" do
        expect(verification_code.valid_code?("1234")).to eq true
        expect(verification_code.valid_code?("1235")).to eq false
      end
    end
  end
end
