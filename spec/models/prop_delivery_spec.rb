require "rails_helper"

describe PropDelivery do
  describe "validations" do
    subject { described_class.new(prop_delivery_params) }

    let!(:sender) { create :user }
    let!(:recipient) { create :user }
    let!(:prop) { create :prop }

    let(:prop_delivery_params) do
      {
        sender: sender,
        recipient: recipient,
        prop: prop,
        content: prop.content,
        emoji: prop.emoji,
        sender_gender: sender.gender
      }
    end

    context "with not equal sender and recipient" do
      it "is valid" do
        is_expected.to be_valid
      end
    end

    context "with sender as a recipient" do
      before { prop_delivery_params[:recipient] = sender }

      it "is not valid" do
        is_expected.to be_invalid
      end
    end

    context "when sender is blank" do
      before { prop_delivery_params[:sender] = nil }

      it "is not valid" do
        is_expected.to be_invalid
      end
    end

    context "with existing delivery" do
      let(:prop_delivery) { create :prop_delivery }

      it "is not possible to update sender to nil" do
        expect(prop_delivery.update(sender_id: nil)).to eq false
      end

      context "when sender is nullified" do
        before do
          prop_delivery.sender.destroy
        end

        it "is valid" do
          is_expected.to be_valid
        end
      end

      context "when recipient is nullified" do
        before do
          prop_delivery.recipient.destroy
        end

        it "is valid" do
          is_expected.to be_valid
        end
      end
    end
  end
end
