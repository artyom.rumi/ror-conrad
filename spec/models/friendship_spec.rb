require "rails_helper"

describe Friendship do
  describe "validations" do
    let!(:first_user) { create :user }
    let!(:second_user) { create :user }

    context "when there is already friendship between same users" do
      before { create :friendship, first_user: first_user, second_user: second_user }

      let(:friendship) { build :friendship, first_user: first_user, second_user: second_user }

      it "is not valid" do
        expect(friendship).to be_invalid
      end
    end

    context "when there is already friendship between same users in reverse order" do
      before { create :friendship, first_user: second_user, second_user: first_user }

      let(:friendship) { build :friendship, first_user: second_user, second_user: first_user }

      it "is not valid" do
        expect(friendship).to be_invalid
      end
    end

    context "when there is no frinedship between same users yet" do
      let(:friendship) { build :friendship, first_user: first_user, second_user: second_user }

      it "is valid" do
        expect(friendship).to be_valid
      end
    end
  end
end
