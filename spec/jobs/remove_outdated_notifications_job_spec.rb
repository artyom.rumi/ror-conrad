require "rails_helper"

describe RemoveOutdatedNotificationsJob do
  include ActiveJob::TestHelper

  describe "#perform_now" do
    let(:first_user) { create :user }
    let(:second_user) { create :user }
    let(:third_user) { create :user }

    let(:friendship) { create :friendship, first_user: first_user, second_user: second_user }
    let(:another_friendship) { create :friendship, first_user: first_user, second_user: third_user }

    let!(:notification) do
      create :notification, notifiable: friendship, user: first_user,
                            action_name: "new_friend", created_at: 5.days.ago
    end

    let!(:expired_notification) do
      create :notification, notifiable: another_friendship, user: first_user,
                            action_name: "new_friend", created_at: 8.days.ago
    end

    it "delete outdated notifications" do
      expect { described_class.perform_now }.to change(Notification, :count).from(2).to(1)
    end
  end
end
