require "rails_helper"

describe CreateRequestJob do
  include ActiveJob::TestHelper

  before do
    allow(PropsRequests::Create).to receive(:call).with(requester: requester, receiver: receiver)
  end

  describe "#perform_now" do
    let(:requester) { create :user }
    let(:receiver) { create :user }

    it "creates request and generates batches of users" do
      expect(PropsRequests::Create).to receive(:call).with(requester: requester, receiver: receiver)
      described_class.perform_now(requester, receiver)
    end
  end
end
