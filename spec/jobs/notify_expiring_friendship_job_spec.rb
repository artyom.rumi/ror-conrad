require "rails_helper"

describe NotifyExpiringFriendshipJob do
  include ActiveJob::TestHelper

  describe "#perform_now" do
    let(:first_user) { create :user }
    let(:second_user) { create :user }
    let(:third_user) { create :user }

    let(:expired_friendship_72h) do
      create :friendship, first_user: first_user, second_user: second_user, expires_at: Time.zone.now + 15.hours
    end

    let(:expired_friendship_12h) do
      create :friendship, first_user: first_user, second_user: third_user, expires_at: Time.zone.now + 10.hours
    end

    it "creates notifications to users about expiring friendship" do
      expect(Notifications::ExpiringFriendship).to receive(:call).with(expiring_friendship: expired_friendship_12h)
      expect(Notifications::ExpiringFriendship).to receive(:call).with(expiring_friendship: expired_friendship_72h)
      described_class.perform_now
    end
  end
end
