require "rails_helper"

describe NotifyExpiredFriendshipJob do
  include ActiveJob::TestHelper

  describe "#perform_now" do
    let(:first_user) { create :user }
    let(:second_user) { create :user }
    let(:third_user) { create :user }

    let!(:notifying_expired_friendship) do
      create :friendship, first_user: first_user, second_user: second_user, expires_at: Time.zone.yesterday
    end

    let!(:notification) do
      create :notification, user: first_user, notifiable: notifying_expired_friendship,
                            action_name: "expired_friendship"
    end

    let!(:expired_friendship) do
      create :friendship, first_user: first_user, second_user: third_user, expires_at: Time.zone.yesterday
    end

    it "creates notifications to users about expired friendship" do
      expect(Notifications::ExpiredFriendship).not_to receive(:call)
        .with(expired_friendship: notifying_expired_friendship)
      expect(Notifications::ExpiredFriendship).to receive(:call)
        .with(expired_friendship: expired_friendship)
      described_class.perform_now
    end
  end
end
