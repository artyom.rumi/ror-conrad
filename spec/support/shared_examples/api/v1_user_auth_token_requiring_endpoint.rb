RSpec.shared_examples "v1 user auth token requiring endpoint" do
  context "without auth token" do
    before do
      header "X-Auth-Token", nil
    end

    example "access without master token", document: false do
      do_request

      expect(status).to be(401)
    end
  end

  context "with invalid auth token" do
    before do
      header "X-Auth-Token", "some-invalid-token"
    end

    example "access with invalid master token", document: false do
      do_request

      expect(status).to be(401)
    end
  end

  context "with expired auth token" do
    let(:expired_auth_token) { create :auth_token, expires_at: 1.minute.ago }

    before do
      header "X-Auth-Token", expired_auth_token.value
    end

    example "access with invalid master token", document: false do
      do_request

      expect(status).to be(401)
    end
  end
end
