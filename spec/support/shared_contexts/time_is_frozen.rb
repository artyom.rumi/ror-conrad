shared_context :time_is_frozen do
  let(:current_time) { Time.new(2001, 1, 1).in_time_zone }

  before { Timecop.freeze(current_time) }

  after { Timecop.return }
end
