shared_context :apnotic_connection do
  let(:apns_connection) { instance_double "Apnotic::Connection" }

  before do
    allow(ENV).to receive(:fetch).and_call_original
    allow(ENV).to receive(:fetch).with("APN_CERTIFICATE_DATA").and_return("some data")
    allow(Apnotic::Connection).to receive(:new) { apns_connection }
    allow(apns_connection).to receive(:push)
    allow(apns_connection).to receive(:close)
  end
end
