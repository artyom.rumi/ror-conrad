shared_context :json_headers do
  header "Accept", "application/json"
  header "Content-type", "application/json"
end

shared_context :user_signed_in do
  include_context :json_headers

  let(:current_user) { create :user }
  let(:auth_token) { current_user.auth_token.value }

  before do
    header "X-Auth-Token", auth_token
  end
end

shared_context :guest_signed_in do
  include_context :json_headers

  let(:auth_token) { create :auth_token, user: nil }
  let(:auth_token_value) { auth_token.value }

  before do
    header "X-Auth-Token", auth_token_value
  end
end
