module WebmockHelpers
  STUB_KEY = "stub_key".freeze

  def stub_twilio_api
    stub_request(:post, /api.twilio.com/)
      .to_return(status: 201)
  end

  def stub_pubnub_api
    stub_pubnub_keys

    stub_request(:get, /pubsub.pubnub.com/)
      .to_return(status: 200)
  end

  private

  def stub_pubnub_keys
    allow(ENV).to receive(:fetch).and_call_original

    %w[PUBNUB_SUBSCRIBE_KEY PUBNUB_PUBLISH_KEY PUBNUB_CLIENT_UUID].each do |key|
      allow(ENV).to receive(:fetch).with(key).and_return(STUB_KEY)
    end
  end
end
