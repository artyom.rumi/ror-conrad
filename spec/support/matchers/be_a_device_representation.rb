RSpec::Matchers.define :be_a_device_representation do |device|
  match do |actual|
    expected = {
      id: device.id,
      user_id: device.user_id,
      phone_number: device.phone_number,
      push_token: device.push_token
    }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
