RSpec::Matchers.define :be_a_feed_representation do |feed, user|
  match do |actual|
    expected = {
      id: feed.id,
      user_id: feed.user_id,
      incognito: feed.incognito,
      likes_count: feed.likes_count,
      liked: feed.liked?(current_user.id),
      created_at: feed.created_at.as_json,
      unread_comments_count: new_comment_count(feed, user),
      text: feed.text,
      link: feed.link,
      image: image(feed),
      count_of_engaged_friends: count_of_engaged_friends(user, feed),
      comments_count: feed.comments_count,
      reposted: feed.reposted?,
      source_feed_id: feed.source_feed_id
    }

    expect(actual["chat"]).to be_a_chat_representation(chat(feed, user), user) if actual["chat"]
    expect(actual.deep_symbolize_keys).to include(expected)
  end

  def count_of_engaged_friends(user, feed)
    ::Users::FeedEngagedFriendsQuery.new(user, feed).count
  end

  def new_comment_count(feed, user)
    return 0 if feed.user != user

    feed.comments.where(status: "Unread").count
  end

  def chat(feed, user)
    return if feed.user == user

    feed.user_chat user
  end

  def image(feed)
    return if feed.image.attachment.blank?

    {
      original: rails_blob_url(feed.image),
      small: rails_representation_url(feed.image.variant(resize: "150x150")),
      medium: rails_representation_url(feed.image.variant(resize: "240x240")),
      large: rails_representation_url(feed.image.variant(resize: "480x480"))
    }
  end
end
