RSpec::Matchers.define :be_a_notification_list_representation do |notifications|
  match do |actual|
    expect(notifications.size).to eq(actual.size)

    actual.each do |actual_notification|
      expected_notification = notifications.find { |x| x["id"] == actual_notification["id"] }
      expect(actual_notification).to be_a_notification_representation(expected_notification)
    end
  end
end
