RSpec::Matchers.define :be_a_prop_representation do |prop|
  match do |actual|
    expected = {
      id: prop.id,
      content: prop.content,
      emoji: prop.emoji,
      gender: prop.gender
    }
    expect(actual.symbolize_keys).to eq(expected)
  end
end
