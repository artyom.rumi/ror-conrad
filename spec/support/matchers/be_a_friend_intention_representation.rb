RSpec::Matchers.define :be_a_friend_intention_representation do |friend_intention|
  match do |actual|
    expected = {
      id: friend_intention.id,
      target_id: friend_intention.target_id
    }

    expect(actual.deep_symbolize_keys).to eq(expected)
  end
end
