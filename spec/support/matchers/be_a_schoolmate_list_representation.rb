RSpec::Matchers.define :be_a_schoolmate_list_representation do |schoolmates, current_user|
  match do |actual|
    expect(schoolmates.size).to eq(actual.size)

    actual.each do |actual_schoolmate|
      expected_schoolmate = schoolmates.find(actual_schoolmate["id"])
      expect(actual_schoolmate).to be_a_schoolmate_representation(expected_schoolmate, current_user)
    end
  end
end
