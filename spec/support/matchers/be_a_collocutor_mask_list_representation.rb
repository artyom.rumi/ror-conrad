RSpec::Matchers.define :be_a_collocutor_mask_list_representation do |collocutor_masks, current_user|
  match do |actual|
    expect(collocutor_masks.size).to eq(actual.size)

    actual.each do |actual_collocutor_mask|
      expected_collocutor_mask = collocutor_masks.find(actual_collocutor_mask["id"])
      expect(actual_collocutor_mask).to be_a_collocutor_mask_representation(expected_collocutor_mask, current_user)
    end
  end
end
