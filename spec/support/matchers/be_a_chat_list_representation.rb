RSpec::Matchers.define :be_a_chat_list_representation do |chats, current_user|
  match do |actual|
    expect(chats.size).to eq(actual.size)

    actual.each do |actual_chat|
      expected_chat = chats.find(actual_chat["id"])
      expect(actual_chat).to be_a_chat_representation(expected_chat, current_user)
    end
  end
end
