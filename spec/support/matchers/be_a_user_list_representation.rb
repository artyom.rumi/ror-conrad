RSpec::Matchers.define :be_a_user_list_representation do |users|
  match do |actual|
    expect(users.size).to eq(actual.size)

    actual.each do |actual_user|
      expected_user = users.find(actual_user["id"])
      expect(actual_user).to be_a_user_representation(expected_user)
    end
  end
end
