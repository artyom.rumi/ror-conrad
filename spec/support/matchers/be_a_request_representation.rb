RSpec::Matchers.define :be_a_request_representation do |request|
  match do |actual|
    expected = { id: request.id, progress: request.progress, respondents_amount: 5 }
    expect(actual.symbolize_keys).to eq(expected)
  end
end
