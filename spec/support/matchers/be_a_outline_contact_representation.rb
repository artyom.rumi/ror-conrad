RSpec::Matchers.define :be_a_outline_contact_representation do |contact|
  match do |actual|
    expected = {
      id: contact.id,
      phone_number: contact.phone_number,
      name: contact.name,
      invited: contact.invited
    }

    expect(actual.deep_symbolize_keys).to eq(expected)
  end
end
