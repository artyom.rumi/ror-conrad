RSpec::Matchers.define :be_a_deanonymization_list_representation do |deanonymizations|
  match do |actual|
    expect(deanonymizations.size).to eq(actual.size)

    actual.each do |actual_deanonymization|
      expected_deanonymization = deanonymizations.find(actual_deanonymization["id"])
      expect(actual_deanonymization).to be_a_deanonymization_representation(expected_deanonymization)
    end
  end
end
