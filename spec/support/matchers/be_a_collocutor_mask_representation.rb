RSpec::Matchers.define :be_a_collocutor_mask_representation do |collocutor_mask, current_user|
  match do |actual|
    expected = {
      id: collocutor_mask.id,
      name: collocutor_mask.name,
      blocked: current_user.blockings.where(blockable: collocutor_mask).any?,
      default_avatar: {
        original: "http://example.org/assets/avatars/avatar-incognito.png",
        small: "http://example.org/assets/small-avatars/avatar-incognito.png",
        medium: "http://example.org/assets/medium-avatars/avatar-incognito.png",
        large: "http://example.org/assets/large-avatars/avatar-incognito.png"
      },
      collocutor_id: collocutor_mask.collocutor_id,
      chat_id: collocutor_mask.chat_id
    }

    expect(actual["avatar"]).to be_an_attachment_representation(collocutor_mask.avatar)
    expect(actual.deep_symbolize_keys).to include(expected)
  end
end
