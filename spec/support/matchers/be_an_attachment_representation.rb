RSpec::Matchers.define :be_an_attachment_representation do |attachment|
  match do |actual|
    if attachment.attached?
      expect(actual.symbolize_keys).to eq(attachment_values(attachment))
    else
      expect(actual).to be_nil
    end
  end

  def attachment_values(attachment)
    {
      original: rails_blob_url(attachment),
      small: rails_representation_url(attachment.variant(resize: "150x150")),
      medium: rails_representation_url(attachment.variant(resize: "240x240")),
      large: rails_representation_url(attachment.variant(resize: "480x480"))
    }
  end
end
