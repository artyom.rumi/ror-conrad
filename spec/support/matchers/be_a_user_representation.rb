RSpec::Matchers.define :be_a_user_representation do |user|
  match do |actual|
    expected = {
      id: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      age: user.age,
      grade: user.grade,
      gender: user.gender,
      phone_number: user.phone_number,
      created_at: user.created_at.as_json,
      friend_boost_at: user.friend_boost_at.as_json,
      bio: user.bio,
      matching_enabled: user.matching_enabled,
      prop_request_notification_enabled: user.prop_request_notification_enabled,
      receiving_prop_notification_enabled: user.receiving_prop_notification_enabled,
      chat_messages_notification_enabled: user.chat_messages_notification_enabled,
      new_friends_notification_enabled: user.new_friends_notification_enabled,
      new_users_notification_enabled: user.new_users_notification_enabled,
      friends_count: user.friends_count,
      pokes_count: user.pokes_count,
      hide_top_pokes: user.hide_top_pokes,
      default_avatar: {
        original: "http://example.org/assets/#{user.default_avatar}",
        small: "http://example.org/assets/small-#{user.default_avatar}",
        medium: "http://example.org/assets/medium-#{user.default_avatar}",
        large: "http://example.org/assets/large-#{user.default_avatar}"
      }
    }

    expect(actual["school"]).to be_a_school_representation(user.school) if actual["school"].present?
    expect(actual["avatar"]).to be_an_attachment_representation(user.avatar)
    if user.hide_top_pokes
      expect(actual["top_pokes"]).to be_nil
    else
      expect(actual["top_pokes"]).to be_a_prop_list_representation(user.top_pokes)
    end

    expected.each do |key, value|
      expect(actual.deep_symbolize_keys.public_send(:[], key)).to eq value
    end
  end
end
