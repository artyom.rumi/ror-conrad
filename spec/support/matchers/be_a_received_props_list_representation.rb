RSpec::Matchers.define :be_a_received_props_list_representation do |expected_props|
  match do |actual|
    expect(expected_props.size).to eq(actual.size)

    actual.each do |actual_received_prop|
      expected_received_prop = expected_props.find { |expected| expected.id == actual_received_prop["id"] }
      expect(actual_received_prop).to be_a_received_prop_representation(expected_received_prop)
    end
  end
end
