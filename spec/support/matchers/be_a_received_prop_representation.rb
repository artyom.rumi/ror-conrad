RSpec::Matchers.define :be_a_received_prop_representation do |prop_delivery|
  match do |actual|
    prop = prop_delivery.prop

    expected = {
      id: prop_delivery.id,
      created_at: prop_delivery.created_at.as_json,
      sender_gender: prop_delivery.sender.gender,
      viewed_at: prop_delivery.viewed_at.as_json,
      type: prop_delivery.type
    }

    expected.keys.each do |key|
      expect(actual.symbolize_keys[key]).to eq expected[key]
    end

    if prop_delivery.type == "ordinary"
      expect(actual["prop"]).to be_a_prop_representation(prop)
    else
      expect(actual["prop"]).to eq(prop_delivery.attributes.slice("content", "emoji"))
    end

    if prop_delivery.reaction
      expect(actual["reaction"]).to be_a_reaction_representation(prop_delivery.reaction)
    else
      expect(actual["reaction"]).to eq(nil)
    end
  end
end
