RSpec::Matchers.define :be_a_prop_list_representation do |props|
  match do |actual|
    expect(props.size).to eq(actual.size)

    actual.each do |actual_prop|
      expected_prop = props.find(actual_prop["id"])
      expect(actual_prop).to be_a_prop_representation(expected_prop)
    end
  end
end
