RSpec::Matchers.define :be_a_notification_representation do |notification|
  match do |actual|
    expected_notification = notification["id"]
    expect(actual.id).to eq(expected_notification)
  end
end
