RSpec::Matchers.define :be_a_schoolmate_representation do |schoolmate, current_user|
  match do |actual|
    expected = {
      id: schoolmate.id,
      first_name: schoolmate.first_name,
      last_name: schoolmate.last_name,
      grade: schoolmate.grade,
      school_name: schoolmate.school.name,
      bio: schoolmate.bio,
      gender: schoolmate.gender,
      followee: schoolmate.incoming_subscriptions.where(subscriber: current_user).any?,
      blocked: current_user.blockings.where(blockable: schoolmate).any?,
      role: schoolmate.role,
      chat_available: current_user.friends_with?(schoolmate),
      custom_prop_available: current_user.friends_with?(schoolmate),
      friends_count: schoolmate.friends_count,
      pokes_count: schoolmate.pokes_count,
      friendship_status: current_user.friendship_status(schoolmate),
      default_avatar: {
        original: "http://example.org/assets/#{schoolmate.default_avatar}",
        small: "http://example.org/assets/small-#{schoolmate.default_avatar}",
        medium: "http://example.org/assets/medium-#{schoolmate.default_avatar}",
        large: "http://example.org/assets/large-#{schoolmate.default_avatar}"
      }
    }

    expect(actual["avatar"]).to be_an_attachment_representation(schoolmate.avatar)
    expect(actual.deep_symbolize_keys).to include(expected)
  end
end
