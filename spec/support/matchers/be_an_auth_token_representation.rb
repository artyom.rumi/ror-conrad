RSpec::Matchers.define :be_an_auth_token_representation do |auth_token|
  match do |actual|
    expected = {
      value: auth_token.value,
      expires_at: auth_token.expires_at.as_json,
      user_id: auth_token.user_id
    }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
