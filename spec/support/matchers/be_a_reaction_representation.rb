RSpec::Matchers.define :be_a_reaction_representation do |reaction|
  match do |actual|
    expected = {
      id: reaction.id,
      content: reaction.content
    }

    expect(actual).to be_present
    expect(actual["photo"]).to be_an_attachment_representation(reaction.photo)
    expect(actual.symbolize_keys).to include(expected)
  end
end
