RSpec::Matchers.define :be_a_request_list_representation do |requests|
  match do |actual|
    expect(requests.size).to eq(actual.size)

    actual.each do |actual_request|
      expected_request = requests.find(actual_request["id"])
      expect(actual_request).to be_a_request_representation(expected_request)
    end
  end
end
