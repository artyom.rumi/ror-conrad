RSpec::Matchers.define :be_a_contact_list_representation do |contacts|
  match do |actual|
    expect(contacts.size).to eq(actual.size)

    actual.each do |actual_contact|
      expected_contact = contacts.find(actual_contact["id"])
      expect(actual_contact).to be_a_contact_representation(expected_contact)
    end
  end
end
