RSpec::Matchers.define :be_a_sent_prop_representation do |prop_delivery, current_user|
  match do |actual|
    prop = prop_delivery.prop
    recipient = prop_delivery.recipient

    expected = {
      created_at: prop_delivery.created_at.as_json
    }

    expected.each do |key, value|
      expect(actual.symbolize_keys.public_send(:[], key)).to eq value
    end

    expect(actual["prop"]).to be_a_prop_representation(prop)
    expect(actual["recipient"]).to be_a_schoolmate_representation(recipient, current_user)
  end
end
