RSpec::Matchers.define :be_a_contact_representation do |contact|
  match do |actual|
    expected = {
      id: contact.id,
      phone_number: contact.phone_number
    }

    if contact.errors.any?
      expect(actual["errors"]).to eq(contact.errors.messages)
    else
      expect(actual["errors"]).to eq([])
    end

    expect(actual.symbolize_keys).to include(expected)
  end
end
