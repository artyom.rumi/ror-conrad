RSpec::Matchers.define :be_a_statistic_representation do |user|
  match do |actual|
    expected = {
      sent: user.outcoming_prop_deliveries.count,
      requested: user.requests_count,
      recieved: user.incoming_prop_deliveries.count
    }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
