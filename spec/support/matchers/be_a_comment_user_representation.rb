RSpec::Matchers.define :be_a_friend_representation do |friend, current_user|
  match do |actual|
    expected = {
      id: friend.id,
      first_name: friend.first_name,
      last_name: friend.last_name,
      grade: friend.grade,
      school_name: friend.school.name,
      bio: friend.bio,
      gender: friend.gender,
      blocked: current_user.blockings.where(blockable: friend).any?,
      role: friend.role,
      chat_available: current_user.friends_with?(friend),
      custom_prop_available: current_user.friends_with?(friend),
      friends_count: friend.friends_count,
      pokes_count: friend.pokes_count,
      friendship_status: current_user.friendship_status(friend)
    }

    expect(actual.deep_symbolize_keys).to include(expected)
  end
end
