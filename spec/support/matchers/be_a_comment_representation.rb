RSpec::Matchers.define :be_a_comment_representation do |comment, user|
  match do |actual|
    expected = {
      id: comment.id,
      text: comment.text,
      status: status(comment, user),
      created_at: comment.created_at.as_json,
      user: {
        id: comment.user.id,
        first_name: comment.user.first_name,
        last_name: comment.user.last_name,
        grade: comment.user.grade,
        school_name: comment.user.school.name,
        bio: comment.user.bio,
        gender: comment.user.gender,
        blocked: comment.feed.user.blockings.where(blockable: comment.user).any?,
        role: comment.user.role,
        chat_available: comment.feed.user.friends_with?(comment.user),
        custom_prop_available: user.friends_with?(comment.user),
        friends_count: user.friends_count,
        pokes_count: user.pokes_count,
        friendship_status: comment.user.friendship_status(user)
      }
    }

    expect(actual["chat"]).to be_a_chat_representation(chat(comment, user), user) if actual["chat"]
    expect(actual.deep_symbolize_keys).to include(expected)
  end

  def chat(comment, user)
    comment.chats.find_by(owner_id: user.id)
  end

  def status(comment, user)
    return "Read" if feed.user != user

    comment.status
  end
end
