RSpec::Matchers.define :be_a_friend_representation do |friend, current_user|
  match do |actual|
    expected = {
      id: friend.id,
      first_name: friend.first_name,
      last_name: friend.last_name,
      grade: friend.grade,
      school_name: friend.school.name,
      bio: friend.bio,
      gender: friend.gender,
      followee: friend.incoming_subscriptions.where(subscriber: current_user).any?,
      blocked: current_user.blockings.where(blockable: friend).any?,
      role: friend.role,
      chat_available: current_user.friends_with?(friend),
      custom_prop_available: current_user.friends_with?(friend),
      friends_count: friend.friends_count,
      pokes_count: friend.pokes_count,
      friendship_status: current_user.friendship_status(friend),
      days_to_expired_friendship: current_user.days_to_expired_friendship(friend),
      default_avatar: {
        original: "http://example.org/assets/#{friend.default_avatar}",
        small: "http://example.org/assets/small-#{friend.default_avatar}",
        medium: "http://example.org/assets/medium-#{friend.default_avatar}",
        large: "http://example.org/assets/large-#{friend.default_avatar}"
      }
    }

    expect(actual["avatar"]).to be_an_attachment_representation(friend.avatar)
    expect(actual.deep_symbolize_keys).to include(expected)
  end
end
