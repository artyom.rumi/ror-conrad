RSpec::Matchers.define :be_a_deanonymization_representation do |deanonymization|
  match do |actual|
    expected = { id: deanonymization.id, user_id: deanonymization.user_id }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
