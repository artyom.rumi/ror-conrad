RSpec::Matchers.define :be_a_chat_representation do |chat, current_user|
  match do |actual|
    expected = {
      id: chat.id,
      pubnub_channel_name: chat.pubnub_channel_name,
      updated_at: chat.updated_at.as_json,
      incognito: chat.incognito,
      feed_id: chat.feed_id,
      comment_id: chat.comment_id,
      name: chat.name,
      is_group: chat.is_group?
    }

    expected.each do |key, value|
      expect(actual.symbolize_keys.public_send(:[], key)).to eq value
    end

    expect(actual["users"]).to be_a_schoolmate_list_representation(chat.users, current_user)
    expect(actual["owner"]).to be_a_schoolmate_representation(chat.owner, current_user)
    expect(actual["deanonymizations"]).to be_a_deanonymization_list_representation(chat.deanonymizations)
  end
end
