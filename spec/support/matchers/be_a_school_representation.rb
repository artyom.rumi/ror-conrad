RSpec::Matchers.define :be_a_school_representation do |school|
  match do |actual|
    expected = {
      id: school.id,
      name: school.name,
      members_amount: school.members_amount,
      address: school.address
    }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
