RSpec::Matchers.define :be_a_prop_delivery_representation do |prop_delivery|
  match do |actual|
    expected = {
      id: prop_delivery.id,
      sender_gender: prop_delivery.sender_gender,
      sender_id: prop_delivery.sender_id,
      recipient_id: prop_delivery.recipient_id,
      content: prop_delivery.content,
      emoji: prop_delivery.emoji,
      prop_id: prop_delivery.prop_id,
      viewed_at: prop_delivery.viewed_at.as_json
    }

    expected.each do |key, value|
      expect(actual.symbolize_keys.public_send(:[], key)).to eq value
    end

    if actual.key? "request"
      if prop_delivery.request
        expect(actual["request"]).to be_a_request_representation(prop_delivery.request)
      else
        expect(actual["request"]).to eq(nil)
      end
    end

    if prop_delivery.reaction
      expect(actual["reaction"]).to be_a_reaction_representation(prop_delivery.reaction)
    else
      expect(actual["reaction"]).to eq(nil)
    end
  end
end
