RSpec::Matchers.define :be_a_subscription_representation do |subscription|
  match do |actual|
    expected = { id: subscription.id, target_id: subscription.target_id, subscriber_id: subscription.subscriber_id }
    expect(actual.symbolize_keys).to eq(expected)
  end
end
