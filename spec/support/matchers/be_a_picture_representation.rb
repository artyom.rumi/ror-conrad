RSpec::Matchers.define :be_a_picture_representation do |picture|
  match do |actual|
    expected = {
      id: picture.id,
      user_id: picture.record_id,
      image: {
        original: rails_blob_url(picture),
        small: rails_representation_url(picture.variant(resize: "150x150")),
        medium: rails_representation_url(picture.variant(resize: "240x240")),
        large: rails_representation_url(picture.variant(resize: "480x480"))
      }
    }

    expect(actual.deep_symbolize_keys).to eq(expected)
  end
end
