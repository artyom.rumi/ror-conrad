RSpec::Matchers.define :be_a_blocking_representation do |blocking|
  match do |actual|
    expected = {
      id: blocking.id,
      user_id: blocking.user_id,
      blockable_type: blocking.blockable_type,
      blockable_id: blocking.blockable_id,
      created_at: blocking.created_at.as_json
    }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
