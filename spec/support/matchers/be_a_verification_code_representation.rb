RSpec::Matchers.define :be_a_verification_code_representation do |verification_code|
  match do |actual|
    expected = {
      phone_number: verification_code.phone_number,
      expires_at: verification_code.expires_at.as_json,
      user_id: verification_code.user_id,
      attempts_count: verification_code.attempts_count
    }

    expect(actual.symbolize_keys).to eq(expected)
  end
end
