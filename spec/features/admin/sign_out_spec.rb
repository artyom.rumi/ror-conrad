require "rails_helper"

feature "Sign Out" do
  include_context :current_admin_signed_in

  scenario "Admin signs out" do
    visit "/"
    click_link "Log out"

    expect(page).to have_content("Sign in")
  end
end
