require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v2/Received props" do
  include_context :user_signed_in

  get "/api/v2/received_props" do
    it_behaves_like "v1 user auth token requiring endpoint"

    before do
      create :prop_delivery, recipient: current_user, viewed_at: 4.days.ago
    end

    let!(:prop_delivery) { create :prop_delivery, :with_reaction, recipient: current_user }
    let!(:viewed_prop_delivery) { create :prop_delivery, recipient: current_user, viewed_at: 2.days.ago }
    let!(:custom_prop_delivery) { create :prop_delivery, :custom, recipient: current_user }

    let(:expected_prop_deliveries) { [prop_delivery, viewed_prop_delivery, custom_prop_delivery] }

    example_request "list of unviewed props and props that viewed in the last 3 days" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_received_props_list_representation(expected_prop_deliveries)
    end
  end
end
