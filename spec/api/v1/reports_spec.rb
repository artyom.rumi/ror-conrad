require "rails_helper"
require "rspec_api_documentation/dsl"

resource "Report user" do
  include_context :user_signed_in

  post "/api/v1/reports" do
    parameter :reported_user_id, "Reported user id", required: true
    let(:reported_user) { create :user }
    let(:reported_user_id) { reported_user.id }

    example_request "Report user" do
      expect(response_status).to eq(204)
    end
  end
end
