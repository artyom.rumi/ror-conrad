require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Suggest props" do
  include_context :user_signed_in

  post "/api/v1/suggest_props" do
    parameter :content, "Suggested prop content", required: true
    parameter :emoji, "Suggested prop emoji"

    let(:content) { Faker::Lorem.sentence }
    let(:emoji) { "\xF0\x9F\x8C\xBA" }

    example_request "Suggest a prop" do
      expect(response_status).to eq(204)
    end
  end
end
