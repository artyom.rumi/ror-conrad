require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Invitations" do
  include_context :user_signed_in

  post "/api/v1/invitation" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :contacts, "Phone numbers", required: true

    let(:contacts) do
      [
        { name: "Johny B. Goode", phone_number: "+1424242424242" },
        { name: "King Creole", phone_number: "+1424242424243" }
      ]
    end

    before { stub_twilio_api }

    example_request "Send invite message to friends" do
      expect(response_status).to eq(204)
    end
  end
end
