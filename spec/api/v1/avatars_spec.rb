require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Avatar" do
  include_context :user_signed_in

  let(:file) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }

  post "/api/v1/avatar" do
    parameter :file, "User photo", required: true, "Type" => "Multipart/Form-data"

    it_behaves_like "v1 user auth token requiring endpoint"

    header "Content-type", "multipart/form-data"

    let(:raw_post) do
      params
    end

    example_request "Upload an image" do
      expect(response_status).to eq(201)

      user = User.find(json_response_body["id"])
      expect(json_response_body).to be_a_user_representation(user)
    end
  end

  delete "/api/v1/avatar" do
    it_behaves_like "v1 user auth token requiring endpoint"

    before do
      current_user.update(avatar: file)
    end

    example_request "Destroy avatar" do
      expect(current_user.reload.avatar).not_to be_attached
      expect(response_status).to eq(204)
    end
  end
end
