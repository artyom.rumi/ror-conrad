require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Fake request props" do
  include_context :user_signed_in
  include_context :apnotic_connection

  let(:apns_connection) { instance_double("Apnotic::Connection").as_null_object }

  before do
    create_list :subscription, 5, subscriber: current_user

    allow(ENV).to receive(:fetch).and_call_original
    allow(ENV).to receive(:fetch).with("APN_CERTIFICATE_DATA").and_return("some data")
    allow(ENV).to receive(:fetch).with("APN_TOPIC").and_return("topic")
  end

  post "/api/v1/fake_requests" do
    it_behaves_like "v1 user auth token requiring endpoint"

    example "Create fake request" do
      expect { do_request }
        .to change(Request, :count).by(1)
                                   .and change(RequestRespondent, :count).by(5)

      expect(response_status).to eq(201)

      request = Request.find(json_response_body["id"])
      expect(json_response_body).to be_a_request_representation(request)
    end
  end
end
