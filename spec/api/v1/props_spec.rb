require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Props" do
  include_context :user_signed_in

  get "/api/v1/props" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :user_id, "User to filter props by"
    parameter :limit, "Amount of props to be fetched"

    let!(:prop) { create :prop, :for_male }
    let!(:second_prop) { create :prop, :for_female }
    let!(:last_prop) { create :prop, :special, gender: nil }
    let!(:discarded_prop) { create :prop, :discarded }

    example_request "get full list of props" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_prop_list_representation(Prop.kept)
    end

    context "with male user specified" do
      let(:user) { create :user, :male }
      let(:user_id) { user.id }

      example_request "get list of props for male" do
        expect(response_status).to eq(200)
        male_props = Prop.where(gender: ["male", nil])
        expect(json_response_body).to be_a_prop_list_representation(male_props)
      end
    end

    context "with non-binary user specified" do
      let(:user) { create :user, :non_binary }
      let(:user_id) { user.id }

      example_request "get non-binary list of props" do
        expect(response_status).to eq(200)
        non_binary_props = Prop.where(gender: nil)
        expect(json_response_body).to be_a_prop_list_representation(non_binary_props)
      end
    end
  end
end
