require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feeds/Comments" do
  post "/api/v1/feeds/:feed_id/mark_comments" do
    include_context :user_signed_in

    let(:feed) { create :feed, user: current_user }

    let!(:first_comment) { create :comment, feed: feed, user: current_user }
    let!(:second_comment) { create :comment, feed: feed, user: current_user }

    parameter :feed_id, "Feed Id for comments", required: true
    parameter :commentIDs, "Comment Ids for mark", required: true

    let(:feed_id) { feed.id }
    let(:commentIDs) { [first_comment.id] }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Mark comments" do
      expect(response_status).to eq(200)

      expect(json_response_body["unread_comments_count"]).to eq 1
    end

    context "when user not feed author" do
      let(:another_user) { create :user }
      let(:feed) { create :feed, user: another_user }

      example_request "Mark comments" do
        expect(response_status).to eq(404)
        expect(feed.unread_comments_count).to eq 2
      end
    end
  end
end
