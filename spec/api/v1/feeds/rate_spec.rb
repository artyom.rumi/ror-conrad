require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feeds/Rate" do
  include_context :user_signed_in

  post "/api/v1/feeds/:feed_id/rate" do

    puts("^^^^^^^^^^^^^^^^^^^ this is the rate post api start point ^^^^^^^^^^^^^^^^^^^^^^^^")
    let(:feed) { create :feed }

    parameter :feed_id, "Feed Id for rate", required: true
    parameter :rate, "Rate rate", required: true

    let(:feed_id) { feed.id }
    let(:rate) { "Rate rate" }

    it_behaves_rate "v1 user auth token requiring endpoint"

    example_request "Create rate" do
      expect(response_status).to eq(201)

      expect(json_response_body).to be_a_feed_representation(feed.reload, current_user)
    end
  end
end
