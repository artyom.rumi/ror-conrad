require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feeds/Reposts" do
  post "/api/v1/feeds/:feed_id/reposts" do
    include_context :user_signed_in

    let(:another_user) { create :user }
    let(:file) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }
    let(:feed) { create :feed, user: another_user, text: "some text", image: file }

    parameter :feed_id, "Feed Id", required: true

    let(:feed_id) { feed.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Create repost feed" do
      expect(response_status).to eq(201)

      reposted_feed = current_user.feeds.find(json_response_body["id"])

      expect(reposted_feed.reload.image).to be_attached
      expect(json_response_body).to be_a_feed_representation(reposted_feed, current_user)
    end
  end
end
