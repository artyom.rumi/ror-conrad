require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feeds/Chats" do
  include_context :user_signed_in

  post "/api/v1/feeds/:feed_id/chats" do
    before { stub_pubnub_api }

    parameter :feed_id, "Feed Id for chat", required: true
    let(:feed) { create :feed }
    let(:feed_id) { feed.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "create chat" do
      expect(response_status).to eq(201)

      chat = Chat.find(json_response_body["id"])
      expect(json_response_body).to be_a_chat_representation(chat, current_user)
    end

    context "when feed author and current user one and the same user" do
      let!(:feed) { create :feed, user: current_user }

      example_request "it cannot be created" do
        expect(response_status).to eq(422)
      end
    end
  end
end
