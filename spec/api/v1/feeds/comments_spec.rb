require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feeds/Comments" do
  include_context :user_signed_in

  post "/api/v1/feeds/:feed_id/comments" do
    let(:feed) { create :feed, user: current_user }

    parameter :feed_id, "Feed Id for comments", required: true
    parameter :text, "Comment text", required: true

    let(:feed_id) { feed.id }
    let(:text) { "Some text" }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Create comment" do
      expect(response_status).to eq(201)

      comment = current_user.comments.find(json_response_body.first["id"])
      expect(json_response_body.first).to be_a_comment_representation(comment, current_user)
    end
  end

  get "/api/v1/feeds/:feed_id/comments" do
    parameter :feed_id, "Feed Id for comments", required: true
    let(:feed_id) { feed.id }

    let(:feed) { create :feed }
    let!(:first_comment) { create :comment, feed: feed, created_at: Time.current }
    let!(:second_comment) { create :comment, feed: feed, created_at: Time.current - 1.day }
    let!(:chat) { create :chat, owner: current_user, comment: first_comment }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of comments" do
      expect(response_status).to eq(200)

      expect(json_response_body.first).to be_a_comment_representation(first_comment, current_user)
      expect(json_response_body.second).to be_a_comment_representation(second_comment, current_user)
    end
  end

  put "/api/v1/feeds/:feed_id/comments/:comment_id" do
    parameter :feed_id, "Feed Id for comments", required: true
    parameter :text, "Text comment", required: true
    parameter :comment_id, "Id for comment", required: true

    let(:feed) { create :feed }
    let(:feed_id) { feed.id }
    let(:comment) { create :comment, feed: feed, created_at: Time.current, user: current_user }
    let(:text) { "Updated text" }
    let(:comment_id) { comment.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Update comment" do
      expect(response_status).to eq(200)

      comment = current_user.comments.find(json_response_body["id"])

      expect(comment.text).to eq(text)
      expect(json_response_body).to be_a_comment_representation(comment, current_user)
    end
  end

  delete "/api/v1/feeds/:feed_id/comments/:comment_id" do
    parameter :feed_id, "Feed Id for comments", required: true
    parameter :comment_id, "Id for comment", required: true

    it_behaves_like "v1 user auth token requiring endpoint"

    let(:feed) { create :feed }
    let(:first_comment) { create :comment, feed: feed, created_at: Time.current, user: current_user }
    let!(:second_comment) { create :comment, feed: feed, created_at: Time.current, user: current_user }
    let(:feed_id) { feed.id }
    let(:comment_id) { first_comment.id }

    example_request "Delete comment" do
      expect(response_status).to eq(200)

      comment = current_user.comments.find(json_response_body.first["id"])

      expect(json_response_body.first).to be_a_comment_representation(comment, current_user)
    end
  end
end
