require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feeds/Likes" do
  include_context :user_signed_in

  post "/api/v1/feeds/:feed_id/likes" do
    let(:feed) { create :feed }

    parameter :feed_id, "Feed Id for like", required: true

    let(:feed_id) { feed.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Create like" do
      expect(response_status).to eq(201)

      expect(json_response_body).to be_a_feed_representation(feed.reload, current_user)
    end
  end

  delete "/api/v1/feeds/:feed_id/likes" do
    let(:feed) { create :feed }
    let(:feed_id) { feed.id }

    parameter :feed_id, "Feed Id for delete like", required: true

    let!(:like) { create :like, user: current_user, feed: feed }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Delete like" do
      expect(response_status).to eq(200)

      feed = Feed.find(json_response_body["id"])
      expect(json_response_body).to be_a_feed_representation(feed, current_user)
    end
  end
end
