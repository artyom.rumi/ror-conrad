require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Feedback" do
  include_context :user_signed_in

  post "/api/v1/feedbacks" do
    parameter :text, "Feedback text", required: true
    let(:text) { Faker::Lorem.sentence }

    example_request "Send feedback" do
      expect(response_status).to eq(204)
    end
  end
end
