require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Prop deliveries" do
  include_context :user_signed_in
  include_context :apnotic_connection

  post "/api/v1/prop_deliveries" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :recipient_id, "User to recieve the prop", required: true
    parameter :prop_id, "Prop to send to user", required: true
    parameter :boost_type, "Type of boost, secret (default) or friend"

    let(:recipient) { create :user }
    let(:prop) { create :prop }

    let(:recipient_id) { recipient.id }
    let(:prop_id) { prop.id }

    example_request "Send prop to the user" do
      expect(response_status).to eq(201)

      prop_delivery = PropDelivery.find(json_response_body["id"])
      expect(json_response_body).to be_a_prop_delivery_representation(prop_delivery)
      expect(prop_delivery.sender).to eq(current_user)
      expect(prop_delivery.recipient.pokes_count).to eq(1)
      expect(prop_delivery.recipient.top_pokes).to eq([prop])
    end

    context "when send friend boost" do
      let(:boost_type) { "friend" }

      example_request "refresh friend boost timestamp" do
        recipient.reload
        prop_delivery = PropDelivery.find(json_response_body["id"])
        expect(recipient.friend_boost_at).to eq(prop_delivery.created_at)
      end
    end

    context "when send secret boost" do
      let(:boost_type) { "secret" }

      example_request "does not refresh friend boost timestamp" do
        recipient.reload
        expect(recipient.friend_boost_at).to be_nil
      end
    end
  end
end
