require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Contacts" do
  include_context :user_signed_in

  let(:user) { create :user }
  let(:second_user) { create :user }

  let(:contacts_ids) { [user.id, second_user.id] }
  let(:contacts) { User.where(id: contacts_ids) }

  post "/api/v1/contacts" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :phone_numbers, "Phone numbers", required: true
    parameter :filter_schoolmates, "Filter out schoolmates"

    let(:phone_numbers) { contacts.pluck(:phone_number) }
    let(:filter_schoolmates) { nil }

    example_request "List of users from contacts" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_schoolmate_list_representation(contacts, current_user)
    end

    context "when schoolmates are filtered out with param" do
      let!(:user) { create :user, school: current_user.school }
      let(:contacts_ids) { [second_user.id] }

      let(:phone_numbers) { [user.phone_number, second_user.phone_number] }
      let(:filter_schoolmates) { true }

      example_request "List of user from contacts without schoolsmates" do
        expect(response_status).to eq(200)
        expect(json_response_body).to be_a_schoolmate_list_representation(contacts, current_user)
      end
    end

    context "when current user's phone number is passed" do
      let(:phone_numbers) do
        contacts.pluck(:phone_number) + [current_user.phone_number]
      end

      example_request "List of users from contacts except myself" do
        expect(response_status).to eq(200)
        expect(json_response_body).to be_a_schoolmate_list_representation(contacts, current_user)
      end
    end
  end
end
