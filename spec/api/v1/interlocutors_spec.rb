require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Interlocutors" do
  include_context :user_signed_in

  let(:brad) { create :user, first_name: "Brad", last_name: "Pitt" }
  let(:jorge) { create :user, first_name: "George", last_name: "Clooney" }
  let(:matt) { create :user, first_name: "Matt", last_name: "Damon" }

  before do
    create :friendship, first_user: current_user, second_user: brad, expires_at: Time.current + 1.day
    create :friendship, first_user: jorge, second_user: current_user, expires_at: Time.current + 1.day
    create :friendship, first_user: current_user, second_user: matt, expires_at: Time.current + 1.day
  end

  get "/api/v1/interlocutors" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"
    parameter :keywords, "Full name search keywords"

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of friends" do
      expect(response_status).to eq(200)

      expect(json_response_body.first).to be_a_schoolmate_representation(brad, current_user)
      expect(json_response_body.second).to be_a_schoolmate_representation(jorge, current_user)
      expect(json_response_body.third).to be_a_schoolmate_representation(matt, current_user)
    end

    context "when pagination params are specificated" do
      let(:page) { 2 }
      let(:per_page) { 2 }

      example_request "List of friends on another page" do
        expect(response_status).to eq(200)

        expect(json_response_body.first).to be_a_schoolmate_representation(matt, current_user)
      end
    end

    context "when keywords param is specified" do
      let(:keywords) { "Brad" }

      example_request "List of interlocutors matched by keywords" do
        expect(response_status).to eq(200)

        expect(json_response_body.size).to eq(1)
        expect(json_response_body.first).to be_a_schoolmate_representation(brad, current_user)
      end
    end
  end
end
