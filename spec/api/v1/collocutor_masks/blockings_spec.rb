require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/CollocutorMasks/Blocking" do
  include_context :user_signed_in

  let(:collocutor_mask) { create :collocutor_mask, user: current_user }
  let(:collocutor_mask_id) { collocutor_mask.id }

  post "/api/v1/collocutor_masks/:collocutor_mask_id/blocking" do
    parameter :collocutor_mask_id, "ID of the collocutor mask to block", required: true

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "create new blocking to specific collocutor mask" do
      expect(response_status).to eq(200)

      blocking = Blocking.find(json_response_body["id"])
      expect(json_response_body).to be_a_blocking_representation(blocking)
    end
  end

  delete "/api/v1/collocutor_masks/:collocutor_mask_id/blocking" do
    parameter :collocutor_mask_id, "ID of the collocutor mask to unblock", required: true

    before do
      create :blocking, :for_collocutor_mask, blockable: collocutor_mask, user: current_user
    end

    context "when blocking with the collocutor_mask_id exists" do
      example "destroy blocking" do
        expect { do_request }.to change { current_user.blockings.for_collocutor_mask.count }.by(-1)
        expect(response_status).to eq(204)
      end
    end

    context "when blocking with the collocutor_mask_id does not exist" do
      let(:collocutor_mask_id) { -1 }

      example "not destroy blocking that not existing", document: false do
        do_request

        expect(response_status).to eq(404)
      end
    end
  end
end
