require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Random friends" do
  include_context :user_signed_in

  before do
    create_list :subscription, 5, subscriber: current_user
  end

  get "/api/v1/random_friends" do
    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of random friends" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_schoolmate_list_representation(current_user.subscribed_to_users, current_user)
    end
  end
end
