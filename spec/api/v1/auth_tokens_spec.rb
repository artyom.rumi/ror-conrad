require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Auth tokens" do
  include_context :json_headers

  post "/api/v1/auth_tokens" do
    parameter :phone_number, "Phone number", required: true
    parameter :verification_code, "Verification code received in SMS message", required: true

    let(:user) { create :user, phone_number: phone_number }
    let(:phone_number) { "+12345" }

    before do
      create :user, phone_number: "0098"
      create :verification_code, code: "1234", phone_number: phone_number, user: user
    end

    context "with valid verification code" do
      let(:verification_code) { "1234" }

      example_request "Create an auth token" do
        expect(response_status).to eq(201)

        expect(json_response_body).to be_an_auth_token_representation(user.auth_token.reload)
      end
    end

    example_request "Invalid or expired verification code" do
      expect(response_status).to eq(422)
      expect(json_response_body.symbolize_keys)
        .to eq(error: "Verification code is invalid")
    end
  end

  delete "/api/v1/logout" do
    include_context :user_signed_in

    before { stub_pubnub_api }

    example_request "Destroy auth token" do
      expect(current_user.reload.auth_token).to eq(nil)
      expect(response_status).to eq(204)
    end
  end
end
