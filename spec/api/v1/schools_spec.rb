require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Schools" do
  let(:school) { create :school, name: "Judges Academy", latitude: 1, longitude: 1 }
  let(:another_school) { create :school, name: "School of Information Systems", latitude: 1.02, longitude: 1.02 }
  let(:third_school) { create :school, name: "School of Information Technologies", latitude: 1.1, longitude: 1.1 }

  let!(:first_grade) { create :grade, school: school, value: 11 }
  let!(:second_grade) { create :grade, school: another_school, value: 10 }
  let!(:third_grade) { create :grade, school: third_school, value: 11 }

  parameter :year_of_ending, "Year of ending", required: true
  parameter :latitude, "User latitude"
  parameter :longitude, "User longitude"
  parameter :keywords, "School name keywords"
  parameter :page, "Page number"
  parameter :per_page, "Per page"

  let!(:year_of_ending) { 2021 }

  get "/api/v1/schools" do
    it_behaves_like "v1 user auth token requiring endpoint"

    context "when the token associated with the user" do
      include_context :user_signed_in

      example_request "List of schools" do
        expect(response_status).to eq(200)
        expect(json_response_body.size).to eq(2)
        expect(json_response_body.first).to be_a_school_representation(school)
        expect(json_response_body.last).to be_a_school_representation(third_school)
      end

      context "when page specified" do
        let(:page) { 2 }
        let(:per_page) { 2 }

        let!(:language_school) do
          create :school, name: "School of Language", grades_attributes: [{ value: 11 }]
        end

        example_request "List of schools on another page" do
          expect(response_status).to eq(200)

          expect(json_response_body.size).to eq(1)
          expect(json_response_body.first).to be_a_school_representation(language_school)
        end
      end

      context "when latitude and longitude specified" do
        let(:latitude) { 1.1 }
        let(:longitude) { 1.1 }

        example_request "List of schools close to a specific place" do
          expect(response_status).to eq(200)
          expect(json_response_body.size).to eq(2)
          expect(json_response_body.first).to be_a_school_representation(third_school)
          expect(json_response_body.last).to be_a_school_representation(school)
        end
      end

      context "when keywords specified" do
        let(:keywords) { "info" }

        example_request "List of schools found by name" do
          expect(response_status).to eq(200)
          expect(json_response_body.size).to eq(1)
          expect(json_response_body.first).to be_a_school_representation(third_school)
        end
      end
    end

    context "when the token isn't associated with a user" do
      include_context :guest_signed_in

      example_request "List of schools" do
        expect(response_status).to eq(200)
        expect(json_response_body.size).to eq(2)
        expect(json_response_body.first).to be_a_school_representation(school)
        expect(json_response_body.last).to be_a_school_representation(third_school)
      end
    end
  end
end
