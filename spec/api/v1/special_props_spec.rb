require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Special props from Emily" do
  include_context :user_signed_in

  get "/api/v1/special_props" do
    it_behaves_like "v1 user auth token requiring endpoint"

    before do
      create_list :prop, 3, :special
      create :prop
    end

    example_request "list of special props" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_prop_list_representation(Prop.special)
    end
  end
end
