require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Followers" do
  include_context :user_signed_in

  let(:school) { current_user.school }
  let(:schoolmate) { create :user, school: school }
  let(:second_schoolmate) { create :user, school: school }

  before do
    create :subscription, target: current_user, subscriber: schoolmate
    create :subscription, target: current_user, subscriber: second_schoolmate
  end

  get "/api/v1/followers" do
    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of followers" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_schoolmate_list_representation(current_user.subscribed_by_users, current_user)
    end
  end
end
