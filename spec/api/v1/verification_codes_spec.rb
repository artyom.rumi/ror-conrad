require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Verification Codes" do
  header "Accept", "application/json"
  header "Content-type", "application/json"

  post "/api/v1/verification_codes" do
    before { stub_twilio_api }

    parameter :phone_number, "Phone number to send SMS with the verification code to", required: true

    let(:phone_number) { "+1" + rand.to_s[2..11] }
    let(:verification_code) { VerificationCode.last }

    example_request "Send verification code to a phone" do
      expect(response_status).to eq(201)

      expect(json_response_body).to be_a_verification_code_representation(verification_code)
      expect(verification_code.code).not_to be_nil
    end
  end
end
