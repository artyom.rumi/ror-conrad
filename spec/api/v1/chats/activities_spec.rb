require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Chats/Activities" do
  include_context :user_signed_in

  post "/api/v1/chats/:id/activities" do
    parameter :id, "Chat Id", required: true

    let(:id) { chat.id }

    context "with id of the chat where current_user exists" do
      let(:chat) { create :chat, users: [current_user], owner: current_user }

      example "update chat updated_at for existing chat" do
        expect { do_request }.to change { chat.reload.updated_at }
        expect(response_status).to eq(200)
      end
    end

    context "with id of the chat where current_user does not exists" do
      let(:chat) { create :chat }

      example "update chat updated_at for not existing chat" do
        expect { do_request }.not_to change { chat.reload.updated_at }
        expect(response_status).to eq(404)
      end
    end

    context "when chat ID that does not exist" do
      let(:id) { -1 }

      example "update chat updated_at", document: false do
        do_request

        expect(response_status).to eq(404)
      end
    end
  end
end
