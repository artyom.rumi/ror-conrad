require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Chats/Deanonymizations" do
  include_context :user_signed_in

  let(:another_user) { create :user }

  let(:chat) { create :chat, :incognito, users: [current_user, another_user] }

  let(:pubnub_client) { instance_double "PubnubAdapter" }

  before do
    allow(PubNubAdapter).to receive(:new) { pubnub_client }
    allow(pubnub_client).to receive(:send_message_to_channel)
  end

  post "/api/v1/chats/:chat_id/deanonymizations" do
    parameter :chat_id, "ID of chat where user wants to be deanonymized", required: true

    let(:chat_id) { chat.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Create deanonymization" do
      expect(response_status).to eq(201)

      deanonymization = Deanonymization.find(json_response_body["id"])
      expect(json_response_body).to be_a_deanonymization_representation(deanonymization)
    end
  end

  get "/api/v1/chats/:chat_id/deanonymizations" do
    parameter :chat_id, "ID of chat where user wants to check deanonymizations", required: true

    let(:chat_id) { chat.id }

    let!(:first_deanonymization) { create :deanonymization, chat: chat, user: current_user }
    let!(:second_deanonymization) { create :deanonymization, chat: chat, user: another_user }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List deanonymizations for specified chat" do
      expect(response_status).to eq(200)
      expect(json_response_body.size).to eq(2)

      expect(json_response_body.first).to be_a_deanonymization_representation(first_deanonymization)
      expect(json_response_body.last).to be_a_deanonymization_representation(second_deanonymization)
    end
  end

  delete "/api/v1/chats/:chat_id/deanonymizations" do
    parameter :chat_id, "ID of chat where user wants to cancel deanonymization", required: true

    let(:chat_id) { chat.id }

    let!(:deanonymization) { create :deanonymization, chat: chat, user: current_user }

    context "when deanonymization with the chat_id exists" do
      example "destroy deanonymization" do
        expect { do_request }.to change { chat.deanonymizations.count }.by(-1)
        expect(response_status).to eq(204)
      end
    end

    context "when deanonymization with the chat_id does not exist" do
      let(:chat_id) { -1 }

      example "not destroy deanonymization that not existing", document: false do
        do_request

        expect(response_status).to eq(404)
      end
    end
  end
end
