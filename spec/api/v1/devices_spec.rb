require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Device" do
  include_context :user_signed_in
  before { stub_pubnub_api }

  let!(:device) { current_user.device }

  patch "/api/v1/device" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :push_token, "FCM registration token"
    parameter :phone_number, "Phone number"

    let(:push_token) { "bLa1Bla2bLA3:rEg1sTrAt10nT0k3nfr0mfcmS3rvic3" }
    let(:phone_number) { "+88005553535" }

    example_request "update user device" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_device_representation(Device.first)
    end
  end
end
