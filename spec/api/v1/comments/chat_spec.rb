require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Comments/Chats" do
  include_context :user_signed_in

  post "/api/v1/comments/:comment_id/chats" do
    before { stub_pubnub_api }

    parameter :comment_id, "Feed Id for chat", required: true
    let(:feed) { create :feed }
    let(:comment) { create :comment, feed: feed }
    let(:comment_id) { comment.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "create chat" do
      expect(response_status).to eq(201)

      chat = Chat.find(json_response_body["id"])
      expect(json_response_body).to be_a_chat_representation(chat, current_user)
    end
  end
end
