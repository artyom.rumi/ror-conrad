require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/PubNub key set" do
  include_context :user_signed_in

  get "/api/v1/pubnub_key_set" do
    it_behaves_like "v1 user auth token requiring endpoint"

    before do
      allow(ENV).to receive(:fetch).and_call_original
      allow(ENV).to receive(:fetch).with("PUBNUB_PUBLISH_KEY").and_return("hakuna")
      allow(ENV).to receive(:fetch).with("PUBNUB_SUBSCRIBE_KEY").and_return("matata")
    end

    example_request "get actual pubnub key set" do
      expect(response_status).to eq(200)
      expect(json_response_body["pubnub_publish_key"]).to eq "hakuna"
      expect(json_response_body["pubnub_subscribe_key"]).to eq "matata"
    end
  end
end
