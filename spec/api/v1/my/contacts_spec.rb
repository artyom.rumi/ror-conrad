require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/Contacts" do
  include_context :user_signed_in

  post "/api/v1/my/contacts" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :phone_numbers, "Phone numbers from user contacts list", required: true

    let(:phone_numbers) do
      [{ name: "John", phone_number: "1234567890" }, { name: "Piter", phone_number: "1234567891" }]
    end

    example_request "Synchronize contacts list" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_contact_list_representation(current_user.contacts)
    end

    let(:school_mate) { create :user, school: current_user.school }
    let(:stranger) { create :user }
    example "Synchronize community for new contacts", :aggregate_failures do
      current_user.incoming_subscriptions.destroy_all
      current_user.outcoming_subscriptions.destroy_all
      expect { do_request(phone_numbers: [{ name: stranger.first_name, phone_number: stranger.phone_number }]) }
        .to change { current_user.outcoming_subscriptions.count }
        .by(1)
        .and change { current_user.incoming_subscriptions.count }
        .by(0)
      current_user.outcoming_subscriptions.reload
      expect(current_user.outcoming_subscriptions.map(&:target)).to match_array([stranger])
    end
  end
end
