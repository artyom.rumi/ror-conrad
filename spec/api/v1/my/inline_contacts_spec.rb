require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/InlineContacts" do
  include_context :user_signed_in

  before do
    create :contact, user: current_user, phone_number: "+11234567890"
    create :contact, user: current_user, phone_number: "+11234567891"
    create :contact, user: current_user, phone_number: "+11234567892"
  end

  get "/api/v1/my/inline_contacts" do
    let!(:first_user) { create :user, phone_number: "+11234567890", first_name: "1" }
    let!(:second_user) { create :user, phone_number: "+11234567891", first_name: "2" }

    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of inline contacts" do
      expect(response_status).to eq(200)

      expect(json_response_body["users"].first).to be_a_schoolmate_representation(first_user, current_user)
      expect(json_response_body["users"].second).to be_a_schoolmate_representation(second_user, current_user)
    end
  end
end
