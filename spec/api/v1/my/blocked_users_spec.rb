require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/Blocked users" do
  include_context :user_signed_in

  get "/api/v1/my/blocked_users" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:blocked_users_ids) { current_user.blockings.for_user.pluck(:blockable_id) }
    let(:blocked_users) { User.where(id: blocked_users_ids) }

    before do
      create_list :blocking, 2, :for_user, user: current_user
    end

    example_request "List of blocked users" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_schoolmate_list_representation(blocked_users, current_user)
    end
  end
end
