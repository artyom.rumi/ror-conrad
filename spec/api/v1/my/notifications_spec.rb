require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/Notifications" do
  include_context :user_signed_in

  let(:avatar) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }
  let(:image) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }
  let(:another_user) { create :user }
  let(:friendship) { create :friendship, first_user: current_user, second_user: another_user }
  let(:feed) { create :feed, user: current_user, image: image }

  let(:my_like) { create :like, user: current_user, feed: feed }
  let(:like) { create :like, user: another_user, feed: feed }
  let(:comment) { create :comment, user: another_user, feed: feed }
  let(:repost) { create :repost, user: another_user, source_feed: feed }

  let!(:new_like_notification) do
    create :notification, notifiable: like, user: another_user, action_name: "new_like"
  end

  let!(:my_like_notification) do
    create :notification, notifiable: my_like, user: current_user, action_name: "new_like", status: "Read"
  end

  let!(:new_comment_notification) do
    create :notification, notifiable: comment, user: current_user, action_name: "new_comment"
  end

  let!(:new_repost_notification) do
    create :notification, notifiable: repost, user: current_user, action_name: "new_repost"
  end

  get "/api/v1/my/notifications" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let!(:new_friend_notification) do
      create :notification, notifiable: friendship, user: current_user, action_name: "new_friend"
    end

    let!(:expired_friend_notification_1) do
      create :notification, notifiable: friendship, user: current_user, action_name: "72h_expired_friend"
    end

    let!(:expired_friend_notification_2) do
      create :notification, notifiable: friendship, user: current_user, action_name: "12h_expired_friend"
    end

    shared_context :expect_list_of_my_notifications do
      example_request "List of my notifications" do
        notifications = Notification.ungrouped.actual.where(user: current_user)
        ungrouped_notifications = json_response_body.reject do |notification|
          notification["payload"]["grouped"]
        end

        expect(response_status).to eq(200)
        expect(notifications).to be_a_notification_list_representation(ungrouped_notifications)
      end
    end

    context "when notifiable objects weren't deleted" do
      include_context :expect_list_of_my_notifications
    end

    context "when notifiable objects were deleted" do
      before do
        comment.destroy
        like.destroy
        repost.destroy
      end

      include_context :expect_list_of_my_notifications
    end
  end

  patch "/api/v1/my/notifications/:id" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :id, "Notification Id", required: true
    let(:id) { new_comment_notification.id }

    example_request "Mark notification as read" do
      expect(response_status).to eq(200)
      expect(json_response_body.first["status"]).to eq("Read")
    end
  end

  patch "/api/v1/my/notifications" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :ids, "Notification Ids", required: true
    let(:ids) { [new_comment_notification.id, new_repost_notification.id] }

    example_request "Mark notifications as read" do
      expect(response_status).to eq(200)

      expect(json_response_body.first["status"]).to eq("Read")
      expect(json_response_body.second["status"]).to eq("Read")
    end
  end
end
