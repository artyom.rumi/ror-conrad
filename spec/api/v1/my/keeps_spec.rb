require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/Keeps" do
  include_context :user_signed_in

  post "/api/v1/my/keeps" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :user_id, "Keep to user"

    let!(:friendship) { create :friendship, first_user: current_user, second_user: friend, second_user_keep: true }
    let(:friend) { create :user }
    let(:user_id) { friend.id }

    example_request "Create keep" do
      expect(response_status).to eq(200)
      expect(json_response_body["id"]).to eq friend.id
    end
  end
end
