require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/OutContacts" do
  include_context :user_signed_in

  get "/api/v1/my/outline_contacts" do
    let!(:user) { create :user, phone_number: "+11234567890" }
    let!(:first_contact) { create :contact, user: current_user, phone_number: "+11234567890" }
    let!(:second_contact) { create :contact, user: current_user, phone_number: "+11234567891" }
    let!(:third_contact) { create :contact, user: current_user, phone_number: "+11234567892" }

    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of outline contacts" do
      expect(response_status).to eq(200)

      expect(json_response_body["users"].first).to be_a_outline_contact_representation(second_contact)
      expect(json_response_body["users"].second).to be_a_outline_contact_representation(third_contact)
    end
  end
end
