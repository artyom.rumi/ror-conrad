require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/Discoveries" do
  include_context :user_signed_in

  get "/api/v1/my/discoveries" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    let!(:friend) { create :user }
    let(:unknown_user) { create :user }
    let(:another_friend_of_friend) { create :user }
    let(:friend_of_friend) { create :user }
    let(:degree_3) { create :user }
    let(:deeply_degree) { create :user }

    let!(:friend_feed) { create :feed, user: friend, created_at: 1.day.ago }
    let!(:current_user_feed) { create :feed, user: current_user, created_at: 3.days.ago }
    let!(:another_friend_of_friend_feed) { create :feed, user: another_friend_of_friend, created_at: 2.days.ago }
    let!(:friend_of_friend_feed) { create :feed, user: friend_of_friend, created_at: 4.days.ago }
    let!(:degree_3_feed) { create :feed, user: degree_3, created_at: 6.days.ago }
    let!(:deeply_degree_feed) { create :feed, user: deeply_degree, created_at: 3.days.ago }

    before do
      create :friendship, first_user: current_user, second_user: friend
      create :friendship, first_user: friend, second_user: friend_of_friend
      create :friendship, first_user: friend, second_user: another_friend_of_friend
      create :friendship, first_user: another_friend_of_friend, second_user: degree_3
      create :friendship, first_user: degree_3, second_user: deeply_degree
    end

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of discoveries feeds" do
      feed_ids = json_response_body.map { |feed| feed["id"] }

      expect(response_status).to eq(200)

      expect(feed_ids).not_to include(friend_feed.id)
      expect(feed_ids).not_to include(current_user_feed.id)
      expect(feed_ids).not_to include(deeply_degree_feed.id)

      expect(json_response_body.first).to be_a_feed_representation(another_friend_of_friend_feed, current_user)
      expect(json_response_body.second).to be_a_feed_representation(friend_of_friend_feed, current_user)
      expect(json_response_body.third).to be_a_feed_representation(degree_3_feed, current_user)
    end
  end
end
