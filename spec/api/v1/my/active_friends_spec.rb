require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/ActiveFriends" do
  include_context :user_signed_in

  let(:first_user) { create :user, phone_number: "+11234567890" }
  let(:second_user) { create :user, phone_number: "+11234567891" }

  before do
    create :friendship, first_user: current_user, second_user: first_user, expires_at: Time.current + 5.days
    create :friendship, first_user: current_user, second_user: second_user, expires_at: Time.current - 50.days
  end

  get "/api/v1/my/active_friends" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of active friends" do
      expect(response_status).to eq(200)

      expect(json_response_body["users"].first).to be_a_friend_representation(first_user, current_user)
    end
  end
end
