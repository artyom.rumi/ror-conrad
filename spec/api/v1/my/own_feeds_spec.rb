require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/OwnFeeds" do
  include_context :user_signed_in

  get "/api/v1/my/own_feeds" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    let!(:friend) { create :user }
    let!(:first_feed) { create :feed, user: friend, created_at: 1.day.ago }
    let!(:second_feed) { create :feed, user: current_user, created_at: 3.days.ago }
    let!(:third_feed) { create :feed, user: current_user, created_at: 2.days.ago }
    let!(:fourth_feed) { create :feed, user: current_user, created_at: 10.days.ago }
    let!(:incognito_feed) { create :feed, user: current_user, incognito: true }

    before do
      create :friendship, first_user: current_user, second_user: friend
    end

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of feeds" do
      expect(response_status).to eq(200)

      expect(json_response_body.size).to eq(2)

      expect(json_response_body.first).to be_a_feed_representation(third_feed, current_user)
      expect(json_response_body.second).to be_a_feed_representation(second_feed, current_user)
    end

    context "when incognito parameter is true" do
      parameter :incognito, "Incognito status"
      let(:incognito) { true }

      example_request "user's anonymous feeds" do
        expect(response_status).to eq(200)

        expect(json_response_body.size).to eq(1)
        expect(json_response_body.first).to be_a_feed_representation(incognito_feed, current_user)
      end
    end
  end
end
