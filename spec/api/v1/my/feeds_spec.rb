require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/Feeds" do
  include_context :user_signed_in

  post "/api/v1/my/feeds" do
    it_behaves_like "v1 user auth token requiring endpoint"

    context "when feed with image" do
      header "Content-type", "multipart/form-data"

      parameter :text, "Feed text"
      parameter :image, "Feed image", required: true, "Type" => "Multipart/Form-data"

      let!(:image) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }
      let!(:text) { "Some text" }
      let(:raw_post) do
        params
      end

      example_request "Create feed" do
        feed = current_user.feeds.find(json_response_body["id"])

        expect(response_status).to eq(201)
        expect(json_response_body).to be_a_feed_representation(feed, current_user)
      end
    end

    context "when feed is incognito" do
      parameter :text, "Feed text"
      parameter :link, "Feed with link"
      parameter :incognito, "Incognito status"

      let!(:text) { "Some text" }
      let!(:link) { "https://www.google.com" }
      let!(:incognito) { true }

      example_request "Create feed" do
        feed = current_user.feeds.find(json_response_body["id"])

        expect(response_status).to eq(201)
        expect(json_response_body).to be_a_feed_representation(feed, current_user)
      end
    end

    context "when feed with link" do
      parameter :text, "Feed text"
      parameter :link, "Feed with link"

      let!(:text) { "Some text" }
      let!(:link) { "https://www.google.com" }

      example_request "Create feed" do
        feed = current_user.feeds.find(json_response_body["id"])

        expect(response_status).to eq(201)
        expect(json_response_body).to be_a_feed_representation(feed, current_user)
      end
    end
  end

  put "/api/v1/my/feeds/:feed_id" do
    parameter :feed_id, "Feed Id", required: true
    parameter :incognito, "Feed status"

    let(:feed) { create :feed, user: current_user }
    let(:feed_id) { feed.id }
    let(:incognito) { "true" }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Update feed" do
      feed = current_user.feeds.find(json_response_body["id"])

      expect(response_status).to eq(200)
      expect(feed.incognito).to eq(true)
      expect(json_response_body).to be_a_feed_representation(feed, current_user)
    end
  end

  get "/api/v1/my/feeds" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    let!(:friend) { create :user }
    let(:second_friend) { create :user }
    let!(:first_feed) { create :feed, user: friend, created_at: 1.day.ago }
    let!(:second_feed) { create :feed, user: current_user, created_at: 3.days.ago }
    let!(:third_feed) { create :feed, user: friend, created_at: 2.days.ago }
    let!(:fourth_feed) { create :feed, user: friend, created_at: 10.days.ago }
    let!(:like) { create :like, user: friend, feed: second_feed }

    before do
      create :friendship, first_user: current_user, second_user: friend
      create :friendship, first_user: current_user, second_user: second_friend
      create :chat, feed: first_feed, owner: current_user
    end

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of feeds" do
      expect(response_status).to eq(200)
      expect(json_response_body.first).to be_a_feed_representation(first_feed, current_user)
      expect(json_response_body.second).to be_a_feed_representation(third_feed, current_user)
      expect(json_response_body.third).to be_a_feed_representation(second_feed, current_user)
    end

    context "with comment, like and repost from the same user" do
      let!(:comment) { create :comment, user: friend, feed: second_feed }
      let!(:repost_feed) { create :repost, source_feed: second_feed, user: friend }

      example_request "Count of engaged friends" do
        expect(response_status).to eq(200)
        expect(json_response_body.last["count_of_engaged_friends"]).to eq(1)
        expect(json_response_body.last).to be_a_feed_representation(second_feed, current_user)
      end
    end

    context "with comment and like from different users" do
      let!(:comment) { create :comment, user: second_friend, feed: second_feed }

      example_request "Count of engaged friends" do
        expect(response_status).to eq(200)
        expect(json_response_body.third["count_of_engaged_friends"]).to eq(2)
        expect(json_response_body.third).to be_a_feed_representation(second_feed, current_user)
      end
    end
  end

  delete "/api/v1/my/feeds/:feed_id" do
    parameter :feed_id, "Feed Id", required: true

    let(:feed_id) { feed.id }
    let!(:feed) { create :feed, user: current_user }

    context "when feed has no reposts" do
      example_request "Delete feed" do
        deleted_feed = Feed.where(id: feed.id)

        expect(deleted_feed).not_to exist
      end
    end

    context "when feed has reposts" do
      let(:another_user) { create :user }
      let!(:repost) { create :repost, source_feed: feed, user: another_user }
      let!(:repost_of_repost) { create :repost, source_feed: repost, user: current_user }

      example_request "Delete feed and its reposts" do
        deleted_repost = Repost.where(id: repost.id)
        deleted_repost_of_repost = Repost.where(id: repost_of_repost.id)

        expect(deleted_repost).not_to exist
        expect(deleted_repost_of_repost).not_to exist
      end
    end

    context "when exists another feeds" do
      context "when deleted feed is incognito"
      let!(:feed) { create :feed, user: current_user, incognito: true }
      let(:another_user) { create :user }
      let!(:second_feed) { create :feed, user: current_user, incognito: true }
      let!(:third_feed) { create :feed, user: current_user, incognito: false }
      let!(:fourth_feed) { create :feed, user: another_user, incognito: true }

      example_request "returns array of my incognito feeds" do
        expect(json_response_body.first).to be_a_feed_representation(second_feed, current_user)
      end

      example_request "not returns not incognito feeds" do
        expect(json_response_body.second).to be_nil
      end
    end
  end
end
