require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/my/Blocked collocutor masks" do
  include_context :user_signed_in

  get "/api/v1/my/blocked_collocutor_masks" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:collocutor_masks_ids) { current_user.blockings.for_collocutor_mask.pluck(:blockable_id) }
    let(:collocutor_masks) { CollocutorMask.where(id: collocutor_masks_ids) }

    before do
      create_list :blocking, 2, :for_collocutor_mask, user: current_user
    end

    example_request "List of collocutor masks" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_collocutor_mask_list_representation(collocutor_masks, current_user)
    end
  end
end
