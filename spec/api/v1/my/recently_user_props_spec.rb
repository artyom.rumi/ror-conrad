require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/my/Recently used Props" do
  include_context :user_signed_in

  def response_props_ids
    json_response_body.map { |prop| prop["id"] }
  end

  get "/api/v1/my/recently_used_props" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :limit, "Amount of props to be fetched"

    let!(:first_prop) { create :prop }
    let!(:second_prop) { create :prop }
    let!(:third_prop) { create :prop, :special }
    let!(:foreign_prop) { create :prop, :special }

    let!(:first_delivery) { create :prop_delivery, sender: current_user, prop: first_prop, created_at: 1.day.ago }
    let!(:second_delivery) { create :prop_delivery, sender: current_user, prop: second_prop, created_at: 1.minute.ago }
    let!(:third_delivery) { create :prop_delivery, sender: current_user, prop: third_prop, created_at: 2.days.ago }
    let!(:another_delivery) { create :prop_delivery, sender: current_user, prop: first_prop, created_at: 3.days.ago }
    let!(:foreign_delivery) { create :prop_delivery, prop: foreign_prop }

    let(:recently_used_props_ids) { [second_prop.id, first_prop.id, third_prop.id] }
    let(:recent_props) { Prop.where(id: recently_used_props_ids) }

    example_request "get full list of props" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_prop_list_representation(recent_props)
      expect(response_props_ids).to eq(recently_used_props_ids)
    end
  end
end
