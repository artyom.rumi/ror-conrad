require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/My/InvitedFriends" do
  include_context :user_signed_in

  let(:user) { create :user, phone_number: "1234567890" }

  before do
    create :friend_intention, user: current_user, target: user
  end

  get "/api/v1/my/invited_friends" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"

    let(:page) { 1 }
    let(:per_page) { 5 }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of active friends" do
      expect(response_status).to eq(200)

      expect(json_response_body["users"].first).to be_a_schoolmate_representation(user, current_user)
    end
  end
end
