require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Subscription" do
  include_context :user_signed_in

  post "/api/v1/subscriptions" do
    parameter :target_id, "User to subscribe", required: true
    let(:target) { create :user }
    let(:target_id) { target.id }

    example_request "Subscribe to the user" do
      expect(response_status).to eq(201)

      subscription = Subscription.find(json_response_body["id"])
      expect(json_response_body).to be_a_subscription_representation(subscription)
      expect(subscription.subscriber).to eq(current_user)
    end
  end

  post "/api/v1/subscriptions" do
    parameter :target_id, "User to subscribe", required: true

    let(:subscription) { create :subscription, subscriber: current_user }
    let(:target_id) { subscription.target_id }

    example_request "Subscribe to the same user again" do
      expect(response_status).to eq(409)
    end
  end

  delete "/api/v1/subscriptions" do
    parameter :id, "Id of the subscription to be deleted", required: true
    let(:subscription) { create :subscription, subscriber: current_user }
    let!(:id) { subscription.id }

    example "Destroy subscription by id" do
      expect { do_request }.to change(Subscription, :count).by(-1)
      expect(response_status).to eq(204)
    end
  end

  delete "/api/v1/subscriptions" do
    parameter :target_id, "Id of user to unsubscribe from", required: true
    let(:subscription) { create :subscription, subscriber: current_user }
    let!(:target_id) { subscription.target_id }

    example "Destroy subscription by id of user to unsubscribe from" do
      expect { do_request }.to change(Subscription, :count).by(-1)
      expect(response_status).to eq(204)
    end
  end
end
