require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/User" do
  include_context :json_headers
  include_context :apnotic_connection

  let(:attributes) { attributes_for :user }
  let(:first_name) { attributes[:first_name] }
  let(:last_name) { attributes[:last_name] }
  let(:age) { attributes[:age] }
  let(:grade) { attributes[:grade] }
  let(:gender) { attributes[:gender] }
  let(:bio) { attributes[:bio] }
  let(:matching_enabled) { attributes[:matching_enabled] }
  let(:school) { create :school }
  let(:school_id) { school.id }
  let(:prop_request_notification_enabled) { attributes[:prop_request_notification_enabled] }
  let(:receiving_prop_notification_enabled) { attributes[:receiving_prop_notification_enabled] }
  let(:chat_messages_notification_enabled) { attributes[:chat_messages_notification_enabled] }
  let(:new_friends_notification_enabled) { attributes[:new_friends_notification_enabled] }
  let(:new_users_notification_enabled) { attributes[:new_users_notification_enabled] }

  post "/api/v1/users" do
    parameter :first_name, "User first name", required: true
    parameter :last_name, "User last name", required: true
    parameter :age, "User age", required: true
    parameter :grade, "Year of graduation", required: true
    parameter :gender, "User gender", required: true
    parameter :school_id, "User school id", required: true
    parameter :bio, "User bio"
    parameter :matching_enabled, "Should system perform smart matching or not"
    parameter :phone_numbers, "Phone numbers"

    example_request "Attempt to Create a user without confirmed device" do
      expect(response_status).to eq(401)
    end

    context "with orphan auth token" do
      let(:auth_token) { create :auth_token, user: nil }

      before do
        header "X-Auth-Token", auth_token.value
      end

      example_request "Create a user" do
        expect(response_status).to eq(201)

        user = User.find(json_response_body["id"])
        expect(json_response_body).to be_a_user_representation(user)
        expect(auth_token.device.reload.user).to eq user
      end

      context "when phone_numbers are specified" do
        let(:friend) { create :user }

        let(:phone_numbers) { [friend.phone_number] }

        example_request "Create a user" do
          expect(response_status).to eq(201)

          user = User.find(json_response_body["id"])
          expect(json_response_body).to be_a_user_representation(user)
          expect(auth_token.device.reload.user).to eq user
          expect(user.subscribed_to_users).to eq [friend]
        end
      end
    end

    context "with user already created" do
      let(:auth_token) { create :auth_token }

      before do
        header "X-Auth-Token", auth_token.value
      end

      example_request "Attempt to create a user with existing user associated with provided auth token" do
        expect(response_status).to eq(422)

        expect(json_response_body["error"]).to eq "You already have a user associated with this phone number"
      end
    end

    context "with expired auth token" do
      let(:auth_token) { create :auth_token, user: nil, expires_at: 1.minute.ago }

      before do
        header "X-Auth-Token", auth_token.value
      end

      example_request "Attempt to create a user with expired token" do
        expect(response_status).to eq(401)
      end
    end
  end

  get "/api/v1/users/:id" do
    include_context :user_signed_in

    parameter :id, "Id of the user", required: true

    let(:another_user) { create :user }
    let(:id) { another_user.id }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Get user info" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_schoolmate_representation(another_user, current_user)
    end

    context "with top pokes hidden" do
      let(:another_user) { create :user, hide_top_pokes: true }

      example_request "no longer returns top pokes" do
        expect(json_response_body["top_pokes"]).to be_nil
      end
    end
  end

  patch "/api/v1/users/:id" do
    include_context :user_signed_in

    parameter :id, "Id of the user to be updated", required: true
    parameter :first_name, "User first name"
    parameter :last_name, "User last name"
    parameter :age, "User age"
    parameter :grade, "Year of graduation"
    parameter :gender, "User gender"
    parameter :school_id, "User school id"
    parameter :bio, "User bio"
    parameter :matching_enabled, "Should system perform smart matching or not"
    parameter :prop_request_notification_enabled, "Should system send notification of prop requests"
    parameter :receiving_prop_notification_enabled, "Should system send notification of received props"
    parameter :chat_messages_notification_enabled, "Should system send notification of chat messages"
    parameter :new_friends_notification_enabled, "Should system send notification of new friends"
    parameter :new_users_notification_enabled, "Should system send notification of new users"
    parameter :hide_top_pokes, "Whether top pokes are shown"

    let(:id) { current_user.id }
    let(:matching_enabled) { false }

    example_request "Update a user" do
      expect(response_status).to eq(200)

      user = User.find(json_response_body["id"])
      expect(json_response_body).to be_a_user_representation(user)
    end

    context "with school changed" do
      let(:new_school) { create :school }
      let(:new_school_mate) { create :user, school: new_school }

      example "subscribe to new school mates" do
        expect { do_request(school_id: new_school.id) }
          .to change { new_school_mate.incoming_subscriptions.count }
          .by(1)
          .and change { new_school_mate.outcoming_subscriptions.count }
          .by(1)
      end
    end
  end
end
