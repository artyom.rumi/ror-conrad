require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/requests PropDelivery" do
  include_context :user_signed_in
  include_context :apnotic_connection

  post "/api/v1/requests/:request_id/prop_deliveries" do
    parameter :request_id, "Request for props", required: true
    parameter :recipient_id, "User to recieve the prop", required: true
    parameter :prop_id, "Prop to send to user", required: true

    let(:recipient) { create :user }
    let(:prop) { create :prop }
    let(:request) { create :request, requester: current_user, receiver: recipient }

    let(:request_id) { request.id }
    let(:recipient_id) { recipient.id }
    let(:prop_id) { prop.id }

    example "Send prop to the user" do
      expect { do_request }.to change { request.reload.progress }.by(1)
      expect(response_status).to eq(201)

      prop_delivery = PropDelivery.find(json_response_body["id"])
      expect(json_response_body).to be_a_prop_delivery_representation(prop_delivery)
      expect(prop_delivery.sender).to eq(current_user)
    end
  end
end
