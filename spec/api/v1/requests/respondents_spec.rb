require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/requests Respondents" do
  include_context :user_signed_in

  let(:request) { create :request, requester: current_user }
  let(:request_respondents) { request.respondents }

  before do
    create_list :request_respondent, 5, request: request
  end

  get "/api/v1/requests/:request_id/respondents" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :request_id, "ID of request which was received by the respondents", required: true

    let(:request_id) { request.id }

    example_request "List of request respondents" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_schoolmate_list_representation(request_respondents, current_user)
    end
  end
end
