require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/requests Skip Delivery" do
  include_context :user_signed_in

  post "/api/v1/requests/:request_id/skip_deliveries" do
    parameter :request_id, "Request for props", required: true

    let(:request) { create :request, requester: current_user }
    let(:request_id) { request.id }

    example "Skip sending prop to the user" do
      expect { do_request }.to change { request.reload.progress }.by(1)

      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_request_representation(request)
    end
  end
end
