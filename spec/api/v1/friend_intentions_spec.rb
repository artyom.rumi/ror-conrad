require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/FriendIntentions" do
  include_context :user_signed_in

  let(:target_user) { create :user }

  post "/api/v1/friend_intentions" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :target_id, "User id for friend intentions", required: true

    let(:target_id) { target_user.id }

    example_request "Friend intention" do
      expect(response_status).to eq(201)

      friend_intention = current_user.friend_intentions.find(json_response_body["id"])
      expect(json_response_body).to be_a_friend_intention_representation(friend_intention)
    end
  end
end
