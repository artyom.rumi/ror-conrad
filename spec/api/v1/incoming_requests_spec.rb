require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Incoming requests" do
  include_context :user_signed_in

  get "/api/v1/incoming_requests" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:another_user) { create :user }

    before do
      create :request, receiver: current_user, progress: 5
      create_list :request, 2, receiver: current_user

      create_list :request, 3, receiver: another_user # some another requests to ensure current user won't get them
    end

    example_request "List of opened incoming requests" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_request_list_representation(current_user.incoming_requests.opened)
    end
  end

  get "/api/v1/incoming_requests/:id" do
    parameter :id, "Id of the incoming request", required: true
    let(:id) { incoming_request.id }

    let(:incoming_request) { create :request, receiver: current_user }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Specific request" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_request_representation(incoming_request)
    end
  end
end
