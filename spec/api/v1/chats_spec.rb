require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Chats" do
  include_context :user_signed_in

  get "/api/v1/chats" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:another_user) { create :user }
    let(:incognito_chat) { create :chat, :incognito, users: [current_user, another_user], owner: current_user }

    before do
      create :chat, users: [current_user, another_user], owner: current_user
      create :chat, users: [current_user, another_user], owner: current_user
      create :chat, users: [another_user], owner: current_user

      create :deanonymization, chat: incognito_chat, user: current_user
    end

    example_request "List of chats" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_chat_list_representation(current_user.chats, current_user)
    end
  end

  get "/api/v1/chats/:id" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :id, "Id of the chat", required: true

    let(:chat) { create :chat, owner: current_user }
    let(:id) { chat.id }

    example_request "Get chat info" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_chat_representation(chat, current_user)
    end
  end

  post "/api/v1/chats" do
    before { stub_pubnub_api }

    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :user_ids,
      "Users ids to chat with (current user id will be added implicitly if not present in array)",
      required: true
    parameter :name, "Chat name"

    let(:chat_friends) { create_list :user, 1 }
    let(:user_ids) { chat_friends.pluck(:id) }
    let(:name) { "Friends" }

    example_request "create chat" do
      expect(response_status).to eq(201)

      chat = Chat.find(json_response_body["id"])
      expect(json_response_body).to be_a_chat_representation(chat, current_user)
    end

    context "when chat already created" do
      let!(:chat) { create :chat, users: chat_friends + [current_user], owner: current_user }

      example_request "always create a new one regardless" do
        expect(response_status).to eq(201)

        expect(json_response_body).not_to be_a_chat_representation(chat, current_user)
      end
    end
  end
end
