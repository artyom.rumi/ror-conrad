require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Followees" do
  include_context :user_signed_in

  let(:school) { current_user.school }
  let(:brad) { create :user, school: school, first_name: "Brad", last_name: "Pitt" }
  let(:jorge) { create :user, first_name: "George", last_name: "Clooney" }
  let(:matt) { create :user, school: school, first_name: "Matt", last_name: "Damon" }
  let(:andy) { create :user, first_name: "Andy", last_name: "Garcia" }

  before do
    create :subscription, target: jorge, subscriber: current_user
    create :subscription, target: current_user, subscriber: jorge

    create :subscription, target: matt, subscriber: current_user
    create :subscription, target: current_user, subscriber: matt

    create :subscription, target: current_user, subscriber: andy
    create :subscription, target: brad, subscriber: current_user
  end

  get "/api/v1/followees" do
    parameter :page, "Page number"
    parameter :per_page, "Per page"
    parameter :keywords, "Full name search keywords"

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of followees" do
      expect(response_status).to eq(200)

      expect(json_response_body.first).to be_a_schoolmate_representation(brad, current_user)
      expect(json_response_body.second).to be_a_schoolmate_representation(jorge, current_user)
      expect(json_response_body.third).to be_a_schoolmate_representation(matt, current_user)
    end

    context "when pagination params are specified" do
      let(:page) { 2 }
      let(:per_page) { 2 }

      example_request "List of followees on another page" do
        expect(response_status).to eq(200)

        expect(json_response_body.first).to be_a_schoolmate_representation(matt, current_user)
      end
    end

    context "when keywords param is specified" do
      let(:keywords) { "Brad" }

      example_request "List of folowees matched by keywords" do
        expect(response_status).to eq(200)

        expect(json_response_body.size).to eq(1)
        expect(json_response_body.first).to be_a_schoolmate_representation(brad, current_user)
      end
    end
  end
end
