require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Prop deliveries from me" do
  include_context :user_signed_in

  get "/api/v1/users/:user_id/prop_deliveries_from_me" do
    parameter :user_id, "Id of the another user", required: true
    let!(:first_subscribed_user) { create :user }
    let!(:second_subscribed_user) { create :user }
    let!(:user_id) { first_subscribed_user.id }
    let!(:first_prop_delivery) { create :prop_delivery, sender: current_user, recipient: first_subscribed_user }
    let!(:second_prop_delivery) { create :prop_delivery, sender: current_user, recipient: second_subscribed_user }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "list of sent props" do
      expect(response_status).to eq(200)
      expect(json_response_body.size).to eq(1)
      expect(json_response_body.first).to be_a_sent_prop_representation(first_prop_delivery, current_user)
    end
  end
end
