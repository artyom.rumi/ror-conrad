require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/schoolmates" do
  include_context :time_is_frozen
  include_context :user_signed_in

  get "/api/v1/schoolmates" do
    it_behaves_like "v1 user auth token requiring endpoint"

    context "when current user is pre graduated" do
      let(:current_time) { Time.zone.local(current_user.grade, 5) }

      before do
        current_user.auth_token.update(expires_at: 180.days.since)
      end

      context "when user is pre graduated" do
        let!(:schoolmate) { create :user, grade: current_user.grade, school: current_user.school }

        example_request "List of schoolmates" do
          expect(response_status).to eq(200)
          expect(json_response_body.size).to eq(1)
          expect(json_response_body.first).to be_a_schoolmate_representation(schoolmate, current_user)
        end
      end

      context "when user is post graduated" do
        let!(:schoolmate) { create :user, grade: current_user.grade - 1, school: current_user.school }

        example_request "List of schoolmates" do
          expect(response_status).to eq(200)
          expect(json_response_body.size).to eq(0)
        end
      end
    end

    context "when current user is post graduated" do
      let(:current_time) { Time.zone.local(current_user.grade, 7) }

      before do
        current_user.auth_token.update(expires_at: 180.days.since)
      end

      context "when user is pre graduated" do
        let!(:schoolmate) { create :user, grade: current_user.grade + 1, school: current_user.school }

        example_request "List of schoolmates" do
          expect(response_status).to eq(200)
          expect(json_response_body.size).to eq(0)
        end
      end

      context "when user is post graduated" do
        let!(:schoolmate) { create :user, grade: current_user.grade, school: current_user.school }

        example_request "List of schoolmates" do
          expect(response_status).to eq(200)
          expect(json_response_body.size).to eq(1)
          expect(json_response_body.first).to be_a_schoolmate_representation(schoolmate, current_user)
        end
      end
    end
  end
end
