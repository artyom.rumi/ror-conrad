require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Pictures" do
  include_context :user_signed_in

  header "Content-type", "multipart/form-data"

  parameter :image, "Image that user sent to chat", required: true, "Type" => "Multipart/Form-data"
  let!(:image) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }

  post "/api/v1/pictures" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:raw_post) do
      params
    end

    example_request "Upload an image" do
      expect(response_status).to eq(201)

      picture = current_user.pictures.find(json_response_body["id"])
      expect(json_response_body).to be_a_picture_representation(picture)
    end
  end
end
