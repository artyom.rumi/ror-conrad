require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Profiles" do
  include_context :user_signed_in

  get "/api/v1/profile" do
    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Show current user" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_user_representation(current_user)
    end
  end
end
