require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Users/Blocking" do
  include_context :user_signed_in

  let(:another_user) { create :user }
  let(:user_id) { another_user.id }

  post "/api/v1/users/:user_id/blocking" do
    before { stub_pubnub_api }

    parameter :user_id, "ID of the user to block", required: true

    it_behaves_like "v1 user auth token requiring endpoint"

    before do
      create :chat, users: [current_user, another_user]
    end

    example_request "create new blocking to specific user" do
      expect(response_status).to eq(200)

      blocking = Blocking.find(json_response_body["id"])
      expect(json_response_body).to be_a_blocking_representation(blocking)
    end
  end

  delete "/api/v1/users/:user_id/blocking" do
    before { stub_pubnub_api }

    parameter :user_id, "ID of the user to unblock", required: true

    before do
      create :blocking, :for_user, blockable: another_user, user: current_user
      create :chat, users: [current_user, another_user]
    end

    context "when blocking with the user_id exists" do
      example "destroy blocking" do
        expect { do_request }.to change { current_user.blockings.for_user.count }.by(-1)
        expect(response_status).to eq(204)
      end
    end

    context "when blocking with the user_id does not exist" do
      let(:user_id) { -1 }

      example "not destroy blocking that not exists", document: false do
        do_request

        expect(response_status).to eq(404)
      end
    end
  end
end
