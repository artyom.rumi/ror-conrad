require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Users/Timelines" do
  include_context :user_signed_in

  let(:user) { create :user }
  let(:user_id) { user.id }
  let!(:first_feed) { create :feed, user: user }
  let!(:second_feed) { create :feed, user: user, created_at: 7.minutes.ago }

  get "api/v1/users/:user_id/timeline" do
    parameter :user_id, "Id of the user", required: true

    example_request "user's timeline" do
      expect(response_status).to eq(200)

      expect(json_response_body.first).to be_a_feed_representation(first_feed, current_user)
      expect(json_response_body.second).to be_a_feed_representation(second_feed, current_user)
    end
  end
end
