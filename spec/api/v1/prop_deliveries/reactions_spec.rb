require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/PropDelivery/Reaction" do
  include_context :user_signed_in

  post "/api/v1/prop_deliveries/:prop_delivery_id/reaction" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :prop_delivery_id, "ID of prop delivery for replying", required: true
    parameter :content, "Text content of reaction", required: true

    let!(:prop_delivery) { create :prop_delivery, recipient: current_user }

    let(:prop_delivery_id) { prop_delivery.id }
    let(:content) { "Your boost is so-so." }

    example_request "Create reaction on received prop" do
      expect(response_status).to eq(201)

      reaction = Reaction.find(json_response_body["id"])
      expect(json_response_body).to be_a_reaction_representation(reaction)
    end
  end
end
