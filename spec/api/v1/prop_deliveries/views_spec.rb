require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/PropDelivery/Views" do
  include_context :user_signed_in

  post "/api/v1/prop_deliveries/:id/views" do
    parameter :id, "Prop delivery ID", required: true

    let!(:my_prop_delivery) { create :prop_delivery, recipient: current_user }
    let!(:another_prop_delivery) { create :prop_delivery }

    context "with id of incoming delivery specified" do
      include_context :time_is_frozen

      let(:id) { my_prop_delivery.id }
      let(:current_time) { Time.new(2001, 1, 1).in_time_zone }

      example "mark prop delivery as viewed" do
        do_request

        expect(response_status).to eq(204)
        expect(my_prop_delivery.reload.viewed_at).to eq(current_time)
      end
    end

    context "with id of someone else incoming delivery specified" do
      let(:id) { another_prop_delivery.id }

      example "mark prop delivery as viewed", document: false do
        do_request

        expect(response_status).to eq(404)
        expect(another_prop_delivery.reload.viewed_at).to eq(nil)
      end
    end
  end
end
