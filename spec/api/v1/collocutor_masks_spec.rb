require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/CollocutorMasks" do
  include_context :user_signed_in

  get "/api/v1/collocutor_masks" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:another_user) { create :user }
    let(:user_collocutor_masks) { current_user.collocutor_masks }

    before do
      create :collocutor_mask, user: current_user
      create :collocutor_mask, user: current_user
      create :collocutor_mask, user: another_user
    end

    example_request "List of collocutor masks" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_collocutor_mask_list_representation(user_collocutor_masks, current_user)
    end
  end

  get "/api/v1/collocutor_masks/:id" do
    it_behaves_like "v1 user auth token requiring endpoint"

    let(:collocutor_mask) { create :collocutor_mask }
    let(:id) { collocutor_mask.id }

    parameter :id, "Id of the collocutor mask", required: true

    example_request "Info about collocutor mask" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_collocutor_mask_representation(collocutor_mask, current_user)
    end
  end

  patch "/api/v1/collocutor_masks/:id" do
    it_behaves_like "v1 user auth token requiring endpoint"

    header "Content-type", "multipart/form-data"

    parameter :id, "Id of the collocutor mask", required: true
    parameter :name, "New name for collocutor mask"
    parameter :avatar, "New avatar for collocutor mask", "Type" => "Multipart/Form-data"

    let(:raw_post) do
      params
    end

    let(:collocutor_mask) { create :collocutor_mask }
    let(:id) { collocutor_mask.id }
    let(:name) { "John Doe" }
    let(:avatar) { fixture_file_upload(Rails.root.join("spec", "support", "fixtures", "avatar.png"), "image/png") }

    example_request "Update a collocutor mask" do
      expect(response_status).to eq(200)

      expect(json_response_body).to be_a_collocutor_mask_representation(CollocutorMask.first, current_user)
    end
  end
end
