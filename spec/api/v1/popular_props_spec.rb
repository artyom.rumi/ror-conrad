require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Popular Props" do
  include_context :user_signed_in

  def response_ids
    json_response_body.map { |prop| prop["id"] }
  end

  get "/api/v1/popular_props" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :user_id, "User to filter props by"
    parameter :limit, "Amount of props to be fetched"

    let!(:male_prop) { create :prop, :for_male, popularity_index: 3 }
    let!(:popular_male_prop) { create :prop, :for_male, popularity_index: 4 }
    let!(:neutral_prop) { create :prop, :special, gender: nil, popularity_index: 2 }
    let!(:unpopular_female_prop) { create :prop, :for_female, popularity_index: 1 }

    example_request "get full list of props" do
      expect(response_status).to eq(200)
      expect(json_response_body).to be_a_prop_list_representation(Prop.all)
      expect(response_ids).to eq [popular_male_prop.id, male_prop.id, neutral_prop.id, unpopular_female_prop.id]
    end

    context "with male user specified" do
      let(:user) { create :user, :male }
      let(:user_id) { user.id }

      example_request "get list of props for male" do
        expect(response_status).to eq(200)
        male_props = Prop.where(gender: ["male", nil])
        expect(json_response_body).to be_a_prop_list_representation(male_props)
        expect(response_ids).to eq [popular_male_prop.id, male_prop.id, neutral_prop.id]
      end
    end

    context "with female user specified" do
      let(:user) { create :user, :female }
      let(:user_id) { user.id }

      example_request "get female list of props" do
        expect(response_status).to eq(200)
        female_props = Prop.where(gender: ["female", nil])
        expect(json_response_body).to be_a_prop_list_representation(female_props)
        expect(response_ids).to eq [neutral_prop.id, unpopular_female_prop.id]
      end
    end
  end
end
