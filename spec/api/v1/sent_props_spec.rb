require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Sent props" do
  include_context :user_signed_in

  get "/api/v1/sent_props" do
    let!(:prop_delivery) { create :prop_delivery, sender: current_user }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "list of sent props" do
      expect(response_status).to eq(200)
      expect(json_response_body.size).to eq(1)
      expect(json_response_body.first).to be_a_sent_prop_representation(prop_delivery, current_user)
    end
  end
end
