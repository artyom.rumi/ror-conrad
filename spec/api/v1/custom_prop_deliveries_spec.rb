require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Custom prop deliveries" do
  include_context :user_signed_in

  post "/api/v1/custom_prop_deliveries" do
    it_behaves_like "v1 user auth token requiring endpoint"

    parameter :recipient_id, "User to recieve the prop", required: true
    parameter :content, "Content of custom prop", required: true

    let(:recipient) { create :user }

    let(:recipient_id) { recipient.id }
    let(:content) { "You are the best person I know!" }

    let(:emojis) { ["😍", "😇"] }
    let(:props_seeds_hash) { { "emojis" => emojis } }

    before do
      allow(YAML).to receive(:load_file).and_return(props_seeds_hash)
      allow(emojis).to receive(:sample).and_return("😍")
    end

    example_request "Send custom prop to the user" do
      expect(response_status).to eq(201)

      prop_delivery = PropDelivery.find(json_response_body["id"])
      expect(json_response_body).to be_a_prop_delivery_representation(prop_delivery)
      expect(prop_delivery.sender).to eq(current_user)
    end
  end
end
