require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Search props" do
  let!(:prop) { create :prop, content: "You warm my heart" }
  let!(:second_prop) { create :prop, content: "You have a lovely voice" }

  get "/api/v1/search_props" do
    include_context :user_signed_in

    parameter :content, "Content of the searched prop", required: true
    let(:content) { "my heart" }

    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "List of props" do
      expect(response_status).to eq(200)
      expect(json_response_body.size).to eq(1)
      expect(json_response_body.first).to be_a_prop_representation(prop)
    end
  end
end
