require "rails_helper"
require "rspec_api_documentation/dsl"

resource "v1/Request props" do
  include_context :user_signed_in

  let!(:subscriber) { create :user }
  let!(:subscription) { create :subscription, target: current_user, subscriber: subscriber }

  post "/api/v1/requests" do
    it_behaves_like "v1 user auth token requiring endpoint"

    example_request "Request props" do
      expect(response_status).to eq(204)
    end
  end
end
