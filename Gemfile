source "https://rubygems.org"

ruby "2.5.1"

gem "pg"
gem "rails", "5.2.1"

# assets
gem "activerecord-import", require: false
gem "autoprefixer-rails"
gem "coffee-rails"
gem "foundation-icons-sass-rails"
gem "foundation-rails"
gem "mini_racer", "~> 0.2.6"
gem "premailer-rails"
gem "sassc-rails"
gem "uglifier", ">= 2.7.2"

source "https://rails-assets.org" do
  gem "rails-assets-jquery-ujs"
  gem "rails-assets-jquery2"
end

# views
gem "active_link_to"
gem "inky-rb", require: "inky"
gem "meta-tags"
gem "simple_form"
gem "slim"

# all other gems
gem "active_model_serializers"
gem "apitome"
gem "apnotic"
gem "aws-sdk-s3", require: false
gem "bootsnap", require: false
gem "decent_decoration"
gem "decent_exposure"
gem "devise"
gem "discard", "~> 1.0"
gem "draper"
gem "erubis"
gem "fcm"
gem "flamegraph"
gem "geocoder"
gem "google-analytics-rails"
gem "health_check"
gem "image_processing"
gem "interactor"
gem "kaminari"
gem "memory_profiler"
gem "pg_search"
gem "pubnub", "~> 4.0.27"
gem "puma"
gem "pundit"
gem "rack-canonical-host"
gem "rack-mini-profiler", require: false, git: "https://github.com/MiniProfiler/rack-mini-profiler.git"
gem "rails_admin"
gem "responders"
gem "rest-client"
gem "rollbar"
gem "rubystats", "~> 0.2.5"
gem "seedbank"
gem "sidekiq"
gem "sidekiq-scheduler"
gem "stackprof"
gem "twilio-ruby"

# Environment variables management
gem "dotenv-rails"

group :staging, :production do
  gem "newrelic_rpm"
end

group :development, :test, :staging do
  gem "factory_bot_rails"
  gem "faker"
end

group :test do
  gem "capybara"
  gem "email_spec"
  gem "formulaic"
  gem "guard-rspec"
  gem "launchy"
  gem "poltergeist"
  gem "rspec-its"
  gem "rspec_api_documentation"
  gem "shoulda-matchers"
  gem "simplecov", require: false
  gem "terminal-notifier-guard"
  gem "timecop"
  gem "webmock", require: false
end

group :development, :test do
  gem "awesome_print"
  gem "brakeman", require: false
  gem "bullet"
  gem "bundler-audit", require: false
  gem "byebug"
  gem "coffeelint"
  gem "jasmine", "> 2.0"
  gem "jasmine-jquery-rails"
  gem "pry-rails"
  gem "rspec-rails", "~> 3.5"
  gem "rubocop", require: false
  gem "rubocop-rspec", require: false
  gem "scss_lint", require: false
  gem "slim_lint", require: false
end

group :development do
  gem "letter_opener"
  gem "rails-erd"
  gem "slim-rails"
  gem "spring"
  gem "spring-commands-rspec"
  gem "spring-watcher-listen"
  gem "web-console"
end
